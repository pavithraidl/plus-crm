-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2017 at 11:15 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pavithra_isuru`
--

-- --------------------------------------------------------

--
-- Table structure for table `systems`
--

DROP TABLE IF EXISTS `systems`;
CREATE TABLE IF NOT EXISTS `systems` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `route` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `allow_default` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `systems`
--

INSERT INTO `systems` (`id`, `name`, `route`, `icon`, `listorder`, `visibility`, `allow_default`, `status`, `created_by`, `created_at`) VALUES
(1, 'Dashboard', 'admin:dashboard', 'fa-dashboard', 10, 1, 1, 1, 10, '2017-05-29 22:02:25'),
(2, 'Users', 'users', 'fa-user', 103, 1, 0, 0, 10, '2017-05-28 23:00:38'),
(3, 'Organizations', 'organizations', ' fa-building-o', 99, 1, 0, 0, 10, '2017-05-28 22:32:46'),
(4, 'Settings', 'settings', 'fa-cog', 200, 1, 0, 0, 10, '2017-05-28 22:32:46'),
(5, 'System', 'system', 'fa-laptop', 201, 1, 0, 1, 10, '2017-05-28 22:45:05'),
(6, 'Users', '#', 'fa-user', 10, 1, 0, 2, 10, '2017-06-01 08:31:27'),
(8, 'Organizations', '#', 'fa-building', 100, 1, 0, 2, 10, '2017-06-06 04:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `system_controllers`
--

DROP TABLE IF EXISTS `system_controllers`;
CREATE TABLE IF NOT EXISTS `system_controllers` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller_url` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_controllers`
--

INSERT INTO `system_controllers` (`id`, `systemid`, `lang`, `controller_url`, `controller_name`, `description`, `status`, `created_by`, `created_at`) VALUES
(1, 5, 'php', 'AccessController.php', 'AccessController', 'Controls and monitor the user access', 1, 10, '2017-05-28 23:14:42'),
(2, 5, 'php', 'SystemController.php', 'SystemController', 'Controls the system administrators functionalities', 1, 10, '2017-05-28 23:25:09'),
(3, 1, 'php', 'DashboardController.php', 'DashboardController', 'Controls all the dashboard functionalities', 1, 10, '2017-05-28 23:34:33'),
(4, 2, 'php', 'UserController.php', 'UserController', 'Controls all the user functionalitis', 1, 10, '2017-05-28 23:35:39'),
(5, 3, 'php', 'OrganizationController.php', 'OrganizationController', 'Controls all the organization functionalities', 1, 10, '2017-05-28 23:36:25'),
(7, 5, 'PHP', 'TestController', 'TestController', '', 1, 10, '2017-06-01 10:38:20'),
(8, 8, 'PHP', 'OrganizationController', 'OrganizationController', 'Controls the each individual org', 1, 10, '2017-06-06 04:37:27'),
(9, 8, 'PHP', 'Test1Controller', 'Test1Controller', 'Testing Controller', 1, 10, '2017-06-06 04:38:44'),
(10, 8, 'PHP', 'Test2Controller', 'Test2Controller', 'Testing Controller', 1, 10, '2017-06-06 04:41:58'),
(11, 1, 'PHP', 'TestingController', 'TestingController', 'To test the functionalities', 1, 10, '2017-06-06 08:49:36');

-- --------------------------------------------------------

--
-- Table structure for table `system_exceptions`
--

DROP TABLE IF EXISTS `system_exceptions`;
CREATE TABLE IF NOT EXISTS `system_exceptions` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `controllerid` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functionid` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exception` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_exceptions`
--

INSERT INTO `system_exceptions` (`id`, `systemid`, `controllerid`, `functionid`, `exception`, `userid`, `ip`, `status`, `created_at`) VALUES
(1, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:02:41'),
(2, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:07:06'),
(3, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''5'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:07:46'),
(4, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''5'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:08:11'),
(5, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:08:35'),
(6, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:09:10'),
(7, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:09:47'),
(8, 5, '2', '5', 'exception ''BadMethodCallException'' with message ''Method [where] does not exist.'' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(''where'', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(''systemid'', ''1'')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getSystemData'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:10:04'),
(9, 5, '2', '12', 'exception ''ErrorException'' with message ''fopen(/home1/pavithraidl/public_html/idloms/app/controllers/TestController.php): failed to open stream: No such file or directory'' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:497\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''fopen(/home1/pa...'', ''/home1/pavithra...'', 497, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(497): fopen(''/home1/pavithra...'', ''wb'')\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''newController'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->c', 10, '101.98.86.113', 1, '2017-06-01 10:20:30'),
(10, 5, '2', '14', 'exception ''ErrorException'' with message ''fopen(/home1/pavithraidl/public_html/assets/js/custom//test.js): failed to open stream: No such file or directory'' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:601\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''fopen(/home1/pa...'', ''/home1/pavithra...'', 601, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(601): fopen(''/home1/pavithra...'', ''w'')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''newFunction'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemCont', 10, '101.98.86.113', 1, '2017-06-06 08:53:09'),
(11, 5, '2', '14', 'exception ''ErrorException'' with message ''fopen(/home1/pavithraidl/public_html/assets/js/custom//): failed to open stream: Is a directory'' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:565\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''fopen(/home1/pa...'', ''/home1/pavithra...'', 565, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(565): fopen(''/home1/pavithra...'', ''wb'')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''newFunction'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(I', 10, '101.98.86.113', 1, '2017-06-06 09:04:57'),
(12, 5, '2', '14', 'exception ''ErrorException'' with message ''fopen(/home1/pavithraidl/public_html/assets/js/custom//): failed to open stream: Is a directory'' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:565\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''fopen(/home1/pa...'', ''/home1/pavithra...'', 565, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(565): fopen(''/home1/pavithra...'', ''wb'')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''newFunction'', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(I', 10, '101.98.86.113', 1, '2017-06-06 09:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `system_functions`
--

DROP TABLE IF EXISTS `system_functions`;
CREATE TABLE IF NOT EXISTS `system_functions` (
  `id` int(11) NOT NULL,
  `controllerid` int(11) DEFAULT NULL,
  `ajax` int(1) DEFAULT NULL,
  `method` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'get',
  `function_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_functions`
--

INSERT INTO `system_functions` (`id`, `controllerid`, `ajax`, `method`, `function_name`, `description`, `status`, `created_by`, `created_at`) VALUES
(1, 1, 0, 'int', 'visitMonitor', 'Monitor the visit for views', 1, 10, '2017-05-28 23:56:49'),
(2, 1, 0, 'get', 'getManage', 'Get the system manage page', 1, 10, '2017-05-28 23:57:21'),
(3, 3, 0, 'get', 'getView', 'Get the Dashboard', 1, 10, '2017-05-29 00:01:29'),
(4, 2, 1, 'post', 'changeStatus', 'Change the status of the systems', 1, 10, '2017-05-29 21:57:50'),
(5, 2, 1, 'get', 'getSystemData', 'Get the basic data of each individual system', 1, 10, '2017-05-29 22:11:11'),
(6, 2, 1, 'get', 'getSystemExceptions', 'Get the exceptions of specific systems', 1, 10, '2017-05-30 05:20:18'),
(7, 2, 1, 'get', 'getExceptionData', 'Get the data of single exception', 1, 10, '2017-05-31 22:25:14'),
(8, 2, 1, 'post', 'changeExceptionStatus', 'Change the status of each exception', 1, 10, '2017-05-31 23:30:39'),
(9, 2, 1, 'post', 'changeAllExceptionStatus', 'change the all exceptions to fixed', 1, 10, '2017-05-31 23:44:11'),
(10, 2, 1, 'post', 'newSystem', 'Add a new system', 1, 10, '2017-06-01 03:11:14'),
(11, 2, 1, 'get', 'GetControllers', 'Get list of controllers of a specific system', 1, 10, '2017-06-01 04:20:39'),
(12, 2, 1, 'post', 'newController', 'Add a new controller to the system', 1, 10, '2017-06-01 04:53:05'),
(13, 2, 1, 'get', 'getControllerData', 'Get the data of a single controller', 1, 10, '2017-06-05 23:53:38'),
(14, 2, 1, 'post', 'newFunction', 'Add a new function to the system', 1, 10, '2017-06-06 02:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `system_intrusions`
--

DROP TABLE IF EXISTS `system_intrusions`;
CREATE TABLE IF NOT EXISTS `system_intrusions` (
  `id` int(11) NOT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functionid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_operations`
--

DROP TABLE IF EXISTS `system_operations`;
CREATE TABLE IF NOT EXISTS `system_operations` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `functionid` int(1) DEFAULT NULL,
  `operation` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_operation_access`
--

DROP TABLE IF EXISTS `system_operation_access`;
CREATE TABLE IF NOT EXISTS `system_operation_access` (
  `id` int(11) NOT NULL,
  `operationid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_tables`
--

DROP TABLE IF EXISTS `system_tables`;
CREATE TABLE IF NOT EXISTS `system_tables` (
  `id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemid` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_table_columns`
--

DROP TABLE IF EXISTS `system_table_columns`;
CREATE TABLE IF NOT EXISTS `system_table_columns` (
  `id` int(11) NOT NULL,
  `tableid` int(11) DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_length` int(11) DEFAULT NULL,
  `default_data_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_keys` int(11) DEFAULT NULL,
  `column_key_type` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_url_routes`
--

DROP TABLE IF EXISTS `system_url_routes`;
CREATE TABLE IF NOT EXISTS `system_url_routes` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeas` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uses` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `controllerid` int(11) DEFAULT NULL,
  `functionid` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_url_routes`
--

INSERT INTO `system_url_routes` (`id`, `systemid`, `type`, `url`, `routeas`, `uses`, `controllerid`, `functionid`, `status`, `created_by`, `created_at`) VALUES
(1, 5, 'get', 'system', 'system', 'SystemController@getManage', 1, 2, 1, 10, '2017-05-28 23:32:23'),
(2, 1, 'get', '/admin', 'admin:dashboard', 'DashboardController@getView', 1, 3, 1, 10, '2017-05-29 00:01:57'),
(4, 5, 'get', '/system/load-system-data', 'system:load-system-data', 'SystemController@getSystemData', 1, 5, 1, 10, '2017-05-29 22:11:25'),
(5, 5, 'get', '/system/get-system-exceptions', 'system:get-system-exception', 'SystemController@getSystemExceptions', 1, 6, 1, 10, '2017-05-30 05:20:29'),
(6, 5, 'get', '/system/get-exception-data', 'system:get-exception-data', 'SystemController@getExceptionData', 1, 7, 1, 10, '2017-05-31 22:25:59'),
(7, 5, 'post', '/system/change-exception-status', 'system:change-exception-status', 'SystemController@changeExceptionStatus', 1, 8, 1, 10, '2017-05-31 23:31:26'),
(8, 5, 'post', '/system/change-all-exception-status', 'system:change-all-exception-status', 'SystemController@changeAllExceptionStatus', 1, 9, 1, 10, '2017-05-31 23:44:22'),
(9, 5, 'post', '/system/new-system', 'system:new-system', 'SystemController@newSystem', 1, 10, 1, 10, '2017-06-01 03:12:01'),
(10, 5, 'get', '/system/get-controllers', 'system:get-controllers', 'SystemController@getControllers', 1, 11, 1, 10, '2017-06-01 04:21:49'),
(11, 5, 'post', '/system/new-controller', 'system:new-controller', 'SystemController@newController', 1, 12, 1, 10, '2017-06-01 04:53:50'),
(12, 5, 'get', '/system/get-controller-data', 'system:get-controller-data', 'SystemController@getControllerData', 1, 13, 1, 10, '2017-06-05 23:54:44'),
(13, 5, 'post', '/system/new-function', 'system:new-function', 'SystemController@newFunction', 1, 14, 1, 10, '2017-06-06 02:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `system_views`
--

DROP TABLE IF EXISTS `system_views`;
CREATE TABLE IF NOT EXISTS `system_views` (
  `id` int(11) NOT NULL,
  `view_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_name` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemid` int(11) DEFAULT NULL,
  `url_route` int(11) DEFAULT NULL,
  `functionid` int(1) DEFAULT NULL,
  `main` int(1) NOT NULL DEFAULT '0',
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'icon-stop',
  `allow_default` int(1) NOT NULL DEFAULT '0',
  `list_order` int(11) DEFAULT '50',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_views`
--

INSERT INTO `system_views` (`id`, `view_name`, `menu_name`, `systemid`, `url_route`, `functionid`, `main`, `icon`, `allow_default`, `list_order`, `status`, `created_by`, `created_at`) VALUES
(1, 'System Administrator', 'Manage', 5, 1, 2, 0, 'fa-laptop', 0, 3, 1, 10, '2017-05-28 23:27:55'),
(2, 'Dashboard', 'Dashboard', 1, 2, 3, 0, 'fa-dashboard', 0, 1, 1, 10, '2017-05-29 00:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `system_view_access`
--

DROP TABLE IF EXISTS `system_view_access`;
CREATE TABLE IF NOT EXISTS `system_view_access` (
  `id` int(11) NOT NULL,
  `viewid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_view_visit`
--

DROP TABLE IF EXISTS `system_view_visit`;
CREATE TABLE IF NOT EXISTS `system_view_visit` (
  `id` int(11) NOT NULL,
  `viewid` int(11) DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=291 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_view_visit`
--

INSERT INTO `system_view_visit` (`id`, `viewid`, `ip`, `userid`, `created_at`) VALUES
(1, 1, '101.98.86.113', 10, '2017-05-29 10:40:15'),
(2, 1, '101.98.86.113', 10, '2017-05-29 10:40:59'),
(3, 1, '101.98.86.113', 10, '2017-05-29 10:51:21'),
(4, 1, '101.98.86.113', 10, '2017-05-29 11:05:20'),
(5, 1, '101.98.86.113', 10, '2017-05-29 11:05:37'),
(6, 1, '101.98.86.113', 10, '2017-05-29 11:06:59'),
(7, 1, '101.98.86.113', 10, '2017-05-29 11:13:26'),
(8, 1, '101.98.86.113', 10, '2017-05-29 11:13:46'),
(9, 1, '101.98.86.113', 10, '2017-05-29 11:14:09'),
(10, 1, '101.98.86.113', 10, '2017-05-29 11:14:21'),
(11, 1, '101.98.86.113', 10, '2017-05-29 11:15:14'),
(12, 1, '101.98.86.113', 10, '2017-05-29 11:17:20'),
(13, 1, '101.98.86.113', 10, '2017-05-29 11:18:02'),
(14, 1, '101.98.86.113', 10, '2017-05-29 11:18:33'),
(15, 1, '101.98.86.113', 10, '2017-05-29 11:23:32'),
(16, 1, '101.98.86.113', 10, '2017-05-29 11:24:40'),
(17, 1, '101.98.86.113', 10, '2017-05-29 11:26:09'),
(18, 1, '101.98.86.113', 10, '2017-05-29 11:26:13'),
(19, 1, '101.98.86.113', 10, '2017-05-29 11:26:14'),
(20, 1, '101.98.86.113', 10, '2017-05-29 11:46:24'),
(21, 1, '101.98.86.113', 10, '2017-05-29 11:48:43'),
(22, 1, '101.98.86.113', 10, '2017-05-29 11:49:14'),
(23, 1, '101.98.86.113', 10, '2017-05-29 11:49:52'),
(24, 1, '101.98.86.113', 10, '2017-05-29 11:51:48'),
(25, 1, '101.98.86.113', 10, '2017-05-29 11:52:18'),
(26, 1, '101.98.86.113', 10, '2017-05-29 11:54:11'),
(27, 1, '101.98.86.113', 10, '2017-05-29 11:54:33'),
(28, 1, '101.98.86.113', 10, '2017-05-29 11:54:58'),
(29, 1, '101.98.86.113', 10, '2017-05-29 11:55:41'),
(30, 1, '101.98.86.113', 10, '2017-05-29 11:55:53'),
(31, 1, '101.98.86.113', 10, '2017-05-29 12:04:54'),
(32, 1, '101.98.86.113', 10, '2017-05-29 12:07:33'),
(33, 1, '101.98.86.113', 10, '2017-05-29 12:07:51'),
(34, 1, '101.98.86.113', 10, '2017-05-29 12:09:16'),
(35, 1, '101.98.86.113', 10, '2017-05-29 12:09:33'),
(36, 1, '101.98.86.113', 10, '2017-05-29 12:09:44'),
(37, 1, '101.98.86.113', 10, '2017-05-29 12:12:58'),
(38, 1, '101.98.86.113', 10, '2017-05-29 12:13:26'),
(39, 1, '101.98.86.113', 10, '2017-05-29 12:14:32'),
(40, 1, '101.98.86.113', 10, '2017-05-29 12:14:52'),
(41, 1, '101.98.86.113', 10, '2017-05-29 12:15:33'),
(42, 1, '101.98.86.113', 10, '2017-05-30 08:08:55'),
(43, 1, '101.98.86.113', 10, '2017-05-30 08:12:18'),
(44, 1, '101.98.86.113', 10, '2017-05-30 08:12:45'),
(45, 1, '101.98.86.113', 10, '2017-05-30 08:13:14'),
(46, 1, '101.98.86.113', 10, '2017-05-30 08:13:56'),
(47, 1, '101.98.86.113', 10, '2017-05-30 08:14:35'),
(48, 1, '101.98.86.113', 10, '2017-05-30 08:14:41'),
(49, 1, '101.98.86.113', 10, '2017-05-30 08:16:38'),
(50, 1, '101.98.86.113', 10, '2017-05-30 08:17:03'),
(51, 1, '101.98.86.113', 10, '2017-05-30 08:19:52'),
(52, 1, '101.98.86.113', 10, '2017-05-30 08:21:36'),
(53, 1, '101.98.86.113', 10, '2017-05-30 08:22:37'),
(54, 1, '101.98.86.113', 10, '2017-05-30 08:22:43'),
(55, 1, '101.98.86.113', 10, '2017-05-30 08:23:11'),
(56, 1, '101.98.86.113', 10, '2017-05-30 08:24:19'),
(57, 1, '101.98.86.113', 10, '2017-05-30 08:24:31'),
(58, 1, '101.98.86.113', 10, '2017-05-30 08:25:03'),
(59, 1, '101.98.86.113', 10, '2017-05-30 08:28:05'),
(60, 1, '101.98.86.113', 10, '2017-05-30 08:29:16'),
(61, 1, '101.98.86.113', 10, '2017-05-30 08:32:06'),
(62, 1, '101.98.86.113', 10, '2017-05-30 08:32:19'),
(63, 1, '101.98.86.113', 10, '2017-05-30 08:32:30'),
(64, 1, '101.98.86.113', 10, '2017-05-30 08:34:39'),
(65, 1, '101.98.86.113', 10, '2017-05-30 08:35:50'),
(66, 1, '101.98.86.113', 10, '2017-05-30 08:42:32'),
(67, 1, '101.98.86.113', 10, '2017-05-30 09:10:45'),
(68, 1, '101.98.86.113', 10, '2017-05-30 14:19:21'),
(69, 1, '101.98.86.113', 10, '2017-05-30 14:23:07'),
(70, 1, '101.98.86.113', 10, '2017-05-30 14:25:56'),
(71, 1, '101.98.86.113', 10, '2017-05-30 14:32:36'),
(72, 1, '101.98.86.113', 10, '2017-05-30 14:36:22'),
(73, 1, '101.98.86.113', 10, '2017-05-30 14:36:42'),
(74, 1, '101.98.86.113', 10, '2017-05-30 14:37:04'),
(75, 1, '101.98.86.113', 10, '2017-05-30 14:37:31'),
(76, 1, '101.98.86.113', 10, '2017-05-30 14:38:33'),
(77, 1, '101.98.86.113', 10, '2017-05-30 14:39:08'),
(78, 1, '101.98.86.113', 10, '2017-05-30 14:39:45'),
(79, 1, '101.98.86.113', 10, '2017-05-30 14:40:44'),
(80, 1, '101.98.86.113', 10, '2017-05-30 14:41:08'),
(81, 1, '101.98.86.113', 10, '2017-05-30 14:41:35'),
(82, 1, '101.98.86.113', 10, '2017-05-30 14:42:23'),
(83, 1, '101.98.86.113', 10, '2017-05-30 14:42:55'),
(84, 1, '101.98.86.113', 10, '2017-05-30 14:43:31'),
(85, 1, '101.98.86.113', 10, '2017-05-30 14:43:47'),
(86, 1, '101.98.86.113', 10, '2017-05-30 14:44:10'),
(87, 1, '101.98.86.113', 10, '2017-05-30 14:44:34'),
(88, 1, '101.98.86.113', 10, '2017-05-30 14:44:55'),
(89, 1, '101.98.86.113', 10, '2017-05-30 14:45:30'),
(90, 1, '101.98.86.113', 10, '2017-05-30 14:45:46'),
(91, 1, '101.98.86.113', 10, '2017-05-30 14:48:53'),
(92, 1, '101.98.86.113', 10, '2017-05-30 14:49:48'),
(93, 1, '101.98.86.113', 10, '2017-05-30 14:50:42'),
(94, 1, '101.98.86.113', 10, '2017-05-30 14:54:26'),
(95, 1, '101.98.86.113', 10, '2017-05-30 15:01:20'),
(96, 1, '101.98.86.113', 10, '2017-05-30 15:03:07'),
(97, 1, '101.98.86.113', 10, '2017-05-30 15:03:49'),
(98, 1, '101.98.86.113', 10, '2017-05-30 15:05:20'),
(99, 1, '101.98.86.113', 10, '2017-05-30 15:06:37'),
(100, 1, '101.98.86.113', 10, '2017-05-30 15:08:30'),
(101, 1, '101.98.86.113', 10, '2017-05-30 15:09:07'),
(102, 1, '101.98.86.113', 10, '2017-05-30 15:11:09'),
(103, 1, '101.98.86.113', 10, '2017-05-30 15:11:52'),
(104, 1, '101.98.86.113', 10, '2017-05-30 15:12:24'),
(105, 1, '101.98.86.113', 10, '2017-05-30 15:13:50'),
(106, 1, '101.98.86.113', 10, '2017-05-30 15:21:42'),
(107, 1, '101.98.86.113', 10, '2017-05-30 15:32:47'),
(108, 1, '101.98.86.113', 10, '2017-05-30 15:33:02'),
(109, 1, '101.98.86.113', 10, '2017-05-30 15:33:38'),
(110, 1, '101.98.86.113', 10, '2017-05-30 15:34:09'),
(111, 1, '101.98.86.113', 10, '2017-05-30 15:37:07'),
(112, 1, '101.98.86.113', 10, '2017-05-30 15:37:31'),
(113, 1, '101.98.86.113', 10, '2017-05-30 15:41:45'),
(114, 1, '101.98.86.113', 10, '2017-05-30 15:54:53'),
(115, 1, '101.98.86.113', 10, '2017-05-30 16:02:15'),
(116, 1, '101.98.86.113', 10, '2017-05-30 16:08:38'),
(117, 1, '101.98.86.113', 10, '2017-05-30 16:12:13'),
(118, 1, '101.98.86.113', 10, '2017-05-30 16:13:00'),
(119, 1, '101.98.86.113', 10, '2017-05-30 16:15:00'),
(120, 1, '101.98.86.113', 10, '2017-05-30 16:17:41'),
(121, 1, '101.98.86.113', 10, '2017-05-30 16:18:23'),
(122, 1, '101.98.86.113', 10, '2017-05-30 16:18:42'),
(123, 1, '101.98.86.113', 10, '2017-05-30 16:21:43'),
(124, 1, '101.98.86.113', 10, '2017-06-01 08:12:33'),
(125, 1, '101.98.86.113', 10, '2017-06-01 08:39:15'),
(126, 1, '101.98.86.113', 10, '2017-06-01 08:42:09'),
(127, 1, '101.98.86.113', 10, '2017-06-01 08:42:41'),
(128, 1, '101.98.86.113', 10, '2017-06-01 08:43:46'),
(129, 1, '101.98.86.113', 10, '2017-06-01 08:44:50'),
(130, 1, '101.98.86.113', 10, '2017-06-01 08:47:46'),
(131, 1, '101.98.86.113', 10, '2017-06-01 08:50:35'),
(132, 1, '101.98.86.113', 10, '2017-06-01 08:52:26'),
(133, 1, '101.98.86.113', 10, '2017-06-01 08:52:32'),
(134, 1, '101.98.86.113', 10, '2017-06-01 09:02:12'),
(135, 1, '101.98.86.113', 10, '2017-06-01 09:02:47'),
(136, 1, '101.98.86.113', 10, '2017-06-01 09:07:32'),
(137, 1, '101.98.86.113', 10, '2017-06-01 09:09:00'),
(138, 1, '101.98.86.113', 10, '2017-06-01 09:10:22'),
(139, 1, '101.98.86.113', 10, '2017-06-01 09:12:01'),
(140, 1, '101.98.86.113', 10, '2017-06-01 09:24:26'),
(141, 1, '101.98.86.113', 10, '2017-06-01 09:32:15'),
(142, 1, '101.98.86.113', 10, '2017-06-01 09:33:04'),
(143, 1, '101.98.86.113', 10, '2017-06-01 09:34:25'),
(144, 1, '101.98.86.113', 10, '2017-06-01 09:35:59'),
(145, 1, '101.98.86.113', 10, '2017-06-01 09:41:20'),
(146, 1, '101.98.86.113', 10, '2017-06-01 09:45:47'),
(147, 1, '101.98.86.113', 10, '2017-06-01 09:52:52'),
(148, 1, '101.98.86.113', 10, '2017-06-01 09:56:25'),
(149, 1, '101.98.86.113', 10, '2017-06-01 09:57:10'),
(150, 1, '101.98.86.113', 10, '2017-06-01 10:03:24'),
(151, 1, '101.98.86.113', 10, '2017-06-01 10:04:26'),
(152, 1, '101.98.86.113', 10, '2017-06-01 10:07:15'),
(153, 1, '101.98.86.113', 10, '2017-06-01 10:08:43'),
(154, 1, '101.98.86.113', 10, '2017-06-01 10:10:31'),
(155, 1, '101.98.86.113', 10, '2017-06-01 10:11:14'),
(156, 1, '101.98.86.113', 10, '2017-06-01 10:18:22'),
(157, 1, '101.98.86.113', 10, '2017-06-01 10:20:04'),
(158, 1, '101.98.86.113', 10, '2017-06-01 10:28:25'),
(159, 1, '101.98.86.113', 10, '2017-06-01 10:29:08'),
(160, 1, '101.98.86.113', 10, '2017-06-01 10:29:36'),
(161, 1, '101.98.86.113', 10, '2017-06-01 10:30:17'),
(162, 1, '101.98.86.113', 10, '2017-06-01 10:32:57'),
(163, 1, '101.98.86.113', 10, '2017-06-01 10:35:56'),
(164, 1, '101.98.86.113', 10, '2017-06-01 10:50:58'),
(165, 1, '101.98.86.113', 10, '2017-06-01 10:56:32'),
(166, 1, '101.98.86.113', 10, '2017-06-01 11:02:30'),
(167, 1, '101.98.86.113', 10, '2017-06-01 11:02:43'),
(168, 1, '101.98.86.113', 10, '2017-06-01 11:04:18'),
(169, 1, '101.98.86.113', 10, '2017-06-01 11:04:35'),
(170, 1, '101.98.86.113', 10, '2017-06-01 11:05:53'),
(171, 1, '101.98.86.113', 10, '2017-06-01 11:12:25'),
(172, 1, '101.98.86.113', 10, '2017-06-01 11:14:33'),
(173, 2, '101.98.86.113', 10, '2017-06-01 11:14:43'),
(174, 1, '101.98.86.113', 10, '2017-06-01 11:14:45'),
(175, 1, '101.98.86.113', 10, '2017-06-01 11:17:44'),
(176, 1, '101.98.86.113', 10, '2017-06-01 11:18:41'),
(177, 1, '101.98.86.113', 10, '2017-06-01 11:19:28'),
(178, 1, '101.98.86.113', 10, '2017-06-01 11:21:19'),
(179, 1, '101.98.86.113', 10, '2017-06-01 11:21:48'),
(180, 1, '101.98.86.113', 10, '2017-06-01 11:22:06'),
(181, 1, '101.98.86.113', 10, '2017-06-01 11:22:27'),
(182, 1, '101.98.86.113', 10, '2017-06-01 11:22:45'),
(183, 1, '101.98.86.113', 10, '2017-06-01 11:23:30'),
(184, 1, '101.98.86.113', 10, '2017-06-01 11:27:52'),
(185, 1, '101.98.86.113', 10, '2017-06-01 11:34:33'),
(186, 1, '101.98.86.113', 10, '2017-06-01 13:19:31'),
(187, 1, '101.98.86.113', 10, '2017-06-01 13:19:53'),
(188, 1, '101.98.86.113', 10, '2017-06-01 13:20:27'),
(189, 1, '101.98.86.113', 10, '2017-06-01 13:29:03'),
(190, 1, '101.98.86.113', 10, '2017-06-01 13:32:30'),
(191, 1, '101.98.86.113', 10, '2017-06-01 13:33:04'),
(192, 1, '101.98.86.113', 10, '2017-06-01 13:34:47'),
(193, 1, '101.98.86.113', 10, '2017-06-01 13:35:39'),
(194, 1, '101.98.86.113', 10, '2017-06-01 13:40:08'),
(195, 1, '101.98.86.113', 10, '2017-06-01 13:42:10'),
(196, 1, '101.98.86.113', 10, '2017-06-01 13:44:03'),
(197, 1, '101.98.86.113', 10, '2017-06-01 13:44:52'),
(198, 1, '101.98.86.113', 10, '2017-06-01 13:45:40'),
(199, 1, '101.98.86.113', 10, '2017-06-01 13:45:44'),
(200, 1, '101.98.86.113', 10, '2017-06-01 13:46:32'),
(201, 1, '101.98.86.113', 10, '2017-06-01 13:59:30'),
(202, 1, '101.98.86.113', 10, '2017-06-01 14:00:15'),
(203, 1, '101.98.86.113', 10, '2017-06-01 14:01:02'),
(204, 1, '101.98.86.113', 10, '2017-06-01 14:02:20'),
(205, 1, '101.98.86.113', 10, '2017-06-01 14:03:55'),
(206, 1, '101.98.86.113', 10, '2017-06-01 14:05:36'),
(207, 1, '101.98.86.113', 10, '2017-06-01 14:06:54'),
(208, 1, '101.98.86.113', 10, '2017-06-01 14:07:17'),
(209, 1, '101.98.86.113', 10, '2017-06-01 14:31:00'),
(210, 1, '101.98.86.113', 10, '2017-06-01 14:52:08'),
(211, 1, '101.98.86.113', 10, '2017-06-01 14:53:02'),
(212, 1, '101.98.86.113', 10, '2017-06-01 14:53:24'),
(213, 1, '101.98.86.113', 10, '2017-06-01 14:55:34'),
(214, 1, '101.98.86.113', 10, '2017-06-01 14:56:00'),
(215, 1, '101.98.86.113', 10, '2017-06-01 14:58:43'),
(216, 1, '101.98.86.113', 10, '2017-06-01 15:00:20'),
(217, 1, '101.98.86.113', 10, '2017-06-01 15:02:01'),
(218, 1, '101.98.86.113', 10, '2017-06-01 15:03:11'),
(219, 1, '101.98.86.113', 10, '2017-06-01 15:05:52'),
(220, 1, '101.98.86.113', 10, '2017-06-01 15:07:24'),
(221, 1, '101.98.86.113', 10, '2017-06-01 15:10:56'),
(222, 1, '101.98.86.113', 10, '2017-06-01 15:11:26'),
(223, 1, '101.98.86.113', 10, '2017-06-01 15:12:13'),
(224, 1, '101.98.86.113', 10, '2017-06-01 15:14:11'),
(225, 1, '101.98.86.113', 10, '2017-06-01 15:34:11'),
(226, 1, '101.98.86.113', 10, '2017-06-01 15:36:04'),
(227, 1, '101.98.86.113', 10, '2017-06-01 15:37:55'),
(228, 1, '101.98.86.113', 10, '2017-06-01 15:48:44'),
(229, 1, '101.98.86.113', 10, '2017-06-01 15:49:01'),
(230, 1, '101.98.86.113', 10, '2017-06-01 15:50:05'),
(231, 1, '101.98.86.113', 10, '2017-06-01 15:50:35'),
(232, 1, '101.98.86.113', 10, '2017-06-01 15:56:27'),
(233, 1, '101.98.86.113', 10, '2017-06-01 15:57:08'),
(234, 1, '101.98.86.113', 10, '2017-06-01 15:58:00'),
(235, 1, '101.98.86.113', 10, '2017-06-01 15:58:40'),
(236, 1, '101.98.86.113', 10, '2017-06-01 16:08:07'),
(237, 1, '101.98.86.113', 10, '2017-06-06 09:55:45'),
(238, 1, '101.98.86.113', 10, '2017-06-06 10:00:45'),
(239, 1, '101.98.86.113', 10, '2017-06-06 10:02:21'),
(240, 1, '101.98.86.113', 10, '2017-06-06 10:15:08'),
(241, 1, '101.98.86.113', 10, '2017-06-06 10:39:11'),
(242, 1, '101.98.86.113', 10, '2017-06-06 10:43:16'),
(243, 1, '101.98.86.113', 10, '2017-06-06 10:54:43'),
(244, 1, '101.98.86.113', 10, '2017-06-06 10:55:55'),
(245, 1, '101.98.86.113', 10, '2017-06-06 11:10:35'),
(246, 1, '101.98.86.113', 10, '2017-06-06 11:24:09'),
(247, 1, '101.98.86.113', 10, '2017-06-06 11:25:56'),
(248, 1, '101.98.86.113', 10, '2017-06-06 11:27:21'),
(249, 1, '101.98.86.113', 10, '2017-06-06 11:30:36'),
(250, 1, '101.98.86.113', 10, '2017-06-06 11:33:17'),
(251, 1, '101.98.86.113', 10, '2017-06-06 11:35:30'),
(252, 1, '101.98.86.113', 10, '2017-06-06 11:36:06'),
(253, 1, '101.98.86.113', 10, '2017-06-06 11:37:46'),
(254, 1, '101.98.86.113', 10, '2017-06-06 11:54:27'),
(255, 1, '101.98.86.113', 10, '2017-06-06 11:56:39'),
(256, 1, '101.98.86.113', 10, '2017-06-06 11:56:59'),
(257, 1, '101.98.86.113', 10, '2017-06-06 11:59:03'),
(258, 1, '101.98.86.113', 10, '2017-06-06 12:06:32'),
(259, 1, '101.98.86.113', 10, '2017-06-06 12:08:31'),
(260, 1, '101.98.86.113', 10, '2017-06-06 12:09:02'),
(261, 1, '101.98.86.113', 10, '2017-06-06 12:10:40'),
(262, 1, '101.98.86.113', 10, '2017-06-06 12:11:02'),
(263, 1, '101.98.86.113', 10, '2017-06-06 12:13:07'),
(264, 1, '101.98.86.113', 10, '2017-06-06 12:14:00'),
(265, 1, '101.98.86.113', 10, '2017-06-06 12:16:39'),
(266, 1, '101.98.86.113', 10, '2017-06-06 12:21:32'),
(267, 1, '101.98.86.113', 10, '2017-06-06 12:22:28'),
(268, 1, '101.98.86.113', 10, '2017-06-06 12:26:41'),
(269, 1, '101.98.86.113', 10, '2017-06-06 12:27:19'),
(270, 1, '101.98.86.113', 10, '2017-06-06 12:28:35'),
(271, 1, '101.98.86.113', 10, '2017-06-06 12:29:40'),
(272, 1, '101.98.86.113', 10, '2017-06-06 12:31:21'),
(273, 1, '101.98.86.113', 10, '2017-06-06 12:32:27'),
(274, 1, '101.98.86.113', 10, '2017-06-06 12:34:16'),
(275, 1, '101.98.86.113', 10, '2017-06-06 12:38:27'),
(276, 1, '101.98.86.113', 10, '2017-06-06 12:40:28'),
(277, 1, '101.98.86.113', 10, '2017-06-06 12:41:21'),
(278, 1, '101.98.86.113', 10, '2017-06-06 12:46:26'),
(279, 1, '101.98.86.113', 10, '2017-06-06 12:46:59'),
(280, 1, '101.98.86.113', 10, '2017-06-06 14:17:01'),
(281, 1, '101.98.86.113', 10, '2017-06-06 14:19:03'),
(282, 1, '101.98.86.113', 10, '2017-06-06 14:22:37'),
(283, 1, '101.98.86.113', 10, '2017-06-06 14:23:14'),
(284, 1, '101.98.86.113', 10, '2017-06-06 14:31:52'),
(285, 1, '101.98.86.113', 10, '2017-06-06 14:34:33'),
(286, 1, '101.98.86.113', 10, '2017-06-06 14:35:06'),
(287, 1, '101.98.86.113', 10, '2017-06-06 14:38:13'),
(288, 1, '101.98.86.113', 10, '2017-06-06 14:38:41'),
(289, 1, '101.98.86.113', 10, '2017-06-06 14:40:13'),
(290, 1, '101.98.86.113', 10, '2017-06-06 14:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_temp` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remember_token` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `roll` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`, `password_temp`, `code`, `active`, `remember_token`, `jobtitle`, `roll`, `orgid`, `status`, `updated_at`, `created_at`) VALUES
(10, 'Pavithra', 'Isuru', 'pavithraisuru@gmail.com', '$2y$10$iIBVz3lmWdA2cX5pB75jPuk/ebFjdl05zBtuN6/JDOT6jMusQwcb6', NULL, '', 1, '109LQyLqWfxmRai5B50sISlPlOKumLrJAFJQIkuMCgullfw7hCsmNTPPBpRH', 'System Administrator', 1, 1, 1, '2017-05-29 01:36:46', '2017-05-29 01:36:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_controllers`
--
ALTER TABLE `system_controllers`
  ADD PRIMARY KEY (`id`), ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  ADD PRIMARY KEY (`id`), ADD KEY `controllerid` (`controllerid`), ADD KEY `functionid` (`functionid`), ADD KEY `userid` (`userid`), ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_functions`
--
ALTER TABLE `system_functions`
  ADD PRIMARY KEY (`id`), ADD KEY `controllerid` (`controllerid`);

--
-- Indexes for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  ADD PRIMARY KEY (`id`), ADD KEY `functionid` (`functionid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `system_operations`
--
ALTER TABLE `system_operations`
  ADD PRIMARY KEY (`id`), ADD KEY `systemid` (`systemid`), ADD KEY `functionid` (`functionid`);

--
-- Indexes for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  ADD PRIMARY KEY (`id`), ADD KEY `operationid` (`operationid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `system_tables`
--
ALTER TABLE `system_tables`
  ADD PRIMARY KEY (`id`), ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_table_columns`
--
ALTER TABLE `system_table_columns`
  ADD PRIMARY KEY (`id`), ADD KEY `tableid` (`tableid`);

--
-- Indexes for table `system_url_routes`
--
ALTER TABLE `system_url_routes`
  ADD PRIMARY KEY (`id`), ADD KEY `systemid` (`systemid`), ADD KEY `controllerid` (`controllerid`), ADD KEY `functionid` (`functionid`);

--
-- Indexes for table `system_views`
--
ALTER TABLE `system_views`
  ADD PRIMARY KEY (`id`), ADD KEY `systemid` (`systemid`), ADD KEY `functionid` (`functionid`);

--
-- Indexes for table `system_view_access`
--
ALTER TABLE `system_view_access`
  ADD PRIMARY KEY (`id`), ADD KEY `userid` (`userid`), ADD KEY `viewid` (`viewid`);

--
-- Indexes for table `system_view_visit`
--
ALTER TABLE `system_view_visit`
  ADD PRIMARY KEY (`id`), ADD KEY `viewid` (`viewid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `systems`
--
ALTER TABLE `systems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `system_controllers`
--
ALTER TABLE `system_controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `system_functions`
--
ALTER TABLE `system_functions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_operations`
--
ALTER TABLE `system_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_tables`
--
ALTER TABLE `system_tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_table_columns`
--
ALTER TABLE `system_table_columns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_url_routes`
--
ALTER TABLE `system_url_routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `system_views`
--
ALTER TABLE `system_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `system_view_access`
--
ALTER TABLE `system_view_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_view_visit`
--
ALTER TABLE `system_view_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=291;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
