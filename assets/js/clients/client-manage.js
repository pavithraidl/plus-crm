
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-06 13:34:30.
 * Controller ID - 54
 */

'use strict';
window.openedClient = null;
window.loading = false;
window.loadingValue = 1;

function openPanel(clientId) {
    if(window.openedClient == null) {
        //none is opened and now opening the clicked one
            window.openedClient = clientId;
            $('#client-'+clientId).css('transform', 'scale(1.07)');
            $('#slide-block-loading-'+clientId).css("width", "1%");
            $('#slide-block-loading-'+clientId).fadeIn(100);
            window.loading = true;

            loadingTimer('slide-block-loading-'+clientId);

            //except jquery here I have written settime out. Remove settimeout and write jquery to get the real data from the server
            setTimeout(function () {
                window.loading = false;
                $('#slide-block-loading-'+clientId).animate({width:'98%'}, 200);
                $('#slide-block-loading-'+clientId).fadeOut(400);

                var appendHtml = $('<hsdflkjal>').html();
                var appendHtml = $(appendHtml).hide();
                var el = $('#client-content-'+clientId);
                blockUI(el);

                $('#client-content-'+clientId).append(appendHtml);
                $('#client-'+clientId).css({'height':'300px', 'marginBottom': '25px', 'marginTop': '20px'});
                setTimeout(function () {
                    $('#client-content-slide').slideDown(300);
                }, 10);
                unBlockUI(el);
                window.loading = false;
                window.loadingValue = 4;


            }, 900);
    }
    else if(window.openedClient == clientId) {
        //clicked on the opend one and now closing it
        $('#client-content-slide').slideUp(300, function () {
            $('#client-'+clientId).css('transform', 'scale(1.0)');
            $('#slide-block-loading-'+clientId).css("width", "0%");
            $('#client-content-slide').remove();
        });
        setTimeout(function () {
            $('#client-'+clientId).animate({height: '40px', 'marginBottom': '5px', 'marginTop': '5px'}, 300);
        }, 100);
        window.openedClient = null;
    }
    else if(window.openedClient != clientId && clientId != null) {
        //another one opened and another one clicked. closing the opened one and opening the clicked one
        $('#client-content-slide').slideUp(300, function () {
            $('#client-'+window.openedClient).css('transform', 'scale(1.0)');
            $('#slide-block-loading-'+window.openedClient).css("width", "0%");
            $('#client-content-slide').remove();

            setTimeout(function () {
                window.openedClient = null;
                openPanel(clientId);
            }, 150);
        });
        setTimeout(function () {
            $('#client-'+window.openedClient).animate({height: '40px', 'marginBottom': '15px', 'marginTop': '5px'}, 300);
        }, 100);
    }
}

function loadingTimer(id) {
    window.loadingValue++;
    var value = window.loadingValue;
    if(value < 45) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 100);
                loadingTimer(id);
            }
        }, 100);
    }
    else if(value < 80) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 200);
                loadingTimer(id);
            }
        }, 200);
    }
    else if(value < 90) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 400);
                loadingTimer(id);
            }
        }, 400);
    }
    else if(value < 98) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 800);
                loadingTimer(id);
            }
        }, 800);
    }
}

