
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-05 08:32:38.
 * Controller ID - 44
 */

'use strict';

window.chk1 = 0;
window.chk2 = 0;
window.agreementId = $('#agreement-id').val();
window.deletingAgreement = 0;

countTotal();


function countTotal() {
    var setupTotal = 0;
    var totalTotal = 0;
    var ongoingTotal = 0;

    for(var i = 0; i < 6; i++) {
        var setup = parseFloat($.trim($('#middle-setup-'+i).val()));
        if(!isNaN(setup))
            setupTotal = parseFloat(setup+setupTotal);

        var total = parseFloat($.trim($('#middle-total-'+i).val()));
        if(!isNaN(total))
            totalTotal = parseFloat(total+totalTotal);

        var ongoing = parseFloat($.trim($('#middle-ongoin-'+i).val()));
        if(!isNaN(ongoing))
            ongoingTotal = parseFloat(ongoing+ongoingTotal);
    }

    var gstSetup = setupTotal*0.15;
    var gstTotal = totalTotal*0.15;
    var gstOngoing = ongoingTotal*0.15;

    var fullTotalSetup = setupTotal+gstSetup;
    var fullTotalTotal = totalTotal+gstTotal;
    var fullOngoingTotal = ongoingTotal+gstOngoing;


    $('#gst-setup').val(gstSetup == 0 ? '' : gstSetup.toFixed(2));
    $('#gst-setup').attr('value', gstSetup == 0 ? '' : gstSetup.toFixed(2));

    $('#gst-total').val(gstTotal == 0 ? '' : gstTotal.toFixed(2));
    $('#gst-total').attr('value', gstTotal == 0 ? '' : gstTotal.toFixed(2));

    $('#gst-ongoing').val(gstOngoing == 0 ? '' : gstOngoing.toFixed(2));
    $('#gst-ongoing').attr('value', gstOngoing == 0 ? '' : gstOngoing.toFixed(2));

    $('#total-setup').val(fullTotalSetup == 0 ? '' : '$'+fullTotalSetup.toFixed(2));
    $('#total-setup').attr('value', fullTotalSetup == 0 ? '' : '$'+fullTotalSetup.toFixed(2));

    $('#total-total').val(fullTotalTotal == 0 ? '' : '$'+fullTotalTotal.toFixed(2));
    $('#total-total').attr('value', fullTotalTotal == 0 ? '' : '$'+fullTotalTotal.toFixed(2));

    $('#total-ongoing').val(fullOngoingTotal == 0 ? '' : '$'+fullOngoingTotal.toFixed(2));
    $('#total-ongoing').attr('value', fullOngoingTotal == 0 ? '' : '$'+fullOngoingTotal.toFixed(2));
}



function addValue(object, filed, page) {
    var value = $('#'+object.id).val();
    $('#'+object.id).attr('value', value);

    value = value.replace('http://', '');

    saveData(value, page, filed, window.agreementId);
    //savePrintHtml(page, window.agreementId);
}
function addTextAreaValue(object, field, page) {
    var value = $('#'+object.id).val();
    $('#'+object.id).text(value);

    saveData(value, page, field, window.agreementId);
}

function iCheckChanged(checkBoxId, state) {
    console.log(checkBoxId+' - '+state);
    if(checkBoxId == 'chk1') {
        saveData(state, 1, 'minimum_term', window.agreementId);
        //savePrintHtml(1, window.agreementId);
    }
    else if(checkBoxId == 'chk2') {
        saveData(state, 1, 'call_recording', window.agreementId);
        //savePrintHtml(1, window.agreementId);
    }
    else if(checkBoxId == 'chk3') {
        saveData(state, 2, 'ip_targeting_national', window.agreementId);
        //savePrintHtml(2, window.agreementId);
    }
    else if(checkBoxId == 'chk4') {
        saveData(state, 2, 'ip_targeting_local', window.agreementId);
        //savePrintHtml(2, window.agreementId);
    }
    else if(checkBoxId == 'chk5') {
        saveData(state, 2, 'business_name_trademark', window.agreementId);
        //savePrintHtml(2, window.agreementId);
    }
    else if(checkBoxId == 'chk6') {
        saveData(state, 3, 'phone_tracking_geo', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    else if(checkBoxId == 'chk7') {
        saveData(state, 3, 'phone_tracking_au', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    else if(checkBoxId == 'chk8') {
        saveData(state, 3, 'phone_tracking_nz', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    else if(checkBoxId == 'chk9') {
        saveData(state, 3, 'phone_tracking_complex', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    else if(checkBoxId == 'chk10') {
        saveData(state, 4, 'cheque', window.agreementId);
        //savePrintHtml(4, window.agreementId);
    }
    else if(checkBoxId == 'chk11') {
        saveData(state, 4, 'visa', window.agreementId);
        //savePrintHtml(4, window.agreementId);
    }
    else if(checkBoxId == 'chk12') {
        saveData(state, 4, 'mastercard', window.agreementId);
        //savePrintHtml(4, window.agreementId);
    }
    else if(checkBoxId == 'chk13') {
        saveData(state, 4, 'american', window.agreementId);
        //savePrintHtml(4, window.agreementId);
    }
    else if(checkBoxId == 'chk14') {
        saveData(state, 4, 'others', window.agreementId);
        //savePrintHtml(4, window.agreementId);
    }
    else if(checkBoxId == 'rdo1') {
        saveData(state, 3, 'call_recording', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    // else if(checkBoxId == 'rdo2') {
    //     saveData(1, 3, 'call_recording', window.agreementId);
    // }
    else if(checkBoxId == 'rdo3') {
        saveData(state, 3, 'exsisting_media_campaigns', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    // else if(checkBoxId == 'rdo4') {
    //     saveData(1, 3, 'exsisting_media_campaigns', window.agreementId);
    // }
    else if(checkBoxId == 'rdo5') {
        saveData(state, 3, 'has_the_client', window.agreementId);
        //savePrintHtml(3, window.agreementId);
    }
    // else if(checkBoxId == 'rdo6') {
    //     alert(state);
    //     saveData(1, 3, 'has_the_client', window.agreementId);
    // }
}


// New try
function run(pageId, agreementId)
{
    $('#top-header').attr('style', 'margin-bottom:-50px !important;');
    var divToPrint = $('#full-form').html();
    //console.log(divToPrint);

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write(' <html>' +
        '                       <head>' +
        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/bootstrap/grid-layout.css">' +
        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/ios7-switch/ios7-switch.css">' +
        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/css/custom.css">' +
        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/css/clients/agreement.css">' +
        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/iCheck/skins/flat/green.css">' +
        '                           <link rel="shortcut icon" href="'+window.domain+'/assets/backend/images/logo-ico.png">' +
        '                           <title>Print Window</title>' +
        '                       </head>' +
        '                       <body onload="window.print();" style="background-color: #fff;">' +
        '                           '+divToPrint+
        '                       </body><script src="'+window.domain+'/assets/backend/libs/jquery/jquery-1.11.1.min.js"></script>' +
        '                       <script src="'+window.domain+'/assets/backend/libs/icheck-1/icheck.min.js"></script><script type="text/javascript"></script>' +
        '                   </html>');

    newWin.document.close();
    $('#top-header').removeAttr('style');
    setTimeout(function(){newWin.close();},200);
    //window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
    savePrint(pageId, agreementId);
};

                    
    // [ID: 140]
    function saveData(value, page, field) {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'clients/savedata',
            data: {value: value, page:page, field:field, agreementid:agreementId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 142]
    function deleteAgreement(agreementId) {
    window.deletingAgreement = agreementId;
    var id = 'deleting-td-'+agreementId;
    nconfirm(id, 'Delete Permanently', 'Do you really want to delete this agreement ?', 'Remove', 'danger', -15, 70);
    }
window.confirmfunction = (function () {
    var agreementId = window.deletingAgreement;
    //develop the function here
    var el = $('#agreement-table');
    blockUI(el);
    $('#agreement-row-'+agreementId).fadeTo(100, 0.7);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'clients/deleteagreement',
        data: {agreementid:agreementId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                $('#agreement-row-'+agreementId).fadeTo(100, 0);
                $('#agreement-row-'+agreementId).remove();
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
});

window.notConfirmed = (function () {});
                    
    // [ID: 146]
    function savePrint(pageId, agreementId) {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'clients/saveprint',
            data: {pageid:pageId, agremeentid:agreementId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function saveAgreement(pageId) {
        savePrintHtml(pageId, window.agreementId);
    }

    // [ID: 150]
    function savePrintHtml(page, agreementId) {
        //$('#top-header').attr('style', 'margin-bottom:-50px !important;');
        var divToPrint = $('#full-form').html();
        //console.log(divToPrint);

        var printHtml = divToPrint;


        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'clients/saveprinthtml',
            data: {printhtml:printHtml, page:page, agreementid:agreementId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 152]
    function getPrintHtml(page) {
        //develop the function here

        var divToPrint = $('#full-form').html();
        //console.log(divToPrint);

        var printHtml = divToPrint;

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'clients/getprinthtml',
            data: {agreementid:window.agreementId, page:page, printhtml:printHtml}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    //$('#top-header').attr('style', 'margin-bottom:-50px !important;');
                    //var divToPrint = $('#full-form').html();
                    //console.log(divToPrint);

                    var newWin=window.open('','Print-Window');

                    newWin.document.open();

                    newWin.document.write(' <html>' +
                        '                       <head>' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/bootstrap/grid-layout.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/ios7-switch/ios7-switch.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/css/custom.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/css/clients/agreement.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/iCheck/skins/flat/green.css">' +
                        '                           <link rel="shortcut icon" href="'+window.domain+'/assets/backend/images/logo-ico.png">' +
                        '                           <title>Print Window</title>' +
                        '                       </head>' +
                        '                       <body onload="window.print();" style="background-color: #fff;">' +
                        '                           '+res+
                        '                       </body><script src="'+window.domain+'/assets/backend/libs/jquery/jquery-1.11.1.min.js"></script>' +
                        '                       <script src="'+window.domain+'/assets/backend/libs/icheck-1/icheck.min.js"></script><script type="text/javascript"></script>' +
                        '                   </html>');

                    newWin.document.close();
                    $('#top-header').removeAttr('style');
                    setTimeout(function(){newWin.close();},200);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function printAgreement(agreementId) {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'clients/getprinthtml',
            data: {agreementid:agreementId, page:null, printhtml:null}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    //$('#top-header').attr('style', 'margin-bottom:-50px !important;');
                    //var divToPrint = $('#full-form').html();
                    //console.log(divToPrint);

                    var newWin=window.open('','Print-Window');

                    newWin.document.open();

                    newWin.document.write(' <html>' +
                        '                       <head>' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/bootstrap/grid-layout.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/ios7-switch/ios7-switch.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/css/custom.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/css/clients/agreement.css">' +
                        '                           <link media="all" type="text/css" rel="stylesheet" href="'+window.domain+'/assets/backend/libs/iCheck/skins/flat/green.css">' +
                        '                           <link rel="shortcut icon" href="'+window.domain+'/assets/backend/images/logo-ico.png">' +
                        '                           <title>Print Window</title>' +
                        '                       </head>' +
                        '                       <body onload="window.print();" style="background-color: #fff;">' +
                        '                           '+res+
                        '                       </body><script src="'+window.domain+'/assets/backend/libs/jquery/jquery-1.11.1.min.js"></script>' +
                        '                       <script src="'+window.domain+'/assets/backend/libs/icheck-1/icheck.min.js"></script><script type="text/javascript"></script>' +
                        '                   </html>');

                    newWin.document.close();
                    $('#top-header').removeAttr('style');
                    // setTimeout(function(){newWin.close();},200);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

