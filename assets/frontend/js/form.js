/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/11/17 09:19.
 */

"use strict";

//region Global Variables

//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
function submitForm() {
    var name = $.trim($('#txt-name').val());
    var email = $.trim($('#txt-email').val());
    var phone = $.trim($('#txt-phone').val());
    var msg = $('#txt-msg').val();

    if(name == '' || email == '') {
        $('#txt-name').trigger('blur');
        $('#txt-email').trigger('blur');
    }
    else {
        var loading = '<img src="'+window.domain+'/assets/frontend/images/loading/loader.svg" style="position: absolute; top: -10px; left: 40%;" />';
        $('#btn-submit').html(loading);

        jQuery.ajax({
            type: 'POST',
            url: window.domain+'contact/contact-form',
            data: {name:name, email:email, phone:phone, msg:msg},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 1) {
                    $('.enquiry-form').slideUp(200);

                    $('#btn-submit').html('SUBMIT');
                    $('.success-message').addClass('fadeInDown', function () {
                        $(this).attr('style', 'opacity:1');
                        $('#txt-name').val('');
                        $('#txt-email').val('');
                        $('#txt-phone').val('');
                        $('#txt-msg').val('').innerHTML('');
                    });

                    setTimeout(function () {
                        $('.success-message').removeAttr('style');
                        $('.success-message').addClass('fadeOutDown', function () {
                            location.reload();
                        });
                    }, 5000);
                }
                else if(data == 0) {
                    dbError();
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    }
}

function hideSuccessMessage() {

}
//endregion