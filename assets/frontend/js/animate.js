/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/17 09:32.
 */

"use strict";

//Startup
$(document).ready(function () {
    setTimeout(function () {
        $('#slider-banner').removeAttr('style');
        $('#slider-banner').animateCss('fadeInDown');
    }, 500);
    setTimeout(function () {
        $('#btn-free-consultation').animateCss('zoomIn');
        $('#btn-free-consultation').removeAttr('style');
        $('#header').fadeTo(700, 1);
        setTimeout(function () {
            $('#top-partners').fadeTo(3000, 1);
        }, 600);

    }, 2500);


});


$( document ).mousemove( function( e ) {
    $( '#img-web' ).parallax( 20, e );
    $( '#img-social' ).parallax( -30, e );
    $( '#img-tele' ).parallax( 40, e );
});

// $( window ).resize(function() {
//     setupParameters();
// }