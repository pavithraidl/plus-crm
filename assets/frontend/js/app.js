/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 17/11/17 10:02.
 */

if('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('./service-worker.js', {scope: './'})
        .then(function(registration) {
            console.log("Service Worker Registered", registration);
        })
        .catch(function (err) {
            console.log("Service Worker Failed to Register", err);
        })
}