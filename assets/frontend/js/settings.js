/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 16/10/17 13:28.
 */

"use strict";


//Page - Services
$('#snav-tab-1').on('click', function () {
    $('#snav-tab-3').removeClass('ui-tabs-active').removeAttr('style');
    var icon1 = 'key';
    var icon2 = 'line-chart';
    var icon3 = 'link';
    var icon4 = 'clock-o';
    var icon5 = 'file-text-o';
    var icon6 = null;

    var title1 = 'Identify the keywords<br/> that will convert';
    var title2 = 'Analyse your<br/> competitors strategies';
    var title3 = 'Get quality websites to<br/> link back to you';
    var title4 = 'Have your website <br/> frequently audited';
    var title5 = 'Receive regular<br/> randing/status reports';
    var title6 = '';


    var img1 = 'identify-the-keywords.png';
    var img2 = 'analyse-competitor-s-strategies.png';
    var img3 = 'get-websites-to-link.png';
    var img4 = 'website-audit.png';
    var img5 = 'ranking-reports.png';
    var img6 = '';

    var text1 = 'First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.';
    var text2 = 'First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.';
    var text3 = 'It is necessary to have high-quality relevant links so Google can recognise and rank your website highly. Our team continuously uses the highest of industry techniques which help your website increase in ranking.';
    var text4 = 'Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines';
    var text5 = 'Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines';
    var text6 = '';
    
    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);

});
$('#snav-tab-2').on('click', function () {
    $('#snav-tab-3').removeClass('ui-tabs-active').removeAttr('style');
    var icon1 = 'cogs';
    var icon2 = 'sitemap';
    var icon3 = 'vcard';
    var icon4 = 'edit';
    var icon5 = 'newspaper-o';
    var icon6 = null;

    var title1 = 'Receive a <br/>customised package';
    var title2 = 'Receive an easy-to-<br/>navigate website';
    var title3 = 'Engage users with a <br/>persuasive copy';
    var title4 = 'Standout with <br/>customised graphics';
    var title5 = 'Rank on Google with <br/>an SEO ready website';
    var title6 = '';

    var img1 = 'customised-package.png';
    var img2 = 'easy-navigate-website.png';
    var img3 = 'engage-users-with-a-persuasive-copy.png';
    var img4 = 'standout-with-customised-graphics.png';
    var img5 = 'seo-ready-website.png';
    var img6 = '';

    var text1 = 'We understand that different companies present varying budget constraints. That is why we work to develop a customised package that suits your budget, so that every company has the opportunity to grow through digital marketing.';
    var text2 = 'We understand that different companies present varying budget constraints. That is why we work to develop a customised package that suits your budget, so that every company has the opportunity to grow through digital marketing.';
    var text3 = 'Stand out from your competitors by having unique content that is not only persuasive, but also captures your vision and gives the right amount of information needed to help with the decision making process. Our copywriters have significant experience in creating marketing content that converts web visitors to customers.' ;
    var text4 = 'Stand out from your competitors by having unique content that is not only persuasive, but also captures your vision and gives the right amount of information needed to help with the decision making process. Our copywriters have significant experience in creating marketing content that converts web visitors to customers.';
    var text5 = 'Gain visibility on Google or other search engines quicker by having an SEO ready website. PlusMedia is your leading name in SEO services Auckland wide, and our team of SEO experts know exactly what to do to ensure your website ranks highly on search engines.';
    var text6 = '';
    
    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-3').on('click', function () {
    $('#snav-tab-4').trigger('click');
    $('#snav-tab-3').addClass('ui-tabs-active').attr('style', 'color: #ec1d24 !important;');
    var icon1 = 'fighter-jet';
    var icon2 = 'laptop';
    var icon3 = 'search';
    var icon4 = 'bullseye';
    var icon5 = 'folder';
    var icon6 = null;

    var title1 = 'Get ahead of your <br/>competitors with<br/> winning sales ads';
    var title2 = 'Receive a complementary <br/>landing page';
    var title3 = 'Receive regular campaign <br/>optimisation';
    var title4 = 'Reach your preferred <br/>audience through effective <br/>targeting';
    var title5 = 'See fast and <br/>transparent results';
    var title6 = '';

    var img1 = 'winning-sales-ad.png';
    var img2 = 'complementary-landing-page.png';
    var img3 = 'regular-campaign-optimisation.png';
    var img4 = 'effective-targeting.png';
    var img5 = 'fast-and-transparent-results.png';
    var img6 = '';

    var text1 = 'With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.';
    var text2 = 'With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.';
    var text3 = 'Want to ensure your campaign is creating the results you desire? Our specialist team have it sorted. They are constantly monitoring and optimising your AdWords campaigns to guarantee you are getting the best possible return on investment.';
    var text4 = 'With our +Media AdPro package you can decide how specific you want your targeting to be. Our experts can provide broad match, phrase match and exact match keywords. This gives you the choice between optimising your page for thousands of related searches or only for a few hundred highly specific searches.';
    var text5 = 'If you sign up for our +Media AdPro package you will receive regular campaign reports providing you with metrics such as cost per click, click through rate, quality score and much more. These reports will help you track your campaign’s progress, so there are no more guessing games.';
    var text6 = '';
    
    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-4').on('click', function () {
    $('#snav-tab-3').addClass('ui-tabs-active').attr('style', 'color: #ec1d24 !important;');
    var icon1 = 'fighter-jet';
    var icon2 = 'laptop';
    var icon3 = 'search';
    var icon4 = 'bullseye';
    var icon5 = 'folder';
    var icon6 = null;

    var title1 = 'Get ahead of your <br/>competitors with<br/> winning sales ads';
    var title2 = 'Receive a complementary <br/>landing page';
    var title3 = 'Receive regular campaign <br/>optimisation';
    var title4 = 'Reach your preferred <br/>audience through effective <br/>targeting';
    var title5 = 'See fast and <br/>transparent results';
    var title6 = '';

    var img1 = 'winning-sales-ad.png';
    var img2 = 'complementary-landing-page.png';
    var img3 = 'regular-campaign-optimisation.png';
    var img4 = 'effective-targeting.png';
    var img5 = 'fast-and-transparent-results.png';
    var img6 = '';

    var text1 = 'With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.';
    var text2 = 'With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.';
    var text3 = 'Want to ensure your campaign is creating the results you desire? Our specialist team have it sorted. They are constantly monitoring and optimising your AdWords campaigns to guarantee you are getting the best possible return on investment.';
    var text4 = 'With our +Media AdPro package you can decide how specific you want your targeting to be. Our experts can provide broad match, phrase match and exact match keywords. This gives you the choice between optimising your page for thousands of related searches or only for a few hundred highly specific searches.';
    var text5 = 'If you sign up for our +Media AdPro package you will receive regular campaign reports providing you with metrics such as cost per click, click through rate, quality score and much more. These reports will help you track your campaign’s progress, so there are no more guessing games.';
    var text6 = '';

    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-5').on('click', function () {
    $('#snav-tab-3').addClass('ui-tabs-active').attr('style', 'color: #ec1d24 !important;');
    var icon1 = 'television';
    var icon2 = 'bullseye';
    var icon3 = 'picture-o';
    var icon4 = 'area-chart';
    var icon5 = 'handshake-o';
    var icon6 = null;

    var title1 = 'Stand out to users with <br/>an eye catching ad';
    var title2 = 'Reach your preferred <br/>audience through <br/>detailed targeting';
    var title3 = 'Use memorable imagery to <br/>enhance brand awareness';
    var title4 = 'Use meaningful data to <br/>track your campaign’s <br/>performance';
    var title5 = 'Turn window shoppers <br/>into customers with <br/>remarketing';
    var title6 = '';

    var img1 = 'eye-catching-ad.png';
    var img2 = 'detailed-targeting.png';
    var img3 = 'imagery-to-enhance-brrand-awarenss.png';
    var img4 = 'track-campaigns-performance.png';
    var img5 = 'track-campaigns-performance.png';
    var img6 = '';

    var text1 = 'It’s easy for users to ignore a regular text ad. However, with display ads, you can use rich images and videos to grab the user’s attention and convince them to click on your ad and learn more about your business.';
    var text2 = 'It is important to target people relevant to your business when advertising. Display ads provide you with many targeting options to ensure your ads are reaching the right people. These ads can be tailored for an individual based on their demographics, interests, topics and location. Additionally, you can decide which websites you want your ads to appear on.';
    var text3 = 'It is important to target people relevant to your business when advertising. Display ads provide you with many targeting options to ensure your ads are reaching the right people. These ads can be tailored for an individual based on their demographics, interests, topics and location. Additionally, you can decide which websites you want your ads to appear on.';
    var text4 = 'It is crucial to track the performance of your marketing activities to see what is working and what adjustments need to be made. Display advertising provides you with meaningful data such as how many users your ads are reaching, how many clicks its receiving, and the user response. By tracking your performance, you can rest assured that you are getting the most out of your investment.';
    var text5 = 'It is crucial to track the performance of your marketing activities to see what is working and what adjustments need to be made. Display advertising provides you with meaningful data such as how many users your ads are reaching, how many clicks its receiving, and the user response. By tracking your performance, you can rest assured that you are getting the most out of your investment.';
    var text6 = '';

    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-6').on('click', function () {
    $('#snav-tab-3').addClass('ui-tabs-active').attr('style', 'color: #ec1d24 !important;');
    var icon1 = 'usd';
    var icon2 = 'sitemap';
    var icon3 = 'hand-pointer-o';
    var icon4 = 'microphone';
    var icon5 = 'th-list';
    var icon6 = null;

    var title1 = 'Get an accurate <br/>cost per lead';
    var title2 = 'Enhance the customer <br/>experience with call routing';
    var title3 = 'Install a “click to call” button <br/>for extra convenience';
    var title4 = 'Have your calls recorded <br/>for best practise';
    var title5 = 'Capture "missed" call details <br/>and never lose a customer';
    var title6 = '';

    var img1 = 'lead.png';
    var img2 = 'enhance-customer-experience-with-call-routing.png';
    var img3 = 'click-to-call-button-for-extra-convenience.png';
    var img4 = 'call-recording.png';
    var img5 = 'call-details.png';
    var img6 = '';

    var text1 = 'Discover the actual cost of acquiring leads and identify exactly which marketing tactics are producing results for your business.';
    var text2 = 'The +Media AdClear system is easily customisable, allowing you to route calls by time, day, location, or to your mobile. These features are all included in the package, meaning there’s no need to buy additional hardware.';
    var text3 = 'With millions of people conducting mobile searches using their smartphones every day, the “click to call” feature is a huge convenience. Installing a “click to call” feature will give you an edge over your competitors, as your potential customers can contact you in the click of a button.';
    var text4 = 'Call recording enables you to replay the calls you answered, allowing your company to identify any shortcomings and improve their practices. Having your calls recorded also gives you the ability to have verbal contract agreements documented.';
    var text5 = 'Did you just miss a call for whatever reason? Don’t stress. +Media AdClear captures the callers’ details so you can call back and potentially gain a new customer.';
    var text6 = '';

    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-7').on('click', function () {
    $('#snav-tab-3').removeClass('ui-tabs-active').removeAttr('style');
    var icon1 = 'handshake-o';
    var icon2 = 'thumbs-o-up';
    var icon3 = 'dot-circle-o';
    var icon4 = 'globe';
    var icon5 = null;
    var icon6 = null;

    var title1 = 'Gain valuable <br/>customer insights';
    var title2 = 'Increase brand <br/>awareness and loyalty';
    var title3 = 'Reach the right people <br/>through targeted ads \n';
    var title4 = 'Increase website traffic <br/>and search ranking';
    var title5 = '';
    var title6 = '';

    var img1 = 'valuable-customer-insights.png';
    var img2 = 'brand-awareness-and-loyalty.png';
    var img3 = 'reach-through-targeted-ads.jpg';
    var img4 = 'increase-web-traffic.png';
    var img5 = '';
    var img6 = '';

    var text1 = 'Social media is continuously generating data about your customers, allowing you the opportunity to gain valuable insights, such as who they are and what they like. This data can be used to make more informed business decisions, that will lead to higher conversion rates.';
    var text2 = 'Social Media is a great way to increase your company’s visibility. Giving your business a social media profile and regularly posting content will help expose your company to a multitude of people and legitimize your brand.';
    var text3 = 'Facebook ads are an extremely cost-effective way to publish content and promote your business. They also offer a variety of targeting options, allowing you to segment your audiences based on things like location, demographics, interests and behaviours. This gives you great control over who sees your content.';
    var text4 = 'Social media does wonders for directing traffic to your website. Every business profile on social media is a path way to your website, and the more high-quality content you post to your profile, the more traffic you will generate. Also, the more shares this content receives, the higher your website’s search ranking will be.';
    var text5 = '';
    var text6 = '';

    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});
$('#snav-tab-8').on('click', function () {
    $('#snav-tab-3').removeClass('ui-tabs-active').removeAttr('style');
    var icon1 = 'phone';
    var icon2 = 'phone';
    var icon3 = 'volume-control-phone';
    var icon4 = 'mobile';
    var icon5 = 'list-ol';
    var icon6 = 'microphone';

    var title1 = 'Encourage customer <br/>calls with toll-free <br/>numbers';
    var title2 = 'Create a local <br/>presence through <br/>geo numbers';
    var title3 = 'Ensure your caller’s <br/>needs are met with call <br/>distribution';
    var title4 = 'Never miss a call <br/>again with call <br/>forwarding';
    var title5 = 'Demonstrate <br/>professionalism with an <br/>options menu';
    var title6 = 'Have your calls <br/>recorded for best <br/>practise';

    var img1 = 'toll-free-number.png';
    var img2 = 'local-presence-through-geo-numbers.png';
    var img3 = 'ensure-needs-with-call-distib.png';
    var img4 = 'never-miss-call-with-call-fwd.png';
    var img5 = 'options-menu.png';
    var img6 = 'calls-recorded-for-best-practice.jpg';

    var text1 = 'We highly recommend the use of toll-free numbers, as customers do not receive call charges, meaning there is less to stop them from calling you. PlusMedia provide cheap toll-free numbers, whether it be a 0800 NZ or 0508 NZ number.';
    var text2 = 'It is common to want to market your business outside of your local area. Our geo numbers allow you to mask your number enabling you to appear locally, even if you are not local. This allows your business to reach into another marketplace without needing to be based in the area, and creates trust through a seemingly local presence.';
    var text3 = 'Our call distribution set up diverts incoming calls to the most suitable agent so that you can ensure the caller’s needs are fully met. Call distribution works by allowing you to divert the call to another mobile or landline of your choice, such as that of a colleague.';
    var text4 = 'Call forwarding ensures you never miss an opportunity due to a missed call. Call forwarding redirects a phone call to another destination so that it doesn’t go unanswered.';
    var text5 = 'We understand how important first impressions are in the business environment. When you choose to produce a personalised greeting and provide an options menu, people feel like they are dealing with professionals.';
    var text6 = 'With call recording you can replay previous calls, allowing your company to identify any shortcomings and make improvements. Recording your calls also allows you to document verbal contract agreements.';

    changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6);
});

//steps js
$(function(){
    $('.btn-circle').on('click',function(){
        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
        $(this).addClass('btn-info').removeClass('btn-default').blur();
    });

    $('.next-step, .prev-step').on('click', function (e){
        var $activeTab = $('.tab-pane.active');

        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

        if ( $(e.target).hasClass('next-step') )
        {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#'+ nextTab +'"]').tab('show');
        }
        else
        {
            var prevTab = $activeTab.prev('.tab-pane').attr('id');
            $('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#'+ prevTab +'"]').tab('show');
        }
    });
});

//setup to click buttons when click titles
$('#title-step-1').on('click', function () {
    $('#btn-step-1').trigger('click');
});
$('#title-step-2').on('click', function () {
    $('#btn-step-2').trigger('click');
});
$('#title-step-3').on('click', function () {
    $('#btn-step-3').trigger('click');
});
$('#title-step-4').on('click', function () {
    $('#btn-step-4').trigger('click');
});
$('#title-step-5').on('click', function () {
    $('#btn-step-5').trigger('click');
});
//setup the selected color
$('#btn-step-1').on('click', function () {
    $('#step-1').addClass('active');
    $('#step-2').removeClass('active');
    $('#step-3').removeClass('active');
    $('#step-4').removeClass('active');
    $('#step-5').removeClass('active');
});
$('#btn-step-2').on('click', function () {
    $('#step-2').addClass('active');
    $('#step-1').removeClass('active');
    $('#step-3').removeClass('active');
    $('#step-4').removeClass('active');
    $('#step-5').removeClass('active');
});
$('#btn-step-3').on('click', function () {
    $('#step-3').addClass('active');
    $('#step-2').removeClass('active');
    $('#step-1').removeClass('active');
    $('#step-4').removeClass('active');
    $('#step-5').removeClass('active');
});
$('#btn-step-4').on('click', function () {
    $('#step-4').addClass('active');
    $('#step-2').removeClass('active');
    $('#step-3').removeClass('active');
    $('#step-1').removeClass('active');
    $('#step-5').removeClass('active');
});
$('#btn-step-5').on('click', function () {
    $('#step-5').addClass('active');
    $('#step-2').removeClass('active');
    $('#step-3').removeClass('active');
    $('#step-4').removeClass('active');
    $('#step-1').removeClass('active');
});

//change step details depending the top category
function changeStepContent(icon1, icon2, icon3, icon4, icon5, icon6, title1, title2, title3, title4, title5, title6, img1, img2, img3, img4, img5, img6, text1, text2, text3, text4, text5, text6) {
    if(icon1 == null) $('#step-1').fadeOut(100); else $('#step-1').fadeIn(100);
    if(icon2 == null) $('#step-2').fadeOut(100); else $('#step-2').fadeIn(100);
    if(icon3 == null) $('#step-3').fadeOut(100); else $('#step-3').fadeIn(100);
    if(icon4 == null) $('#step-4').fadeOut(100); else $('#step-4').fadeIn(100);
    if(icon5 == null) $('#step-5').fadeOut(100); else $('#step-5').fadeIn(100);
    if(icon6 == null) $('#step-6').fadeOut(100); else $('#step-6').fadeIn(100);


    var domain = $('#domain').val();
    var url = domain + '/assets/frontend/images/service/step-main/';

    $('#ico-step-1').removeAttr('class').addClass('fa fa-' + icon1);
    $('#ico-step-2').removeAttr('class').addClass('fa fa-' + icon2);
    $('#ico-step-3').removeAttr('class').addClass('fa fa-' + icon3);
    $('#ico-step-4').removeAttr('class').addClass('fa fa-' + icon4);
    $('#ico-step-5').removeAttr('class').addClass('fa fa-' + icon5);
    $('#ico-step-6').removeAttr('class').addClass('fa fa-' + icon6);

    $('#title-step-1').html(title1);
    $('#title-step-2').html(title2);
    $('#title-step-3').html(title3);
    $('#title-step-4').html(title4);
    $('#title-step-5').html(title5);
    $('#title-step-6').html(title6);

    $('#img-step-1').attr('src', url + img1);
    $('#img-step-2').attr('src', url + img2);
    $('#img-step-3').attr('src', url + img3);
    $('#img-step-4').attr('src', url + img4);
    $('#img-step-5').attr('src', url + img5);
    $('#img-step-6').attr('src', url + img6);

    $('#text-1').html(text1);
    $('#text-2').html(text2);
    $('#text-3').html(text3);
    $('#text-4').html(text4);
    $('#text-5').html(text5);
    $('#text-6').html(text6);

    $('#btn-step-1').trigger('click');
}