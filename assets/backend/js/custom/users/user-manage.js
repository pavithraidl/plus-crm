
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-09 13:28:19.
 * Controller ID - 45
 */

'use strict';

//region Global Variables
window.selectedUser = null;
window.ownUserId = null;
window.aLPaginate = 0;
//endregion

getUserList();

//region Events
$(document).ready(function () {
    setInterval(function () {
        onlineStateCheck();
    }, 10000);
});

$('#user-tab-activity-log-container').scroll(function () {
    var elementHeight = $("#user-tab-activity-log-container")[0].scrollHeight;
    var scrollPosition = $("#user-tab-activity-log-container").height() + $("#user-tab-activity-log-container").scrollTop();

    if (elementHeight -100 < scrollPosition) {
        if (window.aLPaginate != 0) {
            window.aLPaginate++;
            var userId = window.selectedUser;
            getActivityLog(window.aLPaginate, userId);
        }
    }
});
//endregion
                    
    //get the full user list [ID: 90]
    function getUserList() {
        var el = $('#user-manage-widget');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'users/getuserlist',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    var htmlAppend = '';

                    if(window.selectedUser == null) {
                        window.ownUserId = res[0].id;
                        window.selectedUser = res[0].id;
                    }

                    for(var i = 0; i < res.length; i++) {
                        if(res[i].onlinegap < 10 && res[i].onlinegap != null) {
                            var onlineGap = ' style="border: 3px solid #34e069;"';
                        }
                        else {
                            var onlineGap = '';
                        }
                        htmlAppend += '  <li id="user-list-item-'+res[i].id+'" class="user-list-1 ' + (res[i].id == window.selectedUser ? 'user-list-1-selected' : '') + '" onclick="selectUser(' + res[i].id + ');" ' + (res[i].status == 3 ? 'data-toggle="tooltip" title="Account not activated yet"' : "") + (res[i].status ==2 ? 'style="opacity: 0.4;"' : '') + '>' +
                            '                   <div class="row" style="margin-bottom: -15px;"> ' +
                            '                       <div class="col-md-3 col-xs-4"> ' +
                            '                           <span class="rounded-image topbar-profile-image"> ' +
                            '                               <img id="user-avatar-'+res[i].id+'" class="img-circle ' + (res[i].status == 3 ? 'gray-scale"' : '"') + ' src="'+window.domain+res[i].avatar+'"'+onlineGap+' > ' +
                            '                           </span> ' +
                            '                       </div> ' +
                            '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;"> ' +
                            '                           <div class="row"> ' +
                            '                               <span id="o-m-org-list-name-1" class="text-blue-3 pull-left" style="font-size: 15px;' + (res[i].status == 3 ? 'color:#A4A4A4 !important;' : "") + '"><strong>'+res[i].name+'</strong></span> ' +
                            '                               <span id="o-m-org-list-loading-container-1"></span> ' +
                            '                           </div> ' +
                            '                       <div class="row"> ' +
                            '                       <p id="o-m-org-list-roll-1" style="color: rgba(100, 100, 100, 0.31); font-size:12px;">'+res[i].dept+' :: '+res[i].roll+'</p> ' +
                            '                       </div>' +
                            '                           <input type="hidden" id="o-m-active-status-1" value="Value" /> ' +
                            '                       </div> ' +
                            '                   </div> ' +
                            '                   <hr/> ' +
                            '               </li>';
                    }
                    getUserBasicDetails();
                    $('#user-list-container').html(htmlAppend);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 99]
    function getUserBasicDetails() {
        var userId = window.selectedUser;
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'users/getuserbasicdetails',
            data: {userid:userId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    $('.ios-switch-md').removeClass('on');
                    $('.ios-switch-sm').removeClass('on');
                    $('.iswitch-xs').removeClass('on');
                    if(res.status == 3) {
                        $('#is-user-status').fadeOut(200).addClass('on');
                        $('#user-image').addClass('gray-scale').attr('src', window.domain+res.avatar);
                        $('#user-name').attr('style', 'color:#A4A4A4 !important;');
                        $('#user-status-icon').addClass('text-gray-3').removeClass('text-green-2').removeClass('text-yellow-1');
                        $('#user-status-text').text('Pending Activation');
                        $('#user-resend-activation-email').fadeIn(300);
                    }
                    else if(res.status == 2) {
                        $('#is-user-status').removeClass('on');
                        $('#user-image').removeClass('gray-scale').attr('src', window.domain+res.avatar);
                        $('#user-name').removeAttr('style');
                        $('#user-status-icon').addClass('text-yellow-1').removeClass('text-green-2').removeClass('text-gray-3');
                        $('#user-status-text').text('Deactivated');
                        $('#user-resend-activation-email').fadeOut(300);

                    }
                    else if(res.status == 1) {
                        if(res.onlinegap < 10 && res.onlinegap != null) {
                            var onlineBorder = 'border: 3px solid #34e069;';
                            var onlineText = 'online';
                            var onlineColor = '25a94e';
                        }
                        else {
                            var onlineBorder = 'border: none;';
                            var onlineText = 'Last Online: '+res.onlinetimeago;
                            var onlineColor = 'b1b1b1';
                        }
                        $('#is-user-status').addClass('on');
                        $('#user-image').removeClass('gray-scale').attr('src', window.domain+res.avatar).attr('style', onlineBorder);
                        $('#user-idntity').removeAttr('style');
                        $('#user-status-icon').addClass('text-green-2').removeClass('text-yellow-1').removeClass('text-gray-3');
                        $('#user-status-text').html('Active <span style="font-size: 10px; color: #'+onlineColor+'; font-style: italic;">('+onlineText+')</span>');
                        $('#user-url').attr('href', 'http://' + res.url);
                        $('#user-resend-activation-email').fadeOut(300);
                    }

                    //add values to settings input fields
                    $('#user-settings-name').val(res.name);
                    $('#user-settings-email').val(res.email);

                    if(window.selectedUser == window.ownUserId) {
                        $('#is-user-status').fadeOut(200).removeClass('active');
                        $('#user-tab-access').fadeOut(200);
                        $('#user-tab-access-container').removeClass('active in');
                        $('#user-tab-activity-log').addClass('active');
                        $('#user-tab-activity-log-container').addClass('active in');
                        $('#user-delete-user').fadeOut(200);
                        $('#user-tab-settings').removeClass('active');
                        $('#user-tab-settings-container').removeClass('active in');
                    }
                    else {
                        $('#user-tab-access').fadeIn(200).removeClass('active');
                        $('#user-tab-access-container').removeClass('active in');
                        $('#user-tab-activity-log').addClass('active');
                        $('#user-tab-activity-log-container').addClass('active in');
                        $('#user-delete-user').fadeIn(200);
                        $('#user-tab-settings').removeClass('active');
                        $('#user-tab-settings-container').removeClass('active in');
                    }

                    if(res.email == null || res.email == '') {
                        $('#user-email').fadeOut(200);
                        $('#user-resend-activation-email').fadeOut(200);
                    }
                    else {
                        $("#user-email").fadeIn(200);
                        $('#user-email').attr('href', 'mailto:'+res.email);
                    }

                    $('#user-name').text(res.name);
                    $('#user-job-title').text(res.jobtitle);
                    $('#user-dept').text(res.dept);

                    getAccessList();
                    var userId = window.selectedUser;
                    getActivityLog(1, userId);
                    var el = $('#user-manage-widget');
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 101]
    function selectUser(id) {
        $('.user-list-1-selected').removeClass('user-list-1-selected');
        $('#user-list-item-'+id).addClass('user-list-1-selected');
        window.selectedUser = id;
        if(window.selectedUser == window.ownUserId) {
            $('#user-delete-user').fadeOut(200);
            $('#is-user-status').fadeOut(200);
        }
        else {
            $('#user-delete-user').fadeIn(200);
            $('#is-user-status').fadeIn(200)
        }
        getUserBasicDetails();
        window.aLPaginate = 0;
        $('#user-activity-list').html('');
        triggerActivityListLoad();
    }
                    
    // [ID: 103]
    function iSwitchOnChange(id, status) {
        var userId = window.selectedUser;
        var el = $('#user-manage-widget');
        blockUI(el);

        if (id == 'user-status') {
            changeUserStatus(userId, status, el);
        }
        else if(id.substring(0, 5) == 'sa-sy') {
            var viewId = id.slice(6);
            changeSystemAccess(viewId, status, window.selectedUser, el);
        }
        else if(id.substring(0, 5) == 'sa-op') {
            var operationId = id.slice(6);
            changeOperationAccess(operationId, status, window.selectedUser, el);
        }
        
    }
                    
    // [ID: 104]
    function changeUserStatus(userId, status, el) {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'users/changeuserstatus',
            data: {userid:userId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 1) {
                    getUsersList();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 106]
    function changeSystemAccess(viewId, status, selectedUser, el) {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'users/changesystemaccess',
            data: {viewid: viewId, status:status, selecteduser:selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 108]
    function changeOperationAccess(operationId, status, selectedUser, el) {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'users/changeoperationaccess',
            data: {operationid: operationId, status:status, selecteduser:selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    // [ID: 125]
    function saveAccessChoice(operationId) {
        var el = $('#user-manage-widget');
        var choice = $('#user-access-system-operation-choices-2').val();
        var userId = window.selectedUser;

        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'users/saveaccesschoice',
            data: {choice:choice, operationid:operationId, userid:userId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
                else {
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

                    
    // [ID: 110]
    function deleteUser() {
        var id = 'user-delete-user';
        nconfirm(id, 'Delete Permanently', 'Do you really want to remove this user account ?', 'Remove', 'danger', 30, -5);
    }
    window.confirmfunction = (function () {
        var userId = window.selectedUser;
        var el = $('#user-manage-widget');
        $('#user-delete-user').trigger('click');
        blockUI(el);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'users/deleteuser',
            data: {userid: userId},
            success: function (data) {
                if(data == 1) {
                    notifyDone('You have successfully removed that user account');
                    window.selectedUser = null;
                    getUserList();
                    unBlockUI(el);
                }
                else if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    });
    window.notConfirmed = (function () {});


    // [ID: 112]
    function resendActivationEmail() {
        //develop the function here
        
        //ajax method
        var userId = window.selectedUser;
        var el = $('#user-manage-widget');
        blockUI(el);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'users/resendactivationemail',
            data: {userid: userId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data != 0) {
                    var res = JSON.parse(data);
                    notifyDone('Activation email has resent to '+res);
                }
                else {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 114]
    function opeAddUser() {
        setTimeout(function () {
            //$('#mm-user-name').focus();
        }, 300);
        
    }
                    
    // [ID: 115]
    function triggerActivityListLoad() {
        if (window.aLPaginate == 0) {
            var userId = window.selectedUser;
            getActivityLog(1, userId);
        }
        
    }
                    
    // [ID: 116]
    function getAccessList() {
        //develop the function here
        
        //ajax method
        var userId = window.selectedUser;
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'users/getaccesslist',
            data: {userid: userId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data != 0) {
                    var res = JSON.parse(data);
                    for(var i = 0; res.view.length > i; i++) {
                        $("#is-sa-sy-"+res.view[i]).addClass('on');
                        console.log(res);
                    }
                    for(var i = 0; res.ope.length > i; i++) {
                        if(res.ope[i].choice == null) {
                            $("#is-sa-op-"+res.ope[i].id).addClass('on');
                        }
                        else {
                            $('#user-access-system-operation-choices-'+res.ope[i].id).val(res.ope[i].choice);
                        }

                    }
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
                    
    // [ID: 118]
    function getActivityLog(paginate, userId) {
        var el = $('#user-tab-activity-log-container');
        if (window.aLPaginate != -1) {
            var temPaginate = window.aLPaginate;
            window.aLPaginate = 0;
            blockUI(el);

            jQuery.ajax({
                type: 'GET',
                url: window.domain + 'users/getactivitylog',
                data: {paginate: paginate, userid: userId},
                success: function (data) {
                    if(data == -1) {
                        temPaginate = -1;
                        $('#user-activity-list').html('<p style="text-align: center;">No Activities..!</p>');
                        unBlockUI(el);
                    }
                    else if (data != 0) {
                        var ret = JSON.parse(data);
                        var activityList = "";
                        for (var i = 0; i < ret.length; i++) {
                            activityList += (
                                '               <li class="media">' +
                                '                   <p>' + ret[i].activity + '<br />' +
                                '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                                '                   </p>' +
                                '               </li>'
                            );
                        }

                        $('#user-activity-list-loading').remove();
                        $('#user-activity-list').append(activityList);
                        window.aLPaginate = temPaginate;
                        window.aLPaginate++;
                        unBlockUI(el);
                    }
                    else {
                        dbError();
                        unBlockUI(el)
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    noConnection(textStatus);
                }
            });
        }
        else {
            unBlockUI(el);
        }
    }
                    
    // [ID: 120]
    function addUser() {
        var name = $.trim($('#mm-user-name').val());
        var email = $.trim($('#mm-user-email').val());
        var jobTitle = $.trim($('#mm-user-job-title').val());
        var department = $("#mm-user-department").val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var emailObject = $('#mm-user-email');

        if(name == '' || email == '') {
            $('#mm-user-name').trigger('blur');
            $('#mm-user-email').trigger('blur');
        }
        else if(email != '' && !re.test(email)) {
            $('#mm-user-email').trigger('blur');
        }
        else {
            var el = $('#modal-add-user-body');
            blockUI(el);
            jQuery.ajax({
                type: 'POST',
                url: window.domain+'users/adduser',
                data: {name:name, email:email, jobtitle: jobTitle, department:department},
                success: function (data) {
                    if(data == 'Unauthorized') {
                        window.location.reload();
                    }
                    else if(data == 0){
                        $('#mm-btn-user-cancel').trigger('click');
                        dbError();
                    }
                    else if(data == -1) {
                        unBlockUI(el);
                        errorExecution(emailObject, 'Sorry! This email has already used.', 'duplicate');
                    }
                    else {
                        //getUsersList();
                        $('#mm-btn-user-cancel').trigger('click');
                        notifyDone('User has added. Account activation link has sent.');
                        getUserList();
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    noConnection();
                    unBlockUI(el);
                }
            });
        }
    }
                    
    // [ID: 122]
    function cancelUser() {
        $('#user-modal-close').trigger('click');
        $('#mm-user-name').val('');
        $('#mm-user-email').val('');

        $('#model-add-new-user .has-error').removeClass('has-error');
        $('#model-add-new-user .error-msg').remove();
        
    }
                    
    // [ID: 144]
    function onlineStateCheck() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'users/onlinestatecheck',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    console.log('Online state check - DB Error!');
                }
                else {
                    var res = JSON.parse(data);

                    for(var i = 0; i < res.length; i++) {
                        if(res[i].onlinegap < 10 && res[i].onlinegap != null) {
                            $('#user-avatar-'+res[i].id).attr('style', 'border: 3px solid #34e069;');
                        }
                        else {
                            $('#user-avatar-'+res[i].id).removeAttr('style');
                        }
                    }

                }
            },
            error: function (xhr, textShort, errorThrown) {
                console.log('Online state check - No connection!');
            }
        });
    }

