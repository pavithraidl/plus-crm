/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/03/17 00:08.
 */

"use strict";

//region Global Variables
window.toggleInspectionSchedule = 0;
window.tenantArray = new Array();
//endregion

//region Default settings
$('#as-schedule-inspection-container').slideUp();
//endregion

//region Events
$('#mm-pro-add-owner').on('click', function() {
    setPropertyOwner('mm-pro');
});

$('#btn-new-property').on('click', function () {
    getUserList();
});

$('#mm-pro-add-tenant').on('click', function () {
    addTenant();
})

$('#pro-troggle-inspection-schedule').on('click', function () {
    toggleScheduleInspection();
});

$('#pro-btn-add').on('click', function () {
    submitNewProperty();
});

//endregion

//region Functions
function setPropertyOwner(appendDivId) {
    console.log(appendDivId);
    window.contactSubmitFunction = function () {
        console.log(window.contactResponse);
        var res = window.contactResponse;
        console.log(res);

        var avatar = res.avatarid;
        if(avatar == '') {
            avatar = 'default';
        }

        var propertyOwner = $('<div id="'+appendDivId+'-owner" class="tenent-container"> ' +
            '                       <img src="'+window.domain+'assets/images/contact/avatar/'+avatar+'.jpg" class="img-circle contact-image" style="width: 28px; height: 28px;margin-top: -5px; border: 1px solid rgba(64, 113, 148, 0.35);"/> ' +
            '                       <span style="margin-left: 10px;">'+res.fname+' '+res.lname+'</span> ' +
            '                       <span onclick="removeOwner(\''+appendDivId+'\')" style="margin-left: 20px;"><i class="fa fa-close" style="color: #e30010; font-size: 14px;"></i></span> ' +
            '                       <input type="hidden" id="'+appendDivId+'-owner-contact-id" value="'+res.id+'" />' +
            '                   </div>'
            );

        $('#'+appendDivId+'-owner-container').html(propertyOwner);

        //if not the add new property, then update the owner using a separate ajax
        if(appendDivId != 'mm-pro') {
            updateSingleData(res.id, 'property', 'id', 'ownercontactid', 'Changed the owner details', window.openedPropertyId)
        }
    };
    globalOpenContact('<i class="fa fa-user-md"></i> Add Owner');
}

function removeOwner(appendDivId) {
    $('#'+appendDivId+'-owner').fadeOut(200, function () {
        $('#'+appendDivId+'-owner')
            .html('<span style="margin-left: 10px;"><i class="fa fa-user-md"></i> Add Property Owner</span>' +
                '   <input type="hidden" id="'+appendDivId+'-owner-contact-id" value="" />').fadeIn(100);
    });

    setTimeout(function () {
        $('#'+appendDivId+'-owner').attr('onclick', 'setPropertyOwner(\''+appendDivId+'\');');
    }, 300);
}

function addTenant() {
    window.contactSubmitFunction = function () {
        console.log(window.contactResponse);
        var res = window.contactResponse;

        var avatar = res.avatarid;
        if(avatar == '') {
            avatar = 'default';
        }

        var tenant = $('<div id="mm-pro-tenant-'+res.id+'" class="tenent-container"> ' +
            '               <img src="'+window.domain+'assets/images/contact/avatar/'+avatar+'.jpg" class="img-circle contact-image" style="width: 28px; height: 28px;margin-top: -5px; border: 1px solid rgba(64, 113, 148, 0.35);"/> ' +
            '               <span style="margin-left: 10px;">'+res.fname+' '+res.lname+'</span> ' +
            '               <span style="margin-left: 20px;" onclick="removeTenant('+res.id+');"><i class="fa fa-close" style="color: #e30010; font-size: 14px;"></i></span> ' +
            '           </div>'
        );

        window.tenantArray.push(res.id);
        $('#mm-pro-tenant-container').prepend(tenant);
    };
    globalOpenContact('<i class="fa fa-user"></i> Add Tenant');
}

function removeTenant(id) {
    $('#mm-pro-tenant-'+id).fadeOut(200, function () {
        $('#mm-pro-tenant-'+id).remove();
    });
    window.tenantArray.splice(window.tenantArray.indexOf(id), 1);
}

function getUserList() {
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'property/get-user-list',
        success: function (data) {
            var res = JSON.parse(data);
            var options = '<option value="0" disabled selected>No one selected</option>';
            for(var i = 0; i < res.length; i++) {
                options += '<option value="'+res[i].id+'">'+res[i].fname+' '+res[i].lname+'</option>'
            }

            $('#mm-pro-manager').html(options);
            $('#pro-inspector').html(options);
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function toggleScheduleInspection() {
    if(window.toggleInspectionSchedule == 0) {
        $('#as-schedule-inspection-container').slideDown(300);
        $("#pro-toggle-inspection-schedule-icon").addClass('fa-chevron-circle-down').removeClass('fa-chevron-circle-up');
        window.toggleInspectionSchedule = 1;
    }
    else {
        $('#as-schedule-inspection-container').slideUp(300);
        $("#pro-toggle-inspection-schedule-icon").addClass('fa-chevron-circle-up').removeClass('fa-chevron-circle-down');
        window.toggleInspectionSchedule = 0;
    }
}

function submitNewProperty() {
    var propertyOwnerId = $.trim($('#mm-pro-owner-contact-id').val());
    var street = $.trim($('#mm-pro-street').val());
    var suburb = $.trim($('#mm-pro-suburb').val());
    var city = $.trim($('#mm-pro-city').val());
    var postalCode = $.trim($('#mm-pro-postal-code').val());
    var reference = $.trim($('#mm-pro-reference').val());
    var propertyManger = $('#mm-pro-manager').val();

    var tenants = window.tenantArray;

    var inspectionDate = $('#pro-inspection-date').val();
    var inspectionTime = $('#pro-inspection-time').val();
    var inspectionFrequency = $('#pro-frequency').val();
    var inspectionType = $('#pro-inspection-type').val();
    var proInspector = $('#pro-inspector').val();

    var alarm = $.trim($('#pro-alarm').val());
    var key = $.trim($('#pro-key-location').val());

    //validation
    if(street == '' || suburb == '' || city == '' || reference == '') {
        $('#mm-pro-street').trigger('blur');
        $('#mm-pro-suburb').trigger('blur');
        $('#mm-pro-city').trigger('blur');
        $('#mm-pro-reference').trigger('blur');
    }
    else {
        var el = $('#mm-add-property-container');
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'property/add-new-property',
            data:{
                ownerid:propertyOwnerId,
                street:street,
                suburb:suburb,
                city:city,
                postalcode:postalCode,
                reference:reference,
                propertymanager:propertyManger,
                tenants:tenants,
                insdate:inspectionDate,
                instime:inspectionTime,
                insfrequency:inspectionFrequency,
                instype:inspectionType,
                inspector:proInspector,
                alarm:alarm,
                key:key
            },
            success: function (data) {
                if(data == 1) {

                    getPropertyList();
                    unBlockUI(el);
                    setTimeout(function () {
                        $('#btn-as-close').trigger('click');
                    }, 20);

                    setTimeout(function () {
                        clearAddNewProperty();
                    }, 200);

                }
                else {
                    dbError();
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    }
}

function clearAddNewProperty() {
    removeOwner();

    $('#mm-pro-owner').html('<span style="margin-left: 10px;"><i class="fa fa-user-md"></i> Add Property Owner</span>' +
        '   <input type="hidden" id="mm-pro-owner-contact-id" value="" />');

    setTimeout(function () {
        $('#mm-pro-owner').attr('onclick', 'setPropertyOwner();');
    }, 300);

    $('#mm-pro-street').val('');
    $('#mm-pro-suburb').val('');
    $('#mm-pro-city').val('');
    $('#mm-pro-postal-code').val('');
    $('#mm-pro-reference').val('');

    var tenant = window.tenantArray;

    for(var i = 0; i < tenant; i++) {
        removeTenant(tenant[i]);
    }

    $('#pro-inspection-date').val('');
    $('#pro-inspection-time').val('');
    $('#pro-frequency').val('');

    $('#pro-alarm').val('');
    $('#pro-key-location').val('');

    if(window.toggleInspectionSchedule == 1) {
        $('#as-schedule-inspection-container').slideUp(300);
        $("#pro-toggle-inspection-schedule-icon").addClass('fa-chevron-circle-up').removeClass('fa-chevron-circle-down');
        window.toggleInspectionSchedule = 0;
    }

    $('#mm-add-property-container .has-error').removeClass('has-error');
    $('#mm-add-property-container .error-msg').remove();
}
//endregion