<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 17/11/17 11:45.
 */
?>

@extends('frontend.master-white')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p style="text-align: center;">
                    <img src="{{URL::To('/')}}/assets/frontend/images/error/group-3@2x.png" style="width: 40%; height: auto; margin-top: 50px; margin-bottom: 20px;" />
                </p>
            </div>
        </div>
    </div>

@endsection