<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 14/05/17 16:41.
 */
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    {{--Meta Data--}}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="title" content="{{$title}}" >
    <meta name="description" content="{{$metaDescription}}" >
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/backend/images/logo-ico.png">
    <title>Plus Media - Page Name</title>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    {{HTML::style('assets/frontend/css/bootstrap.css')}}
    {{HTML::style('assets/frontend/css/style-home.css')}}
    {{HTML::style('assets/frontend/css/dark.css')}}
    {{HTML::style('assets/frontend/css/animate.css')}}
    {{HTML::style('assets/frontend/css/responsive.css')}}
    {{HTML::style('assets/frontend/css/home.css?v=0.04')}}

    {{HTML::style('assets/backend/libs/liveicons-evo/css/LivIconsEvo.css')}}

</head>
<body>
<section class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->
        <header id="header" class="transparent-header full-header" data-sticky-class="not-dark" style="opacity: 0;">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger" class="livicon-evo sub-icon" data-options="name: list.svg; size: 35px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#000; top:-5px;" ></div>

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu" class="dark">

                        <ul>
                            <li class="current"><a href="{{URL::To('/')}}"><div>Home</div></a></li>
                            <li><a href="{{URL::To('/')}}/about"><div>About</div></a></li>
                            <li><a href="{{URL::To('/')}}/services"><div>Services</div></a></li>
                            <li><a href="{{URL::To('/')}}/contact"><div>Contact</div></a></li>
                        </ul>

                    </nav><!-- #primary-menu end -->

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <p>
                            <a href="#" class="standard-logo" data-dark-logo="images/logo-dark@2x.png"><img alt="PlusMedia Logo" src="{{URL::To('/')}}/assets/frontend/images/logo@2x.png"></a>
                            <a href="#" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img alt="PlusMedia Logo" src="{{URL::To('/')}}/assets/frontend/images/logo@2x.png"></a>
                        </p>

                    </div><!-- #logo end -->

                    <nav id="right-menu" class="dark">
                        <ul>
                            <li><a href="http://login.plusmedia.co.nz/index/login/?c=index&a=index"><div>Client Login</div></a></li>
                            <li><a href="tel:0800758763342"><div class="menu-border">0800 7587 63342</div></a></li>
                        </ul>
                    </nav>

                </div>

            </div>

        </header><!-- #header end -->
        <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
            <div class="slider-parallax-inner">
                <p class="slider-img-container">
                    <img id="slider-banner" alt="PlusMedia website banner" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/banner.gif" style="opacity: 0;" />
                </p>
                <p class="main-button">
                    <a href="{{URL::To('/')}}#free-consultation" >
                    <button id="btn-free-consultation" class="btn free-consultation shine-line" style="opacity: 0; background-image: url({{URL::To('/')}}/assets/frontend/images/icons/shine-reflector.png);">
                        GET A FREE CONSULTATION
                    </button>
                    </a>
                    <h1 style="visibility: hidden; margin-top: -70px;">{{$h1}}</h1>
                </p>
                <div id="top-partners" style="opacity: 0;">
                    <div class="row partners-container hidden-xs hidden-sm">
                        <div class="col-md-10 col-md-offset-1 partners-top-container">
                            <div class="col-md-2">
                                <img alt="PlusMedia Partner" id="img-top-double-click" src="{{URL::To('/')}}/assets/frontend/images/home/partners/doubleclick@2x.png" />
                            </div>
                            <div class="col-md-2">
                                <img alt="PlusMedia Partner" id="img-top-seo" src="{{URL::To('/')}}/assets/frontend/images/home/partners/seo-profiler@2x.png" />
                            </div>
                            <div class="col-md-2">
                                <img alt="PlusMedia Partner" id="img-top-adwards" src="{{URL::To('/')}}/assets/frontend/images/home/partners/adwords@2x.png" />
                            </div>
                            <div class="col-md-2">
                                <img alt="PlusMedia Partner" id="img-top-analytics" src="{{URL::To('/')}}/assets/frontend/images/home/partners/analytics@2x.png" />
                            </div>
                            <div class="col-md-2">
                                <img alt="PlusMedia Partner" id="img-top-salesforce" src="{{URL::To('/')}}/assets/frontend/images/home/partners/salesforce@2x.png" />
                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-12 partners-bottom-container">
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-trademe" src="{{URL::To('/')}}/assets/frontend/images/home/partners/trademe@2x.png" />
                                </div>
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-media" src="{{URL::To('/')}}/assets/frontend/images/home/partners/mediaworks-logo@2x.png" />
                                </div>
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-nzme" src="{{URL::To('/')}}/assets/frontend/images/home/partners/nzme@2x.png" />
                                </div>
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-realestate" src="{{URL::To('/')}}/assets/frontend/images/home/partners/realestate@2x.png" />
                                </div>
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-kepx" src="{{URL::To('/')}}/assets/frontend/images/home/partners/kpex@2x.png" />
                                </div>
                                <div class="col-md-2">
                                    <img alt="PlusMedia Partner" id="img-top-report" src="{{URL::To('/')}}/assets/frontend/images/home/partners/reportgarden@2x.png" />
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row hidden-md hidden-lg mobile-partners-top-container">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
                            <div class="col-xs-6 col-sm-6">
                                <img alt="PlusMedia Partner" class="left" src="{{URL::To('/')}}/assets/frontend/images/home/partners/doubleclick@2x.png" />
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <img alt="PlusMedia Partner" class="right" src="{{URL::To('/')}}/assets/frontend/images/home/partners/seo-profiler@2x.png" />
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 mobile-partners-bottom-container">
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="left" src="{{URL::To('/')}}/assets/frontend/images/home/partners/adwords@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="middle" src="{{URL::To('/')}}/assets/frontend/images/home/partners/analytics@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="right" src="{{URL::To('/')}}/assets/frontend/images/home/partners/salesforce@2x.png" />
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 mobile-partners-bottom-container">
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="left" src="{{URL::To('/')}}/assets/frontend/images/home/partners/trademe@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="middle" src="{{URL::To('/')}}/assets/frontend/images/home/partners/mediaworks-logo@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="right" src="{{URL::To('/')}}/assets/frontend/images/home/partners/nzme@2x.png" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 mobile-partners-bottom-container">
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="left" src="{{URL::To('/')}}/assets/frontend/images/home/partners/realestate@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="middle" src="{{URL::To('/')}}/assets/frontend/images/home/partners/kpex@2x.png" />
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <img alt="PlusMedia Partner" class="right" src="{{URL::To('/')}}/assets/frontend/images/home/partners/reportgarden@2x.png" />
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </section>

        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="section parallax nobottommargin section-1" data-stellar-background-ratio="0.4">
                    <div class="container-fluids">
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="col-md-8 col-md-offset-4 desc">
                                    <p class="title">SEO</p>
                                    <p class="description">Get to the first page of Google's search results</p>
                                    <a href="{{URL::To('/')}}/service/seo"> <button class="btn" onmouseenter="playIcon('seo-learn-more');">LEARN MORE <div id="icon-seo-learn-more" class="livicon-evo sub-icon" data-options="name: angle-wide-right.svg; size: 30px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#d80020; top:-5px;" ></div></button></a>
                                </div>
                            </div>
                            <div id="seo-img-div" class="col-sm-5" style="position: relative;">
                                <img alt="PlusMedia SEO" class="hidden-xs hidden-sm" id="img-seo" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/seo@2x.png" />
                                <img alt="PlusMedia SEO" class="hidden-xs hidden-md hidden-lg" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/seo@2x.png" style="top: 0;" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="section nobottommargin section-2" style="background-image: url('{{ URL::To('') }}/assets/frontend/images/home/backgrounds/web-bg.png');">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <img alt="PlusMedia Web Development" id="img-web" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/web.png"/>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-8 desc">
                                <p class="title">WEB</p>
                                <p class="description">Stand out from the rest with a dynamic, custom made website.</p>
                                <a href="{{URL::To('/')}}/service/web"> <button class="btn" onmouseenter="playIcon('web-learn-more');">LEARN MORE <div id="icon-web-learn-more" class="livicon-evo sub-icon" data-options="name: angle-wide-right.svg; size: 30px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#d80020; top:-5px;" ></div></button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="background-color: #efefef;">
                    <div class="section nobottommargin section-3">
                        <div class="container">
                            <div class="col-sm-12">
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-4 desc">
                                        <p class="title">ADPRO | LOCAL | DISPLAY | GMAIL | ADCLEAR</p>
                                        <p class="description">Run and track different Ad Campaigns</p>
                                        <a id="img-ad-pro" href="{{URL::To('/')}}/service/ad-pro"> <button class="btn" onmouseenter="playIcon('ad-pro-learn-more');">LEARN MORE <div id="icon-ad-pro-learn-more" class="livicon-evo sub-icon" data-options="name: angle-wide-right.svg; size: 30px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#fff; top:-5px;" ></div></button></a>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <img alt="PlusMedia AdPro, Display Add, AdClear" class="hidden-sm hidden-md hidden-lg" style="width: 410px !important; max-width: none;" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/ad-group.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <img id="img-ad-screen" class="rellax hidden-xs"  data-rellax-speed="3" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/ad-group.png" />
                    <div class="section nobottommargin section-4">
                        <div class="container">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="ad-campaign-canvas">
                                        <img alt="PlusMedia Social" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/social-img.png" />
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-12 desc">
                                        <p class="title">SOCIAL MEDIA</p>
                                        <p class="description">Raise brand awareness through social media and connect with customers on a whole new level</p>
                                        <a href="{{URL::To('/')}}/service/social-media"> <button class="btn" onmouseenter="playIcon('social-media-learn-more');">LEARN MORE <div id="icon-social-media-learn-more" class="livicon-evo sub-icon" data-options="name: angle-wide-right.svg; size: 30px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#d80020; top:-5px;" ></div></button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img alt="PlusMedia Telecommunications" id="img-tele" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/telecommunications.png" />
                    <div class="section nobottommargin section-5">
                        <div class="container">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-md-8 col-md-offset-4 desc">
                                        <p class="title">TELECOMMUNICATIONS</p>
                                        <p class="description">Never miss out on a lead again</p>
                                        <a href="{{URL::To('/')}}/service/telecommunications"> <button class="btn" onmouseenter="playIcon('telecommunication-learn-more');">LEARN MORE <div id="icon-telecommunication-learn-more" class="livicon-evo sub-icon" data-options="name: angle-wide-right.svg; size: 30px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#d80020; top:-5px;" ></div></button></a>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section nobottommargin section-6" style="background-image: url('{{ URL::To('') }}/assets/frontend/images/home/backgrounds/asset-2-2-x-copy-6@2x.png');">
                    <div class="col-sm-12">
                        <h4>200+ Companies have driven value with us</h4>
                        <div class="col-sm-12">
                            <p class="logo-container" style="text-align: center">
                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/partners/formsteel@2x.png" />
                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/partners/flybuys@2x.png" />
                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/partners/primesite-logo@2x.png" />
                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/partners/radiance-logo@2x.png" />
                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/partners/carpetlink-logo@2x.png" />
                            </p>
                        </div>
                        <div class="col-md-12">
                            <h5>-AND MANY MORE</h5>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8 col-md-offset-2 containers">
                                <div class="col-md-6 left-container">
                                    <div class="col-sm-12 left-box">
                                        <div class="left-top col-xs-12">
                                            <p>
                                                I am impressed by the service I have received over the past year from PlusMedia. They are always keen to help when required, and excellent at keeping us at the number 1 position. Well done PlusMeida Team!
                                            </p>
                                        </div>
                                        <div class="left-bottom col-xs-12">
                                            <p>
                                                <img alt="PlusMedia Clients" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/sheds.png" /><br/>
                                                DIRECTOR. SHEDS4U
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 right-container">
                                    <div class="col-sm-12 right-box">
                                        <div class="right-top col-xs-12">
                                            <p>
                                                I have been a PlusMedia customer for nearly one year now and the customer service has really impressed me. Corey, my account manager is an absolute star, is a fantastic communicator and really approachable. I would highly recommend Corey and PlusMedia team.
                                            </p>
                                        </div>
                                        <div class="right-bottom col-xs-12">

                                            <p>
                                                <img alt="PlusMedia Clients" class="hidden-xs" src="{{URL::To('/')}}/assets/frontend/images/home/paralax/super-city.jpg" /><br/>
                                                DIRECTOR, SUPERCITY REMOVALS
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{--<div class="section nobottommargin section-7 parallax" style="background-image: url('{{ URL::To('') }}/assets/frontend/images/home/backgrounds/success-stories-bg.png'); padding: 100px 0;" data-stellar-background-ratio="0.4">--}}
                    {{--<div class="col-sm-10 col-sm-offset-1">--}}
                        {{--<div class="col-sm-3">--}}
                            {{--<h4>Read some of our success stories</h4>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-9">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="col-sm-4">--}}

                                {{--</div>--}}
                                {{--<div class="col-sm-4">--}}

                                {{--</div>--}}
                                {{--<div class="col-sm-4">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="section nobottommargin section-8" id="free-consultation" style="background-image: url('{{ URL::To('') }}/assets/frontend/images/home/backgrounds/contact-form-bg.png');">
                    <div class="container">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="col-md-6">
                                <h4>Not sure how to start?</h4>
                                <p>
                                    No worries. We're here to help you in your growth journey.<br/>
                                    Fill in the form below and we will give you a call back for a free consultation.
                                </p>
                                <div class="enquiry-form">
                                    <div class="col-xs-12 col-sm-4" style="padding: 0 5px 0 0;">
                                        <label for="txt-name" style="margin-bottom: 0px; color: #fff;">Name</label>
                                        <input type="text" id="txt-name" onblur="requiredValidator(this, 'Name is required')" placeholder="Name" />
                                    </div>
                                    <div class="col-xs-12 col-sm-4" style="padding: 0 5px 0 0;">
                                        <label for="txt-email" style="margin-bottom: 0px; color: #fff;">Email</label>
                                        <input type="text" id="txt-email" placeholder="Email" onblur="requiredValidator(this, 'Email is required'), emailValidator(this, 'Invalid email!')" />
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <label for="txt-phone" style="margin-bottom: 0px; color: #fff;">Phone</label>
                                        <input class="phone-number" type="text" id="txt-phone" placeholder="Phone No." />
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <label for="txt-msg" style="margin-bottom: 0px; color: #fff;">Message</label>
                                        <textarea placeholder="Message" id="txt-msg"></textarea>
                                    </div>
                                    <p>
                                        <button id="btn-submit" onclick="submitForm();">GET A FREE CONSULTATION</button><br/>

                                    </p>
                                </div>
                                <span class="success-message animated" style="opacity: 0;">
                                    <i class="fa fa-check"></i> We have received your inquiry. Stay tuned, we’ll get back to you very soon.
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section><!-- #content end -->
        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">
                    <p>
                        0800 7587 63342<br/>
                        info@plusmedia.co.nz
                    </p>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <ul>
                    <li><a href="{{URL::To('/')}}"> HOME </a></li>
                    <li><a href="{{URL::To('/')}}/about"> ABOUT </a></li>
                    <li><a href="{{URL::To('/')}}/services"> SERVICES </a></li>
                    <li><a href="{{URL::To('/')}}/contact"> CONTACT </a></li>
                    <li>CLIENT LOGIN</li>
                </ul>

            </div>
            <div class="col-md-12 bottom-white-footer" style="padding: 20px;">
                <div class="col-md-4 logo">
                    <img alt="PlusMedia Logo" class="hidden-xs" src="{{URL::To('/')}}/assets/frontend/images/logo@2x.png"/>
                </div>
                <div class="col-md-4">
                    <p>
                        Follow us on :
                        <a href="https://www.linkedin.com/company/plusmedianz/"><img alt="PlusMedia LinkedIn" src="{{URL::To('/')}}/assets/frontend/images/icons/group-17.png" /></a>
                        <a href="https://www.facebook.com/PlusMediaNZ/"><img alt="PlusMedia Facebook" src="{{URL::To('/')}}/assets/frontend/images/icons/group-16.png" /></a>
                    </p>
                </div>
                <div class="col-md-4">
                    <!-- Go To Top
                    ============================================= -->
                </div>
            </div>

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="livicon-evo sub-icon" data-options="name: chevron-top.svg; size: 50px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#fff; top:-5px;" ></div>
</section>
    <!-- External JavaScripts
    ============================================= -->
{{HTML::script('assets/backend/js/custom/public/constant.js')}}

{{HTML::script('assets/frontend/js/jquery.js')}}
{{HTML::script('assets/frontend/js/plugins.js')}}
{{HTML::script('assets/backend/js/custom/public/validator.js')}}

{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/snap.svg-min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/TweenMax.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/DrawSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/MorphSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/verge.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.defaults.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.min.js')}}
{{HTML::script('assets/backend/js/custom/public/public.functions.js')}}
<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/frontend/js/functions.js')}}
{{HTML::script('assets/frontend/libs/parallax/jquery-parallax.js')}}
{{HTML::script('assets/frontend/js/animate.js')}}
{{HTML::script('assets/frontend/js/app.js')}}

{{HTML::script('assets/frontend/js/form.js')}}

<!-- javascript -->
{{HTML::script('assets/frontend/libs/rellax/rellax.js')}}
<script>
    var rellax = new Rellax('.rellax', {
        // center: true
        callback: function(position) {
            // callback every position change
            console.log(position);
        }
    });
</script>

</body>
</html>