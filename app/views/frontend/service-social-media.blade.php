<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:22.
 */
?>


@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <ul class="sidenav">
                                <li><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="{{URL::To('/')}}/service/web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                <li><a id="snav-tab-4" href="{{URL::To('/')}}/service/ad-pro"><i class="icon-gift"></i>Adpro<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" href="{{URL::To('/')}}/service/display-ads"><i class="icon-adjust"></i>Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" href="{{URL::To('/')}}/service/ad-clear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-7" href="{{URL::To('/')}}/service/social-media" style="color: #e4000e;"><i class="icon-adjust"></i>Social Media<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="{{URL::To('/')}}/service/telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>Social Media</h3>
                                    <p>
                                        Looking to take the leap and introduce social media to your marketing strategy? Leave it to PlusMedia. When it comes to social media, our specialists are always seeking out the best strategies, so we are able to deliver an elite social media campaign that produces tangible results. With our help, you will get your business in front of the right people, raise your brand awareness, and consequently see an increase in sales.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/social/social.png" style="max-width: 160%" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row sub-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                <div class="col-md-3">
                                    <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: us-dollar.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Gain valuable customer insights</h3>
                                    <p class="description">
                                        Social media is continuously generating data about your customers, allowing you the opportunity to gain valuable insights, such as who they are and what they like. This data can be used to make more informed business decisions, that will lead to higher conversion rates.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                <div class="col-md-3">
                                    <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: morph-slider-top-bottom.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Increase brand awareness and loyalty</h3>
                                    <p class="description">
                                        Social Media is a great way to increase your company’s visibility. Giving your business a social media profile and regularly posting content will help expose your company to a multitude of people and legitimize your brand.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                <div class="col-md-3">
                                    <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: users.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Reach the right people through targeted ads</h3>
                                    <p class="description">
                                        Facebook ads are an extremely cost-effective way to publish content and promote your business. They also offer a variety of targeting options, allowing you to segment your audiences based on things like location, demographics, interests and behaviours. This gives you great control over who sees your content.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                <div class="col-md-3">
                                    <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: sky-dish.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Increase website traffic and search ranking</h3>
                                    <p class="description">
                                        Social media does wonders for directing traffic to your website. Every business profile on social media is a path way to your website, and the more high-quality content you post to your profile, the more traffic you will generate. Also, the more shares this content receives, the higher your website’s search ranking will be.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#main-img').addClass('animated fadeInRight');
            }, 200);
        });

        function showPopUpWindow(name) {
            $('#popup-'+name).slideDown(250);
            $('.popup-overlay').fadeIn(300);
            setTimeout(function () {
                window.popupOpen = 1;
            }, 300);

        }

        function closePopUpWindow() {
            $('.popup').slideUp(250);
            $('.popup-overlay').fadeOut(300, function () {
                $('.popup-overlay').hide();
                window.popupOpen = 0;
            })
        }
        $('body').click(function (event)
        {
            if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                closePopUpWindow();
            }
        });

        function playIcon(name) {
            var tracking = jQuery('#icon-'+name);
            tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
        }

        function getPopup() {
            var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                '                       <div id="confirm-content" class="content"> ' +
                '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                '                           <p>test the section</p> ' +
                '                           <p style="text-align: center;">testing</p> ' +
                '                       </div> ' +
                '                   </div>';

            $('.sub-section').parent().append(htmlAppend);
            setTimeout(function () {
                $('#confirm').toggleClass('active');
            }, 40);
        }

    </script>
@endsection

