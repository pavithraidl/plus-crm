 <?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/10/17 09:37.
 */
?>

 @extends('frontend.master-dark')

 @section('styles')
     {{HTML::style('assets/frontend/css/service.css')}}
 @endsection

 @section('content')
     <input type="hidden" id="domain" value="{{URL::To('/')}}" />
     <div class="container clearfix">
         <div id="top-container-row" class="row">
             <div class="col-md-12">
                 <div id="side-navigation" class="custom-js">
                     <div class="col-md-3 nobottommargin">
                         <div class="snav-container">
                             <ul class="sidenav">
                                 <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo" style="color: #e4000e;"><i class="icon-screen"></i>SEO<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                 <li><a id="snav-tab-2" href="{{URL::To('/')}}/service/web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                 {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                 <li><a id="snav-tab-4" href="{{URL::To('/')}}/service/ad-pro"><i class="icon-gift"></i>Adpro<i class="icon-chevron-right"></i></a></li>
                                 <li><a id="snav-tab-5" href="{{URL::To('/')}}/service/display-ads"><i class="icon-adjust"></i>Display Ads<i class="icon-chevron-right"></i></a></li>
                                 <li><a id="snav-tab-6" href="{{URL::To('/')}}/service/ad-clear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                 <li><a id="snav-tab-7" href="{{URL::To('/')}}/service/social-media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                 <li><a id="snav-tab-8" href="{{URL::To('/')}}/service/telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                             </ul>
                         </div>
                     </div>

                     <div class="col-md-9 nobottommargin section-content">
                         {{--Web--}}
                         <div id="Web">
                             <div class="col-md-12">
                                 <div class="col-md-7">
                                     <h3>SEO<span>-Search Engine Optimisation</span></h3>
                                     <p>
                                         You will notice that the results on Google’s search engine are shown either organically or as ads. By appearing in the top results organically, it means that your website is trusted, relevant and has products and services matching the keywords searched.</p>
                                     <p style="font-style: italic;">
                                         <strong>
                                             Here at PlusMedia we are committed to getting your website to the top of the organic search results and making sure it stays there.
                                         </strong>
                                     </p>
                                     <p>
                                         Our team of specialists pride themselves on staying up to date on all the latest developments and trends in Search Engine Optimisation. We use the most effective methods to optimise your website so that it is the first thing people see when they search for keywords relevant to your business.
                                     </p>
                                     <a href="{{URL::To('/')}}#free-consultation" >
                                         <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                     </a>
                                 </div>
                                 <div class="col-md-5">
                                     <img src="{{URL::To('/')}}/assets/frontend/images/service/seo/seo-1.png" style="max-width: 160%" />
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="row sub-section">
         <div class="container">
             <div class="col-md-12" style="margin-top: 80px;">
                 <div class="row" style="margin-bottom: 35px;">
                     <div class="col-md-12" style="display: flex;">
                         <div class="col-md-6 col-xs-12" style="flex: 1; padding: 0 10px;">
                             <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                 <div class="col-md-3">
                                     <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: zoom-in.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                 </div>
                                 <div class="col-md-9" style="margin-top: 25px;">
                                     <h3>Identify the keywords that will convert</h3>
                                     <p class="description">
                                         First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.
                                     </p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-xs-12" style="flex: 1; padding: 0 10px;">
                             <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                 <div class="col-md-3">
                                     <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: pie-chart.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                 </div>
                                 <div class="col-md-9" style="margin-top: 25px;">
                                     <h3>Analyse your competitors strategies</h3>
                                     <p class="description">
                                         First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.
                                     </p>
                                 </div>
                             </div>

                         </div>
                     </div>
                 </div>
                 <div class="row" style="margin-bottom: 35px;">
                     <div class="col-md-12" style="display: flex;">
                         <div class="col-md-6 col-xs-12" style="flex: 1; padding: 0 10px;">
                             <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                 <div class="col-md-3">
                                     <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: link.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                 </div>
                                 <div class="col-md-9" style="margin-top: 25px;">
                                     <h3>Get quality websites to link back to you</h3>
                                     <p class="description">
                                         It is necessary to have high-quality relevant links so Google can recognise and rank your website highly. Our team continuously uses the highest of industry techniques which help your website increase in ranking.
                                     </p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-xs-12" style="flex: 1; padding: 0 10px;">
                             <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                 <div class="col-md-3">
                                     <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: settings.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                 </div>
                                 <div class="col-md-9" style="margin-top: 25px;">
                                     <h3>Have your website frequently audited</h3>
                                     <p class="description">
                                         Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines
                                     </p>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="row" style="margin-bottom: 35px;">
                     <div class="col-md-12" style="display: flex;">
                         <div class="col-md-6 col-xs-12" style="flex: 1; padding: 0 10px;">
                             <div class="col-md-12 sub-box" onmouseenter="playIcon('cloud');">
                                 <div class="col-md-3">
                                     <div id="icon-cloud" class="livicon-evo sub-icon" data-options="name: briefcase.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                 </div>
                                 <div class="col-md-9" style="margin-top: 25px;">
                                     <h3>Receive regular randing/status reports</h3>
                                     <p class="description">
                                         Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines
                                     </p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6">

                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     {{-- Read More Contents --}}
     <div class="popup-overlay" style="display: none;">
     </div>
 @endsection

 @section('scripts')
     <script>
         $(document).ready(function () {
             setTimeout(function () {
                 $('#main-img').addClass('animated fadeInRight');
             }, 200);
         });

         function showPopUpWindow(name) {
             $('#popup-'+name).slideDown(250);
             $('.popup-overlay').fadeIn(300);
             setTimeout(function () {
                 window.popupOpen = 1;
             }, 300);

         }

         function closePopUpWindow() {
             $('.popup').slideUp(250);
             $('.popup-overlay').fadeOut(300, function () {
                 $('.popup-overlay').hide();
                 window.popupOpen = 0;
             })
         }
         $('body').click(function (event)
         {
             if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                 closePopUpWindow();
             }
         });

         function playIcon(name) {
             var tracking = jQuery('#icon-'+name);
             tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
         }

         function getPopup() {
             var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                 '                       <div id="confirm-content" class="content"> ' +
                 '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                 '                           <p>test the section</p> ' +
                 '                           <p style="text-align: center;">testing</p> ' +
                 '                       </div> ' +
                 '                   </div>';

             $('.sub-section').parent().append(htmlAppend);
             setTimeout(function () {
                 $('#confirm').toggleClass('active');
             }, 40);
         }

     </script>
 @endsection
