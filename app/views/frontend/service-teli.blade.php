<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/11/17 15:09.
 */
?>
@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="tabs custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <h4>SERVICES</h4>
                            <ul class="sidenav">
                                <li class="ui-tabs-active"><a id="snav-tab-1" href="#SEO"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="#Web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-4" class="sub" href="#Paid-Ads/Adpro"><i class="icon-gift"></i>- Adpro<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" class="sub" href="#Paid-Ads/Display-Ads"><i class="icon-adjust"></i>- Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" class="sub" href="#Paid-Ads/Adclear"><i class="icon-adjust"></i>- Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="#Social-Media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="#Telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--SEO--}}
                        <div id="SEO">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media SEO</h3>
                                    <p>
                                        You will notice that the results on Google’s search engine are shown either organically or as ads. By appearing in the top results organically, it means that your website is trusted, relevant and has products and services matching the keywords searched.</p>
                                    <p>
                                        <strong>
                                            Here at PlusMedia we are committed to getting your website to the top of the organic search results and making sure it stays there.
                                        </strong>
                                    </p>
                                    <p>
                                        Our team of specialists pride themselves on staying up to date on all the latest developments and trends in Search Engine Optimisation. We use the most effective methods to optimise your website so that it is the first thing people see when they search for keywords relevant to your business.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/seo/group-15@2x.png" />
                                </div>
                            </div>
                        </div>
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Web</h3>
                                    <p>
                                        Engage visitors and build trust in your brand with a well-crafted and professional looking website from PlusMedia.
                                    </p>
                                    <p>
                                        In an industry where the majority of companies are taking the easy way out by using templates, we build every website from scratch to ensure your personal vision will be upheld. We work alongside you to develop a website that both parties will be proud of.
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/web/web.png" />
                                </div>
                            </div>
                        </div>
                        {{--Paid Ads--}}
                        <div id="Paid-Ads">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Adpro</h3>
                                    <p>
                                        Advertising with Google AdWords is a quick and efficient way of making your website visible on search engine results to your target audience. With Google AdWords, advertisers pay a fee each time one of their ads is clicked, essentially purchasing visits to their site. You only pay for results, making it one of the most cost effective digital marketing strategies out there.
                                    </p>
                                    <p>
                                        PlusMedia is a certified Google Partner, so you know you are working with experts in the industry. Our search engine advertising techniques are designed to maximise your conversion rate at the lowest possible price. With a campaign on Google reaching your target audience, you will see a return on your advertising investment sooner than you expected.<br/>
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/adpro.png" />
                                </div>
                            </div>
                        </div>
                        {{--Adpro--}}
                        <div id="Paid-Ads/Adpro">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Adpro</h3>
                                    <p>
                                        Advertising with Google AdWords is a quick and efficient way of making your website visible on search engine results to your target audience. With Google AdWords, advertisers pay a fee each time one of their ads is clicked, essentially purchasing visits to their site. You only pay for results, making it one of the most cost effective digital marketing strategies out there.
                                    </p>
                                    <p>
                                        PlusMedia is a certified Google Partner, so you know you are working with experts in the industry. Our search engine advertising techniques are designed to maximise your conversion rate at the lowest possible price. With a campaign on Google reaching your target audience, you will see a return on your advertising investment sooner than you expected.<br/>
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/adpro.png" />
                                </div>
                            </div>
                        </div>
                        {{--Display Ads--}}
                        <div id="Paid-Ads/Display-Ads">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Display Ads</h3>
                                    <p>
                                        With display ads, you can reach potential customers as they are checking their Gmail, watching Youtube videos, or simply browsing the net. A display ad is a form of paid advertising that typically comes in various formats that users can engage with, such as text, image, video and rich media. Users can click on the ad to then be taken to the corresponding landing page where they can potentially make a purchase or an enquiry.
                                    </p>
                                    <p>
                                        If you’re interested in engaging users, raising brand awareness and boosting your sales, then the Google Display Network is definitely the way to go.
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/display-ads.png" />
                                </div>
                            </div>
                        </div>
                        {{--Adclear--}}
                        <div id="Paid-Ads/Adclear">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Adclear</h3>
                                    <p>
                                        With our +Media AdClear package, you can track all leads that come through your SEO, PPC or any other advertising campaigns you are running. This will allow you to analyse the effectiveness of your advertising campaigns, so you can make the right adjustments and allocate your resources more effectively.
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/adclear.png" />
                                </div>
                            </div>
                        </div>
                        {{--Social Media--}}
                        <div id="Social-Media">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Social</h3>
                                    <p>
                                        Looking to take the leap and introduce social media to your marketing strategy? Leave it to PlusMedia. When it comes to social media, our specialists are always seeking out the best strategies, so we are able to deliver an elite social media campaign that produces tangible results. With our help, you will get your business in front of the right people, raise your brand awareness, and consequently see an increase in sales.
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/social/social.png" />
                                </div>
                            </div>
                        </div>
                        {{--Telecommunications--}}
                        <div id="Telecommunications">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3>+Media Telecommunications</h3>
                                    <p>
                                        Make the most of the latest technology in the telecommunications industry by coming on board with our +Media Telecommunications package. We offer toll-free numbers and geo numbers, coming with a host of sophisticated features such as call tracking, call distribution and call forwarding. If you want to improve the way you engage and service customers, then our Telecommunications package is the way to go.
                                    </p>
                                    <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/telecommunications/telecommunications.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row bottom-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: phone.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Call Tracking</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: bell.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Call Conversion</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: satellite.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Call Routing</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: bar-chart.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Call Analytics</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: cloud-upload.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Cloud Basedd Call Center</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: camcoder.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Call Recording</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: phone-alt.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Toll-free Numbers</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12" style="box-shadow: 7px 8px 20px -5px rgba(0,0,0,0.39); padding: 23px 27px;border: 1px solid #f3f3f3;">
                                <div class="col-md-3">
                                    <div class="livicon-evo" data-options="name: dropper.svg; size: 80px; style: original; tryToSharpen: true" style="margin: 70% auto; border-radius: 50%; border: 2px solid rgb(231, 231, 231); visibility: visible; width: 100px; padding: 14px;"></div>
                                </div>
                                <div class="col-md-9">
                                    <h3 style="text-align: center; color: #ec1d24 !important; font-size: 16px; font-weight: 800;">Geo Numbers</h3>
                                    <p style="text-align: justify; margin-top: -25px; font-size: 14px;">
                                        Optimise your marketing performance with our Call Tracking system. Identify the advertising channel, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your advertising spend.
                                    </p>
                                    <p style="text-align: center; margin-top: -20px; font-size: 11px;">
                                        <a href="#" style="color: #4e8fca;">LEARN MORE</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
