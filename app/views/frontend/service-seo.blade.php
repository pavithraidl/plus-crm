<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:21.
 */
?>

@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="tabs custom-js">
                    <div class="col-md-3 nobottommargin" style="margin-left: -60px;">
                        <div class="snav-container">
                            <h4>SERVICES</h4>
                            <ul class="sidenav">
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-1" href="#SEO"><i class="icon-screen"></i>SEO<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="#Web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-4" class="sub" href="#Paid-Ads/Adpro"><i class="icon-gift"></i>- Adpro<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" class="sub" href="#Paid-Ads/Display-Ads"><i class="icon-adjust"></i>- Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" class="sub" href="#Paid-Ads/Adclear"><i class="icon-adjust"></i>- Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="#Social-Media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="#Telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--SEO--}}
                        <div id="SEO">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>SEO<span>-Search Engine Optimisation</span></h3>
                                    <p>
                                        You will notice that the results on Google’s search engine are shown either organically or as ads. By appearing in the top results organically, it means that your website is trusted, relevant and has products and services matching the keywords searched.</p>
                                    <p style="font-style: italic;">
                                        <strong>
                                            Here at PlusMedia we are committed to getting your website to the top of the organic search results and making sure it stays there.
                                        </strong>
                                    </p>
                                    <p>
                                        Our team of specialists pride themselves on staying up to date on all the latest developments and trends in Search Engine Optimisation. We use the most effective methods to optimise your website so that it is the first thing people see when they search for keywords relevant to your business.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/seo/seo-1.png" style="width: 480px !important; margin-top: 32px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row bottom-section">
        <div class="container">
            <div class="col-md-12">
                <div class="process">
                    <div class="process-row nav nav-tabs">
                        <div id="step-1" class="process-step active">
                            <button id="btn-step-1" type="button" class="btn btn-steps" data-toggle="tab" href="#menu1"><i id="ico-step-1" class="fa fa-key" aria-hidden="true"></i> </button>
                            <p><small id="title-step-1">Identify the keywords<br/> that will convert</small></p>
                        </div>
                        <div id="step-2" class="process-step">
                            <button id="btn-step-2" type="button" class="btn btn-steps" data-toggle="tab" href="#menu2"><i id="ico-step-2" class="fa fa-line-chart" aria-hidden="true"></i> </button>
                            <p><small id="title-step-2">Analyse your<br/> competitors strategies</small></p>
                        </div>
                        <div id="step-3" class="process-step">
                            <button id="btn-step-3" type="button" class="btn btn-steps" data-toggle="tab" href="#menu3"><i id="ico-step-3" class="fa fa-link" aria-hidden="true"></i> </button>
                            <p><small id="title-step-3">Get quality websites to<br/> link back to you</small></p>
                        </div>
                        <div id="step-4" class="process-step">
                            <button id="btn-step-4" type="button" class="btn btn-steps" data-toggle="tab" href="#menu4"><i id="ico-step-4" class="fa fa-clock-o" aria-hidden="true"></i> </button>
                            <p><small id="title-step-4">Have your website <br/> frequently audited</small></p>
                        </div>
                        <div id="step-5" class="process-step">
                            <button id="btn-step-5" type="button" class="btn btn-steps" data-toggle="tab" href="#menu5"><i id="ico-step-5" class="fa fa-file-text-o" aria-hidden="true"></i> </button>
                            <p><small id="title-step-5">Receive regular<br/> randing/status reports</small></p>
                        </div>
                        <div id="step-6" class="process-step" style="display: none;">
                            <button id="btn-step-6" type="button" class="btn btn-steps" data-toggle="tab" href="#menu5"><i id="ico-step-6" class="fa fa-file-text-o" aria-hidden="true"></i> </button>
                            <p><small id="title-step-6">Receive regular<br/> randing/status reports</small></p>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade active in">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-1" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/identify-the-keywords.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-1">
                                    First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.
                                </p>
                            </div>
                        </div>

                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-2" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/analyse-competitor-s-strategies.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-2">
                                    First and foremost it is important to identify the keywords your target audience are using when searching for services or products you offer. Our experts understand the value of using the right keywords and are able to point out the keywords that will convert.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-3" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/get-websites-to-link.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-3">
                                    It is necessary to have high-quality relevant links so Google can recognise and rank your website highly. Our team continuously uses the highest of industry techniques which help your website increase in ranking.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-4" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/website-audit.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-4">
                                    Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="menu5" class="tab-pane fade">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-5" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/ranking-reports.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-5">
                                    Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="menu6" class="tab-pane fade">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-7">
                                <img id="img-step-6" src="{{URL::To('/')}}/assets/frontend/images/service/step-main/ranking-reports.png" />
                            </div>
                            <div class="col-md-5">
                                <p id="text-6">
                                    Frequent auditing needs to be done to ensure your website is able to continue ranking highly. Our experts are continuously learning and keeping up-to-date with Google’s ever changing regulations, thus ensuring they are complying with Google Webmaster guidelines
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

