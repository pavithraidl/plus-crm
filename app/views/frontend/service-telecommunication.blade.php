<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:23.
 */
?>

@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <ul class="sidenav">
                                <li><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="{{URL::To('/')}}/service/web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                <li><a id="snav-tab-4" href="#Paid-Ads/Adpro"><i class="icon-gift"></i>Adpro<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" href="#Paid-Ads/Display-Ads"><i class="icon-adjust"></i>Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" href="#Paid-Ads/Adclear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="#Social-Media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-8" href="#Telecommunications" style="color: #e4000e;"><i class="icon-adjust"></i>Telecommunications<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>+Media Telecommunication</h3>
                                    <p>
                                        Make the most of the latest technology in the telecommunications industry by coming on board with our +Media Telecommunications package. We offer toll-free numbers and geo numbers, coming with a host of sophisticated features such as call tracking, call distribution and call forwarding. If you want to improve the way you engage and service customers, then our Telecommunications package is the way to go.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img class="hidden-lg" src="{{URL::To('/')}}/assets/frontend/images/service/new-images/telecom.png" style="max-width: 160%" />
                                    <img id="main-img" class="hidden-xs hidden-sm hidden-md" src="{{URL::To('/')}}/assets/frontend/images/service/new-images/telecom.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row sub-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                <div class="col-md-3">
                                    <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: phone.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Call Tracking</h3>
                                    <p class="description">
                                        Optimise your marketing performance with our Call Tracking system. Identify the ad source, keywords and location your calls originate from to evaluate the effectiveness of your marketing campaigns and make informed decisions about your ad spend.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('tracking');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                <div class="col-md-3">
                                    <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: servers.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Call Conversion</h3>
                                    <p class="description">
                                        Track the customer journey from the beginning of their search to their final conversion.
                                        Pin point the ad source which generated the call, use our Call Recording feature to evaluate how the call was handled, and use our Call Outcome feature to discover the ROI of that call.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('conversion');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                <div class="col-md-3">
                                    <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: satellite.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Call Routing</h3>
                                    <p class="description">
                                        Get your calls directed to the right person at the right time, with our range of complex Call Routing solutions. By routing your inbound calls to the appropriate answer point, calls can be dealt with in a more flexible and customised manner, enhancing the customer experience.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('routing');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                <div class="col-md-3">
                                    <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: line-chart.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Call Analytics</h3>
                                    <p class="description">
                                        Make smart and informed decisions about how to improve your marketing with Call Analytics. We provide advanced data and reporting that will help guide effective marketing decisions.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('analytics');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('cloud');">
                                <div class="col-md-3">
                                    <div id="icon-cloud" class="livicon-evo sub-icon" data-options="name: cloud-upload.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Cloud Based Call Center</h3>
                                    <p class="description">
                                        Our Cloud Based Call Centre is an efficient way to handle calls outside of operating hours. ARTIC (AVANSER Real-Time Inbound Call-Centre) allows you to manage your inbound calls in a cost effective and simple manner.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('cloud');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('recording');">
                                <div class="col-md-3">
                                    <div id="icon-recording" class="livicon-evo sub-icon" data-options="name: morph-speaker.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Call Recording</h3>
                                    <p class="description">
                                        Monitor sales and service calls with our Call Recording solution. Use this feature to review how your team handles calls and make improvements. Having your calls recorded also allows you to protect yourself against misrepresentation.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('recording');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('toll');">
                                <div class="col-md-3">
                                    <div id="icon-toll" class="livicon-evo sub-icon" data-options="name: phone-alt.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Toll-free Numbers</h3>
                                    <p class="description">
                                        If you have a toll-free number your customers will not receive call charges, meaning there is less to stop them from calling you. PlusMedia provide affordable 0800 or 0508 numbers throughout NZ.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('toll');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('geo');">
                                <div class="col-md-3">
                                    <div id="icon-geo" class="livicon-evo sub-icon" data-options="name: location.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Geo Numbers</h3>
                                    <p class="description">
                                        It is common to want to market your business outside of your local area. Our Geo Numbers allow you to mask your number enabling you to appear locally, even if you are not local. This allows your business to reach into another marketplace without needing to be based in the area, and creates trust through a seemingly local presence.
                                    </p>
                                    <p class="more-button" onclick="showPopUpWindow('geo');">
                                        <span>LEARN MORE</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{-- Read More Contents --}}
    <div class="popup-overlay" style="display: none;">
    </div>

    {{--Call Tracking--}}
    <div id="popup-tracking" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-tracking" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Call Tracking</h3>
                    <p class="main-description">
                        With PlusMedia’s real-time call tracking solution, you are able to track every inbound call in order to evaluate the performance of your marketing campaigns.
                        Our call tracking solution is fully hosted and requires no hardware or software. Thanks to our online portal, it boasts multi-level access from any part of the globe.
                    </p>
                    <div class="row" style="padding: 30px;">
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>KEYWORD-LEVEL TRACKING</h4>
                                    <p class="content">
                                        Find out which keywords are driving your inbound calls with our keyword tracking. By knowing which keywords generate ROI and which do not, you can optimise your marketing spend.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>DYNAMIC NUMBERS</h4>
                                    <p class="content">
                                        Our Dynamic Tracking Numbers reveal the origin of each person that lands on your website and calls your business. A different phone number will be shown depending on the ad source that directed the visitor to your business. This is thanks to a tracking code we have embedded on your business’ website.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>MULTI-CHANNEL ONLINE & OFFLINE TRACKING</h4>
                                    <p class="content">
                                        Track your offline and online marketing to discover which channels are driving your inbound calls. Analyse the information to determine which channels are generating revenue, and which ones are not performing.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Call Conversion--}}
    <div id="popup-conversion" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-conversion" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Call Conversion</h3>
                    <p class="main-description">
                        Track the customer journey from the beginning of their search to their final conversion.
                        Pin point the ad source which generated the call, use our Call Recording feature to evaluate how the call was handled, and use our Call Outcome feature to discover the ROI of that call.
                    </p>
                    <div class="row" style="padding: 30px;">
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>Click-To-Connect</h4>
                                    <p class="content">
                                        This feature allows website visitors to instantly make a free phone call to your business. This a great way to encourage visitors to contact you and increase your chances of conversion.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>Missed Call Notification</h4>
                                    <p class="content">
                                        All of your missed calls and the details of these calls will be sent to you by email or SMS.  This allows you to immediately return any missed calls and convert the sales lead.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>Call Whisper</h4>
                                    <p class="content">
                                        Our Call Whisper is an automated announcement that will inform you which ad source generated the inbound call (i.e. “Lead from Google Adwords”).
                                        The Call Whisper is also ideal for businesses with multiple services/products, to identify the caller’s intended destination (i.e. “call for Credit Cards” vs “call for Insurance”).
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>Call Recording</h4>
                                    <p class="content">
                                        Call Recording allows you to access the recordings for every call, and gives you greater insight into how these calls have been handled. Thanks to this feature, you can get a better picture surrounding the effectiveness of your sales or customer service process.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>Call Evaluation</h4>
                                    <p class="content">
                                        We will listen to your calls for you to identify areas of improvement and help you to improve your sales strategy.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>Call Outcome</h4>
                                    <p class="content">
                                        This feature allows companies to use their phone system to take note of the outcome of the call, and the amount of the resulting sale. It will also tell you which marketing channel prompted that call, so you better understand the ROI for each channel.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>Inbound Lead Notification </h4>
                                    <p class="content">
                                        This tracking tool sends you an email notification each time your business takes a call from a potential customer. Additionally, it sends a recording of the conversation to your mobile so you can listen to them instantly.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    {{--Call Routing--}}
    <div id="popup-routing" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-routing" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Call Routing</h3>
                    <p class="main-description">
                        Direct your calls to the right person with our range of Call Routing services.
                    </p>
                    <div class="row" style="padding: 30px;">
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>SIMPLE ROUTING</h4>
                                    <p class="content">
                                        <strong>Transfer</strong><br/>
                                        Direct callers to the right department inside your company.<br/>
                                        <strong>Voicemail</strong><br/>
                                        Send your callers directly to voicemail, and get the recordings emailed to you right away.<br/>
                                        <strong>Prompt</strong><br/>
                                        Play a pre-recorded message to your callers.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>CODE INPUT</h4>
                                    <p class="content">
                                        <strong>Pin-Based Routing</strong><br/>
                                        Set a list of extensions and direct calls according to the extension that the caller entered. Additionally, you can pass all calls to a single answer point and just capture the extension entered by the caller to determine their intention for calling.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>CALL-CENTRE MANAGEMENT</h4>
                                    <p class="content">
                                        ARTIC: AVANSER Real-Time Inbound Call Centre
                                        Distribute inbound calls amongst team members, no matter where they are located.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>CALL DISTRIBUTION</h4>
                                    <p class="content">
                                        <strong>Call Rotator</strong><br/>
                                        Distribute calls to the appropriate team/department.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>TIME SPECIFIC ROUTING</h4>
                                    <p class="content">
                                        <strong>Time-Based Routing</strong><br/>
                                        Direct calls to the appropriate destination depending on the time. For example, if it’s a busy time of year for your business, you can route calls to more staff to share the load.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>CALLER SELECTION</h4>
                                    <p class="content">
                                        <strong>IVR</strong><br/>
                                        Interactive Voice Response presents callers with a phone menu system, and allows them to choose their own destination (e.g. press #1 for customer support).
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>LOCATION ROUTING</h4>
                                    <p class="content">
                                        <strong>Postcode Prompting</strong><br/>
                                        Directs callers based on their current postcode.<br/>
                                        <strong>Exchange Based Routing</strong><br/>
                                        Directs the caller to the appropriate answer point based on their telephone exchange.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Call Analytics--}}
    <div id="popup-analytics" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-analytics" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Call Analytics</h3>
                    <p class="main-description">
                        Derive insights from our accurate data, and make informed decisions about how to improve your marketing efforts.
                    </p>
                    <div class="row" style="padding: 30px;">
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>ADVANCED REPORTING</h4>
                                    <p class="content">
                                        Our analytics dashboard will provide precise data to help you make smarter sales and marketing decisions. We provide real-time feedback so you can make immediate adjustments.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6" style="flex: 1;">
                                <div class="col-md-12 content-container">
                                    <h4>MARKETING INSIGHTS</h4>
                                    <p class="content">
                                        The analytics dashboard shows you the ad source of every inbound call, giving you the opportunity to adjust your marketing strategy.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>DISCOVER MOST EFFECTIVE KEYWORDS</h4>
                                    <p class="content">
                                        See which keywords are delivering inbound calls, and reallocate your ad spend to campaigns that actually produce results.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>TRACK CALL PERFORMANCE & ROI</h4>
                                    <p class="content">
                                        Are your team handling sales calls effectively? Our dashboard allows you to measure the cost and ROI of each call, and the average duration of a successful call. With this data at your disposal, you will be able to measure and improve call performance.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 parent-content-container">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>CLOUD-BASED CALL CENTRE</h4>
                                    <p class="content">
                                        ARTIC (AVANSER Real-Time Inbound Call-Centre) is a cloud-based call centre solution that is powerful and easy to use. This call centre allows you to manage inbound calls, and improve the way these calls are handled.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>SOPHISTICATED TRACKING TOOLS</h4>
                                    <p class="content">
                                        ARTIC allows you to track the ad source of every call, which agent took the call, how the call was handled, and the final outcome of that call. These insights will help you enhance your marketing efforts and improve your team’s performance.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-12 content-container">
                                    <h4>COST EFFECTIVE </h4>
                                    <p class="content">
                                        With ARTIC everything is handled inside the cloud, meaning you are not forced to pay for expensive hardware or software costs – only the monthly subscription.<br/>
                                        <strong>ARTIC offers many more benefits:</strong>
                                    <ul style="margin-left: 10%">
                                        <li>Enhance the customer experience</li>
                                        <li>Account for your Agent times</li>
                                        <li>Increase your conversion rate</li>
                                        <li>Reduce call abandonment</li>
                                        <li>Scale for peak periods</li>
                                        <li>Gain marketing insights</li>
                                        <li>Control remote offices</li>
                                        <li>Access the system from anywhere</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Cloud based call center --}}
    <div id="popup-cloud" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-cloud" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    cloud
                </div>
            </div>
        </div>
    </div>

    {{--Call Recording--}}
    <div id="popup-recording" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-recording" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Call Recording</h3>
                    <p class="main-description">
                        Record any call with the click of a button, improve practises and increase conversion with our Call Recording solution.
                    <ul style="margin-left: 5%">
                        <li>Monitor the way your calls are handled and improve your team’s techniques.</li>
                        <li>Determine the quality of the calls generated from marketing campaigns.</li>
                        <li>Re-contact callers who have been poorly handled and convert them.</li>
                        <li>Defend yourself against misrepresentation.</li>
                    </ul><br/><br/>
                    <strong>Features</strong>
                    <ul style="margin-left: 5%;">
                        <li>No hardware or software to install</li>
                        <li>All call recordings stored for 3 months</li>
                        <li>Restricted user access</li>
                        <li>Each time a recording is accessed it is time stamped and logged</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>

    {{--Toll free numbers--}}
    <div id="popup-toll" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-toll" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>Toll Free Numbers</h3>
                    <p class="main-description">
                        If you want to increase your inbound calls then a toll-free number is the way to go. Our toll-free service allows you to advertise an 0800 number that your customers can call at no charge to them.<br/>
                        With a toll-free number, you can grow the national presence of your business. Toll free numbers make your business more accessible to customers beyond your local proximity, potentially increasing your customer base.<br/><br/>
                    <strong>How will an 0800 number benefit my business?</strong>
                    <ul style="margin-left: 5%;">
                        <li><strong>Toll free charges:</strong> Customers call you free of charge.</li>
                        <li><strong>Professional image:</strong> Enhance your company’s credibility.</li>
                        <li><strong>Be memorable:</strong> 0800 numbers are much easier to remember.</li>
                        <li><strong>Vanity number:</strong> Select an appealing vanity number to help with branding.</li>
                        <li><strong>Portability:</strong> Relocating your business? Take your number with you!</li>
                        <li><strong>Flexibility:</strong> Direct your number to a mobile or landline of your choice.</li>
                        <li><strong>Risk free:</strong> Enjoy month-to-month billing - no contract, no obligation. </li>
                        <li><strong>Reporting:</strong> Receive information such as location of calls, call duration, call status and much more. </li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>

    {{--Geo numbers--}}
    <div id="popup-geo" class="popup" style="display: none;">
        <div class="popup-container">
            <div id="icon-geo" class="livicon-evo sub-icon icon-styles" data-options="name: close.svg; size: 30px; style: original; tryToSharpen: true; drawOnViewport:true" onclick="closePopUpWindow();" ></div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h3>GEO Numbers</h3>
                    <p class="main-description">
                        Our Geo Numbers help your business to create a presence well beyond your immediate area. These numbers allow you to mask your number enabling you to appear locally, even if you are based elsewhere. This allows your business to reach into another marketplace without needing to be based in the area, and creates trust through a seemingly local presence.<br/><br/>
                        <strong>Key Benefits of a Geo Number</strong>
                    <ul style="margin-left: 5%;">
                        <li><strong>Establish a local presence in multiple cities</strong> <br/>Appear locally nationwide and build confidence in your business. </li>
                        <li><strong>Keep your existing phone line</strong> <br/>Answer all incoming calls on your existing landline or mobile phone.</li>
                        <li><strong>Never miss a sales opportunity</strong> <br/>Missed a call? We will send you a notification via email with the caller’s details. </li>
                        <li><strong>Enjoy flexible plans</strong> <br/>We offer complete flexibility with no minimum contract or cancellation fees. </li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#main-img').addClass('animated fadeInRight');
            }, 200);
        });

        function showPopUpWindow(name) {
            $('#popup-'+name).slideDown(250);
            $('.popup-overlay').fadeIn(300);
            setTimeout(function () {
                window.popupOpen = 1;
            }, 300);

        }

        function closePopUpWindow() {
            $('.popup').slideUp(250);
            $('.popup-overlay').fadeOut(300, function () {
                $('.popup-overlay').hide();
                window.popupOpen = 0;
            })
        }
        $('body').click(function (event)
        {
            if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                closePopUpWindow();
            }
        });

        function playIcon(name) {
            var tracking = jQuery('#icon-'+name);
            tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
        }

        function getPopup() {
            var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                '                       <div id="confirm-content" class="content"> ' +
                '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                '                           <p>test the section</p> ' +
                '                           <p style="text-align: center;">testing</p> ' +
                '                       </div> ' +
                '                   </div>';

            $('.sub-section').parent().append(htmlAppend);
            setTimeout(function () {
                $('#confirm').toggleClass('active');
            }, 40);
        }

    </script>
@endsection
