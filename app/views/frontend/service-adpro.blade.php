<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:21.
 */
?>


@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <ul class="sidenav">
                                <li><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="{{URL::To('/')}}/service/web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-4" href="{{URL::To('/')}}/service/ad-pro" style="color: #e4000e;"><i class="icon-gift"></i>Adpro<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" href="{{URL::To('/')}}/service/display-ads"><i class="icon-adjust"></i>Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" href="{{URL::To('/')}}/service/ad-clear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="{{URL::To('/')}}/service/social-media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="{{URL::To('/')}}/service/telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>Ad Pro</h3>
                                    <p>
                                        Advertising with Google AdWords is a quick and efficient way of making your website visible on search engine results to your target audience. With Google AdWords, advertisers pay a fee each time one of their ads is clicked, essentially purchasing visits to their site. You only pay for results, making it one of the most cost effective digital marketing strategies out there.
                                    </p>
                                    <p>
                                        PlusMedia is a certified Google Partner, so you know you are working with experts in the industry. Our search engine advertising techniques are designed to maximise your conversion rate at the lowest possible price. With a campaign on Google reaching your target audience, you will see a return on your advertising investment sooner than you expected.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/adpro.png" style="max-width: 160%" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row sub-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                <div class="col-md-3">
                                    <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: paper-plane.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Get ahead of your competitors with<br/> winning sales ads</h3>
                                    <p class="description">
                                        With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                <div class="col-md-3">
                                    <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: flag.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Receive a complementary landing page</h3>
                                    <p class="description">
                                        With the ever-growing amount of competition in your industry, it is important to have an edge over the rest. Here at PlusMedia, our specialists will create stand-out ads that will draw in your target audience and guide them to your website.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                <div class="col-md-3">
                                    <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: morph-stack.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Receive regular campaign optimisation</h3>
                                    <p class="description">
                                        Want to ensure your campaign is creating the results you desire? Our specialist team have it sorted. They are constantly monitoring and optimising your AdWords campaigns to guarantee you are getting the best possible return on investment.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                <div class="col-md-3">
                                    <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: compass.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Reach your preferred audience through effective targeting</h3>
                                    <p class="description">
                                        With our +Media AdPro package you can decide how specific you want your targeting to be. Our experts can provide broad match, phrase match and exact match keywords. This gives you the choice between optimising your page for thousands of related searches or only for a few hundred highly specific searches.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('cloud');">
                                <div class="col-md-3">
                                    <div id="icon-cloud" class="livicon-evo sub-icon" data-options="name: dashboard.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>See fast and transparent results</h3>
                                    <p class="description">
                                        If you sign up for our +Media AdPro package you will receive regular campaign reports providing you with metrics such as cost per click, click through rate, quality score and much more. These reports will help you track your campaign’s progress, so there are no more guessing games.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#main-img').addClass('animated fadeInRight');
            }, 200);
        });

        function showPopUpWindow(name) {
            $('#popup-'+name).slideDown(250);
            $('.popup-overlay').fadeIn(300);
            setTimeout(function () {
                window.popupOpen = 1;
            }, 300);

        }

        function closePopUpWindow() {
            $('.popup').slideUp(250);
            $('.popup-overlay').fadeOut(300, function () {
                $('.popup-overlay').hide();
                window.popupOpen = 0;
            })
        }
        $('body').click(function (event)
        {
            if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                closePopUpWindow();
            }
        });

        function playIcon(name) {
            var tracking = jQuery('#icon-'+name);
            tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
        }

        function getPopup() {
            var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                '                       <div id="confirm-content" class="content"> ' +
                '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                '                           <p>test the section</p> ' +
                '                           <p style="text-align: center;">testing</p> ' +
                '                       </div> ' +
                '                   </div>';

            $('.sub-section').parent().append(htmlAppend);
            setTimeout(function () {
                $('#confirm').toggleClass('active');
            }, 40);
        }

    </script>
@endsection
