<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:22.
 */
?>


@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <ul class="sidenav">
                                <li><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-2" href="{{URL::To('/')}}/service/web"><i class="icon-magic"></i>Web<i class="icon-chevron-right"></i></a></li>
                                {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                <li><a id="snav-tab-4" href="{{URL::To('/')}}/service/ad-pro"><i class="icon-gift"></i>Adpro<i class="icon-chevron-right"></i></a></li>
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-5" href="{{URL::To('/')}}/service/display-ads" style="color: #e4000e;"><i class="icon-adjust"></i>Display Ads<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" href="{{URL::To('/')}}/service/ad-clear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="{{URL::To('/')}}/service/social-media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="{{URL::To('/')}}/service/telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>Display Ads</h3>
                                    <p>
                                        With display ads, you can reach potential customers as they are checking their Gmail, watching Youtube videos, or simply browsing the net. A display ad is a form of paid advertising that typically comes in various formats that users can engage with, such as text, image, video and rich media. Users can click on the ad to then be taken to the corresponding landing page where they can potentially make a purchase or an enquiry.
                                    </p>
                                    <p>
                                        If you’re interested in engaging users, raising brand awareness and boosting your sales, then the Google Display Network is definitely the way to go.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/service/paid-ads/display-ads.png" style="max-width: 160%" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row sub-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                <div class="col-md-3">
                                    <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: image.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Stand out to users with an eye catching ad</h3>
                                    <p class="description">
                                        It’s easy for users to ignore a regular text ad. However, with display ads, you can use rich images and videos to grab the user’s attention and convince them to click on your ad and learn more about your business.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                <div class="col-md-3">
                                    <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: notebook.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Reach your preferred audience through detailed targeting</h3>
                                    <p class="description">
                                        It is important to target people relevant to your business when advertising. Display ads provide you with many targeting options to ensure your ads are reaching the right people. These ads can be tailored for an individual based on their demographics, interests, topics and location. Additionally, you can decide which websites you want your ads to appear on.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                <div class="col-md-3">
                                    <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: bulb.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Use memorable imagery to enhance brand awareness</h3>
                                    <p class="description">
                                        It is important to target people relevant to your business when advertising. Display ads provide you with many targeting options to ensure your ads are reaching the right people. These ads can be tailored for an individual based on their demographics, interests, topics and location. Additionally, you can decide which websites you want your ads to appear on.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                <div class="col-md-3">
                                    <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: morph-bar-chart.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Use meaningful data to track your campaign’s performance</h3>
                                    <p class="description">
                                        It is crucial to track the performance of your marketing activities to see what is working and what adjustments need to be made. Display advertising provides you with meaningful data such as how many users your ads are reaching, how many clicks its receiving, and the user response. By tracking your performance, you can rest assured that you are getting the most out of your investment.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('cloud');">
                                <div class="col-md-3">
                                    <div id="icon-cloud" class="livicon-evo sub-icon" data-options="name: user.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Turn window shoppers into customers with remarketing</h3>
                                    <p class="description">
                                        It is crucial to track the performance of your marketing activities to see what is working and what adjustments need to be made. Display advertising provides you with meaningful data such as how many users your ads are reaching, how many clicks its receiving, and the user response. By tracking your performance, you can rest assured that you are getting the most out of your investment.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#main-img').addClass('animated fadeInRight');
            }, 200);
        });

        function showPopUpWindow(name) {
            $('#popup-'+name).slideDown(250);
            $('.popup-overlay').fadeIn(300);
            setTimeout(function () {
                window.popupOpen = 1;
            }, 300);

        }

        function closePopUpWindow() {
            $('.popup').slideUp(250);
            $('.popup-overlay').fadeOut(300, function () {
                $('.popup-overlay').hide();
                window.popupOpen = 0;
            })
        }
        $('body').click(function (event)
        {
            if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                closePopUpWindow();
            }
        });

        function playIcon(name) {
            var tracking = jQuery('#icon-'+name);
            tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
        }

        function getPopup() {
            var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                '                       <div id="confirm-content" class="content"> ' +
                '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                '                           <p>test the section</p> ' +
                '                           <p style="text-align: center;">testing</p> ' +
                '                       </div> ' +
                '                   </div>';

            $('.sub-section').parent().append(htmlAppend);
            setTimeout(function () {
                $('#confirm').toggleClass('active');
            }, 40);
        }

    </script>
@endsection
