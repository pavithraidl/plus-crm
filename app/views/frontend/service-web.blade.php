<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/17 10:21.
 */
?>

@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/service.css')}}
@endsection

@section('content')
    <input type="hidden" id="domain" value="{{URL::To('/')}}" />
    <div class="container clearfix">
        <div id="top-container-row" class="row">
            <div class="col-md-12">
                <div id="side-navigation" class="custom-js">
                    <div class="col-md-3 nobottommargin">
                        <div class="snav-container">
                            <ul class="sidenav">
                                <li><a id="snav-tab-1" href="{{URL::To('/')}}/service/seo"><i class="icon-screen"></i>SEO<i class="icon-chevron-right"></i></a></li>
                                <li class="ui-tabs-active" style="background-color: #f6f6f6;"><a id="snav-tab-2" href="{{URL::To('/')}}/service/web" style="color: #e4000e;"><i class="icon-magic"></i>Web<span style="float: right;"> > </span><i class="icon-chevron-right"></i></a></li>
                                {{--<li><a id="snav-tab-3" class="menu" href="#Paid-Ads"><i class="icon-tint"></i>Paid Ads<i class="icon-chevron-right"></i></a></li>--}}
                                <li><a id="snav-tab-4" href="{{URL::To('/')}}/service/ad-pro"><i class="icon-gift"></i>Adpro<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-5" href="{{URL::To('/')}}/service/display-ads"><i class="icon-adjust"></i>Display Ads<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-6" href="{{URL::To('/')}}/service/ad-clear"><i class="icon-adjust"></i>Adclear<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-7" href="{{URL::To('/')}}/service/social-media"><i class="icon-adjust"></i>Social Media<i class="icon-chevron-right"></i></a></li>
                                <li><a id="snav-tab-8" href="{{URL::To('/')}}/service/telecommunications"><i class="icon-adjust"></i>Telecommunications<i class="icon-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9 nobottommargin section-content">
                        {{--Web--}}
                        <div id="Web">
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <h3>Web</h3>
                                    <p>
                                        Engage visitors and build trust in your brand with a well-crafted and professional looking website from PlusMedia.
                                    </p>
                                    <p>
                                        In an industry where the majority of companies are taking the easy way out by using templates, we build every website from scratch to ensure your personal vision will be upheld. We work alongside you to develop a website that both parties will be proud of.
                                    </p>
                                    <a href="{{URL::To('/')}}#free-consultation" >
                                        <button class="btn free-consultation">GET A FREE CONSULTATION</button>
                                    </a>
                                </div>
                                <div class="col-md-5">
                                    <img class="hidden-xs" src="{{URL::To('/')}}/assets/frontend/images/service/new-images/website.png" style="max-width: 160%" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row sub-section">
        <div class="container">
            <div class="col-md-12" style="margin-top: 80px;">
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('tracking');">
                                <div class="col-md-3">
                                    <div id="icon-tracking" class="livicon-evo sub-icon" data-options="name: grid.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Receive a customised package</h3>
                                    <p class="description">
                                        We understand that different companies present varying budget constraints. That is why we work to develop a customised package that suits your budget, so that every company has the opportunity to grow through digital marketing.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('conversion');">
                                <div class="col-md-3">
                                    <div id="icon-conversion" class="livicon-evo sub-icon" data-options="name: diagram.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Receive an easy-to-navigate website</h3>
                                    <p class="description">
                                        We understand that different companies present varying budget constraints. That is why we work to develop a customised package that suits your budget, so that every company has the opportunity to grow through digital marketing.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('routing');">
                                <div class="col-md-3">
                                    <div id="icon-routing" class="livicon-evo sub-icon" data-options="name: screenshot.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Engage users with a persuasive copy</h3>
                                    <p class="description">
                                        Stand out from your competitors by having unique content that is not only persuasive, but also captures your vision and gives the right amount of information needed to help with the decision making process. Our copywriters have significant experience in creating marketing content that converts web visitors to customers.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('analytics');">
                                <div class="col-md-3">
                                    <div id="icon-analytics" class="livicon-evo sub-icon" data-options="name: thumbnails-small.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Standout with customised graphics</h3>
                                    <p class="description">
                                        Stand out from your competitors by having unique content that is not only persuasive, but also captures your vision and gives the right amount of information needed to help with the decision making process. Our copywriters have significant experience in creating marketing content that converts web visitors to customers.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 35px;">
                    <div class="col-md-12" style="display: flex;">
                        <div class="col-md-6" style="flex: 1; padding: 0 10px;">
                            <div class="col-md-12 sub-box" onmouseenter="playIcon('cloud');">
                                <div class="col-md-3">
                                    <div id="icon-cloud" class="livicon-evo sub-icon" data-options="name: globe.svg; size: 100px; style: original; tryToSharpen: true; drawOnViewport:true" ></div>
                                </div>
                                <div class="col-md-9" style="margin-top: 25px;">
                                    <h3>Rank on Google with an SEO ready website</h3>
                                    <p class="description">
                                        Gain visibility on Google or other search engines quicker by having an SEO ready website. PlusMedia is your leading name in SEO services Auckland wide, and our team of SEO experts know exactly what to do to ensure your website ranks highly on search engines.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#main-img').addClass('animated fadeInRight');
            }, 200);
        });

        function showPopUpWindow(name) {
            $('#popup-'+name).slideDown(250);
            $('.popup-overlay').fadeIn(300);
            setTimeout(function () {
                window.popupOpen = 1;
            }, 300);

        }

        function closePopUpWindow() {
            $('.popup').slideUp(250);
            $('.popup-overlay').fadeOut(300, function () {
                $('.popup-overlay').hide();
                window.popupOpen = 0;
            })
        }
        $('body').click(function (event)
        {
            if(!$(event.target).closest('.popup').length && !$(event.target).is('.popup') && window.popupOpen == 1) {
                closePopUpWindow();
            }
        });

        function playIcon(name) {
            var tracking = jQuery('#icon-'+name);
            tracking.playLiviconEvo({duration: 1, repeat: 6}, true);
        }

        function getPopup() {
            var htmlAppend = '  <div id="confirm" class="popup-confirm" draggable="true" style="z-index: 1000 !important;"> ' +
                '                       <div id="confirm-content" class="content"> ' +
                '                           <h2><i class="fa fa-edit success" style="font-size: 25px;"></i> testing</h2> ' +
                '                           <p>test the section</p> ' +
                '                           <p style="text-align: center;">testing</p> ' +
                '                       </div> ' +
                '                   </div>';

            $('.sub-section').parent().append(htmlAppend);
            setTimeout(function () {
                $('#confirm').toggleClass('active');
            }, 40);
        }

    </script>
@endsection