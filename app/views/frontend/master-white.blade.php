<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/10/17 09:19.
 */
?>

        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    {{--Meta Data--}}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="title" content="{{$title}}" >
    <meta name="description" content="{{$metaDescription}}" >
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/backend/images/logo-ico.png">
    <title>Plus Media - Page Name</title>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{--<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />--}}

    {{HTML::style('assets/frontend/css/bootstrap.css')}}
    {{HTML::style('assets/frontend/css/style.css')}}
    {{HTML::style('assets/frontend/css/dark.css')}}
    {{HTML::style('assets/frontend/css/animate.css')}}
    {{HTML::style('assets/frontend/css/responsive.css')}}
    {{HTML::style('assets/frontend/css/master.css')}}
    {{HTML::style('assets/backend/libs/liveicons-evo/css/LivIconsEvo.css')}}

    @yield('styles')

    <meta name="viewport" content="width=device-width, initial-scale=1" />

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header full-header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="fa fa-bars"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="{{URL::To('/')}}" class="standard-logo" data-dark-logo="{{URL::To('/')}}/assets/frontend/images/logo-w.png"><img src="{{URL::To('/')}}/assets/frontend/images/logo-w.png"></a>
                    <a href="{{URL::To('/')}}" class="retina-logo" data-dark-logo="{{URL::To('/')}}/assets/frontend/images/logo-w.png"><img src="{{URL::To('/')}}/assets/frontend/images/logo-w.png"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul>
                        <li><a href="{{URL::To('/')}}"><div @if($page == 'home') class="active" @endif>Home</div></a>
                        <li><a href="{{URL::To('/')}}/about"><div @if($page == 'about') class="active" @endif>About</div></a></li>
                        <li><a href="{{URL::To('/')}}/services"><div @if($page == 'service') class="active" @endif>Services</div></a></li>
                        <li><a href="{{URL::To('/')}}/contact"><div @if($page == 'contact') class="active" @endif>Contact</div></a></li>
                        <li><a href="http://login.plusmedia.co.nz/index/login/?c=index&a=index"><div @if($page == 'client') class="active" @endif>Client Login</div></a></li>
                        <li><a href="tel:0800758763342"><div class="red-border">0800 7587 63342</div></a> </li>
                    </ul>
                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->
    <h1 style="visibility: hidden; margin-top: -155px;">{{$h1}}</h1>
    @yield('content')

<!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

        <div class="container">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">
                <p>
                    0800 7587 63342<br/>
                    info@plusmedia.co.nz
                </p>

            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">
            <ul>
                <li><a href="{{URL::To('/')}}"> HOME</a></li>
                <li><a href="{{URL::To('/')}}/about">ABOUT</a></li>
                <li><a href="{{URL::To('/')}}/services">SERVICES</a></li>
                <li><a href="{{URL::To('/')}}">CONTACT</a></li>
                <li><a href="{{URL::To('/')}}">CLIENT LOGIN</a></li>
            </ul>

        </div>
        <div class="col-md-12 bottom-white-footer" style="padding: 20px;">
            <div class="col-md-4 logo">
                <img class="hidden-xs" src="{{URL::To('/')}}/assets/frontend/images/logo@2x.png"/>
            </div>
            <div class="col-md-4">
                <p>
                    Follow us on :
                    <a href="https://www.linkedin.com/company/plusmedianz/"><img src="{{URL::To('/')}}/assets/frontend/images/icons/group-17.png" /></a>
                    <a href="https://www.facebook.com/PlusMediaNZ/"><img src="{{URL::To('/')}}/assets/frontend/images/icons/group-16.png" /></a>
                </p>
            </div>
            <div class="col-md-4">
                <!-- Go To Top
                ============================================= -->

            </div>
        </div>

    </footer><!-- #footer end -->
    <div id="gotoTop" class="livicon-evo sub-icon" data-options="name: chevron-top.svg; size: 50px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#fff; top:-5px;" ></div>
</div><!-- #wrapper end -->

<!-- External JavaScripts
============================================= -->
{{HTML::script('assets/backend/js/custom/public/constant.js')}}
{{HTML::script('assets/frontend/js/jquery.js')}}
{{HTML::script('assets/frontend/js/plugins.js')}}
<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/frontend/js/functions.js')}}
{{HTML::script('assets/frontend/js/settings.js')}}
{{HTML::script('assets/backend/js/custom/public/validator.js')}}

{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/snap.svg-min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/TweenMax.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/DrawSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/MorphSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/verge.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.defaults.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.min.js')}}

{{HTML::script('assets/frontend/js/form.js')}}


@yield('scripts')
</body>
</html>
