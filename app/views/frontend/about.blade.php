<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/10/17 09:17.
 */
?>

@extends('frontend.master-dark')

@section('styles')
    {{HTML::style('assets/frontend/css/about.css?v=0.01')}}
@endsection

@section('content')
    <div class="col-md-12" style="padding: 0px;">
        <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
            <ul class="tab-nav clearfix">
                <li><a href="#tabs-1">COMPANY</a></li>
                <li><a href="#tabs-2">VALUES</a></li>
                <li><a href="#tabs-3">TEAM</a></li>
                <li><a href="#tabs-4">CAREER</a></li>
            </ul>
            <div class="tab-container">
                <div class="tab-content clearfix" id="tabs-1">
                    <div id="about-company">
                        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                            <p>
                                PlusMedia is a digital marketing agency based in Takapuna, Auckland. We offer our clients the latest solutions, designed to maximise advertising campaigns and increase sales.<br/><br/>

                                Our team comes from a diverse range of backgrounds, enabling us to offer comprehensive solutions to our clientele. We have specialists in Google AdWords, Search Engine Optimisation, Web Design/Development, Telecommunications, Content and Marketing.<br/><br/>

                                You will also receive the additional benefit of our dedicated Account Managers who are here to help you throughout the process, keeping you up to date on the status of your campaign and guiding you through any issues.
                            </p>
                        </div>

                        <div id="bottom-company" class="container" style="width: 100%; padding: 0px;">
                            <div class="col-md-12">
                                <div class="row">
                                    <img src="{{URL::To('/')}}/assets/frontend/images/about/bottom-slider/full-slider.jpg" style="width: 100%; padding: 0 !important;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content clearfix" id="tabs-2">
                    <div id="about-values">
                        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                            <p>
                                At PlusMedia, our mission is to create effective online marketing campaigns that provide a high return on investment.<br/><br/>

                                We work with you to create a superior online presence for your business that enhances brand awareness and drives sales.

                            </p>
                        </div>
                        <div id="bottom-values" class="col-md-12">
                            <div class="col-md-10 col-md-offset-1">
                                <h2>Core Values</h2>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <P class="title-icon">
                                            <img src="{{URL::To('/')}}/assets/frontend/images/about/values/innovation.png" />
                                        </P>
                                        <h3>INNOVATION</h3>
                                        <p class="content">
                                            We foster a creative work environment and are constantly coming up with new and unique solutions to ensure our customers receive excellent value.
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <P class="title-icon">
                                            <img src="{{URL::To('/')}}/assets/frontend/images/about/values/collaboration.png" />
                                        </P>
                                        <h3>COLLABORATION</h3>
                                        <p class="content">
                                            We work together with our clients to formulate ideas and shape strategies that will help them to achieve their goals and targets..
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <P class="title-icon">
                                            <img src="{{URL::To('/')}}/assets/frontend/images/about/values/integrity.png" />
                                        </P>
                                        <h3>INTEGRITY</h3>
                                        <p class="content">
                                            Our team of trusted professionals adhere to the highest levels of integrity at all times. We are transparent in our affairs, treat all our clients with honesty and respect, and always exercise confidentiality.
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <P class="title-icon">
                                            <img src="{{URL::To('/')}}/assets/frontend/images/about/values/dedication.png" />
                                        </P>
                                        <h3>DEDICATION</h3>
                                        <p class="content">
                                            We constantly strive to be the best. We set ourselves high standards and are passionate about delivering excellent results to our clients.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content clearfix" id="tabs-3">
                    <div id="about-team">
                        <div class="col-md-6 col-md-offset-3">
                            {{--team member--}}
                            <div class="col-md-12 team-member">
                                <div class="col-md-4 col-xs-12">
                                    <img class="team-img-left" src="{{URL::To('/')}}/assets/frontend/images/about/team/paul.png" />
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <h2>Paul</h2>
                                    <h3>MANAGING DIRECTOR</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                </div>
                            </div>
                            {{--team member--}}
                            <div class="col-md-12 team-member">
                                <div class="hidden-sm hidden-md hidden-lg col-xs-12">
                                    <img class="team-img-left" src="{{URL::To('/')}}/assets/frontend/images/about/team/ivan.png" />
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <h2>Ivan</h2>
                                    <h3>FINANCE DIRECTOR</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                </div>
                                <div class="col-md-4 hidden-xs">
                                    <img class="team-img-right" src="{{URL::To('/')}}/assets/frontend/images/about/team/ivan.png" />
                                </div>
                            </div>
                            {{--team member--}}
                            <div class="col-md-12 team-member">
                                <div class="col-md-4 col-xs-12">
                                    <img class="team-img-left" src="{{URL::To('/')}}/assets/frontend/images/about/team/chris.png" />
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <h2>Chris</h2>
                                    <h3>ACCOUNT DIRECTOR</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 team-bottom">
                            <div class="col-md-6 col-md-offset-3">
                                {{--Team Grid--}}
                                <div class="team-grid">
                                    <h3 class="red hidden-xs">The PLUSMEDIA TEAM</h3>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/joshua.png" />
                                        <h4>Joshua</h4>
                                        <p>HEAD OF SALES</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/phoebe.png" />
                                        <h4>Phoebe</h4>
                                        <p>SENIOR BUSINESS DEVELOPMENT MANAGER</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/michael.png" />
                                        <h4>Michael</h4>
                                        <p>BUSINESS DEVELOPMENT MANAGER</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/prince.png" />
                                        <h4>Prince</h4>
                                        <p>DIGITAL PROJECT MANAGER</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/dinesh.png" />
                                        <h4>Dinesh</h4>
                                        <p>HEAD OF DESIGNS</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/allen.png" />
                                        <h4>Allen</h4>
                                        <p>IT SUPPORT</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/isuru.png" />
                                        <h4>Isuru</h4>
                                        <p>WEB DEVELOPER</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/ameya.png" />
                                        <h4>Ameya</h4>
                                        <p>SOCIAL MEDIA SPECIALIST</p>
                                    </div>
                                    <div class="member">
                                        <img src="{{URL::To('/')}}/assets/frontend/images/about/team/angela.png" />
                                        <h4>Angela</h4>
                                        <p>ACCOUNTS/FINANCE MANAGER</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content clearfix" id="tabs-4">
                    <div id="about-career">
                        <div class="col-md-8 col-md-offset-2">
                            <p>
                                Love all things digital?<br/>
                                Start your career with Auckland's leading digital marketing agency
                            </p>
                        </div>
                        <div class="bottom-section col-md-12">
                            <h2>Open Positions</h2>
                            <div class="col-md-12">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="col-md-12">
                                        {{--<div class="col-md-3">--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<select>--}}
                                                    {{--<option>Location</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<select>--}}
                                                    {{--<option>Department</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 result-section">--}}
                                            {{--<div class="result">--}}
                                                {{--<h3>Phone Based Sales Representative</h3>--}}
                                                {{--<p class="tags">--}}
                                                    {{--<span><img src="{{URL::To('/')}}/assets/frontend/images/icons/edit.png" /> By PlusMedia</span>--}}
                                                    {{--<span><img src="{{URL::To('/')}}/assets/frontend/images/icons/date.png" /> 2017-02-18</span>--}}
                                                    {{--<span><img src="{{URL::To('/')}}/assets/frontend/images/icons/tag.png" /> Auckland</span>--}}
                                                {{--</p>--}}
                                                {{--<p class="description">--}}
                                                    {{--“WANTED” for an immediate start. We are looking for a dynamic person to join our team. There is a huge opportunity for growth into “Team Leader” positions and even “Management” positions as we are a fast growing company. Looking to step into the corporate world or even further your career to become a key member--}}
                                                {{--</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <h3 style="text-align: center">Will let you know if there are any current vacancies</h3>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection