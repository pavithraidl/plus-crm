<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 17/10/17 08:23.
 */
?>
        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    {{--Meta Data--}}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="title" content="{{$title}}" >
    <meta name="description" content="{{$metaDescription}}" >
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/backend/images/logo-ico.png">
    <title>Plus Media - {{$page}}</title>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{--<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />--}}

    {{HTML::style('assets/frontend/css/bootstrap.css?v=0.01')}}
    {{HTML::style('assets/frontend/css/style.css')}}
    {{HTML::style('assets/frontend/css/dark.css')}}
    {{HTML::style('assets/frontend/css/animate.css')}}
    {{HTML::style('assets/frontend/css/responsive.css')}}
    {{HTML::style('assets/frontend/css/master.css?v=0.02')}}
    {{HTML::style('assets/backend/libs/liveicons-evo/css/LivIconsEvo.css')}}

    @yield('styles')

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header dark full-header" data-sticky-class="dark">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="fa fa-bars"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="{{URL::To('/')}}" class="standard-logo" data-dark-logo="{{URL::To('/')}}/assets/frontend/images/logo.png"><img src="{{URL::To('/')}}/assets/frontend/images/logo.png"></a>
                    <a href="{{URL::To('/')}}" class="retina-logo" data-dark-logo="{{URL::To('/')}}/assets/frontend/images/logo.png"><img src="{{URL::To('/')}}/assets/frontend/images/logo.png"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul>
                        <li><a href="{{URL::To('/')}}"><div @if($page == 'home') class="active" @endif>Home</div></a>
                        <li><a href="{{URL::To('/')}}/about"><div @if($page == 'about') class="active" @endif>About</div></a></li>
                        <li><a href="{{URL::To('/')}}/services"><div @if($page == 'service') class="active" @endif>Services</div></a></li>
                        <li><a href="{{URL::To('/')}}/contact"><div @if($page == 'contact') class="active" @endif>Contact</div></a></li>
                        <li><a href="http://login.plusmedia.co.nz/index/login/?c=index&a=index"><div @if($page == 'client') class="active" @endif>Client Login</div></a></li>
                        <li><a href="tel:0800758763342"><div class="red-border">0800 7587 63342</div></a> </li>
                    </ul>
                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    <!-- Page Title
    ============================================= -->
    <section id="page-title" class="page-title-parallax page-title-dark" style="padding: 130px 0px; background-color: rgba(0,0,0,0.81); padding-top: 50px; z-index: 100 !important;" data-stellar-background-ratio="0.4">

        <div class="container clearfix">
            <h1 style="visibility: hidden; margin-top: -50px;">{{$h1}}</h1>
            <div class="col-md-6 col-md-offset-3">
                <p>{{$quote}}</p>
            </div>
        </div>

    </section><!-- #page-title end -->

    <div class="hidden-xs hidden-sm">
        <video poster="{{URL::To('/')}}/assets/frontend/images/about/bg/group-3.png" id="bgvid" playsinline autoplay muted loop>
            <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
            {{--<source src="{{URL::To('/')}}/assets/frontend/images/video/explore.webm" type="video/webm">--}}
            <source src="{{URL::To('/')}}/assets/frontend/images/video/27028060.mp4" type="video/mp4">
        </video>
    </div>
    

    {{--<div class="section dark parallax notopmargin nobottommargin noborder" style="height: 500px; padding: 145px 0;">--}}

        {{--<div class="container clearfix">--}}

            {{--<div class="slider-caption slider-caption-center" style="position: relative;">--}}
                {{--<div data-animate="fadeInUp">--}}
                    {{--<h2 style="font-size: 42px;">Beautiful HTML5 Videos</h2>--}}
                    {{--<p>Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>--}}
                    {{--<a href="#" class="button button-border button-rounded button-white button-light button-large noleftmargin nobottommargin" style="margin-top: 20px;">Show More</a>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}

        {{--<div class="video-wrap">--}}
            {{--<video poster="{{URL::To('/')}}/assets/frontend/images/about/bg/group-3.png" preload="none" loop autoplay muted>--}}
                {{--<source src='{{URL::To('/')}}/assets/frontend/images/video/explore.mp4' type='video/mp4' />--}}
                {{--<source src='{{URL::To('/')}}/assets/frontend/images/video/explore.webm' type='video/webm' />--}}
            {{--</video>--}}
            {{--<div class="video-overlay" style="background-color: rgba(0,0,0,0.3);"></div>--}}
        {{--</div>--}}

    {{--</div>--}}

    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap">
            @yield('content')
        </div>
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

        <div class="container">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">
                <p>
                    0800 7587 63342<br/>
                    info@plusmedia.co.nz
                </p>

            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">
            <ul>
                <li><a href="{{URL::To('/')}}"> HOME</a></li>
                <li><a href="{{URL::To('/')}}/about">ABOUT</a></li>
                <li><a href="{{URL::To('/')}}/services">SERVICES</a></li>
                <li><a href="{{URL::To('/')}}/contact">CONTACT</a></li>
                <li><a href="{{URL::To('/')}}">CLIENT LOGIN</a></li>
            </ul>

        </div>
        <div class="col-md-12 bottom-white-footer" style="padding: 20px;">
            <div class="col-sm-4 logo hidden-xs">
                <p style="text-align: center">
                    <img src="{{URL::To('/')}}/assets/frontend/images/logo@2x.png"/>
                </p>
            </div>
            <div class="col-sm-4 col-xs-12">
                <p>
                    Follow us on :
                    <a href="https://www.linkedin.com/company/plusmedianz/"><img alt="PlusMedia LinkedIn" src="{{URL::To('/')}}/assets/frontend/images/icons/group-17.png" /></a>
                    <a href="https://www.facebook.com/PlusMediaNZ/"><img alt="PlusMedia Facebook" src="{{URL::To('/')}}/assets/frontend/images/icons/group-16.png" /></a>
                </p>
            </div>
            <div class="col-sm-4 col-xs-12">
                <!-- Go To Top
                ============================================= -->
            </div>
        </div>

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->
<div id="gotoTop" class="livicon-evo sub-icon" data-options="name: chevron-top.svg; size: 50px; style: lines; tryToSharpen: true; drawOnViewport:true; strokeColor:#fff; top:-5px;" ></div>
<!-- External JavaScripts
============================================= -->
{{HTML::script('assets/backend/js/custom/public/constant.js')}}
{{HTML::script('assets/frontend/js/jquery.js')}}
{{HTML::script('assets/frontend/js/plugins.js')}}

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/frontend/js/functions.js')}}
{{HTML::script('assets/frontend/js/animate-dark.js')}}

{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/snap.svg-min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/TweenMax.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/DrawSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/MorphSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/verge.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.defaults.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.min.js')}}


<script>
    var vid = document.getElementById("bgvid");
    var pauseButton = document.querySelector("#polina button");

    if (window.matchMedia('(prefers-reduced-motion)').matches) {
        vid.removeAttribute("autoplay");
        vid.pause();
        pauseButton.innerHTML = "Paused";
    }

    function vidFade() {
        vid.classList.add("stopfade");
    }

    vid.addEventListener('ended', function()
    {
// only functional if "loop" is removed
        vid.pause();
// to capture IE10
        vidFade();
    });


    pauseButton.addEventListener("click", function() {
        vid.classList.toggle("stopfade");
        if (vid.paused) {
            vid.play();
            pauseButton.innerHTML = "Pause";
        } else {
            vid.pause();
            pauseButton.innerHTML = "Paused";
        }
    })
</script>

@yield('scripts')
</body>
</html>