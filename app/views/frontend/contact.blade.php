<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 24/10/17 10:51.
 */
?>

@extends('frontend.master-white')

@section('styles')
    {{HTML::style('assets/frontend/css/contact.css?v=0.01')}}
@endsection

@section('content')
    <div class="wrap">
        <div class="container">
            <div class="col-md-10 col-md-offset-2 contact-container">
                <div class="col-md-5">
                    <div class="left">
                        <h3>Contact Us:</h3>
                        <div class="address">
                            <img src="{{URL::To('/')}}/assets/frontend/images/icons/location.png" />
                            <p>
                                <a href="https://goo.gl/maps/sQA9Jpn2iAq" target="_blank" style="color: #313131;">
                                    PlusMedia<br/>
                                    10 Northcroft Street<br/>
                                    Takapuna, New Zealand
                                </a>
                            </p>
                        </div>
                        <div class="telephone">
                            <img src="{{URL::To('/')}}/assets/frontend/images/icons/mobile.png" />
                            <p>
                                <a href="tel:0800758763342"style="color: #313131;">
                                    0800 7587 63342
                                </a>
                            </p>
                        </div>
                        <div class="email">
                            <img src="{{URL::To('/')}}/assets/frontend/images/icons/email.png" />
                            <p>
                                <a href="mailto:info@plusmedia.co.nz" style="color: #313131;">
                                    info@plusmedia.co.nz
                                </a>

                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-md-5">
                    <div class="right">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3195.1994162009255!2d174.7697170154486!3d-36.78976887995065!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d39d66a9d2c89%3A0x1f164f5711496896!2sPlusMedia!5e0!3m2!1sen!2snz!4v1510527158645" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row bottom-row">
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-md-12">
                        <h3>Leave us a message</h3>
                        <div class="col-md-4" style="padding: 10px;">
                            <input id="txt-name" type="text" onblur="requiredValidator(this, 'Please enter your name')" placeholder="Name" />
                        </div>
                        <div class="col-md-4" style="padding: 10px;">
                            <input id="txt-email" onblur="requiredValidator(this, 'Please enter you email'), emailValidator(this, 'Email is not valid!')" type="text" placeholder="Email" />
                        </div>
                        <div class="col-md-4" style="padding: 10px;">
                            <input class="phone-number" id="txt-phone" type="text" placeholder="Phone no." />
                        </div>
                        <div class="col-md-12" style="padding: 10px; margin-top: -30px;">
                            <textarea id="txt-msg" placeholder="Message" rows="4" ></textarea>
                        </div>
                        <div class="col-md-12" style="padding: 10px;">
                            <div id="btn-submit" onclick="submitForm();" class="btn btn-red" style="position: relative;">
                                SUBMIT
                            </div>
                            <span class="success-message animated" style="opacity: 0;">
                                <i class="fa fa-check"></i> We have received your inquiry. Stay tuned, we’ll get back to you very soon.
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


