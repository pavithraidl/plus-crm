<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-15 22:31:57.
 * View ID - 35
 */
 ?>

@extends('backend.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Projects <small>100% Opportunities</small></h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Projects</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th style="width: 20%">Project Name</th>
                                    <th>Team Members</th>
                                    <th>Project Progress</th>
                                    <th>Status</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="57"></div>
                                        </div>
                                        <small>57% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs">Working</button>
                                    </td>
                                    <td>
                                        <a href="{{URL::To('/')}}/projects/project" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="47"></div>
                                        </div>
                                        <small>47% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs">Working</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100"></div>
                                        </div>
                                        <small>100% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">Completed</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                                        </div>
                                        <small>60% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs">Working</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="12"></div>
                                        </div>
                                        <small>12% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs">Working</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="0"></div>
                                        </div>
                                        <small>0% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-xs">Pending</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="0"></div>
                                        </div>
                                        <small>0% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-xs">Pending</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="77"></div>
                                        </div>
                                        <small>77% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-xs">Declined</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Pesamakini Backend UI</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{URL::To('/')}}/assets/backend/images/users/default/user-35.jpg" class="avatar" alt="Avatar">
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="95"></div>
                                        </div>
                                        <small>95% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-xs">On Review</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- end project list -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('scripts')
    <!-- bootstrap-progressbar -->
    {{HTML::script('assets/backend/libs/bootstrap-progressbar/bootstrap-progressbar.min.js')}}

    <script>
        $(document).ready(function () {
            $('.progress').removeAttr('style');
        })
    </script>
@endsection
