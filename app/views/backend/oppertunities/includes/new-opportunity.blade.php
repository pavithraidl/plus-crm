<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 6/11/17 15:21.
 */
?>

<div id="mm-add-client-container" class="modal fade bs-new-client-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-folder-open"></i> Add A New Client</h4>
            </div>
            <div class="modal-body" style="max-height: 600px;">
                <div class="form-horizontal form" style="max-height: 525px; overflow-y: auto;">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-fname">Name</span>
                        </label>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <select style="height: 35px;">
                                <option>None</option>
                                <option>Mr.</option>
                                <option>Ms.</option>
                                <option value="">Mrs.</option>
                                <option value="">Dr.</option>
                                <option value="">Prof.</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-5">
                            <input type="text" id="mm-new-client-fname" placeholder="Fist Name" onblur="" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="4">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <input type="text" id="mm-new-client-lname" placeholder="Last Name" maxlength="10" class="form-control col-md-7 col-xs-12" tabindex="5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-fname">Phone</label>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input type="text" id="mm-new-client-fname" placeholder="Mobile" onblur="requiredValidator(this, 'Please fill the city');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="4">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <select style="height: 35px;">
                                <option value="">Mobile</option>
                                <option value="">Work</option>
                                <option>Home</option>
                            </select>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <div class="livicon-evo" data-options="name: plus-alt.svg; size: 25px; style: original; tryToSharpen: true" style="margin: 20% auto; cursor: pointer;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email:</label>
                        <div class="col-md-4 col-sm-4 col-xs-8">
                            <input type="text" id="mm-pro-street" placeholder="Email" onblur="requiredValidator(this, 'Please fill the street');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <select style="height: 35px;">
                                <option value="">Work</option>
                                <option value="">Account</option>
                                <option>Personal</option>
                            </select>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <div class="livicon-evo" data-options="name: plus-alt.svg; size: 25px; style: original; tryToSharpen: true" style="margin: 20% auto; cursor: pointer;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="mm-pro-street" placeholder="Street" onblur="requiredValidator(this, 'Please fill the street');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-suburb"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="mm-pro-suburb" placeholder="Suburb" onblur="requiredValidator(this, 'Please fill the suburb');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-city"></span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input type="text" id="mm-pro-city" placeholder="City" onblur="requiredValidator(this, 'Please fill the city');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="4">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input type="text" id="mm-pro-postal-code" placeholder="Postal Code" maxlength="10" class="form-control col-md-7 col-xs-12" tabindex="5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-manager">Lead Source</label>
                        <div class="col-md-4 col-sm-4 col-xs-8" tabindex="7">
                            <select id="mm-pro-manager" class="form-control col-md-7 col-xs-12">
                                <option value="">Gopher</option>
                                <option value="">Yellow Pages</option>
                                <option value="">Trade Me</option>
                                <option value="">No cow boys</option>
                                <option value="">Finda</option>
                                <option value="">Find Us</option>
                                <option value="">Customer Referrals</option>
                                <option value="">Empoyee Referrals</option>
                                <option value="">Incoming Enquiry</option>
                                <option value="">Office</option>
                                <option value="">Others</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4" tabindex="7">
                            <select id="mm-pro-manager" class="form-control col-md-7 col-xs-12">
                                <option value="">Open</option>
                                <option value="">Hot</option>
                                <option value="">Worm</option>
                                <option value="">Cold</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group has-feedback margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-date">Expected Close Date:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="pro-inspection-date" class="datepicker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group has-feedback margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-date">Call Back Date:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="pro-inspection-date" class="datepicker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group has-feedback" style="margin-top: 35px; cursor: pointer;">
                        <div id="pro-troggle-inspection-schedule" class="col-md-push-3  col-sm-push-3 col-md-6 col-sm-6 col-xs-12" tabindex="9">
                            <h5><i class="fa fa-building"></i> Client Businesses</h5>
                            <hr style="margin-left: 40%; margin-top: -4%;"/>
                            <i id="pro-toggle-inspection-schedule-icon" class="fa fa-chevron-circle-up" style="position: absolute; right: 0px; top: 10px; font-size: 16px;"></i>
                        </div>
                    </div>
                    <div id="as-schedule-inspection-container">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">Business Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-new-client-company" placeholder="Business Name" onblur="requiredValidator(this, 'Please fill the company');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Industry:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="pro-frequency" class="form-control col-md-7 col-xs-12">
                                    <option disabled>-- None --</option>
                                    <option value="1">Accommodation</option>
                                    <option value="2">Accountant</option>
                                    <option value="3">Adult Entertaitment</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">Website:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-new-client-company" placeholder="website" onblur="requiredValidator(this, 'Please fill the company');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">Decision Makers:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">What They Do:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">Categories They Service:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">Areas They Service:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company"></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="livicon-evo-button rounded success evo-md">
                                    <div class="livicon-evo" data-options="name: plus-alt.svg; size: 24px; style: lines; strokeColor: #fff; eventOn: parent">
                                    </div>
                                    <span>New Business</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-new-client-company">What More Can You Sell Him:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                    <button id="pro-btn-add" type="button" class="btn btn-success" tabindex="12"><i class="fa fa-save"></i> Add </button>
                </div>
            </div>
        </div>
    </div>
</div>
