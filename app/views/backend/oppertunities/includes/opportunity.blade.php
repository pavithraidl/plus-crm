<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/11/17 13:51.
 */
?>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 900px; background-color: #f8fdff;">
            <div class="modal-header">
                <button type="button" class="minimize" aria-hidden="true" onclick="minimizeModal();">-</button>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Facebook Add Campaign - <span style="color: #acbdde;">Devnoport Pools Ltd</span></h4>

            </div>
            <div class="modal-body">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-cc-visa"></i> Payments</a></li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-list"></i> Posts</a></li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-bullhorn"></i> Activity Log</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Commencement Date</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    26 Oct '16
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">PlusMedia Package Type</label>
                                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 7px;">
                                                    Hosting
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Close Date</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    26 Oct '17
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Opportunity Name</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Facebook Add Campaign
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Account Name</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Devnoport Pools Ltd
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Possibility</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    100%
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Stage</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Won
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">State</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Running
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab" style="padding: 15px 25px;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Monthly Fee</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    $100
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Contract Term</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    12
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Payment Method</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Bank Deposit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Payment Term</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Pay Monthly
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Total Amount</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    $1200
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Paid Amount</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    $600
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Payments</h4>
                                    <ul class="list-unstyled msg_list">
                                        <li style="background: #ffd4d4;">
                                            <a>
                                                <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                                  <i class="fa fa-info" style="color: #e4000e;"></i>
                                                </span>
                                                <span>
                                                    <span style="margin-left: 8px;">Term Payment - Pending</span>
                                                    <span class="time" style="right: 60px; color: #e4000e;">Due 05th Nov / Overdue by 11 Days</span>
                                                </span>
                                                <span class="message" style="margin-left: 50px; color: #b0000e;">
                                                    Service will be suspended in 4 days if the payment will not be completed!
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                                  <i class="fa fa-info"></i>
                                                </span>
                                                <span>
                                                    <span style="margin-left: 8px;">Term Payment - Paid</span>
                                                    <span class="time" style="right: 60px;">Due 05th Oct / 03rd Oct</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                                  <i class="fa fa-info"></i>
                                                </span>
                                                <span>
                                                    <span style="margin-left: 8px;">Term Payment - Paid</span>
                                                    <span class="time" style="right: 60px;">Due 05th Sep / 05th Sep</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                                  <i class="fa fa-info"></i>
                                                </span>
                                                <span>
                                                    <span style="margin-left: 8px;">Term Payment - Paid</span>
                                                    <span class="time" style="right: 60px;">Due 05th Aug / 01 Aug</span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <div class="col-md-12" id="add-post-container">
                                <div class="form-horizontal form">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-manager">Subject</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12" tabindex="7">
                                            <input type="text" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="first-name" style="float: left;">Description</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div id="description" class='edit' data-rich="0"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="livicon-evo-button rounded success evo-md" style="float: right; margin-right: 30px;" onclick="slidePost();">
                                <div class="livicon-evo" data-options="name: comment.svg; size: 24px; style: lines; strokeColor: #fff; eventOn: parent"></div>
                                <span>Add Post</span>
                            </div>
                            <ul class="list-unstyled msg_list">
                                <li>
                                    <a>
                                        <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                          <i class="fa fa-list"></i>
                                        </span>
                                        <span>
                                            <span style="margin-left: 8px; font-size: 15px; font-weight: 600;">Final Changes</span>
                                            <span class="time" style="right: 60px;">Isuru L / 3 mins ago</span>
                                        </span>
                                        <p class="message" style="margin-left: 50px;">
                                            <ul>
                                                <li>Footer on all pages - change to "Tree Magic offer tree pruning, removals, reductions and thinning, stump grinding and more. Don’t hesitate to call or message us to request a quote."</li>
                                                <li>Home page and all other pages - change address to just say Drury, Auckland (no street address)</li>
                                                <li>Home page - change the transparent right photo (also appears on other pages in the top left position) to the one attached titled image 4494 (zoom in a little)</li>
                                                <li>Home page - remove the counter with hours of work, cups of coffee etc</li>
                                                <li>Home page - change the 'Need professional help with your trees. Contact us today. image to an image of a Stihl chainsaw on wood if possible</li>
                                            </ul>
                                        </p>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                          <i class="fa fa-list"></i>
                                        </span>
                                        <span>
                                            <span style="margin-left: 8px; font-size: 15px; font-weight: 600;">Project Proposal</span>
                                            <span class="time" style="right: 60px;">Isuru L / 3 days ago</span>
                                        </span>
                                        <p class="message" style="margin-left: -120px; margin-top: 25px; font-size: 13px;">
                                            Find the attached proposal below<br/>
                                            <a href="{{URL::To('/')}}/assets/backend/1.pdf" target="_blank"><span><i class="fa fa-file-pdf-o"></i> Proposal.pdf</span></a>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-12" style="padding: 10px 20px;">
                                    <ul class="list-unstyled msg_list">
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->