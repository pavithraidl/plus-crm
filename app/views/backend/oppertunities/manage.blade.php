<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-10 03:35:56.
 * View ID - 34
 */
 ?>

@extends('backend.master')

@section('styles')
    <!-- Include Editor style. -->
    {{HTML::style('/assets/backend/libs/text-editor/froala_editor.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/froala_style.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/code_view.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/colors.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/emoticons.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/image_manager.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/image.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/line_breaker.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/table.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/char_counter.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/video.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/fullscreen.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/quick_insert.css')}}
    {{HTML::style('/assets/backend/libs/text-editor/plugins/file.css')}}

    <!-- Ion.RangeSlider -->
    {{HTML::style('/assets/backend/libs/normalize-css/normalize.css')}}
    {{HTML::style('/assets/backend/libs/ion.rangeSlider/css/ion.rangeSlider.css')}}
    {{HTML::style('/assets/backend/libs/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css')}}

    {{HTML::style('/assets/backend/libs/text-editor/themes/gray.css')}}
@endsection

@section('content')
    @include('backend.oppertunities.includes.opportunity')
    @include('backend.oppertunities.includes.new-opportunity')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-slider"></i> Opportunity</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-sliders"></i> Manage <small>Opportunities</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="col-md-12">
                            <p style="text-align: right">
                                <button class="btn btn-success" type="button"  data-toggle="modal" data-target=".bs-new-client-modal-lg"><i class="fa fa-plus"></i> Add Opportunity</button>
                            </p>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="title_right" style="margin-top: -50px; margin-bottom: 10px;">
                                    <div class="top_search">
                                        <div class="input-group">
                                            <input id="property-table-search-text" type="text" onkeyup="tableSearchFilter(this);" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                        <button id="property-table-search-go" class="btn btn-default" type="button">Go!</button>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 client-container">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">OP000831</th>
                                    <td><a id="open-modal" href="#myModal" data-backdrop="false" data-toggle="modal">Facebook Add Campaign</a></td>
                                    <td onclick="$('#open-modal').trigger('click');">Devonport Pools Ltd</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Isuru L / 13 min ago</td>
                                </tr>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">OP000843</th>
                                    <td onclick="$('#open-modal').trigger('click');">Website</td>
                                    <td onclick="$('#open-modal').trigger('click');">Caprice Car Valet</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Dinesh T / 14 Jun</td>
                                </tr>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">OP000812</th>
                                    <td onclick="$('#open-modal').trigger('click');">SEO Promotion</td>
                                    <td onclick="$('#open-modal').trigger('click');">Whaley Harris Durney, ByPass, Buy & Sale</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Prince L / 21 Aug 16</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('/assets/backend/libs/text-editor/froala_editor.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/align.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/code_beautifier.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/code_view.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/colors.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/draggable.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/emoticons.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/font_size.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/font_family.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/image.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/file.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/image_manager.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/line_breaker.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/link.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/lists.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/paragraph_format.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/paragraph_style.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/video.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/table.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/url.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/entities.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/char_counter.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/inline_style.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/quick_insert.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/save.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/fullscreen.min.js')}}
    {{HTML::script('/assets/backend/libs/text-editor/plugins/quote.min.js')}}
    {{HTML::script('/assets/backend/libs/ion.rangeSlider/js/ion.rangeSlider.min.js')}}

    <script>
        $(".modal").draggable({
            handle: ".modal-header"
        });

        $('#description').froalaEditor({
            theme: 'gray'
        });

        $(document).ready(function () {
            $('#add-post-container').slideUp();
        });

        function slidePost() {
            $('#add-post-container').slideToggle(200);
        }
    </script>
@endsection
