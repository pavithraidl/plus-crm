<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-01 20:10:32.
 * View ID - 30
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/clients/agreement.css?v=1.3')}}
@endsection

@section('content')

    <?php
        $pageData = DB::table('temp_agreement_data3')->where('id', $agreementId)->first();
    ?>

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-file-text-o"></i> Service Agreement page 3</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> Service Order Sheet <small>Preview</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="full-form" id="full-form" style="background-color: #fff">
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="{{URL::To('/')}}/assets/backend/images/logo.png" />
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr>
                                            <td class="field">Business Name</td>
                                            <td id="top-business-name-td"><input type="text" onchange="addValue(this, 'business_name', 3);"  class="input-invisible" id="top-business-name" value="<?php if(isset($pageData)) echo($pageData->business_name) ?>" /> </td>
                                            <td class="field">Marketing Consultant</td>
                                            <td id="top-marketing-consultant-td"><input type="text" onchange="addValue(this, 'marketing_consultant', 3);"  class="input-invisible" id="top-marketing-consultant" value="<?php if(isset($pageData)) echo($pageData->marketing_consultant) ?>" /> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Tracking & Conversions</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Phone Tracking Type</td>
                                            <td class="data" style="padding: 0 10px;" colspan="3">
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk6" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->phone_tracking_geo == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Geo</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk7" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->phone_tracking_au == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">1300(AU)</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk8" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->phone_tracking_nz == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">0800(NZ)</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk9" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->phone_tracking_complex == 1) checked @endif @endif style="margin-left: 30px;" /> <span style="font-size: 11px !important;">Complex Routing</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Call Recording?</td>
                                            <td class="data"style="padding: 0 10px;" >
                                                <input type="radio" onclick="setupVariable(1);" id="rdo1" name="call-recording" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->call_recording == 1) checked @endif @endif  /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">On</span></span>
                                                <input type="radio" onclick="setupVariable(1);" id="rdo2" name="call-recording" data-parsley-mincheck="2" required class="flat" style="margin-left: 30px;" @if(isset($pageData)) @if($pageData->call_recording == 0) checked @endif @endif /> <span style="font-size: 11px !important;">Off</span>
                                            </td>
                                            <td class="field">Special Requirements</td>
                                            <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'special_requirements', 3);"  id="top-special-requirements" value="<?php if(isset($pageData)) echo($pageData->special_requirements) ?>" /> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Call Tracking Details</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field" colspan="2">Numbers on Website</td>
                                            <td class="field" colspan="2">Destination Numbers</td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'numbers_on_website1', 3);"  id="top-numbers-on-website-1" value="<?php if(isset($pageData)) echo($pageData->numbers_on_website1) ?>" /> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'destination_numbers1', 3);"  id="top-number-on-destination-1" value="<?php if(isset($pageData)) echo($pageData->destination_numbers1) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'numbers_on_website2', 3);"  id="top-numbers-on-website-2" value="<?php if(isset($pageData)) echo($pageData->numbers_on_website2) ?>" /> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'destination_numbers2', 3);"  id="top-number-on-destination-2" value="<?php if(isset($pageData)) echo($pageData->destination_numbers2) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'numbers_on_website3', 3);"  id="top-numbers-on-website-3" value="<?php if(isset($pageData)) echo($pageData->numbers_on_website3) ?>" /> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'destination_numbers3', 3);"  id="top-number-on-destination-3" value="<?php if(isset($pageData)) echo($pageData->destination_numbers3) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'numbers_on_website4', 3);"  id="top-numbers-on-website-4" value="<?php if(isset($pageData)) echo($pageData->numbers_on_website4) ?>" /> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, 'destination_numbers4', 3);"  id="top-number-on-destination-4" value="<?php if(isset($pageData)) echo($pageData->destination_numbers4) ?>" /> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Numbers to be Omitted from Tracking</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'number_to_be_omitted1', 3);"  class="input-invisible" id="top-numbers-to-be-omitted-1" value="<?php if(isset($pageData)) echo($pageData->number_to_be_omitted1) ?>" /> </td>
                                            <td class="field" style="background-color: #fff !important;"><input type="text" onchange="addValue(this, 'number_to_be_omitted2', 3);"  class="input-invisible" id="top-numbers-to-be-omitted-2" value="<?php if(isset($pageData)) echo($pageData->number_to_be_omitted2) ?>" /> </td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'number_to_be_omitted3', 3);"  class="input-invisible" id="top-numbers-to-be-omitted-3" value="<?php if(isset($pageData)) echo($pageData->number_to_be_omitted3) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Landing URL : http://</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'landing_url', 3);"  class="input-invisible" id="top-landing-url" value="<?php if(isset($pageData)) echo($pageData->landing_url) ?>" /> </td>
                                            <td class="field">Mobile site URL : http://</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'mobile_url', 3);"  class="input-invisible" id="top-mobile-site-url" value="<?php if(isset($pageData)) echo($pageData->mobile_url) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Online Conversions to Track</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'online_conversion_track1', 3);"  class="input-invisible" id="top-online-conversions-track-1" value="<?php if(isset($pageData)) echo($pageData->online_conversion_track1) ?>" /> </td>
                                            <td class="field" style="background-color: #fff !important;"><input type="text" onchange="addValue(this, 'online_conversion_track2', 3);"  class="input-invisible" id="top-online-conversions-track-2" value="<?php if(isset($pageData)) echo($pageData->online_conversion_track2) ?>" /> </td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'online_conversion_track3', 3);"  class="input-invisible" id="top-online-conversions-track-3" value="<?php if(isset($pageData)) echo($pageData->online_conversion_track3) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Existing Media Campaigns</td>
                                            <td class="data"style="padding: 0 10px;" >
                                                <input type="radio" onclick="setupVariable(1);" id="rdo3" name="media-campaigns" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->exsisting_media_campaigns == 1) checked @endif @endif  /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Yes</span></span>
                                                <input type="radio" onclick="setupVariable(1);" id="rdo4" name="media-campaigns" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->exsisting_media_campaigns == 0) checked @endif @endif  style="margin-left: 30px;" /> <span style="font-size: 11px !important;">No</span>
                                            </td>
                                            <td class="field">CIDs</td>
                                            <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'cids', 3);"  id="top-cids" value="<?php if(isset($pageData)) echo($pageData->cids) ?>" /> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Tracking & Conversions</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Website (CMS) Login Details</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'website_login_details', 3);"  class="input-invisible" id="top-website-cms-login" value="<?php if(isset($pageData)) echo($pageData->website_login_details) ?>" /> </td>
                                            <td class="field">Loging URL</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'logging_url', 3);"  class="input-invisible" id="top-login-url" value="<?php if(isset($pageData)) echo($pageData->logging_url) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'website_username', 3);"  class="input-invisible" id="top-username" value="<?php if(isset($pageData)) echo($pageData->website_username) ?>" /> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'website_password', 3);"  class="input-invisible" id="top-password" value="<?php if(isset($pageData)) echo($pageData->website_password) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">FTP Details</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'ftp_details', 3);"  class="input-invisible" id="top-ftp-details" value="<?php if(isset($pageData)) echo($pageData->ftp_details) ?>" /> </td>
                                            <td class="field">Logging URL</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'ftp_url', 3);"  class="input-invisible" id="top-loging-url-2" value="<?php if(isset($pageData)) echo($pageData->ftp_url) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'ftp_username', 3);"  class="input-invisible" id="top-user-name2" value="<?php if(isset($pageData)) echo($pageData->ftp_username) ?>" /> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'ftp_password', 3);"  class="input-invisible" id="top-password-2" value="<?php if(isset($pageData)) echo($pageData->ftp_password) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Domain Logins</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'domain_login', 3);"  class="input-invisible" id="top-domain-logins" value="<?php if(isset($pageData)) echo($pageData->domain_login) ?>" /> </td>
                                            <td class="field">Hosting Company</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'hosting_company', 3);"  class="input-invisible" id="top-hosting-company" value="<?php if(isset($pageData)) echo($pageData->hosting_company) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'domain_username', 3);"  class="input-invisible" id="top-user-name3" value="<?php if(isset($pageData)) echo($pageData->domain_username) ?>" /> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'domain_password', 3);"  class="input-invisible" id="top-password-3" value="<?php if(isset($pageData)) echo($pageData->domain_password) ?>" /> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Website Developer/Maintenance</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Company</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'company', 3);"  class="input-invisible" id="top-user-name3" value="<?php if(isset($pageData)) echo($pageData->company) ?>" /> </td>
                                            <td class="field">Contact Name</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'contact_name', 3);"  class="input-invisible" id="top-password-3" value="<?php if(isset($pageData)) echo($pageData->contact_name) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Contact Email</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'contact_email', 3);"  class="input-invisible" id="top-contact-email" value="<?php if(isset($pageData)) echo($pageData->contact_email) ?>" /> </td>
                                            <td class="field">Contact Number</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'contact_number', 3);"  class="input-invisible" id="top-contact-number" value="<?php if(isset($pageData)) echo($pageData->contact_number) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Has the client requested PlusMedia to add the code to their website?</td>
                                            <td class="data"style="padding: 0 10px;" colspan="2">
                                                <input type="radio" onclick="setupVariable(1);" id="rdo5" name="add-code" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->has_the_client == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Yes</span></span>
                                                <input type="radio" onclick="setupVariable(1);" id="rdo6" name="add-code" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->has_the_client == 0) checked @endif @endif style="margin-left: 30px;" /> <span style="font-size: 11px !important;">No</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12" style="padding-top: 5px; page-break-after: always; margin-top: 95px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="{{URL::To('/')}}/assets/backend/images/full-logo.png" />
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="editor"></div>
                        <p style="text-align: left; position: absolute;">
                            <a href="{{URL::To('/')}}/clients/agreements"><button class="btn btn-sm btn-success"><i class="fa fa-home"></i> Agreements Page </button></a>
                        </p>
                        <p style="text-align: right">
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement-page-2/{{ $agreementId }}" onclick="saveAgreement(3);"><button class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Previous Page </button></a>
                            <button id="save-pdf" class="btn btn-sm btn-info" onclick="getPrintHtml(3);"><i class="fa fa-print"></i> Print </button>
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement-page-4/{{ $agreementId }}" onclick="saveAgreement(3);"><button class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Next Page </button></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $agreementId }}" id="agreement-id" />
@endsection

@section('scripts')
    {{HTML::script('assets/js/clients/agreement.js?v=1.3')}}
@endsection