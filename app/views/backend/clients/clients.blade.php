<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-04 03:34:39.
 * View ID - 20
 */
 ?>

@extends('backend.master')

@section('content')

    @include('backend.clients.includes.client')
    @include('backend.clients.includes.new-client')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-folder-open"></i> Clients </h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-sliders"></i> Manage <small>Clients</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="col-md-12">
                            <p style="text-align: right">
                                <button class="btn btn-success" type="button"  data-toggle="modal" data-target=".bs-new-client-modal-lg"><i class="fa fa-plus"></i> Add Client</button>
                            </p>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="title_right" style="margin-top: -50px; margin-bottom: 10px;">
                                    <div class="top_search">
                                        <div class="input-group">
                                            <input id="property-table-search-text" type="text" onkeyup="tableSearchFilter(this);" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                        <button id="property-table-search-go" class="btn btn-default" type="button">Go!</button>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 client-container">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">CL000831</th>
                                    <td><a id="open-modal" href="#myModal" data-backdrop="false" data-toggle="modal">Richard</a></td>
                                    <td onclick="$('#open-modal').trigger('click');">Devonport Pools Ltd</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Isuru L / 13 min ago</td>
                                </tr>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">CL000843</th>
                                    <td onclick="$('#open-modal').trigger('click');">Steve</td>
                                    <td onclick="$('#open-modal').trigger('click');">Caprice Car Valet</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Dinesh T / 14 Jun</td>
                                </tr>
                                <tr>
                                    <th scope="row" onclick="$('#open-modal').trigger('click');">CL000812</th>
                                    <td onclick="$('#open-modal').trigger('click');">Harris Bryce</td>
                                    <td onclick="$('#open-modal').trigger('click');">Whaley Harris Durney, ByPass, Buy & Sale</td>
                                    <td align="right" onclick="$('#open-modal').trigger('click');">Prince L / 21 Aug 16</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(".modal").draggable({
            handle: ".modal-header"
        });
        $(document).ready(function () {
            $('#add-call-container').slideUp();
        });

        function slideCall() {
            $('#add-call-container').slideToggle(200);
        }
    </script>
    {{HTML::script('/assets/js/clients/client-manage.js')}}
@endsection
