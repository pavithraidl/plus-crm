<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-04 20:38:40.
 * View ID - 22
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/clients/agreement.css?v=1.3')}}
@endsection

@section('content')

    <?php
        $pageData = DB::table('temp_agreement_data1')->where('id', $agreementId)->first();
    ?>

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-file-text-o"></i> Service Agreement </h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> Service Order Sheet <small>Preview</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="full-form" id="full-form" style="background-color: #fff">
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="{{URL::To('/')}}/assets/backend/images/logo.png" />
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr>
                                            <td class="field">Company Name</td>
                                            <td colspan="3" id="top-company-name-td"><input type="text" onchange="addValue(this, 'company_name', 1);" class="input-invisible" id="top-company-name" value="<?php if(isset($pageData)) echo($pageData->company_name) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Client Name</td>
                                            <td class="data" id="top-client-name-td"><input type="text" class="input-invisible" onchange="addValue(this, 'client_name', 1);" id="top-client-name" value="<?php if(isset($pageData)) echo($pageData->client_name) ?>" /> </td>
                                            <td class="field">Mobile/Landline</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-mobile" onchange="addValue(this, 'mobile', 1);" value="<?php if(isset($pageData)) echo($pageData->mobile) ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address Line 1</td>
                                            <td class="data" id="top-address-line-1-data"><input type="text" class="input-invisible" onchange="addValue(this, 'adline1', 1);" id="top-address-line-1" value="<?php if(isset($pageData)) echo($pageData->adline1) ?>" /> </td>
                                            <td class="field">Account Phone</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-account-phone" onchange="addValue(this, 'account_phone', 1);" value="<?php if(isset($pageData)) echo($pageData->account_phone) ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address Line 2</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-address-line-2" onchange="addValue(this, 'adline2', 1);" value="<?php if(isset($pageData)) echo($pageData->adline2) ?>" /></td>
                                            <td class="field">Account Email</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-account-email" onchange="addValue(this, 'account_email', 1);" value="<?php if(isset($pageData)) echo($pageData->account_email) ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Email</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-email" onchange="addValue(this, 'email', 1);" value="<?php if(isset($pageData)) echo($pageData->email) ?>" /></td>
                                            <td class="field">Website http://</td>
                                            <td class="data"><input type="text" class="input-invisible" value="<?php if(isset($pageData)) echo($pageData->website) ?>" onchange="addValue(this, 'website', 1);" id="top-website" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered top-data">
                                        <tbody>
                                        <tr>
                                            <?php
                                                if(isset($pageData)) $pageData->target_start_date != '0000-00-00' ? $targetDate = $pageData->target_start_date : $targetDate = '';
                                            ?>
                                            <td class="field">Target start date</td>
                                            <td class="data"><input type="text" class="input-invisible show-date-picker" id="top-target-start-date" onchange="addValue(this, 'target_start_date', 1);" value="{{ $targetDate }}" /> </td>
                                            <td colspan="2" style="border-color: #fff;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row details-data-table">
                                <div class="col-md-12">
                                    <table class="table table-bordered details-table">
                                        <thead>
                                        <tr>
                                            <th class="td-services" style="background-color: #a01d1d;">Services</th>
                                            <th class="td-terms">Terms</th>
                                            <th class="td-budget">Monthly Budget</th>
                                            <th class="td-setup">Setup</th>
                                            <th class="td-total">1st Month Total</th>
                                            <th class="td-ongoing">Monthly Ongoing</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for($i = 0; $i < 6; $i++)
                                            <tr>
                                                <?php
                                                    $service = 'service'.($i+1);
                                                    $terms = 'terms'.($i+1);
                                                    $monthlyBudget = 'monthlybudget'.($i+1);
                                                    $setup = 'setup'.($i+1);
                                                    $monthTotal = 'month_total'.($i+1);
                                                    $ongoing = 'ongoing'.($i+1);

                                                ?>
                                                <td><input type="text" class="input-invisible" id="middle-services-{{ $i }}" onchange="addValue(this, '{{ $service }}', 1);" value="<?php if(isset($pageData)) echo($pageData->{$service}) ?>" /></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-{{ $i }}" onchange="addValue(this, '{{ $terms }}', 1);" value="<?php if(isset($pageData)) echo($pageData->{$terms}) ?>" /></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-{{ $i }}" onchange="addValue(this, '{{ $monthlyBudget }}', 1);" value="<?php if(isset($pageData)) echo($pageData->{$monthlyBudget}) ?>" /></td>
                                                <td class="middle-table-data-right"><input type="text"  class="input-invisible" id="middle-setup-{{ $i }}" onchange="addValue(this, '{{ $setup }}', 1); countTotal();" value="<?php if(isset($pageData)) echo($pageData->{$setup}) ?>" /></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-{{ $i }}" onchange="addValue(this, '{{ $monthTotal }}', 1); countTotal();" value="<?php if(isset($pageData)) echo($pageData->{$monthTotal}) ?>" /></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-{{ $i }}" onchange="addValue(this, '{{ $ongoing }}', 1); countTotal();" value="<?php if(isset($pageData)) echo($pageData->{$ongoing}) ?>" /></td>
                                            </tr>
                                        @endfor
                                        <tr>
                                            <td class="no-border" colspan="3"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="no-border bottom-title" colspan="3" style="text-align: right;">15% GST</td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-setup" disabled /></td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-total" disabled /> </td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-ongoing" disabled /></td>
                                        </tr>
                                        <tr>
                                            <td class="no-border bottom-title" colspan="3" style="border-right-color: #fff !important;">Total Monthly Cost</td>
                                            <td class="total-row" style="    opacity: 0; border-bottom-color: #fff !important;"><input type="text" class="input-total" id="total-setup" disabled /></td>
                                            <td class="total-row"><input type="text" class="input-total" id="total-total" disabled /></td>
                                            <td class="total-row"><input type="text" class="input-total" id="total-ongoing" disabled /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row check-button-section">
                                <div class="col-xs-12 check-section">
                                    <div class="col-xs-1">
                                        <input type="checkbox" onclick="setupVariable(1);" id="chk1" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->minimum_term == 1) checked @endif @endif />
                                    </div>
                                    <div class="col-xs-11">
                                        <strong>Minimum Term & Cancellation:</strong> <span style="font-size: 10px;"> You acknowledge that you have ordered PlusMedia's marketing service for at least the minimum number of cycles set out in the order summery above.
                                        The marketing services will be continued to be supplied after the Minimum Term until you cancel. As set out in the Marketing Terms & Conditions, if you give us writtern notice of the termination of your marketing services at least 7 days prior to the expiry of the Minimum Term then such termination will take effect on the expiry of the Minimum Term.
                                        if you give us written notice of termination less than 7 days before the expiry of the Minimum Term or at any time after the expiry of the Minimum Term, then such termination will take effect on the date of expiry of the first full cycle following the date of the termination
                                            notice.</span>
                                    </div>
                                </div>
                                <div class="col-xs-12" style="margin-top: 20px;">
                                    <div class="col-xs-1">
                                        <input type="checkbox" onclick="setupVariable(1);" id="chk2" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->call_recording == 1) checked @endif @endif />
                                    </div>
                                    <div class="col-xs-11">
                                        <strong>Call Recording:</strong> <span style="font-size: 10px;"> Client acknowledges and agrees that they have ordered the call recording service. The terms of the supply of the call recording service are governed by the Marketing Terms and Conditions.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row bottom-data-row">
                                <div class="col-xs-12 print-changes">
                                    <table class="table table-bordered bottom-data">
                                        <tbody>
                                        <tr>
                                            <td class="client-signature">Client Signature</td>
                                            <td class="client-signature-input"><input type="text" class="input-invisible" id="footer-client-signature" onchange="addValue(this, null, null);" /></td>
                                            <td class="client-date">Date</td>
                                            <?php
                                                if(isset($pageData)) $pageData->client_date != '0000-00-00' ? $clientDate = $pageData->client_date : $clientDate = '';
                                            ?>
                                            <td class="client-date-input"><input type="text" class="input-invisible show-date-picker" id="footer-client-date" onchange="addValue(this, 'client_date', 1);" value="{{ $clientDate }}" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td class="management-signature">Management Signature</td>
                                            <td class="management-signature-input"><input type="text" class="input-invisible" id="footer-management-signature" onchange="addValue(this, null, null);" /></td>
                                            <td class="management-date">Date</td>
                                            <?php
                                                if(isset($pageData)) $pageData->management_date != '0000-00-00' ? $managementDate = $pageData->management_date : $managementDate = '';
                                            ?>
                                            <td class="management-date-input"><input type="text" class="input-invisible show-date-picker" id="footer-management-date" onchange="addValue(this, 'management_date', 1);" value="{{ $managementDate }}" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12 footer-print-style" style="border-top: 1px solid #e0e0e0;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="{{URL::To('/')}}/assets/backend/images/full-logo.png" />
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="editor"></div>
                        <p style="text-align: left; position: absolute;">
                            <a href="{{URL::To('/')}}/clients/agreements"><button class="btn btn-sm btn-success"><i class="fa fa-home"></i> Agreements Page </button></a>
                        </p>
                        <p style="text-align: right">
                            {{--<button id="save-pdf" class="btn btn-sm btn-info" onclick="run(1, {{$agreementId}});"><i class="fa fa-print"></i> Print </button>--}}
                            <button id="save-pdf" class="btn btn-sm btn-info" onclick="getPrintHtml(1);"><i class="fa fa-print"></i> Print </button>
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement-page-2/{{ $agreementId }}" onclick="saveAgreement(1);"><button class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Next Page </button></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $agreementId }}" id="agreement-id" />
    <script>
        (function(){
            var widget,initAF = function(){
                widget = new AddressFinder.Widget(
                    document.getElementById('top-address-line-1'),
                    "NLMFV9YXKU3HT8B6RA7P",
                    'NZ',
                    {show_locations: false}
                );
                widget.on("result:select", function(fullAddress, metaData) {
                    var selected = new AddressFinder.NZSelectedAddress(fullAddress, metaData);
                    if(selected.suburb() == '') {
                        var adline2 = selected.city()+' '+selected.postcode();
                    }
                    else {
                        var adline2 = selected.suburb() + ', ' + selected.city()+' '+selected.postcode();
                    }

                    document.getElementById('top-address-line-1').value = selected.address_line_1()+ ' ' +selected.address_line_2();
                    document.getElementById('top-address-line-2').value = adline2;

                    $('#top-address-line-1').trigger('change');
                    $('#top-address-line-2').trigger('change');

                    // If you only have one address line
                    // document.getElementById('address_line').value = selected.address_line_1_and_2()

                    // document.getElementById('suburb').value =
                    // document.getElementById('city').value = selected.city()
                    // document.getElementById('postcode').value = selected.postcode()
                });
            };

            function downloadAF(f){
                var script = document.createElement('script');
                script.src = 'https://api.addressfinder.io/assets/v3/widget.js';
                script.async = true;
                script.onload=f;
                document.body.appendChild(script);
            };

            document.addEventListener("DOMContentLoaded", function () {
                downloadAF(initAF);
            });

        })();

        $(document).ready(function() {
            $('.show-date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
@endsection

@section('scripts')
    {{HTML::script('assets/js/clients/agreement.js?v=1.3')}}

@endsection



