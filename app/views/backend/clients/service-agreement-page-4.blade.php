<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-01 21:30:15.
 * View ID - 31
 */
 ?>
@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/clients/agreement.css?v=1.3')}}
@endsection

@section('content')

    <?php
        $pageData = DB::table('temp_agreement_data4')->where('id', $agreementId)->first();
    ?>

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-file-text-o"></i> Service Agreement page 4</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> Service Order Sheet <small>Preview</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="full-form" id="full-form" style="background-color: #fff">
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="{{URL::To('/')}}/assets/backend/images/logo.png" />
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Method of Payment</td>
                                        </tr>
                                        <tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;">
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk10" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->cheque == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Cheque (Enclosed)</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk11" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->visa == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Visa</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk12" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->mastercard == 1) checked @endif @endif /> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Mastercard</span></span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk13" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->american == 1) checked @endif @endif style="margin-right: 30px;" /> <span style="font-size: 11px !important; margin-right: 30px;">American Express</span>
                                                <input type="checkbox" onclick="setupVariable(1);" id="chk14" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->others == 1) checked @endif @endif style="margin-left: 30px;" /> <span style="font-size: 11px !important;">Others</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Credit/Debit Card Number (No Dash)</td>
                                            <td class="data" colspan="2"><input type="text" onchange="addValue(this, 'card_number', 4);" class="input-invisible" id="top-credit-debit-card-number" value="<?php if(isset($pageData)) echo($pageData->card_number) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Name as Printed on the Credit/Debit Card</td>
                                            <td class="data" colspan="2"><input type="text" onchange="addValue(this, 'card_name', 4);" class="input-invisible" id="top-credit-debit-card-name" value="<?php if(isset($pageData)) echo($pageData->card_name) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Expiration Date (Month & Year)</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_exp', 4);" class="input-invisible" id="top-expiration-date" value="<?php if(isset($pageData)) echo($pageData->card_exp) ?>" /> </td>
                                            <td class="field">CSV Number</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_csv', 4);" class="input-invisible" id="top-csv-number" value="<?php if(isset($pageData)) echo($pageData->card_csv) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="row-empty less" colspan="2" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td>
                                            <td class="row-empty less" colspan="2" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important; border-bottom-color: #fff;"></td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Credit/Debit Card Billing Address</td>
                                            <td class="data" colspan="2" style="border-top-color: #fff; border-right-color: #fff;"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_address', 4);" class="input-invisible" id="top-address" value="<?php if(isset($pageData)) echo($pageData->card_address) ?>" /> </td>
                                            <td class="field">City</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_city', 4);" class="input-invisible" id="top-city" value="<?php if(isset($pageData)) echo($pageData->card_city) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">State</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_state', 4);" class="input-invisible" id="top-state" value="<?php if(isset($pageData)) echo($pageData->card_state) ?>" /> </td>
                                            <td class="field">Zip Code</td>
                                            <td class="data"><input type="text" onchange="addValue(this, 'card_zip', 4);" class="input-invisible" id="top-zip-code" value="<?php if(isset($pageData)) echo($pageData->card_zip) ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2" style="font-size: 11px;">I authorize PlusMedia to Charge my Credit/Debit Card</td>
                                            <td class="field">Signature</td>
                                            <td class="data"></td>
                                        </tr>
                                        <tr>
                                            <td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; padding-bottom: 50px !important; height: 0 !important;">
                                                <p style="font-size: 11px;">Your Credit/Debit card will be charged the above stated amount upon receipt of your application. The charget will be reflected on your Credit/Debit card statement in the event</p>
                                            </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;For Bank Online Deposit</td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="4">
                                                <p style="font-size: 11px;"><strong>Please make the payment in to Bank Account No. <span style="color: #e4000e;">02-1244-0137403-00</span></strong></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12 footer-print" style="padding-top: 5px; margin-top: 510px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="{{URL::To('/')}}/assets/backend/images/full-logo.png" />
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 footer-normal-style">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="{{URL::To('/')}}/assets/backend/images/full-logo.png" />
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="editor"></div>
                        <p style="text-align: left; position: absolute;">
                            <a href="{{URL::To('/')}}/clients/agreements"><button class="btn btn-sm btn-success"><i class="fa fa-home"></i> Agreements Page </button></a>
                        </p>
                        <p style="text-align: right">
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement-page-3/{{ $agreementId }}" onclick="saveAgreement(4);"><button class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Previous Page </button></a>
                            <button id="save-pdf" class="btn btn-sm btn-info" onclick="getPrintHtml(4);"><i class="fa fa-print"></i> Print </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $agreementId }}" id="agreement-id" />
    <script>
        (function(){
            var widget,initAF = function(){
                widget = new AddressFinder.Widget(
                    document.getElementById('top-address'),
                    "NLMFV9YXKU3HT8B6RA7P",
                    'NZ',
                    {show_locations: false}
                );

                widget.on("result:select", function(fullAddress, metaData) {
                    var selected = new AddressFinder.NZSelectedAddress(fullAddress, metaData);
                    if(selected.suburb() == '') {
                        var city = selected.city();
                    }
                    else {
                        var city = selected.suburb();
                    }
                    console.log(selected);

                    document.getElementById('top-address').value = selected.address_line_1()+ ' ' +selected.address_line_2();
                    document.getElementById('top-city').value = selected.suburb()+ ' ' +selected.city();
                    document.getElementById('top-state').value = metaData.region;
                    document.getElementById('top-zip-code').value = selected.postcode();


                    $('#top-address').trigger('change');
                    $('#top-city').trigger('change');
                    $('#top-state').trigger('change');
                    $('#top-zip-code').trigger('change');


                    // If you only have one address line
                    // document.getElementById('address_line').value = selected.address_line_1_and_2()

                    // document.getElementById('suburb').value =
                    // document.getElementById('city').value = selected.city()
                    // document.getElementById('postcode').value = selected.postcode()
                });
            };

            function downloadAF(f){
                var script = document.createElement('script');
                script.src = 'https://api.addressfinder.io/assets/v3/widget.js';
                script.async = true;
                script.onload=f;
                document.body.appendChild(script);
            };

            document.addEventListener("DOMContentLoaded", function () {
                downloadAF(initAF);
            });

        })();
    </script>
@endsection

@section('scripts')
    {{HTML::script('assets/js/clients/agreement.js?v=1.3')}}
@endsection