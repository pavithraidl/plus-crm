<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/11/17 13:51.
 */
?>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 900px;">
            <div class="modal-header">
                <button type="button" class="minimize" aria-hidden="true" onclick="minimizeModal();">-</button>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Richard - Devnoport Pools Ltd</h4>

            </div>
            <div class="modal-body">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-building"></i> Businesses</a></li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-phone"></i> Call Log</a></li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-bullhorn"></i> Activity Log</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Name</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Mr. Richard Doe
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Phone</label>
                                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 7px;">
                                                    <a href="tel:0222647713">0800 231 2311</a><span style="margin-left: 10px; color: #d2d2d2; font-style: italic;">Work</span><br/>
                                                    <a href="tel:0222647713">022 264 7713</a><span style="margin-left: 10px; color: #d2d2d2; font-style: italic;">Home</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Email</label>
                                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 7px;">
                                                    <a href="mailto:isuru.l@plusmedia.co.nz">isuru.l@plusmedia.co.nz</a><span style="margin-left: 10px; color: #d2d2d2; font-style: italic;">Private</span><br/>
                                                    <a href="mailto:isuru.l@plusmedia.co.nz">accounts@plusmedia.co.nz</a><span style="margin-left: 10px; color: #d2d2d2; font-style: italic;">Work</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Address</label>
                                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 7px;">
                                                    10 Northcroft Street,<br/>
                                                    Takapuna,<br/>
                                                    Auckland 2646 New Zealand.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-horizontal form">
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Lead Source</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Gopher
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Lead State</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Open
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Expected Close Date</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    Today
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mm-new-client-fname">Call Back Date</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 7px;">
                                                    2 Days Ago
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab" style="padding: 15px 25px;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <!-- required for floating -->
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#home" data-toggle="tab">Business One</a></li>
                                            <li><a href="#profile" data-toggle="tab">Business Two</a></li>
                                        </ul>
                                    </div>

                                    <div class="col-xs-9">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="" role="tabpanel" data-example-id="togglable-tabs-busi">
                                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                                <li role="presentation" class="active"><a href="#tab_content-busi-1" id="home-tab-busi" role="tab" data-toggle="tab" aria-expanded="true">Info</a>
                                                                </li>
                                                                <li role="presentation" class=""><a href="#tab_content-busi-2" role="tab" id="profile-tab-busi" data-toggle="tab" aria-expanded="false">Opportunities</a>
                                                                </li>
                                                            </ul>
                                                            <div id="myTabContent" class="tab-content">
                                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content-busi-1" aria-labelledby="home-tab-busi">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            business info will be here...
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="tab_content-busi-2" aria-labelledby="profile-tab-busi">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="livicon-evo-button rounded success evo-md" style="float: right; margin-right: 30px;">
                                                                                <div class="livicon-evo" data-options="name: shoppingcart.svg; size: 24px; style: lines; strokeColor: #fff; eventOn: parent"></div>
                                                                                <span>New Opportunity</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" id="opportunities-container">
                                                                        <div class="col-md-12">
                                                                            <!-- start accordion -->
                                                                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                                                                <div class="panel">
                                                                                    <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                                        <h4 class="panel-title">Opportunity one</h4>
                                                                                    </a>
                                                                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                                        <div class="panel-body">
                                                                                            <div class="col-md-12 bs-callout bs-callout-info">
                                                                                                <div class="form-horizontal form">
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Oppertunity ID:</label>
                                                                                                        <div id="system" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            OPT000832
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">PlusMedia Package Type:</label>
                                                                                                        <div id="system-route" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            AdPro Mini
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Opportunity Name:</label>
                                                                                                        <div id="system-list-order" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            Beta
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Possibility:</label>
                                                                                                        <div id="system-list-order" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            100%
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Commencement Date:</label>
                                                                                                        <div id="system-controllers" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            17th Dec
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Close Date:</label>
                                                                                                        <div id="system-functions" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            16th Feb
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Created By:</label>
                                                                                                        <div id="system-created-by" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            Isuru L
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Created At:</label>
                                                                                                        <div id="system-created-at" class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 10px;">
                                                                                                            4 days Ago
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <p style="text-align: center;">
                                                                                                        <button class="btn btn-sm btn-info">Open Full Opportunity</button>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel">
                                                                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                        <h4 class="panel-title">Opportunity two</h4>
                                                                                    </a>
                                                                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                                        <div class="panel-body">
                                                                                            <p><strong>Collapsible Item 2 data</strong>
                                                                                            </p>
                                                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel">
                                                                                    <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                                        <h4 class="panel-title">Opportunity three</h4>
                                                                                    </a>
                                                                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                                        <div class="panel-body">
                                                                                            <p><strong>Collapsible Item 3 data</strong>
                                                                                            </p>
                                                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- end of accordion -->
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane" id="profile">
                                                oppertunity list will be here as well...
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <div class="col-md-12" id="add-call-container">
                                <div class="form-horizontal form">
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-pro-manager">Contact Person</label>
                                        <div class="col-md-7 col-sm-7 col-xs-12" tabindex="7">
                                            <select id="mm-pro-manager" class="form-control col-md-7 col-xs-12">
                                                <option value="">John Smith</option>
                                                <option value="">Allen Doe</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Description</label>
                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="livicon-evo-button rounded success evo-md" style="float: right; margin-right: 30px;" onclick="slideCall();">
                                <div class="livicon-evo" data-options="name: phone.svg; size: 24px; style: lines; strokeColor: #fff; eventOn: parent"></div>
                                <span>New Call</span>
                            </div>
                            <ul class="list-unstyled msg_list">
                                <li>
                                    <a>
                                        <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                          <i class="fa fa-info"></i>
                                        </span>
                                        <span>
                                            <span style="margin-left: 8px;">John Smith (022 264 7713)</span>
                                            <span class="time" style="right: 60px;">Isuru L / 3 mins ago</span>
                                        </span>
                                        <span class="message" style="margin-left: 50px;">
                                            Here you can add the entire discription about the call log. What you have discussed with the client, what was the end result and more.
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                          <i class="fa fa-info"></i>
                                        </span>
                                        <span>
                                            <span style="margin-left: 8px;">John Smith (022 264 7713)</span>
                                            <span class="time" style="right: 60px;">Isuru L / 3 mins ago</span>
                                        </span>
                                        <span class="message" style="margin-left: 50px;">
                                            Here you can add the entire discription about the call log. What you have discussed with the client, what was the end result and more.
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image" style="border-radius: 50%; border: 2px solid #d5ecff; padding: 8px 11px 8px 14px; font-size: 18px;">
                                          <i class="fa fa-info"></i>
                                        </span>
                                        <span>
                                            <span style="margin-left: 8px;">John Smith (022 264 7713)</span>
                                            <span class="time" style="right: 60px;">Isuru L / 3 mins ago</span>
                                        </span>
                                        <span class="message" style="margin-left: 50px;">
                                            Here you can add the entire discription about the call log. What you have discussed with the client, what was the end result and more.
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-12" style="padding: 10px 20px;">
                                    <ul class="list-unstyled msg_list">
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                        <span>
                                            <span style="margin-left: 8px;">Isuru L</span>
                                            <span class="time" style="right: 60px;">3 mins ago</span>
                                        </span>
                                                <span class="message" style="">
                                            Add a new opportunity - SEO blah balh.
                                        </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->