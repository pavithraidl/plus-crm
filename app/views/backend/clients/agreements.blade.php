<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-04 03:53:56.
 * View ID - 21
 */
 ?>

@extends('backend.master')

@section('content')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-file-text-o"></i> Service Agreements</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> Service agreement list </h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="col-md-12" style="overflow: auto;max-height: 500px;">
                            <p style="text-align: right">
                                <a href="{{URL::To('/')}}/clients/agreements/service-agreement/0"><button class="btn btn-sm btn-success"><i class="fa fa-plus"></i> New Agreement</button> </a>
                            </p>
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">ID </th>
                                        <th class="column-title">Company Name </th>
                                        <th class="column-title">Client Name </th>
                                        <th class="column-title"></th>
                                    </tr>
                                    </thead>

                                    <tbody id="agreement-table">
                                        <?php
                                            $agreementList = DB::table('temp_agreement_data1')->where('userid', Auth::user()->id)->where('status', 1)->lists('id');
                                        ?>
                                        @if(sizeof($agreementList) > 0)
                                            @foreach($agreementList as $agreementId)
                                            <tr id="agreement-row-{{ $agreementId }}">
                                                <td><a href="{{URL::To('/')}}/clients/agreements/service-agreement/{{ $agreementId }}"> {{ $agreementId }} </a></td>
                                                <td><a href="{{URL::To('/')}}/clients/agreements/service-agreement/{{ $agreementId }}"> {{ DB::table('temp_agreement_data1')->where('id', $agreementId)->pluck('company_name') }} </a></td>
                                                <td><a href="{{URL::To('/')}}/clients/agreements/service-agreement/{{ $agreementId }}"> {{ DB::table('temp_agreement_data1')->where('id', $agreementId)->pluck('client_name') }} </a></td>
                                                <td id="deleting-td-{{ $agreementId }}"><button class="btn btn-sm btn-info" style="height: 20px; padding-top: 0px;" onclick="printAgreement({{ $agreementId }});"><i class="fa fa-print"></i> Print</button>  <i class="fa fa-trash" style="color: #e4000e; font-size: 16px; cursor: pointer; float: right;" onclick="deleteAgreement({{ $agreementId }});"></i> </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td align="center" colspan="4">No agreements has created yet!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/js/clients/agreement.js')}}
@endsection
