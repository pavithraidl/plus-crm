<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/11/17 09:56.
 */
?>
@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/clients/agreement.css?v=1.3')}}
@endsection

@section('content')

    <?php
        $pageData = DB::table('temp_agreement_data2')->where('id', $agreementId)->first();
    ?>

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12 page-main-title">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-file-text-o"></i> Service Agreement page 2</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> Service Order Sheet <small>Preview</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="full-form" id="full-form" style="background-color: #fff">
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="{{URL::To('/')}}/assets/backend/images/logo.png" />
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                            <tr>
                                                <td class="field">Business Name</td>
                                                <td id="top-business-name-td"><input type="text" onchange="addValue(this, 'business_name', 2);" class="input-invisible" id="top-business-name" value="<?php if(isset($pageData)) echo($pageData->business_name); ?>" /> </td>
                                                <td class="field">Marketing Consultant</td>
                                                <td id="top-marketing-consultant-td"><input type="text" onchange="addValue(this, 'marketing_consultant', 2);" class="input-invisible" id="top-marketing-consultant" value="<?php if(isset($pageData)) echo($pageData->marketing_consultant); ?>" /> </td>
                                            </tr>
                                            <tr>
                                                <td class="field">Client Name</td>
                                                <td class="data" id="top-client-name-td"><input type="text" class="input-invisible" onchange="addValue(this, 'client_name', 2);" id="top-client-name" value="<?php if(isset($pageData)) echo($pageData->client_name); ?>" /> </td>
                                                <td class="field">Mobile/Landline</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-mobile" onchange="addValue(this, 'mobile', 2);" value="<?php if(isset($pageData)) echo($pageData->mobile); ?>" /></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Business Analysis</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Close Ratio</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'close_ratio', 2);" id="top-close-ratio" value="<?php if(isset($pageData)) echo($pageData->close_ratio); ?>" /> </td>
                                                <td class="field">Average Length of Sales</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-length-of-sales" onchange="addValue(this, 'average_length', 2);" value="<?php if(isset($pageData)) echo($pageData->average_length); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Customer Value ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-value" onchange="addValue(this, 'customer_value', 2);" value="<?php if(isset($pageData)) echo($pageData->customer_value); ?>" /></td>
                                                <td class="field">Average Gross Sale ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-gross-sale" onchange="addValue(this, 'average_gross_sale', 2);" value="<?php if(isset($pageData)) echo($pageData->average_gross_sale); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Effect of Seasonality on Revenue</td>
                                                <td class="data" colspan="3" style="border-right-color: #fff;"></td>

                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-customer-value"  onchange="addValue(this, 'effect_revenue', 2);"><?php if(isset($pageData)) echo($pageData->effect_revenue); ?></textarea></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Current Business Growth Plans</td>
                                                <td class="data" colspan="3" style="border-right-color: #fff;"></td>

                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-customer-value-2" onchange="addTextAreaValue(this, 'current_business_growth', 2);" ><?php if(isset($pageData)) echo($pageData->current_business_growth); ?></textarea></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Strategy</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">IP Targeting</td>
                                                <td class="data" style="padding: 0 10px;">
                                                    <input type="checkbox" onclick="setupVariable(1);" id="chk3" data-parsley-mincheck="2" required class="flat" @if(isset($pageData)) @if($pageData->ip_targeting_national == 1) checked @endif @endif /> <span style="margin-right: 50px;"><span style="font-size: 11px !important;">National</span></span>
                                                    <input type="checkbox" onclick="setupVariable(1);" id="chk4" data-parsley-mincheck="2" required class="flat" style="margin-left: 20px;"  @if(isset($pageData)) @if($pageData->ip_targeting_local == 1) checked @endif @endif /> <span style="font-size: 11px !important;">Local</span>
                                                </td>
                                                <td class="field">Address</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'address', 2);" id="top-address-line-1" value="<?php if(isset($pageData)) echo($pageData->address); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Primary Target Location</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'primarty_target_location', 2);" id="top-primary-target-location" value="<?php if(isset($pageData)) echo($pageData->primarty_target_location); ?>" /> </td>
                                                <td class="field">High Priority Target Areas</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-high-priority-target-location" onchange="addValue(this, 'high_priority_target_areas', 2);" value="<?php if(isset($pageData)) echo($pageData->high_priority_target_areas); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Areas/Locations(Not to target)</td>
                                                <td class="data" id="top-areas-locations-not-to-target-td"><input type="text" class="input-invisible" onchange="addValue(this, 'not_to_target_areas', 2);" id="top-areas-locations-not-to-target" value="<?php if(isset($pageData)) echo($pageData->not_to_target_areas); ?>" /> </td>
                                                <td class="field">Products/Services Promote</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-product-services-promote" onchange="addValue(this, 'product_promote', 2);" value="<?php if(isset($pageData)) echo($pageData->product_promote); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Keyword Exclusions</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'keyword_exclusions', 2);" id="top-keyword-exclutions" value="<?php if(isset($pageData)) echo($pageData->keyword_exclusions); ?>" /> </td>
                                                <td class="field">Any specific Terms</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-any-specific-terms" onchange="addValue(this, 'any_specific_terms', 2);" value="<?php if(isset($pageData)) echo($pageData->any_specific_terms); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Business Name Trademarked?</td>
                                                <td class="data" style="padding: 0 10px;" colspan="3">
                                                    <input type="checkbox" onclick="setupVariable(1);" id="chk5" data-parsley-mincheck="2" required class="flat"  @if(isset($pageData)) @if($pageData->business_name_trademark == 1) checked @endif @endif /> <span style="font-size: 11px !important;">Keyword Restrictions</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="field" colspan="2">Are There Any Elements From Your Adwords Campaign That You Want Us To Consider?</td>
                                                <td class="data" colspan="2" style="border-right-color: #fff;"></td>
                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-are-there-any-elements" onchange="addTextAreaValue(this, 'are_there_any_elements', 2);" ><?php if(isset($pageData)) echo($pageData->are_there_any_elements); ?> </textarea></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Objectives</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Key Objectives</td>
                                                <td class="data" colspan="3"><input type="text" class="input-invisible" id="top-key-objectives" onchange="addValue(this, 'key_bojects', 2);" value="<?php if(isset($pageData)) echo($pageData->key_bojects); ?>" /></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Estimated Contacts</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Contacts</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'contacts', 2);" id="top-contacts" value="<?php if(isset($pageData)) echo($pageData->contacts); ?>" /> </td>
                                                <td class="field">Campaign Budget ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-campaign-budget" onchange="addValue(this, 'campaign_budget', 2);" value="<?php if(isset($pageData)) echo($pageData->campaign_budget); ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Cost per Contact</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, 'cost_per_contact', 2);" id="top-cost-per-contact" value="<?php if(isset($pageData)) echo($pageData->cost_per_contact); ?>" /> </td>
                                                <td class="field">Target Start Date</td>
                                                <?php
                                                    if(isset($pageData)) $pageData->target_start_date != '0000-00-00' ? $targetStartDate = $pageData->target_start_date : $targetStartDate = '';
                                                ?>
                                                <td class="data"><input type="text" class="input-invisible show-date-picker" id="top-target-start-date" onchange="addValue(this, 'target_start_date', 2);" value="<?php if(isset($targetStartDate)) echo $targetStartDate; ?>" /></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4"></td></tr>
                                            <tr>
                                                <td class="field">Client Signature</td>
                                                <td class="data" colspan="3"><input type="text" class="input-invisible" id="top-client-signature" value="" /> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p style="font-size: 9px;">
                                        *Estimated Contacts is an estimated derived from historic PlusMedia data and is not a guarantee of actual campain performance. Several factors outside our control affect actual campaign performance. Including publisher and the quality of your website.
                                    </p>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12" style="margin-top: 90px; padding-top: 5px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="{{URL::To('/')}}/assets/backend/images/full-logo.png" />
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="editor"></div>
                        <p style="text-align: left; position: absolute;">
                            <a href="{{URL::To('/')}}/clients/agreements"><button class="btn btn-sm btn-success"><i class="fa fa-home"></i> Agreements Page </button></a>
                        </p>
                        <p style="text-align: right">
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement/{{ $agreementId }}" onclick="saveAgreement(2);"><button class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Previous Page </button></a>
                            <button id="save-pdf" class="btn btn-sm btn-info" onclick="getPrintHtml(2);"><i class="fa fa-print"></i> Print </button>
                            <a href="{{URL::To('/')}}/clients/agreements/service-agreement-page-3/{{ $agreementId }}"  onclick="saveAgreement(2);"><button class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Next Page </button></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $agreementId }}" id="agreement-id" />

    <script>
        $(document).ready(function() {
            $('.show-date-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <script>
        (function(){
            var initAF = function(){
                var widget = new AddressFinder.Widget(
                    document.getElementById('top-address-line-1'),
                    'NLMFV9YXKU3HT8B6RA7P',
                    'NZ'
                );
            };

            $( document ).ready(function() {
                $.getScript('https://api.addressfinder.io/assets/v3/widget.js', initAF);
            });

        })();
    </script>
@endsection

@section('scripts')
    {{HTML::script('assets/js/clients/agreement.js?v=1.3')}}

@endsection