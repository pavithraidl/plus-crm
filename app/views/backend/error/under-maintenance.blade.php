<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/11/17 09:13.
 */
?>


        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plus - Under Maintenance</title>
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/backend/images/logo-ico.png">

    <!-- Bootstrap -->
{{HTML::style('assets/backend/libs/bootstrap/dist/css/bootstrap.min.css')}}
<!-- Font Awesome -->
{{HTML::style('assets/backend/libs/font-awesome/css/font-awesome.min.css')}}
<!-- NProgress -->
{{HTML::style('assets/backend/libs/nprogress/nprogress.css')}}
<!-- Animate.css -->
{{HTML::style('assets/backend/libs/animate.css/animate.min.css')}}
<!-- Custom Theme Style -->
    {{HTML::style('assets/backend/css/custom.min.css')}}
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form role="form" action="{{ URL::route('post-login') }}" method="post" id="registerForm">
                    <h1>Under Maintenance!</h1>
                    <img src="{{URL::To('/')}}/assets/backend/images/error/under-maintanance.gif" style="width: 200px;" />
                    <h3 style="font-size: 14px;">Apologize about the inconvenience. We won't take long. We will be back soon.</h3>
                        <br />
                        <br/>
                        <div>
                            <h1><img src="{{URL::To('/')}}/assets/frontend/images/logo-w.png" style="width: 40px; height: auto; margin-top: -25px;" /> </i> Plus</h1>
                            <p>©{{date('Y')}} All Rights Reserved. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <form>
                    <h1>Create Account</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="email" class="form-control" placeholder="Email" required="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <a class="btn btn-default submit" href="index.html">Submit</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">Already a member ?
                            <a href="#signin" class="to_register"> Log in </a>
                        </p>

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                            <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
</html>
