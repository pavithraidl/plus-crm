<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/17 13:08.
 */
?>

@extends('backend.external-master')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <div class="mid_center">
                    <div id="active-confirm">
                        <h1>Oops!</h1><br/>
                        <h2>Your account has already activated or invalied link. Please try to login if you have an account.</h2><br/>
                        <a href="{{URL::Route('get-login')}}"> <button class="btn btn-lg btn-info">Login</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
