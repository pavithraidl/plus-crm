<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 17/09/17 12:12.
 */
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Property Manager | Login</title>

    <!-- Bootstrap -->
{{HTML::style('assets/backend/libs/bootstrap/dist/css/bootstrap.min.css')}}
<!-- Font Awesome -->
{{HTML::style('assets/backend/libs/font-awesome/css/font-awesome.min.css')}}
<!-- NProgress -->
{{HTML::style('assets/backend/libs/nprogress/nprogress.css')}}
<!-- Animate.css -->
{{HTML::style('assets/backend/libs/animate.css/animate.min.css')}}
<!-- Custom Theme Style -->
    {{HTML::style('assets/backend/css/custom.min.css')}}
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper" style="max-width: 550px;">
        <div class="animate form login_form">
            <section class="login_content">
                <p style="text-align: center">
                    <img src="{{URL::To('/')}}/assets/backend/images/error/robot-msg-error.png" style="width: 80%;"/>
                </p>
                <h3 style="font-size: 50px;">Oops!</h3>
                <h4 style="font-size: 15px;">Something went wrong. We will fix it soon...</h4>
                <a onclick="showAdvanced();" style="color: #d8d7d7; cursor: pointer;">Advanced</a>
                <div id="error-msg" style="display: none; margin-top: 30px;">
                    <p>{{$errorMsg}}</p>
                </div>
            </section>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function showAdvanced() {
        document.getElementById("error-msg").style.display = 'block';
    }
</script>
</html>