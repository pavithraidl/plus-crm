<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/06/17 16:39.
 */
?>

<div id="mm-add-new-controller" class="modal fade bs-new-controller-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="width: 75%;">
    <div class="modal-dialog modal-sm" style="width: 75%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Controller</h4>
            </div>
            <div id="mm-system-exception-body-container" class="modal-body" style="max-height: 270px;">
                <div class="col-md-12" style="overflow: auto;max-height: 220px;">
                    <div class="form-horizontal form" style="max-height: 220px; overflow-y: auto;">
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-controller-name">Controller Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-controller-name" placeholder="ExampleController" onblur="requiredValidator(this, 'Please fill the controller name');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-controller-lang">Lang:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-controller-lang" class="form-control col-md-7 col-xs-12">
                                    <option value="PHP" selected>PHP</option>
                                    <option value="JS" >JQuery</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-controller-description">Description:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-controller-description" placeholder="Controller Description" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 190px;">
                    <p style="text-align: center">
                        <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                        <button id="btn-as-close" onclick="addController();" type="button" class="btn btn-success" data-dismiss="modal" tabindex="13">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
