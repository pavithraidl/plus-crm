<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/06/17 16:39.
 */
?>

<div id="mm-add-system" class="modal fade bs-new-function-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 170%;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Function</h4>
            </div>
            <div id="mm-system-exception-body-container" class="modal-body" style="max-height: 325px;">
                <div class="col-md-12" style="overflow: auto;max-height: 250px;">
                    <div class="form-horizontal form" style="max-height: 240px; overflow-y: auto;">
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-function-type">Function Type:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-function-type" class="form-control col-md-7 col-xs-12" onchange="checkFunctionTypeStatus();">
                                    <option value="1" selected>Ajax</option>
                                    <option value="2">PHP</option>
                                    <option value="3">JS</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-function-name-name">Function Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-function-name" placeholder="getData" onblur="requiredValidator(this, 'Please fill the function name');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div  id="mm-sys-ajax-method-container">
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-ajax-method">Ajax Method:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                    <select id="mm-sys-ajax-method" class="form-control col-md-7 col-xs-12">
                                        <option value="1" selected>GET</option>
                                        <option value="2">POST</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-js-script">JS Script:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="mm-sys-js-script" class="form-control col-md-7 col-xs-12">
                                        {{--JQuery Append--}}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-php-controller">PHP Controller:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="mm-sys-php-controller" class="form-control col-md-7 col-xs-12">
                                        {{--JQuery Append--}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-controller-description">Description:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-function-description" placeholder="Controller Description" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 250px;">
                    <p style="text-align: center">
                        <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                        <button id="btn-as-close" onclick="addFunction();" type="button" class="btn btn-success" data-dismiss="modal" tabindex="13">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
