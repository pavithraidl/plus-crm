<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/06/17 16:39.
 */
?>

<div id="mm-add-new-view" class="modal fade bs-new-view-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="width: 75%;">
    <div class="modal-dialog modal-sm" style="width: 50%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New View</h4>
            </div>
            <div id="mm-system-view-body-container" class="modal-body" style="max-height: 460px;">
                <div class="col-md-12" style="overflow: auto;max-height: 380px;">
                    <div class="form-horizontal form" style="max-height: 380px; overflow-y: auto;">
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-name">View Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-view-name" placeholder="ExampleController" onblur="requiredValidator(this, 'Please fill the view name');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-url">View Url:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-view-url" placeholder="manage" onblur="requiredValidator(this, 'Please fill the url for the view');" maxlength="128" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-controller-lang">PHP Controller:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-view-controllers" class="form-control col-md-7 col-xs-12">
                                    {{-- JQuery Append --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-allow-default">Allow for Public:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-view-allow-default" class="form-control col-md-7 col-xs-12">
                                    <option value="0">Under access controller</option>
                                    <option value="1">Public (available for everyone)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-list-order">List Order:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-view-list-order" class="form-control col-md-7 col-xs-12">
                                    @for($i = 1; $i < 20; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-name">View Icon:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-view-icon" placeholder="Ex: fa-user" onblur="requiredValidator(this, 'Please add a icon');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-show-menu">Show Main Menu:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-view-show-menu" class="form-control col-md-7 col-xs-12">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-view-description">Description:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-view-description" placeholder="View Description" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 380px;">
                    <p style="text-align: center">
                        <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                        <button id="btn-as-close" onclick="addView();" type="button" class="btn btn-success" data-dismiss="modal" tabindex="13">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
