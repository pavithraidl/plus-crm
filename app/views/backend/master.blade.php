<?php
/************************| master-dark.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 10:43.
 */
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/backend/images/logo-ico.png">
    <title>Plus - {{ System::where('id', $systemId)->pluck('name') }} {{$systemId}}</title>

    <!-- Bootstrap -->
{{HTML::style('assets/backend/libs/bootstrap/dist/css/bootstrap.min.css')}}
<!-- Font Awesome -->
{{HTML::style('assets/backend/libs/font-awesome/css/font-awesome.min.css')}}
<!-- NProgress -->
{{HTML::style('assets/backend/libs/nprogress/nprogress.css')}}
<!-- iCheck -->
{{HTML::style('assets/backend/libs/iCheck/skins/flat/green.css')}}
<!-- bootstrap-progressbar -->
{{HTML::style('assets/backend/libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}
<!-- JQVMap -->
{{HTML::style('assets/backend/libs/jqvmap/dist/jqvmap.min.css')}}
<!-- bootstrap-daterangepicker -->
{{HTML::style('assets/backend/libs/bootstrap-daterangepicker/daterangepicker.css')}}
{{--Notify--}}
{{HTML::style('assets/backend/libs/pnotify/dist/pnotify.css')}}
{{HTML::style('assets/backend/libs/pnotify/dist/pnotify.buttons.css')}}
{{HTML::style('assets/backend/libs/pnotify/dist/pnotify.nonblock.css')}}

    {{--Datepicker--}}
{{HTML::style('assets/backend/libs/datepicker/themes/classic.css')}}
{{HTML::style('assets/backend/libs/datepicker/themes/classic.date.css')}}
{{HTML::style('assets/backend/libs/datepicker/themes/classic.time.css')}}

    {{--Livicons--}}
{{HTML::style('assets/backend/libs/liveicons-evo/css/LivIconsEvo.css')}}

    {{--Image Uploader--}}
{{HTML::script('/assets/backend/libs/jquery/jquery-1.11.1.min.js')}}
{{ HTML::script('assets/backend/libs/jquery-forms/jquery.form.js')}}
{{HTML::script('assets/backend/js/custom/public/image-uploader.js')}}
{{HTML::script('assets/backend/js/custom/public/constant.js')}}

{{--IOS Switch--}}
{{HTML::style('assets/backend/libs/ios7-switch/ios7-switch.css')}}

<!-- Custom Theme Style -->
    {{HTML::style('assets/backend/css/custom.css')}}

    {{--Custom pagebuild styles--}}
    @yield('styles')
    {{--End custom pagebuild styles--}}
</head>
<?php
        $userId = Auth::user()->id;
    $profilePic = URL::To('/').'/assets/backend/images/users/'.$userId.'/user-35.jpg';
    try {
        getimagesize($profilePic);
    }
    catch(Exception $ex) {
        $profilePic = URL::To('/').'/assets/backend/images/users/default/user-35.jpg';
    }
?>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{URL::Route('admin:dashboard')}}" class="site_title"><img style="width: 35px; height: auto; background-color: #fff; border-radius: 50%; box-shadow: inset 12px 12px 3px rgba(0, 0, 0, 0.3);" src="{{URL::To('/')}}/assets/backend/images/logo.png" /> <span>Plus</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{$profilePic}}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{Auth::user()->fname}} {{Auth::user()->lname}}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br /><br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="margin-top: 50px;">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <?php $systemList = System::where('status', 1)->where('visibility', 1)->orderBy('listorder')->lists('id'); ?>
                            @foreach($systemList as $menuSystemId)
                                {{--Check whether the system is single or multiple views--}}
                                <?php
                                    $viewCount = SystemView::where('systemid', $menuSystemId)->where('status', '!=', 0)->where('show_menu', 1)->count();
                                ?>
                                @if($viewCount > 1)
                                    <?php
                                        $viewList = SystemView::where('systemid', $menuSystemId)->where('status', '!=', 0)->where('show_menu', 1)->lists('id');
                                        $systemName = System::where('id', $menuSystemId)->pluck('name');
                                        $icon = System::where('id', $menuSystemId)->pluck('icon');
                                        unset($subMenuList);
                                    ?>
                                    <!--Check view access-->
                                    @foreach($viewList as $viewId)
                                        @if(SystemViewAccess::where('viewid', $viewId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id')
                                            || SystemView::where('id', $viewId)->where('allow_default', 1)->pluck('id')
                                            || Auth::user()->roll < 2
                                            )
                                            <?php
                                            $viewName = SystemView::where('id', $viewId)->pluck('menu_name');
                                            $viewIcon = SystemView::where('id', $viewId)->pluck('icon');
                                            $urlRouteId = SystemView::where('id', $viewId)->pluck('url_route');
                                            $href = SystemUrlRoutes::where('id', $urlRouteId)->pluck('url');

                                            $subMenuList[] = array(
                                                'name' => $viewName,
                                                'icon' => $viewIcon,
                                                'href' => $href
                                            );
                                            ?>
                                        @endif
                                    @endforeach
                                    @if(isset($subMenuList))
                                        <li><a><i class="fa {{ $icon }}"></i> {{ $systemName }} <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                @foreach($subMenuList as $subMenu)
                                                    <li><a href="{{URL::To('/')}}/{{ $subMenu['href'] }}"> {{ $subMenu['name'] }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @else
                                    {{--Get the view details--}}
                                    <?php
                                        $viewId = SystemView::where('systemid', $menuSystemId)->where('status', 1)->where('show_menu', 1)->pluck('id');
                                        $systemName = System::where('id', $menuSystemId)->pluck('name');
                                        $icon = System::where('id', $menuSystemId)->pluck('icon');
                                        $urlRouteId = SystemView::where('id', $viewId)->pluck('url_route');
                                        $href = SystemUrlRoutes::where('id', $urlRouteId)->pluck('url');
                                    ?>
<!--                                    Check view access-->
                                    @if(SystemViewAccess::where('viewid', $viewId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id')
                                        || SystemView::where('id', $viewId)->where('allow_default', 1)->pluck('id')
                                        || Auth::user()->roll < 2
                                        )
                                        <li><a href="{{ URL::to('/') }}/{{ $href }}"><i class="fa {{ $icon }}"></i> {{ $systemName }} </a></li>
                                    @endif
                                @endif
                            @endforeach

                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{ $profilePic }}" alt="">{{Auth::user()->fname}} {{Auth::user()->lname}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                {{--<li><a href="javascript:;"> Profile</a></li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:;">--}}
                                        {{--<span class="badge bg-red pull-right">50%</span>--}}
                                        {{--<span>Settings</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li><a href="javascript:;">Help</a></li>--}}
                                <li><a href="{{URL::Route('log-out')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        {{--<li role="presentation" class="dropdown">--}}
                            {{--<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="fa fa-globe"></i>--}}
                                {{--<span class="badge bg-green">6</span>--}}
                            {{--</a>--}}
                            {{--<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">--}}
                                {{--<li>--}}
                                    {{--<a>--}}
                                        {{--<span class="image"><img src="{{URL::To('/')}}/assets/backend/images/user.jpg" alt="Profile Image" /></span>--}}
                                        {{--<span>--}}
                          {{--<span>Isuru Liyanage</span>--}}
                          {{--<span class="time">3 mins ago</span>--}}
                        {{--</span>--}}
                                        {{--<span class="message">--}}
                          {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a>--}}
                                        {{--<span class="image"><img src="{{URL::To('/')}}/assets/backend/images/user.jpg" alt="Profile Image" /></span>--}}
                                        {{--<span>--}}
                          {{--<span>Isuru Liyanage</span>--}}
                          {{--<span class="time">3 mins ago</span>--}}
                        {{--</span>--}}
                                        {{--<span class="message">--}}
                          {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a>--}}
                                        {{--<span class="image"><img src="{{URL::To('/')}}/assets/backend/images/user.jpg" alt="Profile Image" /></span>--}}
                                        {{--<span>--}}
                          {{--<span>Isuru Liyanage</span>--}}
                          {{--<span class="time">3 mins ago</span>--}}
                        {{--</span>--}}
                                        {{--<span class="message">--}}
                          {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a>--}}
                                        {{--<span class="image"><img src="{{URL::To('/')}}/assets/backend/images/user.jpg" alt="Profile Image" /></span>--}}
                                        {{--<span>--}}
                          {{--<span>Isuru Liyanage</span>--}}
                          {{--<span class="time">3 mins ago</span>--}}
                        {{--</span>--}}
                                        {{--<span class="message">--}}
                          {{--Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="text-center">--}}
                                        {{--<a>--}}
                                            {{--<strong>See All Alerts</strong>--}}
                                            {{--<i class="fa fa-angle-right"></i>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        {{--Content connects to here--}}
            @yield('content')
        {{--Content end from here--}}

    <!-- footer content -->
        <footer>
            <div class="pull-right">
                IDL Creations
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
{{--{{HTML::script('assets/backend/libs/jquery/dist/jquery.min.js')}}--}}
<!-- Bootstrap -->
{{HTML::script('assets/backend/libs/bootstrap/dist/js/bootstrap.min.js')}}
{{--JQuery UI--}}
{{HTML::script('assets/backend/libs/jquery-ui/jquery-ui.js')}}
<!-- FastClick -->
{{HTML::script('assets/backend/libs/fastclick/lib/fastclick.js')}}
<!-- NProgress -->
{{HTML::script('assets/backend/libs/nprogress/nprogress.js')}}
<!-- bootstrap-progressbar -->
{{HTML::script('assets/backend/libs/bootstrap-progressbar/bootstrap-progressbar.min.js')}}
<!-- iCheck -->
{{HTML::script('assets/backend/libs/icheck-1/icheck.js')}}
<!-- Skycons -->
{{HTML::script('assets/backend/libs/skycons/skycons.js')}}
<!-- Flot -->
{{HTML::script('assets/backend/libs/Flot/jquery.flot.js')}}
{{HTML::script('assets/backend/libs/Flot/jquery.flot.pie.js')}}
{{HTML::script('assets/backend/libs/Flot/jquery.flot.time.js')}}
{{HTML::script('assets/backend/libs/Flot/jquery.flot.stack.js')}}
{{HTML::script('assets/backend/libs/Flot/jquery.flot.resize.js')}}
<!-- Flot plugins -->
{{HTML::script('assets/backend/libs/flot.orderbars/js/jquery.flot.orderBars.js')}}
{{HTML::script('assets/backend/libs/flot-spline/js/jquery.flot.spline.min.js')}}
{{HTML::script('assets/backend/libs/flot.curvedlines/curvedLines.js')}}
<!-- DateJS -->
{{HTML::script('assets/backend/libs/DateJS/build/date.js')}}
{{HTML::script('assets/backend/libs/jqvmap/examples/js/jquery.vmap.sampledata.js')}}
<!-- bootstrap-daterangepicker -->
{{HTML::script('assets/backend/libs/moment/min/moment.min.js')}}
{{HTML::script('assets/backend/libs/bootstrap-daterangepicker/daterangepicker.js')}}

{{--Date Picker--}}
{{HTML::script('assets/backend/libs/datepicker/legacy.js')}}
{{HTML::script('assets/backend/libs/datepicker/picker.js')}}
{{HTML::script('assets/backend/libs/datepicker/picker.date.js')}}
{{HTML::script('assets/backend/libs/datepicker/picker.time.js')}}

<!-- Custom Theme Scripts -->
{{HTML::script('assets/backend/js/custom.min.js')}}
{{HTML::script('assets/backend/js/custom/public/public.functions.js')}}
{{HTML::script('assets/backend/js/custom/public/validator.js')}}

{{--Notiify--}}
{{HTML::script('assets/backend/libs/pnotify/dist/pnotify.js')}}
{{HTML::script('assets/backend/libs/pnotify/dist/pnotify.buttons.js')}}
{{HTML::script('assets/backend/libs/pnotify/dist/pnotify.nonblock.js')}}

{{--IOS Switch--}}
{{HTML::script('/assets/backend/libs/ios7-switch/ios7.switch.js')}}

{{--Canvas print--}}
{{HTML::script('/assets/backend/libs/canvas/html2canvas.min.js')}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>

{{--Livicons--}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/snap.svg-min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/TweenMax.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/DrawSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/MorphSVGPlugin.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/tools/verge.min.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.defaults.js')}}
{{HTML::script('assets/backend/libs/liveicons-evo/js/LivIconsEvo.min.js')}}

@yield('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //ICHECK
        $('input:not(.ios-switch)').iCheck({
            checkboxClass: 'icheckbox_square-aero',
            radioClass: 'iradio_square-aero',
            increaseArea: '20%' // optional
        });

// IOS7 SWITCH
        $(".ios-switch").each(function(){
            mySwitch = new Switch(this);
        });
    });

//    Date picker
    $('.datepicker').pickadate();
    $('.timepicker').pickatime({
        min: new Date(2015,3,20,7),
        max: new Date(2015,7,14,18,30)
    });
</script>

</body>
</html>

