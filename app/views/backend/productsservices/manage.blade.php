<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-05 22:06:59.
 * View ID - 33
 */
 ?>

@extends('backend.master')

@section('content')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-sliders"></i> Manage</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
            
            <!-- new html here -->
            <h2>We are working on this! </h2>
            
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- scripts here... -->
@endsection
