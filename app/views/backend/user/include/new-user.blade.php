<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/01/17 08:10.
 */
?>

<div id="model-add-new-user" class="modal fade bs-add-user-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button id="user-modal-close" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New User</h4>
            </div>
            <div id="modal-add-user-body" class="modal-body" style="padding: 20px 30px;">
                <div class = "row">
                    <div class = "form-group">
                        <label>Name</label>
                        <input id = "mm-user-name" onblur="requiredValidator(this, 'Please enter the user\'s name.');" onkeyup="requiredValidator(this, 'Please enter the user\'s name.');" type = "text" class = "form-control" placeholder = "Jone" maxlength = "32" required tabindex="1">
                    </div>
                    <div class = "form-group">
                        <label>Job Title</label>
                        <input id = "mm-user-job-title" onblur="requiredValidator(this, 'Please enter the user\'s job title.');" onkeyup="requiredValidator(this, 'Please enter the user\'s job title.');" type = "text" class = "form-control" placeholder="Head of Sales" maxlength = "32" required tabindex="2">
                    </div>
                    <div class="form-group">
                        <label>Department:</label>
                        <select id="mm-user-department" class="form-control">
                            <option value="1">Sales Team</option>
                            <option value="2">Digital Crew</option>
                            <option value="4">Accounts Department</option>
                        </select>
                    </div>
                    <div class = "form-group">
                        <label>Email</label>
                        <input id = "mm-user-email" onblur="requiredValidator(this, 'Please enter the email address.')" onkeyup="emailValidator(this, 'Invalid email address.');" type = "email" class = "form-control" placeholder = "someone&#64;example.com" maxlength = "60" required tabindex="3">
                    </div>
                    <p style = "text-align: center; margin-top: 40px;">
                        <button id = "mm-btn-user-cancel" onclick="cancelUser();" class = "btn btn-danger md-close">Cancel</button>
                        <button id = "mm-btn-user-add" onclick="addUser();" type = "button" class = "btn btn-primary" tabindex="4">Request</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div><!-- End div .md-modal .md-3d-flip-vertical-->

