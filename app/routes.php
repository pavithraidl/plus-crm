<?php

$checkVariables = new VariableController();
$checkVariables ->checkDb();
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//region -----| Frontend |-------
//----------------------------------------------------------------------------------------------------------------------
Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@getHome'
));
Route::get('/about', array(
    'as' => 'about',
    'uses' => 'HomeController@getAbout'
));

Route::get('/services', array(
    'as' => 'services',
    'uses' => 'HomeController@getServices'
));

Route::get('/service/seo', array(
    'as' => 'service-seo',
    'uses' => 'HomeController@getServiceSeo'
));

Route::get('/service/web', array(
    'as' => 'service-web',
    'uses' => 'HomeController@getServiceWeb'
));

Route::get('/service/ad-pro', array(
    'as' => 'service-ad-pro',
    'uses' => 'HomeController@getServiceAdPro'
));

Route::get('/service/display-ads', array(
    'as' => 'service-display-ads',
    'uses' => 'HomeController@getServiceDisplayAds'
));

Route::get('/service/ad-clear', array(
    'as' => 'service-ad-clear',
    'uses' => 'HomeController@getServiceAdClear'
));

Route::get('/service/social-media', array(
    'as' => 'service-social-media',
    'uses' => 'HomeController@getServiceSocialMedia'
));

Route::get('/service/telecommunications', array(
    'as' => 'service-telecommunications',
    'uses' => 'HomeController@getServiceTelecommunication'
));

Route::get('/contact', array(
    'as' => 'contact',
    'uses' => 'HomeController@getContact'
));

Route::post('contact/contact-form', array(
    'as' => 'contact-form',
    'uses' => 'HomeController@submitContact'
));

Route::get('test/email', function () {
    return View::make('backend.emails.enquiry');
});

Route::post('/register-client', array(
    'as' => 'register',
    'uses' => 'HomeController@registerClient'
));
//endregion



//region -----| backend |--------
//----------------------------------------------------------------------------------------------------------------------
Route::group( array( 'before' => 'auth' ), function () {

    //region ** System URL Routs **
    /*========================================================================================================*/
    /*========================================================================================================*/
    $routeList = DB::table('system_url_routes')->where('status', 1)->lists('id');
    foreach($routeList as $id){
        if(SystemUrlRoutes::where('id', $id)->pluck('type') == 'get'){
            Route::get(SystemUrlRoutes::where('id', $id)->pluck('url'), array(
                'as' => SystemUrlRoutes::where('id', $id)->pluck('routeas'),
                'uses' => SystemUrlRoutes::where('id', $id)->pluck('uses')
            ));
        }
        else{
            Route::post(SystemUrlRoutes::where('id', $id)->pluck('url'), array(
                'as' => SystemUrlRoutes::where('id', $id)->pluck('routeas'),
                'uses' => SystemUrlRoutes::where('id', $id)->pluck('uses')
            ));
        }
    }
    /*-------------------------------------------------------------------------------------------------------*/
    //endregion



    Route::get('/system/manage', array(
        'as' => 'system',
        'uses' => 'SystemController@getManage'
    ));

    Route::post('/system/change-system-status', array(
        'as' => 'system:change-system-status',
        'uses' => 'SystemController@changeStatus'
    ));

//    Route::get('/admin', array(
//        'as' => 'admin:dashboard',
//        'uses' => 'DashboardController@getView'
//    ));
    Route::get('/admin', function () {
        return Redirect::intended('clients/agreements');
    });

    Route::post( '/users/onlineState', array(
        'as'   => 'online-state',
        'uses' => 'UserController@onlineState'
    ));

    Route::get( '/logout', array(
        'as'   => 'log-out',
        'uses' => 'UserController@getLogOut'
    ));

    Route::get( 'client/get-single-client', function () {
        return View::make('backend.clients.append.single-client');
    });


    Route::get('/system/test', function () {
        return View::make('backend.error.under-maintenance');

    });
});




Route::group( array( 'before' => 'guest' ), function () {
    Route::get('/active/{code}', array(
        'as' => 'active',
        'uses' => 'UserController@getActivate'
    ));

    Route::post('active/active-account', array(
        'as' => 'post-active',
        'uses' => 'UserController@postActive'
    ));

    Route::get('login', array(
        'as' => 'get-login',
        'uses' => 'UserController@getLogin'
    ));

    Route::post('login', array(
        'as' => 'post-login',
        'uses' => 'UserController@postLogin'
    ));

    App::missing(function($exception)
    {
        try{
            $t = Auth::user()->id;
            return Response::view('frontend.404',
                array(
                    'page' => 404,
                    'title' => '',
                    'metaDescription' => '',
                    'h1' => ''
                ), 404);
        }
        catch(Exception $ex){
            return Response::view('frontend.404',
                array(
                    'page' => 404,
                    'title' => '',
                    'metaDescription' => '',
                    'h1' => ''
                ), 404);
        }
    });

});

//endregion