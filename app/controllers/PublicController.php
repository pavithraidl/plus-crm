<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {9/04/17} {11:22}.
 */
 
class PublicController extends BaseController {

    public function updateSingleField() {
        $table = Input::get('table');
        $where = Input::get('where');
        $field = Input::get('field');
        $value = Input::get('value');
        $activityLog = Input::get('activity');
        $id = Input::get('id');

        try {
            $oldValue = DB::table($table)->where($where, $id)->pluck($field);
            $execute = DB::table($table)->where($where, $id)->update(array(
                $field => $value
                )
            );

            if($table == 'property') {
                $propertyId = $id;
                $inspectionId = null;
            }
            else if($table == 'inspection_schedule') {
                $propertyId = InspectionSchedule::where('id', $id)->pluck('propertyid');
                $inspectionId = InspectionSchedule::where('id', $id)->pluck('inspectionid');
            }
            else {
                $propertyId = null;
                $inspectionId = null;
            }

            if($execute) {
                $activity = new UserController();
                $activity -> saveActivity('Updated '.$activityLog.' from '.$oldValue.' to '.$value, $propertyId, $inspectionId);
            }

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PublicController', 'updateSingleField', $ex);
            return $ex;
        }
    }
}