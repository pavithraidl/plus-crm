<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getHome()
	{
		return View::make('frontend.default', array(
		    'title' => 'Leading Digital Marketing Agency in Auckland, NZ',
            'metaDescription' => 'PlusMedia offers high quality digital marketing services such SEO, Adwords, Web Design, Web Development, Social Media and more.',
            'h1' => 'Auckland’s Leading Digital Marketing Agency'
        ));
	}

	public function getAbout() {
	    return View::make('frontend.about', array(
	        'page' => 'about',
            'quote' => 'PlusMedia empowers your excel in todays\'s digital marketplace',
            'title' => 'Leading Digital Marketing Agency in Auckland, NZ',
            'metaDescription' => 'PlusMedia offers high quality digital marketing services such SEO, Adwords, Web Design, Web Development, Social Media and more.',
            'h1' => 'About PlusMedia, Auckland’s Leading Digital Marketing Agency'
        ));
    }

    public function getServices() {
	    return View::make('frontend.service', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'PlusMedia - Auckland’s Leading SEO Company',
            'metaDescription' => 'Looking to get to the top of Google’s search results? PlusMedia provides high quality SEO services for small & large businesses in NZ.',
            'h1' => 'Auckland’s Top SEO Company'
        ));
    }

    public function getServiceSeo() {
	    return View::make('frontend.service', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'PlusMedia - Auckland’s Leading SEO Company',
            'metaDescription' => 'Looking to get to the top of Google’s search results? PlusMedia provides high quality SEO services for small & large businesses in NZ.',
            'h1' => ''
        ));
    }

    public function getServiceWeb() {
	    return View::make('frontend.service-web', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'Web Design and Web Development in Auckland',
            'metaDescription' => 'PlusMedia offers high quality website design and development in Auckland. Our talented web developers custom-build every website to ensure your vision is upheld.',
            'h1' => 'Web Design and Web Development in Auckland'
        ));
    }

    public function getServiceAdPro() {
	    return View::make('frontend.service-adpro', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'PlusMedia - Auckland’s Leading Digital Advertising Company',
            'metaDescription' => 'PlusMedia offers the best Digital Advertising in Auckland. Our talented advertising specialists can run custom-build add campaign to promote your business on top of digital medias.',
            'h1' => ''
        ));
    }

    public function getServiceDisplayAds() {
	    return View::make('frontend.service-display-ads', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'PlusMedia - Auckland’s Leading Digital Advertising Company',
            'metaDescription' => 'PlusMedia offers the best Digital Advertising in Auckland. Our talented advertising specialists can run custom-build add campaign to promote your business on top of digital medias.',
            'h1' => ''
        ));
    }

    public function getServiceAdClear() {
	    return View::make('frontend.service-ad-clear', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'Dynamic Call Tracking Providers in Auckland – PlusMedia',
            'metaDescription' => 'Are you looking for dynamic call tracking providers in Auckland? Look no further than PlusMedia.',
            'h1' => ''
        ));
    }

    public function getServiceSocialMedia() {
	    return View::make('frontend.service-social-media', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'PlusMedia - Auckland’s Leading Social Advertising Company',
            'metaDescription' => 'PlusMedia offers the best Social Media Advertising in Auckland. Our talented social media specialists can run custom-build add campaign to promote your business on top of social medias.',
            'h1' => 'PlusMedia - Auckland’s Leading Social Advertising Company'
        ));
    }

    public function getServiceTelecommunication() {
	    return View::make('frontend.service-telecommunication', array(
	        'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'Dynamic Call Tracking Providers in Auckland – PlusMedia',
            'metaDescription' => 'Are you looking for call tracking providers in Auckland? Look no further than PlusMedia.',
            'h1' => 'Dynamic Call Tracking Providers in Auckland'
        ));
    }



    public function getServiceTeli() {
        return View::make('frontend.service-teli', array(
            'page' => 'service',
            'quote' => 'All that you require to grow digitally',
            'title' => 'Dynamic Call Tracking Providers in Auckland – PlusMedia',
            'metaDescription' => 'Are you looking for call tracking providers in Auckland? Look no further than PlusMedia.',
            'h1' => 'Dynamic Call Tracking Providers in Auckland'
        ));
    }

    public function getContact() {
        return View::make('frontend.contact', array(
            'page' => 'contact',
            'quote' => '',
            'title' => 'PlusMedia empowers your excel in todays\'s digital marketplace',
            'metaDescription' => 'Contact PlusMedia for high quality digital marketing services such SEO, Adwords, Web Design, Web Development, Social Media and more.',
            'h1' => 'Contact PlusMedia - Auckland’s Leading Digital Advertising Company'
        ));
    }

    public function submitContact() {
	    $name = Input::get('name');
	    $email = Input::get('email');
	    $phone = Input::get('phone');
	    $msg = Input::get('msg');
	    $ourEmail = 'isuru.l@plusmedia.co.nz';

	    try {
            Mail::send('backend.emails.enquiry', array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'msg' => $msg
            ), function ($message) use ($ourEmail) {
                $message->to($ourEmail)->subject('PlusMedia | New client enquiry');
            });

            Mail::send('backend.emails.contact', array(
                'name' => $name
            ), function ($message) use ($email) {
                $message->to($email)->subject('PlusMedia | Thanks for your interest in us');
            });

            return 1;
        } catch (Exception $ex) {
	        return $ex;
        }
    }

    public function test() {
	    $email = 'pavithraidl@yahoo.com';
        $thisYear = intval(date("Y"));

        $usersLastYear = User::where('email', $email)->orderBy('created_at', 'desc')->first('created_at');

        $date = new DateTime($usersLastYear);
        $usersLastYear = intval($date->format('Y'));

        if($thisYear == $usersLastYear) {
//            show the message
        }

    }

}
