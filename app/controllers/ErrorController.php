<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 18/12/16
 * Time: 1:01 PM
 */

class ErrorController extends BaseController
{
    public function getException() {
        return View::make('backend.exception');
    }

    public function saveException($systemId, $controllerId, $functionId, $ex) {
        try {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $exception = new SystemException();
            $exception -> systemid = $systemId;
            $exception -> controllerid = $controllerId;
            $exception -> functionid = $functionId;
            $exception -> exception = $ex;
            $exception -> userid = Auth::user()->id;
            $exception -> ip = $ip;
            $exception -> status = 3;
            $exception -> created_at = \Carbon\Carbon::now('UTC');
            $exception -> save();

            return 1;
        } catch (Exception $ex) {
            Log::error($ex);

            return $ex;
        }
    }


}