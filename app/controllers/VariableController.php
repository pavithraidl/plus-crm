<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-09-17 11:19:01.
 * Controller ID - 32
 */

class VariableController extends BaseController
{
    public function getVariable($name) {
        $exSystemId = 5;
        $exControllerId = 32;
        $exFunctionId = 19;

        try {
            //primary variables
            $domain = URL::to('/').'/'; //website domain
            $version = '1.0';
            $localPassword = '123456';


            //returning relevant variable
            switch ($name) {
                case "domain":
                    return $domain;
                    break;

                case "version":
                    return $version;
                    break;

                case "local-password":
                    return $localPassword;
                    break;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException($exSystemId, $exControllerId, $exFunctionId, $ex);
            return 0;
        }


    }

    //check the database connection
    public function checkDb() {
        try {
            DB::connection()->getPdo();
            if(DB::connection()->getDatabaseName()){
                //Database connection is ok.
            }
        } catch (\Exception $e) {
            $errorMsg = 'Unable to connect with the database!'.'<br/><br/><br/>'.'<p style="text-align: left">'.$e.'</p>';
            echo View::make('backend.error.server', array('errorMsg' => $errorMsg));
            die();
        }
    }
}

