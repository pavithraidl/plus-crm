<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-10 16:34:21.
 * Controller ID - 55
 */

class OppertunityController extends BaseController
{
    const SystemId = 19;
    const ControllerId = 55;


    public function getManage() {
        $viewId = 34;
        $urlRouteId = 72;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.oppertunities.manage', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }
}