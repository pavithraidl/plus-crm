<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-10-04 16:52:02.
 * Controller ID - 43
 */

class AgreementController extends BaseController
{
    const SystemId = 10;
    const ControllerId = 43;


    public function getAgreements() {
        $viewId = 21;
        $urlRouteId = 39;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.clients.agreements', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function getServiceAgreement($agreementId) {
        //Function data
        $functionId = 89;

        $viewId = 22;
        $urlRouteId = 40;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($agreementId == 0) {
            //region Agreement Html
            $agreementHtml = '
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/logo.png">
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr>
                                            <td class="field">Company Name</td>
                                            <td colspan="3" id="top-company-name-td"><input type="text" onchange="addValue(this, \'company_name\', 1);" class="input-invisible" id="top-company-name" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Client Name</td>
                                            <td class="data" id="top-client-name-td"><input type="text" class="input-invisible" onchange="addValue(this, \'client_name\', 1);" id="top-client-name" value=""> </td>
                                            <td class="field">Mobile/Landline</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-mobile" onchange="addValue(this, \'mobile\', 1);" value=""></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address Line 1</td>
                                            <td class="data" id="top-address-line-1-data"><input type="text" class="input-invisible" onchange="addValue(this, \'adline1\', 1);" id="top-address-line-1" value="" autocomplete="off"> </td>
                                            <td class="field">Account Phone</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-account-phone" onchange="addValue(this, \'account_phone\', 1);" value=""></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address Line 2</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-address-line-2" onchange="addValue(this, \'adline2\', 1);" value=""></td>
                                            <td class="field">Account Email</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-account-email" onchange="addValue(this, \'account_email\', 1);" value=""></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Email</td>
                                            <td class="data"><input type="text" class="input-invisible" id="top-email" onchange="addValue(this, \'email\', 1);" value=""></td>
                                            <td class="field">Website http://</td>
                                            <td class="data"><input type="text" class="input-invisible" value="" onchange="addValue(this, \'website\', 1);" id="top-website"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered top-data">
                                        <tbody>
                                        <tr>
                                                                                        <td class="field">Target start date</td>
                                            <td class="data"><input type="text" class="input-invisible show-date-picker" id="top-target-start-date" onchange="addValue(this, \'target_start_date\', 1);" value="07/11/2017"> </td>
                                            <td colspan="2" style="border-color: #fff;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row details-data-table">
                                <div class="col-md-12">
                                    <table class="table table-bordered details-table">
                                        <thead>
                                        <tr>
                                            <th class="td-services" style="background-color: #a01d1d;">Services</th>
                                            <th class="td-terms">Terms</th>
                                            <th class="td-budget">Monthly Budget</th>
                                            <th class="td-setup">Setup</th>
                                            <th class="td-total">1st Month Total</th>
                                            <th class="td-ongoing">Monthly Ongoing</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-0" onchange="addValue(this, \'service1\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-0" onchange="addValue(this, \'terms1\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-0" onchange="addValue(this, \'monthlybudget1\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-0" onchange="addValue(this, \'setup1\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-0" onchange="addValue(this, \'month_total1\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-0" onchange="addValue(this, \'ongoing1\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-1" onchange="addValue(this, \'service2\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-1" onchange="addValue(this, \'terms2\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-1" onchange="addValue(this, \'monthlybudget2\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-1" onchange="addValue(this, \'setup2\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-1" onchange="addValue(this, \'month_total2\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-1" onchange="addValue(this, \'ongoing2\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-2" onchange="addValue(this, \'service3\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-2" onchange="addValue(this, \'terms3\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-2" onchange="addValue(this, \'monthlybudget3\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-2" onchange="addValue(this, \'setup3\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-2" onchange="addValue(this, \'month_total3\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-2" onchange="addValue(this, \'ongoing3\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-3" onchange="addValue(this, \'service4\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-3" onchange="addValue(this, \'terms4\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-3" onchange="addValue(this, \'monthlybudget4\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-3" onchange="addValue(this, \'setup4\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-3" onchange="addValue(this, \'month_total4\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-3" onchange="addValue(this, \'ongoing4\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-4" onchange="addValue(this, \'service5\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-4" onchange="addValue(this, \'terms5\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-4" onchange="addValue(this, \'monthlybudget5\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-4" onchange="addValue(this, \'setup5\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-4" onchange="addValue(this, \'month_total5\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-4" onchange="addValue(this, \'ongoing5\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                    <tr>
                                                                                                <td><input type="text" class="input-invisible" id="middle-services-5" onchange="addValue(this, \'service6\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-terms-5" onchange="addValue(this, \'terms6\', 1);" value=""></td>
                                                <td class="middle-table-data-center"><input type="text" class="input-invisible" id="middle-budget-5" onchange="addValue(this, \'monthlybudget6\', 1);" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-setup-5" onchange="addValue(this, \'setup6\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-total-5" onchange="addValue(this, \'month_total6\', 1); countTotal();" value=""></td>
                                                <td class="middle-table-data-right"><input type="text" class="input-invisible" id="middle-ongoin-5" onchange="addValue(this, \'ongoing6\', 1); countTotal();" value=""></td>
                                            </tr>
                                                                                <tr>
                                            <td class="no-border" colspan="3"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="no-border bottom-title" colspan="3" style="text-align: right;">15% GST</td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-setup" disabled="" value=""></td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-total" disabled="" value=""> </td>
                                            <td class="gst-row"><input type="text" class="input-gst" id="gst-ongoing" disabled="" value=""></td>
                                        </tr>
                                        <tr>
                                            <td class="no-border bottom-title" colspan="3" style="border-right-color: #fff !important;">Total Monthly Cost</td>
                                            <td class="total-row" style="    opacity: 0; border-bottom-color: #fff !important;"><input type="text" class="input-total" id="total-setup" disabled="" value=""></td>
                                            <td class="total-row"><input type="text" class="input-total" id="total-total" disabled="" value=""></td>
                                            <td class="total-row"><input type="text" class="input-total" id="total-ongoing" disabled="" value=""></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row check-button-section">
                                <div class="col-xs-12 check-section">
                                    <div class="col-xs-1">
                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk1" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                    </div>
                                    <div class="col-xs-11">
                                        <strong>Minimum Term &amp; Cancellation:</strong> <span style="font-size: 10px;"> You acknowledge that you have ordered PlusMedia\'s marketing service for at least the minimum number of cycles set out in the order summery above.
                                        The marketing services will be continued to be supplied after the Minimum Term until you cancel. As set out in the Marketing Terms &amp; Conditions, if you give us writtern notice of the termination of your marketing services at least 7 days prior to the expiry of the Minimum Term then such termination will take effect on the expiry of the Minimum Term.
                                        if you give us written notice of termination less than 7 days before the expiry of the Minimum Term or at any time after the expiry of the Minimum Term, then such termination will take effect on the date of expiry of the first full cycle following the date of the termination
                                            notice.</span>
                                    </div>
                                </div>
                                <div class="col-xs-12" style="margin-top: 20px;">
                                    <div class="col-xs-1">
                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk2" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                    </div>
                                    <div class="col-xs-11">
                                        <strong>Call Recording:</strong> <span style="font-size: 10px;"> Client acknowledges and agrees that they have ordered the call recording service. The terms of the supply of the call recording service are governed by the Marketing Terms and Conditions.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row bottom-data-row">
                                <div class="col-xs-12 print-changes">
                                    <table class="table table-bordered bottom-data">
                                        <tbody>
                                        <tr>
                                            <td class="client-signature">Client Signature</td>
                                            <td class="client-signature-input"><input type="text" class="input-invisible" id="footer-client-signature" onchange="addValue(this, null, null);"></td>
                                            <td class="client-date">Date</td>
                                                                                        <td class="client-date-input"><input type="text" class="input-invisible show-date-picker" id="footer-client-date" onchange="addValue(this, \'client_date\', 1);" value="07/11/2017"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td class="management-signature">Management Signature</td>
                                            <td class="management-signature-input"><input type="text" class="input-invisible" id="footer-management-signature" onchange="addValue(this, null, null);"></td>
                                            <td class="management-date">Date</td>
                                                                                        <td class="management-date-input"><input type="text" class="input-invisible show-date-picker" id="footer-management-date" onchange="addValue(this, \'management_date\', 1);" value="07/11/2017"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12 footer-print-style" style="border-top: 1px solid #e0e0e0;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/full-logo.png">
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        ';
            //endregion
            //region Agreement 2 Html
            $agreement2Html = '
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/logo.png">
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                            <tr>
                                                <td class="field">Business Name</td>
                                                <td id="top-business-name-td"><input type="text" onchange="addValue(this, \'business_name\', 2);" class="input-invisible" id="top-business-name" value=""> </td>
                                                <td class="field">Marketing Consultant</td>
                                                <td id="top-marketing-consultant-td"><input type="text" onchange="addValue(this, \'marketing_consultant\', 2);" class="input-invisible" id="top-marketing-consultant" value=""> </td>
                                            </tr>
                                            <tr>
                                                <td class="field">Client Name</td>
                                                <td class="data" id="top-client-name-td"><input type="text" class="input-invisible" onchange="addValue(this, \'client_name\', 2);" id="top-client-name" value=""> </td>
                                                <td class="field">Mobile/Landline</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-mobile" onchange="addValue(this, \'mobile\', 2);" value=""></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Business Analysis</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Close Ratio</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'close_ratio\', 2);" id="top-close-ratio" value=""> </td>
                                                <td class="field">Average Length of Sales</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-length-of-sales" onchange="addValue(this, \'average_length\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Customer Value ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-value" onchange="addValue(this, \'customer_value\', 2);" value=""></td>
                                                <td class="field">Average Gross Sale ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-average-gross-sale" onchange="addValue(this, \'average_gross_sale\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Effect of Seasonality on Revenue</td>
                                                <td class="data" colspan="3" style="border-right-color: #fff;"></td>

                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-customer-value" onchange="addValue(this, \'effect_revenue\', 2);"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Current Business Growth Plans</td>
                                                <td class="data" colspan="3" style="border-right-color: #fff;"></td>

                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-customer-value-2" onchange="addTextAreaValue(this, \'current_business_growth\', 2);"></textarea></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Strategy</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">IP Targeting</td>
                                                <td class="data" style="padding: 0 10px;">
                                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk3" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 50px;"><span style="font-size: 11px !important;">National</span></span>
                                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk4" data-parsley-mincheck="2" required="" class="flat" style="margin-left: 20px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">Local</span>
                                                </td>
                                                <td class="field">Address</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'address\', 2);" id="top-address-line-1" value="" autocomplete="off"></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Primary Target Location</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'primarty_target_location\', 2);" id="top-primary-target-location" value=""> </td>
                                                <td class="field">High Priority Target Areas</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-high-priority-target-location" onchange="addValue(this, \'high_priority_target_areas\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Areas/Locations(Not to target)</td>
                                                <td class="data" id="top-areas-locations-not-to-target-td"><input type="text" class="input-invisible" onchange="addValue(this, \'not_to_target_areas\', 2);" id="top-areas-locations-not-to-target" value=""> </td>
                                                <td class="field">Products/Services Promote</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-product-services-promote" onchange="addValue(this, \'product_promote\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Keyword Exclusions</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'keyword_exclusions\', 2);" id="top-keyword-exclutions" value=""> </td>
                                                <td class="field">Any specific Terms</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-any-specific-terms" onchange="addValue(this, \'any_specific_terms\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Business Name Trademarked?</td>
                                                <td class="data" style="padding: 0 10px;" colspan="3">
                                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk5" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">Keyword Restrictions</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="field" colspan="2">Are There Any Elements From Your Adwords Campaign That You Want Us To Consider?</td>
                                                <td class="data" colspan="2" style="border-right-color: #fff;"></td>
                                            </tr>
                                            <tr>
                                                <td class="data" colspan="4"><textarea class="input-invisible" id="top-are-there-any-elements" onchange="addTextAreaValue(this, \'are_there_any_elements\', 2);"> </textarea></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Objectives</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Key Objectives</td>
                                                <td class="data" colspan="3"><input type="text" class="input-invisible" id="top-key-objectives" onchange="addValue(this, \'key_bojects\', 2);" value=""></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                                <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Estimated Contacts</td>
                                            </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                            <tr>
                                                <td class="field">Contacts</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'contacts\', 2);" id="top-contacts" value=""> </td>
                                                <td class="field">Campaign Budget ($)</td>
                                                <td class="data"><input type="text" class="input-invisible" id="top-campaign-budget" onchange="addValue(this, \'campaign_budget\', 2);" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Cost per Contact</td>
                                                <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'cost_per_contact\', 2);" id="top-cost-per-contact" value=""> </td>
                                                <td class="field">Target Start Date</td>
                                                                                                <td class="data"><input type="text" class="input-invisible show-date-picker" id="top-target-start-date" onchange="addValue(this, \'target_start_date\', 2);" value="07/11/2017"></td>
                                            </tr>
                                            <tr><td class="row-empty" colspan="4"></td></tr>
                                            <tr>
                                                <td class="field">Client Signature</td>
                                                <td class="data" colspan="3"><input type="text" class="input-invisible" id="top-client-signature" value=""> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p style="font-size: 9px;">
                                        *Estimated Contacts is an estimated derived from historic PlusMedia data and is not a guarantee of actual campain performance. Several factors outside our control affect actual campaign performance. Including publisher and the quality of your website.
                                    </p>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12" style="margin-top: 90px; padding-top: 5px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/full-logo.png">
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        ';
            //endregion
            //region Agreement 3 HTMl
            $agreement3Html = '
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/logo.png">
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr>
                                            <td class="field">Business Name</td>
                                            <td id="top-business-name-td"><input type="text" onchange="addValue(this, \'business_name\', 3);" class="input-invisible" id="top-business-name" value=""> </td>
                                            <td class="field">Marketing Consultant</td>
                                            <td id="top-marketing-consultant-td"><input type="text" onchange="addValue(this, \'marketing_consultant\', 3);" class="input-invisible" id="top-marketing-consultant" value=""> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Tracking &amp; Conversions</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Phone Tracking Type</td>
                                            <td class="data" style="padding: 0 10px;" colspan="3">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk6" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Geo</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk7" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">1300(AU)</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk8" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">0800(NZ)</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk9" data-parsley-mincheck="2" required="" class="flat" style="margin-left: 30px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">Complex Routing</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Call Recording?</td>
                                            <td class="data" style="padding: 0 10px;">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo1" name="call-recording" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">On</span></span>
                                                <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo2" name="call-recording" data-parsley-mincheck="2" required="" class="flat" style="margin-left: 30px; position: absolute; opacity: 0;" checked=""><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">Off</span>
                                            </td>
                                            <td class="field">Special Requirements</td>
                                            <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'special_requirements\', 3);" id="top-special-requirements" value=""> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Call Tracking Details</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field" colspan="2">Numbers on Website</td>
                                            <td class="field" colspan="2">Destination Numbers</td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'numbers_on_website1\', 3);" id="top-numbers-on-website-1" value=""> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'destination_numbers1\', 3);" id="top-number-on-destination-1" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'numbers_on_website2\', 3);" id="top-numbers-on-website-2" value=""> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'destination_numbers2\', 3);" id="top-number-on-destination-2" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'numbers_on_website3\', 3);" id="top-numbers-on-website-3" value=""> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'destination_numbers3\', 3);" id="top-number-on-destination-3" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'numbers_on_website4\', 3);" id="top-numbers-on-website-4" value=""> </td>
                                            <td class="data" colspan="2"><input type="text" class="input-invisible" onchange="addValue(this, \'destination_numbers4\', 3);" id="top-number-on-destination-4" value=""> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Numbers to be Omitted from Tracking</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'number_to_be_omitted1\', 3);" class="input-invisible" id="top-numbers-to-be-omitted-1" value=""> </td>
                                            <td class="field" style="background-color: #fff !important;"><input type="text" onchange="addValue(this, \'number_to_be_omitted2\', 3);" class="input-invisible" id="top-numbers-to-be-omitted-2" value=""> </td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'number_to_be_omitted3\', 3);" class="input-invisible" id="top-numbers-to-be-omitted-3" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Landing URL : http://</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'landing_url\', 3);" class="input-invisible" id="top-landing-url" value=""> </td>
                                            <td class="field">Mobile site URL : http://</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'mobile_url\', 3);" class="input-invisible" id="top-mobile-site-url" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Online Conversions to Track</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'online_conversion_track1\', 3);" class="input-invisible" id="top-online-conversions-track-1" value=""> </td>
                                            <td class="field" style="background-color: #fff !important;"><input type="text" onchange="addValue(this, \'online_conversion_track2\', 3);" class="input-invisible" id="top-online-conversions-track-2" value=""> </td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'online_conversion_track3\', 3);" class="input-invisible" id="top-online-conversions-track-3" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Existing Media Campaigns</td>
                                            <td class="data" style="padding: 0 10px;">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo3" name="media-campaigns" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Yes</span></span>
                                                <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo4" name="media-campaigns" data-parsley-mincheck="2" required="" class="flat" checked="" style="margin-left: 30px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">No</span>
                                            </td>
                                            <td class="field">CIDs</td>
                                            <td class="data"><input type="text" class="input-invisible" onchange="addValue(this, \'cids\', 3);" id="top-cids" value=""> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Campaign Tracking &amp; Conversions</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Website (CMS) Login Details</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'website_login_details\', 3);" class="input-invisible" id="top-website-cms-login" value=""> </td>
                                            <td class="field">Loging URL</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'logging_url\', 3);" class="input-invisible" id="top-login-url" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'website_username\', 3);" class="input-invisible" id="top-username" value=""> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'website_password\', 3);" class="input-invisible" id="top-password" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">FTP Details</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'ftp_details\', 3);" class="input-invisible" id="top-ftp-details" value=""> </td>
                                            <td class="field">Logging URL</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'ftp_url\', 3);" class="input-invisible" id="top-loging-url-2" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'ftp_username\', 3);" class="input-invisible" id="top-user-name2" value=""> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'ftp_password\', 3);" class="input-invisible" id="top-password-2" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Domain Logins</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'domain_login\', 3);" class="input-invisible" id="top-domain-logins" value=""> </td>
                                            <td class="field">Hosting Company</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'hosting_company\', 3);" class="input-invisible" id="top-hosting-company" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Username</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'domain_username\', 3);" class="input-invisible" id="top-user-name3" value=""> </td>
                                            <td class="field">Password</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'domain_password\', 3);" class="input-invisible" id="top-password-3" value=""> </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Website Developer/Maintenance</td>
                                        </tr><tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;"></td></tr>
                                        <tr>
                                            <td class="field">Company</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'company\', 3);" class="input-invisible" id="top-user-name3" value=""> </td>
                                            <td class="field">Contact Name</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'contact_name\', 3);" class="input-invisible" id="top-password-3" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Contact Email</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'contact_email\', 3);" class="input-invisible" id="top-contact-email" value=""> </td>
                                            <td class="field">Contact Number</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'contact_number\', 3);" class="input-invisible" id="top-contact-number" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Has the client requested PlusMedia to add the code to their website?</td>
                                            <td class="data" style="padding: 0 10px;" colspan="2">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo5" name="add-code" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Yes</span></span>
                                                <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" onclick="setupVariable(1);" id="rdo6" name="add-code" data-parsley-mincheck="2" required="" class="flat" checked="" style="margin-left: 30px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">No</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12" style="padding-top: 5px; page-break-after: always; margin-top: 95px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/full-logo.png">
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        ';
            //endregion
            //region Agreement 4
            $agreement4Html = '
                            <div id="top-header" class="col-md-12 header right-to-left">
                                <div class="col-xs-8">
                                    <h3 class="header-title">+Media</h3>
                                    <p class="header-sub-title">Service Order Sheet</p>
                                </div>
                                <div class="col-xs-4">
                                    <p class="logo-container">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/logo.png">
                                    </p>

                                </div>
                            </div>
                            <div class="row company-data-table">
                                <div class="col-md-12" style="width: 100% !important;">
                                    <table class="table table-bordered top-data top-data-top">
                                        <tbody>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;Method of Payment</td>
                                        </tr>
                                        <tr><td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 3px !important; height: 0 !important;">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk10" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Cheque (Enclosed)</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk11" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Visa</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk12" data-parsley-mincheck="2" required="" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="margin-right: 30px;"><span style="font-size: 11px !important;">Mastercard</span></span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk13" data-parsley-mincheck="2" required="" class="flat" style="margin-right: 30px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important; margin-right: 30px;">American Express</span>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" onclick="setupVariable(1);" id="chk14" data-parsley-mincheck="2" required="" class="flat" style="margin-left: 30px; position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <span style="font-size: 11px !important;">Others</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Credit/Debit Card Number (No Dash)</td>
                                            <td class="data" colspan="2"><input type="text" onchange="addValue(this, \'card_number\', 4);" class="input-invisible" id="top-credit-debit-card-number" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Name as Printed on the Credit/Debit Card</td>
                                            <td class="data" colspan="2"><input type="text" onchange="addValue(this, \'card_name\', 4);" class="input-invisible" id="top-credit-debit-card-name" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Expiration Date (Month &amp; Year)</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_exp\', 4);" class="input-invisible" id="top-expiration-date" value=""> </td>
                                            <td class="field">CSV Number</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_csv\', 4);" class="input-invisible" id="top-csv-number" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="row-empty less" colspan="2" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important;"></td>
                                            <td class="row-empty less" colspan="2" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; height: 0 !important; border-bottom-color: #fff;"></td>
                                        </tr>
                                        <tr>
                                            <td class="field" colspan="2">Credit/Debit Card Billing Address</td>
                                            <td class="data" colspan="2" style="border-top-color: #fff; border-right-color: #fff;"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Address</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_address\', 4);" class="input-invisible" id="top-address" value="" autocomplete="off"> </td>
                                            <td class="field">City</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_city\', 4);" class="input-invisible" id="top-city" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="field">State</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_state\', 4);" class="input-invisible" id="top-state" value=""> </td>
                                            <td class="field">Zip Code</td>
                                            <td class="data"><input type="text" onchange="addValue(this, \'card_zip\', 4);" class="input-invisible" id="top-zip-code" value=""> </td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="2" style="font-size: 11px;">I authorize PlusMedia to Charge my Credit/Debit Card</td>
                                            <td class="field">Signature</td>
                                            <td class="data"></td>
                                        </tr>
                                        <tr>
                                            <td class="row-empty less" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 5px !important; padding-bottom: 50px !important; height: 0 !important;">
                                                <p style="font-size: 11px;">Your Credit/Debit card will be charged the above stated amount upon receipt of your application. The charget will be reflected on your Credit/Debit card statement in the event</p>
                                            </td>
                                        </tr>
                                        <tr><td class="row-empty" colspan="4" style="border-left-color: #fff !important; border-right-color: #fff !important; padding: 0px !important; height: 0 !important;"></td></tr><tr>
                                            <td class="row-title" colspan="4" style="background-color: #000; color: #fff; font-size: 11px; font-weight: 600; padding: 6px; height: 20px !important;">&nbsp;&nbsp;For Bank Online Deposit</td>
                                        </tr>
                                        <tr>
                                            <td class="data" colspan="4">
                                                <p style="font-size: 11px;"><strong>Please make the payment in to Bank Account No. <span style="color: #e4000e;">02-1244-0137403-00</span></strong></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row form-footer" style="page-break-after: always !important;">
                                <div class="col-xs-12 footer-print" style="padding-top: 5px; margin-top: 510px;">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/full-logo.png">
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 footer-normal-style">
                                    <div class="col-xs-3 footer-full-logo">
                                        <img src="http://crm.plusmedia.co.nz/assets/backend/images/full-logo.png">
                                    </div>
                                    <div class="col-xs-9">
                                        <p class="footer-contact-section">
                                            <a href="#">www.plusmedia.co.nz</a>|
                                            <a href="#">0800 PLUS MEDIA</a>|
                                            <a href="#">infor@pludmedia.co.nz</a>
                                        </p>
                                        <p class="footer-text">
                                            The contents of the documents are the property of PlusMedia and cannot be copied or transferred without prior written consent.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        ';
            //endregion

            $agreement = new TempAgreementData1();
            $agreement -> userid = Auth::user()->id;
            $agreement -> html = $agreementHtml;
            $agreement -> status = 1;
            $agreement -> save();
            $agreementId = $agreement -> id;

            $agreement2 = new TempAgreementData2();
            $agreement2 -> id = $agreementId;
            $agreement2 -> userid = Auth::user()->id;
            $agreement2 -> html = $agreement2Html;
            $agreement2 -> status = 1;
            $agreement2 -> save();

            $agreement3 = new TempAgreementData3();
            $agreement3 -> id = $agreementId;
            $agreement3 -> userid = Auth::user()->id;
            $agreement3 -> html = $agreement3Html;
            $agreement3 -> status = 1;
            $agreement3 -> save();

            $agreement4 = new TempAgreementData4();
            $agreement4 -> id = $agreementId;
            $agreement4 -> userid = Auth::user()->id;
            $agreement4 -> html = $agreement4Html;
            $agreement4 -> status = 1;
            $agreement4 -> save();

            $activity = new UserController();
            $activity->saveActivity('Created a new sales agreement under ID:'.$agreementId);
        }

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.clients.service-agreement', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'agreementId' => $agreementId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }


    public function getServiceAgreementPage2($agreementId) {
        $viewId = 26;
        $urlRouteId = 58;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {

            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.clients.service-agreement-page-2', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'agreementId' => $agreementId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }


    public function getServiceAgreementPage3($agreementId) {
        $viewId = 30;
        $urlRouteId = 62;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.clients.serviceagreementpage3', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'agreementId' => $agreementId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }


    public function getServiceAgreementPage4($agreementId) {
        $viewId = 31;
        $urlRouteId = 63;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.clients.service-agreement-page-4', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'agreementId' => $agreementId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function saveData() {
        //Function data
        $functionId = 141;

        $value = Input::get('value');
        $page = Input::get('page');
        $field = Input::get('field');
        $agreementId = Input::get('agreementid');

        try {
            $table = 'temp_agreement_data'.$page;

            if(DB::table($table)->where('id', $agreementId)->pluck('id')) {
                DB::table($table)->where('id', $agreementId)->update(array(
                    $field => $value
                ));
            }
            else {
                DB::table($table)->insert(array(
                    'id' => $agreementId,
                     'userid' => Auth::user()->id
                ));

                DB::table($table)->where('id', $agreementId)->update(array(
                    $field => $value
                ));
            }


            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function deleteAgreement() {
        //Function data
        $functionId = 143;

        $agreementId = Input::get('agreementid');

        try {
            TempAgreementData1::where('id', $agreementId)->update(array(
                'status' => 0
            ));

            $activity = new UserController();
            $activity->saveActivity('Deleted agreement under ID:'.$agreementId);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function savePrint() {
        //Function data
        $functionId = 147;

        $pageId = Input::get('pageid');
        $agreementId = Input::get('agremeentid');

        if($pageId == 1)
            $pageId = '1st';
        else if($pageId == 2)
            $pageId = '2nd';
        else if($pageId == 3)
            $pageId = '3rd';
        else if($pageId == 4)
            $pageId = '4th';

        try {
            $userName = User::where('id', Auth::user()->id)->pluck('fname').' '.User::where('id', Auth::user()->id)->pluck('lname');
            $msg = sprintf('%s printed the %s page of agreement %s', $userName, $pageId, $agreementId);
            $activity = new UserController();
            $activity->saveActivity($msg);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function savePrintHtml() {
        //Function data
        $functionId = 151;

        $page = Input::get('page');
        $agreementId = Input::get('agreementid');
        $printHtml = Input::get('printhtml');

        try {
            $table = 'temp_agreement_data'.$page;
            DB::table($table)->where('id', $agreementId)->update(array(
                'html' => $printHtml
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getPrintHtml() {
        //Function data
        $functionId = 153;

        $agreementId = Input::get('agreementid');
        $page = Input::get('page');
        $printHtml = Input::get('printhtml');

        try {
            if($page != null) {
                $table = 'temp_agreement_data'.$page;
                DB::table($table)->where('id', $agreementId)->update(array(
                    'html' => $printHtml
                ));
            }

            $page1 = TempAgreementData1::where('id', $agreementId)->pluck('html');
            $page2 = DB::table('temp_agreement_data2')->where('id', $agreementId)->pluck('html');
            $page3 = DB::table('temp_agreement_data3')->where('id', $agreementId)->pluck('html');
            $page4 = DB::table('temp_agreement_data4')->where('id', $agreementId)->pluck('html');

            $fullHtml = $page1.$page2.$page3.$page4;

            return json_encode($fullHtml);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}