<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 10/10/17 09:03.
 */


public function postUserStatus() {
    $userId = Input::get('userid');
    $status = Input::get('status');

    try {
        if($status == 0) $status = 2;
        User::where('id', $userId)->update(array(
            'status' => $status
        ));

        $status = ($status == 1 ? 'Enabled' : 'Disabled');
        $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') User account', null, null);

        return 1;
    } catch (Exception $ex) {

        return 0;
    }
}

public function postRemoveUserAccount() {
    $userId = Input::get('userid');

    try {
        User::where('id', $userId)->update(array(
            'status' => 0,
            'active' => 0,
            'email' => ''
        ));

        $this -> saveActivity('Deleted - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').' User account', null, null);

        return 1;
    } catch (Exception $ex) {

        return 0;
    }
}

public function postResendActivationEmail() {
    $userId = Input::get('userid');

    try {
        $code = User::where('id', $userId)->pluck('code');
        $fName = User::where('id', $userId)->pluck('fname');
        $lName = User::where('id', $userId)->pluck('lname');
        $email = User::where('id', $userId)->pluck('email');
        $orgName = Org::where('id', Auth::user()->orgid)->pluck('name');
        $name = $fName.' '.$lName;

        Mail::send( 'emails.active', array(
            'link'      => URL::route( 'active', $code ),
            'orgName' => $orgName,
            'name' => $fName.' '.$lName,
            'homeurl' => URL::To('/')
        ), function ( $message ) use ( $email, $fName) {
            $message->to( $email, $fName )->subject( 'Activate your account' );
        });

        $this->saveActivity('Resent the activation email to '.$name.'('.$email.')', null, null);

        return json_encode($email);
    } catch (Exception $ex) {

        return 0;
    }
}

public function getActivityList() {
    $paginate = Input::get('paginate');
    $userId = Input::get('userid');
    $arr = [];

    try {
        $activityList = DB::table('activity_log')->where('userid', $userId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

        if(sizeof($activityList) > 0){
            //creating objects
            $timeScence = new CalculationController();

            foreach ($activityList as $activityId) {
                //get values form database
                $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                //calculations
                $setCreatedAt = new DateTime($createdAt, new DateTimeZone('UTC'));
                $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                //return json
                $arr[] = array(
                    'activity' => $activity,
                    'timeago' => $setCreatedAt,
                    'createdat' => $createdAt
                );
            }
            return json_encode($arr);
        }
        else{
            return -1;
        }
    }  catch (Exception $ex) {

        return 0;
    }
}

public function changeUserAccess() {
    $userId = Input::get('selecteduser');
    $status = Input::get('status');
    $systemId = Input::get('systemid');

    try {
        if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->pluck('id')) {
            SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->update(array(
                'allow' => $status
            ));
        }
        else {
            $systemAccess = new SystemRoutingAccess();
            $systemAccess -> systemid = $systemId;
            $systemAccess -> userid = $userId;
            $systemAccess -> allow = $status;
            $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
            $systemAccess -> save();
        }

        $status = ($status == 1 ? 'Enabled' : 'Disabled');
        $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access to '.System::where('id', $systemId)->pluck('name'), null, null);

        return 1;

    }  catch (Exception $ex) {

        return 0;
    }
}

public function changeUserOperationAccess() {
    $userId = Input::get('selecteduser');
    $status = Input::get('status');
    $operationId = Input::get('operationid');

    try {
        if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
            SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                'allow' => $status
            ));
        }
        else {
            $systemAccess = new SystemOperationAccess();
            $systemAccess -> operationid = $operationId;
            $systemAccess -> userid = $userId;
            $systemAccess -> allow = $status;
            $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
            $systemAccess -> save();
        }

        $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
        $status = ($status == 1 ? 'Enabled' : 'Disabled');
        $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('operation'), null, null);

        return 1;

    }  catch (Exception $ex) {

        return 0;
    }
}

public function getAccessList() {
    $userId = Input::get('userid');

    try {
        $systemList = SystemRoutingAccess::where('userid', $userId)->where('allow', 1)->lists('systemid');
        $operationList = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->lists('operationid');

        $ret = array(
            'sys' => $systemList,
            'ope' => $operationList
        );

        return json_encode($ret);

    } catch (Exception $ex) {

        return 0;
    }
}

    public function addUser() {
        $name = Input::get('name');
        $email = Input::get('email');
        $jobTitle = Input::get('jobtitle');


        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {

                $words = explode( ' ', $name);
                array_shift( $words);
                $lName = implode( ' ', $words);
                $name = explode(" ", $name);
                $fName = $name[0];

                $roll = 4;
                $orgId = Auth::user()->orgid;
                $orgName = 'ABC Company';
                $code = str_random(60);

                $user = new User();
                $user -> fname = ucfirst($fName);
                $user -> lname = ucfirst($lName);
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> jobtitle = $jobTitle;
                $user -> roll = $roll;
                $user -> orgid = $orgId;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();

                if($email != '') {
                    Mail::send( 'emails.active', array(
                        'link'      => URL::route( 'active', $code ),
                        'orgName' => $orgName,
                        'name' => $fName.' '.$lName,
                        'homeurl' => URL::To('/')
                    ), function ( $message ) use ( $user, $email, $fName) {
                        $message->to( $email, $fName )->subject( 'Activate your account' );
                    });
                }

                return 1;
            }
        } catch (Exception $ex) {

        }
    }