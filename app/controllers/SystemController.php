<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {23/05/17} {11:50}.
 */
 
class SystemController extends BaseController {

    const SystemId = 5;
    const ControllerId = 2;

    public function getManage() {
        $viewId = 1;
        $urlRouteId = 1;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == 1) {
                return View::make('backend.system.manage', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

    public function changeStatus() {
        //Function data
        $exFunctionId = 4;

        try {
            $systemId = Input::get('systemid');
            $status = Input::get('status');

            if($status == 0)
                $status = 2;

            System::where('id', $systemId)->update(array(
                'status' => $status
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    public function getSystemData() {
        //Function data
        $exFunctionId = 5;

        $systemId = Input::get('systemid');

        try {
            $systemName = System::where('id', $systemId)->pluck('name');
            $viewIds = SystemView::where('systemid', $systemId)->lists('id');
            $visits = 0;
            $visitors = 0;

            foreach ($viewIds as $viewId) {
                $visits += SystemViewVisit::where('viewid', $viewId)->count('id');

                $allVisits = SystemViewVisit::where('viewid', $viewId)->lists('userid');
                $visitors += sizeof(array_unique($allVisits));
            }

            $totalEx = SystemException::where('status', '!=', -1)->count('id');
            $runningEx = SystemException::where('systemid', $systemId)->where('status', 3)->count('id');
            $pendingEx = SystemException::where('systemid', $systemId)->where('status', 2)->count('id');
            $solvedEx = SystemException::where('systemid', $systemId)->where('status', 1)->count('id');

            $route = System::where('id', $systemId)->pluck('route');
            $listOrder = System::where('id', $systemId)->pluck('listorder');

            $controllers = SystemControllerModel::where('systemid', $systemId)->count('id');
            $controllerList = SystemControllerModel::where('systemid', $systemId)->lists('id');

            $functions = 0;

            foreach ($controllerList as $controllerId) {
                $functions += SystemFunction::where('controllerid', $controllerId)->count('id');
            }

            $createdBy = User::where('id', System::where('id', $systemId)->pluck('created_by'))->pluck('fname')
                            .' '.User::where('id', System::where('id', $systemId)->pluck('created_by'))->pluck('lname');

            $createdAt = System::where('id', $systemId)->pluck('created_at');
            $timeAgo = new CalculationController();
            $createdAt = $timeAgo -> shortDate($createdAt);

            $ret = array(
                'name' => $systemName,
                'visits' => $visits,
                'visitors' => $visitors,
                'totalex' => $totalEx,
                'runningex' => $runningEx,
                'pendingex' => $pendingEx,
                'solvedex' => $solvedEx,
                'route' => $route,
                'listorder' => $listOrder,
                'controllers' => $controllers,
                'func' => $functions,
                'createdby' => $createdBy,
                'createdat' => $createdAt
            );

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    function getSystemExceptions() {
        //Function data
        $exFunctionId = 6;

        $systemId = Input::get('systemid');
        $paginate = 1;
        $ret = array();

        try {
            $exceptionList = SystemException::where('systemid', $systemId)->orderBy('status', 'desc')->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            foreach ($exceptionList as $exceptionId) {
                $controller = SystemControllerModel::where('id', SystemException::where('id', $exceptionId)->pluck('controllerid'))->pluck('controller_name');
                $func = SystemFunction::where('id', SystemException::where('id', $exceptionId)->pluck('functionid'))->pluck('function_name');
                $userName = User::where('id', SystemException::where('id', $exceptionId)->pluck('userid'))->pluck('fname').' '
                    .User::where('id', SystemException::where('id', $exceptionId)->pluck('userid'))->pluck('lname');
                $ip = SystemException::where('id', $exceptionId)->pluck('ip');
                $exception = SystemException::where('id', $exceptionId)->pluck('exception');
                $exceptionArray = explode('.',$exception);
                $exception = $exceptionArray[0];
                $status = SystemException::where('id', $exceptionId)->pluck('status');

                $ret[] = array(
                    'id' => $exceptionId,
                    'controller' => $controller,
                    'func' => $func,
                    'username' => $userName,
                    'ip' => $ip,
                    'exception' => substr($exception, 0, 200),
                    'status' => $status
                );
            }

            if(sizeof($ret) < 1) {
                $ret = null;
            }

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }

    }

    function getExceptionData() {
        //Function data
        $exFunctionId = 7;

        $exceptionId = Input::get('exceptionid');

        try {
            $systemName = System::where('id', SystemException::where('id', $exceptionId)->pluck('systemid'))->pluck('name');
            $controller = SystemControllerModel::where('id', SystemException::where('id', $exceptionId)->pluck('controllerid'))->pluck('controller_name');
            $func = SystemFunction::where('id', SystemException::where('id', $exceptionId)->pluck('functionid'))->pluck('function_name');
            $userName = User::where('id', SystemException::where('id', $exceptionId)->pluck('userid'))->pluck('fname').' '
                .User::where('id', SystemException::where('id', $exceptionId)->pluck('userid'))->pluck('lname');
            $ip = SystemException::where('id', $exceptionId)->pluck('ip');
            $exception = SystemException::where('id', $exceptionId)->pluck('exception');
            $createdAt = SystemException::where('id', $exceptionId)->pluck('created_at');

            $exceptionArray = explode("'",$exception);
            $exceptionTitle = $exceptionArray[1];

            $timeAgo = new CalculationController();
            $createdAt = $timeAgo->timeAgo($createdAt->format('Y-m-d H:i:s'));

            $ret = array(
                'system' => $systemName,
                'controller' => $controller,
                'func' => $func,
                'user' => $userName,
                'ip' => $ip,
                'exception' => $exception,
                'title' => $exceptionTitle,
                'createdat' => $createdAt
            );

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    function changeExceptionStatus() {
        //Function data
        $exFunctionId = 9;

        $exceptionId = Input::get('exceptionid');
        $status = Input::get('status');

        try{
            SystemException::where('id', $exceptionId)->update(array(
                'status' => $status
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    public function changeAllExceptionStatus() {
        //Function data
        $exFunctionId = 9;

        try{
            SystemException::where('status', '>', 1)->update(array(
                'status' => 1
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    public function newSystem() {
        //Function data
        $exFunctionId = 10;

        $systemName = Input::get('name');
        $systemIcon = Input::get('icon');
        $listOrder = Input::get('listorder');
        $allowDefault = Input::get('allowdefault');

        try {
            $systemNameValidator = System::where('name', $systemName)->pluck('id');
            if($systemNameValidator) {
                return -1;
            }
            else {
                $system = new System();
                $system -> name = $systemName;
                $system -> route = '#';
                $system -> icon = $systemIcon;
                $system -> listorder = $listOrder;
                $system -> visibility = 1;
                $system -> allow_default = $allowDefault;
                $system -> status = 2;
                $system -> created_by = Auth::user()->id;
                $system -> created_at = \Carbon\Carbon::now('UTC');
                $system -> save();
                $systemId = $system->id;

                //create the class path
                $path = new SystemClassPath();
                $path -> systemid = $systemId;
                $path -> path = strtolower($systemName);
                $path -> status = 1;
                $path -> created_at = \Carbon\Carbon::now('UTC');
                $path -> save();


                //region New system content
                $newSystem = (
                    '<div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading-'.$systemId.'" data-toggle="collapse" onclick="loadSystemData('.$systemId.');" data-parent="#accordion" href="#collapse'.$systemId.'" aria-expanded="false" aria-controls="collapse'.$systemId.'" style="position: relative;">
                            <h4 class="panel-title"><i class="fa '.$systemIcon.'"></i>&nbsp;&nbsp; '.$systemName.'</h4>
                            <div id="system-led-status-'.$systemId.'" class="led-yellow" style="position: absolute; right: 20px; top: 16px;"></div>
                            <div id="system-led-exceptions-'.$systemId.'" class="led-green" style="position: absolute; right: 40px; top: 16px;"></div>
                        </a>
                        <div id="collapse'.$systemId.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-'.$systemId.'">
                            <div id="panel-body-'.$systemId.'" class="panel-body">
                                <input data-id= "system-status-'.$systemId.'" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right"/>
                                <div class="" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: 40px;">
                                    <ul id="tab-'.$systemId.'" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_info-'.$systemId.'" id="info-tab-'.$systemId.'" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                                        <li role="presentation" class=""><a href="#tab_class-'.$systemId.'" role="tab" id="class-tab-'.$systemId.'" data-toggle="tab" aria-expanded="false"><i class="fa fa-code"></i> Classes </a></li>
                                        <li role="presentation" class=""><a href="#tab_view-'.$systemId.'" role="tab" id="view-tab-'.$systemId.'" data-toggle="tab" aria-expanded="false"><i class="fa fa-list-alt"></i> Views </a></li>
                                    </ul>
                                    <div id="system-tabs-'.$systemId.'" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_info-'.$systemId.'" aria-labelledby="info-tab-'.$systemId.'" style="padding: 10px;">
                                            <div class="row top_tiles" style="margin: 10px 0;">
                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                    <span>Total Visits</span>
                                                    <h2 id="system-total-visits-'.$systemId.'">null</h2>
                                                    <span class="sparkline_one" style="height: 160px;">
                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                </span>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                    <span>Total Visitors</span>
                                                    <h2 id="system-total-visitors-'.$systemId.'">null</h2>
                                                    <span class="sparkline_one" style="height: 160px;">
                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                </span>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                    <span>Total Traffic</span>
                                                    <h2>231</h2>
                                                    <span class="sparkline_one" style="height: 160px;">
                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                </span>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                    <span>Total bandwidth</span>
                                                    <h2>809</h2>
                                                    <span class="sparkline_two" style="height: 160px;">
                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-12 bs-callout bs-callout-danger">
                                                    <h4>System Exceptions</h4>
                                                    <div class="form-horizontal form">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Exception Ratio:</label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <progress max="100" value="0" id="system-exception-ratio-progress-'.$systemId.'"></progress>
                                                                <span style="  position: absolute; top: 10px; left: 45%; color: #91acff; font-weight: 700;" id="system-exception-ratio-text-'.$systemId.'">null%</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Running Exception:</label>
                                                            <div id="system-runing-ex-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 8px; font-size: 16px; color: #fd132f;">
                                                                0
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Pending Exception:</label>
                                                            <div id="system-pending-ex-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #e49a48;">
                                                                0
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Fixed Bugs:</label>
                                                            <div id="system-fixed-ex-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #5cd07e;">
                                                                0
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p style="text-align: right">
                                                        <button class="btn btn-danger" data-toggle="modal" data-target=".bs-exceptions-modal-lg" onclick="openExceptions('.$systemId.');">Exceptions</button>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-12 bs-callout bs-callout-info">
                                                    <h4>System Info</h4>
                                                    <div class="form-horizontal form">
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">ID:</label>
                                                            <div id="system-id-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                '.$systemId.'
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Route:</label>
                                                            <div id="system-route-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">List Order:</label>
                                                            <div id="system-list-order-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Controllers:</label>
                                                            <div id="system-controllers-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Functions:</label>
                                                            <div id="system-functions-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created By:</label>
                                                            <div id="system-created-by-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top: -10px;">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created At:</label>
                                                            <div id="system-created-at-'.$systemId.'" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                null
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_class-'.$systemId.'" aria-labelledby="class-tab-'.$systemId.'">
                                            <div id="sys-controller-container-'.$systemId.'" class="col-md-12" style="padding: 25px; margin-bottom: 30px;">
                                                <!--Start of accordion-->
                                                    <div class="accordion" id="accordion-controller-'.$systemId.'" role="tablist" aria-multiselectable="true">
                                                        <!--JQuery Append-->
                                                    </div>
                                                    <!-- end of accordion -->
                                                    <span id="btn-new-controller-'.$systemId.'" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-controller-modal-lg" style="bottom: -35px !important; background-color: #1a96bb !important; width: 50px; height: 50px; padding: 10px; padding-left: 16px;"><i class="fa fa-plus"></i> </span>
                                                </div>
                                            </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_view-'.$systemId.'" aria-labelledby="view-tab-'.$systemId.'">
                                            <p>xxFood truck fixie locavore, accusamus mcsweeney\'s marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                                                booth letterpress, commodo enim craft beer mlkshk 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>');
                //endregion

                return json_encode($newSystem);
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }

    }

    public function getControllers() {
        //Function data
        $exFunctionId = 11;

        $systemId = Input::get('systemid');

        try {
            $jsControllerList = SystemControllerModel::where('systemid', $systemId)->where('lang', 'JS')->where('status', 1)->lists('id');
            $phpControllerList = SystemControllerModel::where('systemid', $systemId)->where('lang', 'PHP')->where('status', 1)->lists('id');
            $appendHtml = '';

            if(sizeof($jsControllerList) > 0) $appendHtml = '<h3>JS</h3>';

            foreach ($jsControllerList as $controllerId) {
                $controllerName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name');

                $appendHtml = ($appendHtml.
                    '<div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading-controller-'.$controllerId.'" onclick="openController('.$controllerId.', 1);" data-toggle="collapse" data-parent="#accordion-controller-'.$systemId.'" href="#collapse-controller-'.$controllerId.'" aria-expanded="false" aria-controls="collapse-controller-'.$controllerId.'" style="background: #c7bcd4; color: #fcfdff;">
                            <h4 class="panel-title">'.$controllerId. '.&nbsp;&nbsp;&nbsp;'.$controllerName.'</h4>
                        </a>
                        <div id="collapse-controller-'.$controllerId.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-controller-'.$controllerId.'" style="position:relative; padding-bottom:20px;">
                            <div id="sys-controller-body-'.$controllerId.'" class="panel-body" style="padding: 30px;">
                                
                            </div>
                        </div>
                    </div>');
            }

            if(sizeof($phpControllerList) > 0) $appendHtml = $appendHtml.'<h3 style="margin-top: 50px;">PHP</h3>';

            foreach ($phpControllerList as $controllerId) {
                $controllerName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name');

                $appendHtml = ($appendHtml.
                    '<div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading-controller-'.$controllerId.'" onclick="openController('.$controllerId.', 2);" data-toggle="collapse" data-parent="#accordion-controller-'.$systemId.'" href="#collapse-controller-'.$controllerId.'" aria-expanded="false" aria-controls="collapse-controller-'.$controllerId.'" style="background: #b3b3b3; color: #fcfdff;">
                            <h4 class="panel-title">'.$controllerId. '.&nbsp;&nbsp;&nbsp;'.$controllerName.'</h4>
                        </a>
                        <div id="collapse-controller-'.$controllerId.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-controller-'.$controllerId.'" style="position:relative; padding-bottom:20px;">
                            <div id="sys-controller-body-'.$controllerId.'" class="panel-body" style="padding: 30px;">
                                
                            </div>
                        </div>
                    </div>');
            }

            if($appendHtml == '')
                $appendHtml = '<p style="text-align: center;"><strong>No Controllers Developed!</strong></p>';

            return json_encode($appendHtml);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    public function newController() {
        //Function data
        $exFunctionId = 12;

        $systemId = Input::get('systemid');
        $lang = Input::get('lang');
        $name = Input::get('name');
        $description = Input::get('description');

        try {
            $controllerNameValidator = SystemControllerModel::where('controller_name', $name)->where('lang', $lang)->pluck('id');

            if($controllerNameValidator) {
                return -1;
            }
            else {
                //Setup files and folders in the server
                //$userName = Auth::user()->fname.' '.Auth::user()->lname;
                $date = \Carbon\Carbon::now('Pacific/Auckland');
                $primary = System::where('id', $systemId)->pluck('primary');

                //setup file url
                $systemName = strtolower(System::where('id', $systemId)->pluck('name'));
                if($primary == 1)
                    $pathRoute = $lang == 'PHP' ? 'app/controllers' : 'assets/backend/js/custom';
                else
                    $pathRoute = $lang == 'PHP' ? 'app/controllers' : 'assets/js';

                $fileUrl = str_replace(' ', '', sprintf( "%s/%s/",$pathRoute, $systemName));


                //create records in the database
                $controller = new SystemControllerModel();
                $controller -> systemid = $systemId;
                $controller -> lang = $lang;
                $controller -> controller_url = $fileUrl;
                $controller -> controller_name = $name;
                $controller -> description = $description;
                $controller -> status = 2;
                $controller -> created_by = Auth::user()->id;
                $controller -> created_at = \Carbon\Carbon::now('UTC');
                $controller -> save();
                $controllerId = $controller -> id;

                $userName = Auth::user()->fname.' '.Auth::user()->lname;

                if($lang == 'PHP') {
                    //region PHP Content
                    $content = "<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - $userName
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - $date.
 * Controller ID - $controllerId
 */

class $name extends BaseController
{
    const SystemId = $systemId;
    const ControllerId = $controllerId;
}

";
                    //endregion
                    $fileName = sprintf('%s.php', $name);
                }
                else if($lang == 'JS') {
                    //region JS Content
                    $content = "
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - $userName
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - $date.
 * Controller ID - $controllerId
 */

'use strict';


";
                    //endregion
                    $fileName = sprintf('%s.js', $name);
                }

                //endregion
                if (!file_exists($fileUrl)) {
                    mkdir($fileUrl, 0777);
                }
                $fp = fopen( $_SERVER['DOCUMENT_ROOT'] .'/'. $fileUrl. $fileName, "w" );
                fwrite( $fp, $content );
                fclose( $fp );

                //change the controller status if the controller properly created
                SystemControllerModel::where('id', $controllerId)->update(array('status' => 1));

                return 1;
            }


        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }


    public function getControllerData() {
        //Function data
        $exFunctionId = 13;

        $controllerId = Input::get('controllerid');

        try {

            $functionList = SystemFunction::where('controllerid', $controllerId)->where('status', 1)->lists('id');
            $arr = array();

            foreach ($functionList as $functionId) {
                $arr[] = array(
                    'id' => $functionId,
                    'name' => SystemFunction::where('id', $functionId)->pluck('function_name'),
                    'type' => SystemFunction::where('id', $functionId)->pluck('method'),
                    'ajax' => SystemFunction::where('id', $functionId)->pluck('ajax')
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    public function newFunction() {
        //Function data
        $exFunctionId = 14;

        //need to develop from here...
        $controllerId = Input::get('controllerid');
        $type = Input::get('type');
        $name = Input::get('name');
        $method = Input::get('method');
        $scriptId = Input::get('script');
        $controller = Input::get('controller');
        $description = Input::get('description');

        try {
            $functionNameValidator = SystemFunction::where('controllerid', $controllerId)->where('function_name', $name)->pluck('id');
            if($functionNameValidator) {
                return -1;
            }
            else {
                if($type == 1) {
                    $userName = Auth::user()->fname.' '.Auth::user()->lname;
                    $date = \Carbon\Carbon::now('UTC');

                    $systemId = SystemControllerModel::where('id', $controllerId)->pluck('systemid');
                    $systemName = System::where('id', $systemId)->pluck('name');
                    $primary = System::where('id', $systemId)->pluck('primary');

                    //create the js data and js file-----------------
                    //create the js link that need to be opened
                    $jsLink = SystemControllerModel::where('id', $scriptId)->pluck('controller_url');
                    $scriptName = SystemControllerModel::where('id', $scriptId)->pluck('controller_name').'.js';

                    //create the js function
                    $scriptUrl = $_SERVER['DOCUMENT_ROOT'] .'/' . $jsLink;

                    //ajax data
                    $routeUrl = strtolower($systemName).'/'.$name;
                    $ajaxMethod = $method == 1 ? 'GET' : 'POST';
                    $ajax = $type == 1 ? 1:0;

                    $jsFunctionId = $this->createJsFunction($name, $scriptUrl, $scriptName, $ajaxMethod, $scriptId, $ajax, $routeUrl, $description);

                    //create php function
                    $phpLink = SystemControllerModel::where('id', $controller)->pluck('controller_url');
                    $phpName = SystemControllerModel::where('id', $controller)->pluck('controller_name').'.php';

                    $phpUrl = $_SERVER['DOCUMENT_ROOT'] .'/' . $phpLink;

                    $phpFunctionId = $this->createPhpFunction($name, $phpUrl, $phpName, $ajaxMethod, $controller, $ajax, $description);
                    $routeUrl = strtolower(str_replace(' ', '', $routeUrl));

                    $controllerName = SystemControllerModel::where('id', $controller)->pluck('controller_name');
                    $systemName = strtolower(str_replace(' ', '', $systemName));

                    if($primary == 1) {
                        $uses = $controllerName.'@'.$name;
                    }
                    else {
                        $uses = $controllerName.'@'.$name;
                    }



                    //create routes for ajax
                    $route = new SystemUrlRoutes();
                    $route -> systemid = $systemId;
                    $route -> type = strtolower($ajaxMethod);
                    $route -> url = $routeUrl;
                    $route -> routeas = str_replace('/', '-', $routeUrl);
                    $route -> uses = $uses;
                    $route -> phpcontrollerid = $controller;
                    $route -> phpfunctionid = $phpFunctionId;
                    $route -> jscontrollerid = $scriptId;
                    $route -> jsfunctionid = $jsFunctionId;
                    $route -> status = 1;
                    $route -> created_by = Auth::user()->id;
                    $route -> created_at = \Carbon\Carbon::now('UTC');
                    $route -> save();

                }
                else if($type == 2) {
                    //create php function
                    $phpLink = SystemControllerModel::where('id', $controllerId)->pluck('controller_url');
                    $phpName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name').'.php';

                    $phpUrl = $_SERVER['DOCUMENT_ROOT'] .'/' . $phpLink;

                    $phpFunctionId = $this->createPhpFunction($name, $phpUrl, $phpName, null, $controller, 0, $description);
                }
                else if($type == 3) {
                    $systemId = SystemControllerModel::where('id', $controllerId)->pluck('systemid');
                    $systemName = System::where('id', $systemId)->pluck('name');

                    $jsLink = SystemControllerModel::where('id', $controllerId)->pluck('controller_url');
                    $scriptName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name').'.js';

                    //create the js function
                    $scriptUrl = $_SERVER['DOCUMENT_ROOT'] .'/' . $jsLink;

                    //ajax data
                    $ajaxUrl = str_replace(' ', '', URL::To('/') .'/' . strtolower($systemName).'/'.$name);
                    $ajaxMethod = $method == 1 ? 'GET' : 'POST';
                    $ajax = $type == 1 ? 1:0;

                    $jsFunctionId = $this->createJsFunction($name, $scriptUrl, $scriptName, null, $scriptId, 0, null, $description);
                }
                else {
                    return -2;
                }

                return 1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

    private function createJsFunction($name, $url, $fileName, $ajaxMethod, $controllerId, $ajax, $ajaxUrl, $description) {
        //Function data
        $exFunctionId = 63;

        try {
            //create records in the database
            $jsFunction = new SystemFunction();
            $jsFunction -> controllerid = $controllerId;
            $jsFunction -> ajax = $ajax;
            $jsFunction -> method = strtolower($ajaxMethod);
            $jsFunction -> function_name = $name;
            $jsFunction -> description = $description;
            $jsFunction -> status = 2;
            $jsFunction -> created_by = Auth::user()->id;
            $jsFunction -> created_at = \Carbon\Carbon::now('UTC');
            $jsFunction -> save();
            $jsFunctionId = $jsFunction -> id;

            if($ajax == 0) {
                //region content
                $content = "
                    
    //$description [ID: $jsFunctionId]
    function $name() {
        //develop the function here
        
    }

";
                //endregion
            }
            else {
                $ajaxUrl = strtolower($ajaxUrl);
                //region content
                $content = "
                    
    //$description [ID: $jsFunctionId]
    function $name() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: '$ajaxMethod',
            url: window.domain + '$ajaxUrl',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

";
                //endregion
            }

            $fileUrl = sprintf("%s%s", $url, $fileName);

            if (!file_exists($fileUrl)) {
                mkdir($fileUrl, 0777);
            }
            if (file_exists($fileUrl)) {

                //add function to the relevant function
                $file     = rtrim( file_get_contents( $fileUrl ) );
                $file     = sprintf( "%s%s", $file, $content );
                $fp       = fopen( $fileUrl, "wb" );
                fwrite( $fp, $file );
                fclose( $fp );

                //change the database status if the function properly created
                SystemFunction::where('id', $jsFunctionId)->update(array('status' => 1));
            }


            return $jsFunctionId;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }

    }

    private function createPhpFunction($name, $url, $fileName, $ajaxMethod, $controllerId, $ajax, $description) {
        //Function data
        $exFunctionId = 64;

        try {
            //create records in the database
            $phpFunction = new SystemFunction();
            $phpFunction -> controllerid = $controllerId;
            $phpFunction -> ajax = $ajax;
            $phpFunction -> method = strtolower($ajaxMethod);
            $phpFunction -> function_name = $name;
            $phpFunction -> description = $description;
            $phpFunction -> status = 2;
            $phpFunction -> created_by = Auth::user()->id;
            $phpFunction -> created_at = \Carbon\Carbon::now('UTC');
            $phpFunction -> save();
            $phpFunctionId = $phpFunction -> id;

            //region content
            $content = "
                    
    public function $name() {
        //Function data
        \$functionId = $phpFunctionId;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception \$ex) {
            \$exception = new ErrorController();
            \$exception -> saveException(self::SystemId, self::ControllerId, \$functionId, \$ex);
            return 0;
        }
    }
}";
            //endregion

            $fileUrl = sprintf("%s%s", $url, $fileName);

            if (!file_exists($fileUrl)) {
                mkdir($fileUrl, 0777);
            }
            if (file_exists($fileUrl)) {

                //add function to the relevant function
                $file     = rtrim( file_get_contents( $fileUrl ) );
                $file     = rtrim( $file, '}' );
                $file     = sprintf( "%s%s", $file, $content );
                $fp       = fopen( $fileUrl, "wb" );
                fwrite( $fp, $file );
                fclose( $fp );

                //change the database status if the function properly created
                SystemFunction::where('id', $phpFunctionId)->update(array('status' => 1));
            }


            return $phpFunctionId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }

    }



    public function getSystemScript() {
        //Function data
        $exFunctionId = 15;

        $systemId = Input::get('systemid');

        try {
            $systemJsScriptList = SystemControllerModel::where('systemid', $systemId)->where('lang', 'JS')->where('status', 1)->lists('id');
            $systemPhpControllerList = SystemControllerModel::where('systemid', $systemId)->where('lang', 'PHP')->where('status', 1)->lists('id');
            $jsArr = array();
            $phpArr = array();
            $arr = array();

            foreach ($systemJsScriptList as $jsId) {
                $jsArr []= array(
                    'id' => $jsId,
                    'name' => SystemControllerModel::where('id', $jsId)->pluck('controller_name')
                );
            }

            foreach ($systemPhpControllerList as $phpId) {
                $phpArr []= array(
                    'id' => $phpId,
                    'name' => SystemControllerModel::where('id', $phpId)->pluck('controller_name')
                );
            }

            $arr = array(
                'js' => $jsArr,
                'php' => $phpArr
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }
    }

                    
    public function loadNewView() {
        //Function data
        $functionId = 68;

        $systemId = Input::get('systemid');

        try {
            $controllerList = SystemControllerModel::where('systemid', $systemId)->where('lang', 'PHP')->lists('id');
            $arr = array();

            foreach ($controllerList as $controllerId) {
                $controllerName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name');

                $arr[] = array(
                    'id' => $controllerId,
                    'name' => $controllerName
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addView() {
        //Function data
        $functionId = 70;

        $name = Input::get('name');
        $systemId = Input::get('systemid');
        $controllerId = Input::get('controllerid');
        $allowDefault = Input::get('allowdefault');
        $listOrder = Input::get('listorder');
        $icon = Input::get('icon');
        $description = Input::get('description');
        $url = Input::get('url');
        $showMenu = Input::get('showmenu');

        try {
            $functionName = 'get'.ucfirst(str_replace(' ', '', $name));
            $systemName = strtolower(System::where('id', $systemId)->pluck('name'));
            $url = strtolower(str_replace(' ', '-', $url));

            //create view get function
            $viewFunction = new SystemFunction();
            $viewFunction -> controllerid = $controllerId;
            $viewFunction -> ajax = 0;
            $viewFunction -> method = 'get';
            $viewFunction -> function_name = $functionName;
            $viewFunction -> description = $description;
            $viewFunction -> status = 2;
            $viewFunction -> created_by = Auth::user()->id;
            $viewFunction -> created_at = \Carbon\Carbon::now('UTC');
            $viewFunction -> save();
            $viewFunctionId = $viewFunction->id;

            //add view to database
            $view = new SystemView();
            $view -> view_name = $name;
            $view -> menu_name = $name;
            $view -> systemid = $systemId;
            $view -> url_route = null;
            $view -> functionid = $viewFunctionId;
            $view -> main = 0;
            $view -> icon = $icon;
            $view -> allow_default = $allowDefault;
            $view -> list_order = $listOrder;
            $view -> show_menu = $showMenu;
            $view -> status = 2;
            $view -> created_by = Auth::user()->id;
            $view -> created_at = \Carbon\Carbon::now('UTC');
            $view -> save();
            $viewId = $view -> id;

            //genarate data for route
            $url = sprintf('%s/%s', strtolower($systemName), $url);
            $primary = System::where('id', $systemId)->pluck('primary');
            $controllerName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name');

            $uses = $controllerName.'@'.$functionName;

            //add the url route
            $route = new SystemUrlRoutes();
            $route -> systemid = $systemId;
            $route -> type = 'get';
            $route -> url = $url;
            $route -> routeas = str_replace('/', '-', $url);
            $route -> uses = $uses;
            $route -> phpcontrollerid = $controllerId;
            $route -> phpfunctionid = $viewFunctionId;
            $route -> jscontrollerid = 0;
            $route -> jsfunctionid = 0;
            $route -> status = 1;
            $route -> created_by = Auth::user()->id;
            $route -> created_at = \Carbon\Carbon::now('UTC');
            $route -> save();
            $routeId = $route -> id;

            //view printing data
            $userName = Auth::user()->fname.' '.Auth::user()->lname;
            $date = \Carbon\Carbon::now('UTC');

            //region View Content
            $content = "<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - $userName
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - $date.
 * View ID - $viewId
 */
 ?>

@extends('backend.master')

@section('content')

    <div class=\"right_col\" role=\"main\">
        <div id=\"property-container\">
            <div class=\"col-xs-12\">
                <h3 style=\"text-align: center; font-size: 28px;\"><i class=\"fa $icon\"></i> $name</h3>
            </div>
            <div class=\"col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 10px;\">
            
            <!-- new html here -->
            <h2>We are working on this! </h2>
            
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- scripts here... -->
@endsection
";
            //endregion

            //setup file url

            $pathRoute = $_SERVER['DOCUMENT_ROOT'] .'/'.'app/views/backend';

            $filePath = str_replace(' ', '', sprintf( "%s/%s/",$pathRoute, $systemName));
            $fileName = sprintf('%s.blade.php', str_replace(' ', '-', strtolower($name)));
            $fileUrl = $filePath.$fileName;


            if ( ! file_exists( $filePath ) ) {
                mkdir( $filePath, 0777 );
            }
            $fp = fopen( $fileUrl, "wb" );
            fwrite( $fp, $content );
            fclose( $fp );

            $name = str_replace(' ', '-', strtolower($name));

            //create the function to call the view

            //region content
            $content = "

    public function $functionName() {
        \$viewId = $viewId;
        \$urlRouteId = $routeId;

        \$access = new AccessController();
        \$visitMonitor = \$access -> visitMonitor(\$viewId);
        \$accessCheck = \$access ->getSecureRoute(\$viewId);

        if(\$visitMonitor) {
            if(\$accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if(\$accessCheck == 1) {
                return View::make('backend.$systemName.$name', array(
                    'systemId' => self::SystemId,
                    'viewId' => \$viewId
                ));
            }
            else {
                \$access -> denyAccess(\$urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }
}";
            //endregion

            //create the controller file url
            $controllerUrl = SystemControllerModel::where('id', $controllerId)->pluck('controller_url');
            $controllerName = SystemControllerModel::where('id', $controllerId)->pluck('controller_name');
            $controllerFileUrl = sprintf('%s/%s.php', $controllerUrl, $controllerName);

            if (!file_exists($controllerFileUrl)) {
                mkdir($controllerFileUrl, 0777);
            }
            if (file_exists($controllerFileUrl)) {

                //add function to the relevant function
                $file = rtrim(file_get_contents($controllerFileUrl));
                $file = rtrim($file, '}');
                $file = sprintf("%s%s", $file, $content);
                $fp = fopen($controllerFileUrl, "wb");
                fwrite($fp, $file);
                fclose($fp);
            }

            SystemView::where('id', $viewId)->update(array('status' => 1));
            SystemFunction::where('id', $viewFunctionId)->update(array('status' => 1));
            SystemView::where('id', $viewId)->update(array('url_route' => $routeId));

            return 1;

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addOperation() {
        //Function data
        $functionId = 94;

        $name = Input::get('name');
        $icon = Input::get('icon');
        $description = Input::get('description');
        $systemId = Input::get('systemid');
        $viewId = Input::get('viewid');

        try {
            $operation = new SystemOperation();
            $operation -> name = $name;
            $operation -> systemid = $systemId;
            $operation -> functionid = null;
            $operation -> viewid = $viewId;
            $operation -> icon = $icon;
            $operation -> description = $description;
            $operation -> status = 1;
            $operation -> created_at = \Carbon\Carbon::now('UTC');
            $operation -> save();
            $operationId = $operation -> id;

            return $operationId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addOperationChoice() {
        //Function data
        $functionId = 98;

        $operationId = Input::get('operationid');
        $choice = Input::get('choice');

        try {
            $newChoice = new SystemOperationChoises();
            $newChoice -> operationid = $operationId;
            $newChoice -> choise = $choice;
            $newChoice -> status = 1;
            $newChoice -> created_at = \Carbon\Carbon::now('UTC');
            $newChoice -> save();

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}