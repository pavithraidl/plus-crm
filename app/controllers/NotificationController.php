<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {7/05/17} {20:30}.
 */
 
class NotificationController extends BaseController {

    public function saveNotification($msg, $userId, $url) {

        try {
            $notification = new Notifications();
            $notification -> msg = $msg;
            $notification -> url = $url;
            $notification -> userid = $userId;
            $notification -> status = 1;
            $notification -> created_at = \Carbon\Carbon::now('UTC');
            $notification -> save();

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('NotificationController', 'saveNotification', $ex);
            return $ex;
        }
    }
}