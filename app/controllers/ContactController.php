<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {27/03/17} {22:32}.
 */
 
class ContactController extends BaseController {

    public function name() {
    
    }

    public function uploadAvatar() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;
        $ext = strtolower($imageFileType);


        try {
            //create the contact avatar record in the modal
            $avatar = new ContactAvatar();
            $avatar -> status = 1;
            $avatar -> created_at = \Carbon\Carbon::now('UTC');
            $avatar -> save();
            $avatarId = $avatar -> id;

            //get the filename from the new avatarid
            $dirPath = "assets/images/contact/avatar/";
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if($ext == "jpg" || $ext == "jpeg") {
                $fileType = 1;
            }
            else {
                $fileType = 0;
            }

            if($fileType == 0){
                return json_encode("-1");
            }
            else{
                $name = sprintf("%s.jpg", $avatarId);
                $ret = sprintf('%s%s', $dirPath, $name);

                Input::file('file')->move($dirPath,$name);

                if($fileType == 1) {
                    $imageResize = new imageProcessingController();
                    $resizeResult = $imageResize->setImageSizes($ret,80, 100, 100);
                }

                $ret = array(
                    'avatarid' => $avatarId,
                    'src' => $ret
                );

                return $ret;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> saveException('ContactController', 'uploadAvatar', $ex);
            return 0;
        }
    }

    public function addNewContact() {
        $avatarId = Input::get('avatarid');
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $phone1 = Input::get('phone1');
        $phone2 = Input::get('phone2');
        $phone3 = Input::get('phone3');
        $phoneType1 = Input::get('phonetype1');
        $phoneType2 = Input::get('phonetype2');
        $phoneType3 = Input::get('phonetype3');
        $email1 = Input::get('email1');
        $email2 = Input::get('email2');
        $street = Input::get('street');
        $suburb = Input::get('suburb');
        $city = Input::get('city');
        $postalCode = Input::get('postalcode');

        try {
            $contact = new Contact();
            $contact -> fname = $fName;
            $contact -> lname = $lName;
            $contact -> avatarid = $avatarId;
            $contact -> orgid = Auth::user()->orgid;
            $contact -> userid = Auth::user()->id;
            $contact -> status = 1;
            $contact -> created_at = \Carbon\Carbon::now('UTC');
            $contact -> save();
            $contactId = $contact -> id;

            if($phone1 != null || $phone1 != '') {
                $phone = new ContactPhone();
                $phone -> contactid = $contactId;
                $phone -> number = $phone1;
                $phone -> type = $phoneType1;
                $phone -> status = 1;
                $phone -> created_at = \Carbon\Carbon::now('UTC');
                $phone -> save();
            }
            else if($phone2 != null || $phone2 != '') {
                $phone = new ContactPhone();
                $phone -> contactid = $contactId;
                $phone -> number = $phone2;
                $phone -> type = $phoneType2;
                $phone -> status = 1;
                $phone -> created_at = \Carbon\Carbon::now('UTC');
                $phone -> save();
            }
            else if($phone3 != null || $phone3 != '') {
                $phone = new ContactPhone();
                $phone -> contactid = $contactId;
                $phone -> number = $phone3;
                $phone -> type = $phoneType3;
                $phone -> status = 1;
                $phone -> created_at = \Carbon\Carbon::now('UTC');
                $phone -> save();
            }

            if($email1 != null || $email1 != '') {
                $email = new ContactEmail();
                $email -> contactid = $contactId;
                $email -> email = $email1;
                $email -> status = 1;
                $email -> created_at = \Carbon\Carbon::now('UTC');
                $email -> save();
            }
            else if($email2 != null || $email2 != '') {
                $email = new ContactEmail();
                $email -> contactid = $contactId;
                $email -> email = $email2;
                $email -> status = 1;
                $email -> created_at = \Carbon\Carbon::now('UTC');
                $email -> save();
            }

            if($street != '' || $suburb != '' || $city != '' || $postalCode != '') {
                $address = new ContactAddress();
                $address -> contactid = $contactId;
                $address -> street = $street;
                $address -> adline1 = $suburb;
                $address -> city = $city;
                $address -> postalcode = $postalCode;
//                $address -> country = $country;
                $address -> status = 1;
                $address -> created_at = \Carbon\Carbon::now('UTC');
                $address -> save();
            }

            $ret = array(
                'id' => $contactId,
                'avatarid' => $avatarId,
                'fname' => $fName,
                'lname' => $lName
            );

            return $ret;

        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> saveException('ContactController', 'addNewContact', $ex);
            return 0;
        }
    }
}