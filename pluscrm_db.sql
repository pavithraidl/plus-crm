-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 25, 2017 at 02:40 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pluscrm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `general_address`
--

DROP TABLE IF EXISTS `general_address`;
CREATE TABLE `general_address` (
  `id` int(11) NOT NULL,
  `street` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adline1` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adline2` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_email`
--

DROP TABLE IF EXISTS `general_email`;
CREATE TABLE `general_email` (
  `id` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_telephone`
--

DROP TABLE IF EXISTS `general_telephone`;
CREATE TABLE `general_telephone` (
  `id` int(11) NOT NULL,
  `telephone` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `org_departments`
--

DROP TABLE IF EXISTS `org_departments`;
CREATE TABLE `org_departments` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `authorid` int(11) DEFAULT NULL,
  `projecturl` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `technologies` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `order_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `clientid`, `authorid`, `projecturl`, `type`, `technologies`, `description`, `startdate`, `enddate`, `status`, `order_by`, `created_by`, `created_at`) VALUES
(1, 'Haws', NULL, 10, 'http://www.haws.sg/home/', 2, 'Laravel', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-09-01', '2014-10-14', 1, 6, 10, '2017-06-18 01:00:46'),
(2, 'Basic Deco', NULL, 10, 'http://basicdeco.com.sg/', 2, 'Laravel', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-10-15', '2014-10-30', 1, 8, 10, '2017-06-18 01:00:46'),
(3, 'Lee Hung Ming Eye Centre', NULL, 10, 'http://www.leehmeyecentre.com.sg/', 2, 'Laravel', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-10-31', '2014-11-06', 1, 10, 10, '2017-06-18 01:03:48'),
(4, 'Amaris B. Clinic', NULL, 10, 'http://www.amaris-b.com/', 2, 'Laravel', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-11-10', '2014-11-25', 1, 11, 10, '2017-06-18 01:03:49'),
(5, 'YEAP', NULL, 10, 'http://dryeapplasticsurgery.sg/', 2, 'WordPress', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-11-25', '2014-12-15', 1, 12, 10, '2017-06-18 01:05:59'),
(6, 'Carpe Diem', NULL, 10, 'http://carpediem.com.sg/', 2, 'WordPress', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2014-12-16', '2014-12-23', 1, 13, 10, '2017-06-18 01:07:46'),
(7, 'Aesthetic & Reconstructive Centre', NULL, 10, 'http://www.andrewkhoo.com/', 2, 'WordPress', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2015-01-05', '2015-01-16', 1, 14, 10, '2017-06-18 01:11:10'),
(8, 'Car Club', NULL, 10, 'https://www.carclub.com.sg/', 2, 'Laravel', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2015-01-19', '2015-01-30', 1, 9, 10, '2017-06-18 01:11:10'),
(9, 'Kates Catering Services', NULL, 10, 'http://www.katescatering.com.sg/', 2, 'WordPress', 'One of the Singapore websites completed under Finetech (Pvt)Ltd. Both front end and the CMS is developed from the scratch.', '2015-02-02', '2015-02-06', 1, 15, 10, '2017-06-18 01:12:26'),
(10, 'Sri Lanka European Union Business Council', NULL, 10, 'http://sleubc.com/', 2, 'Laravel', 'Developed a separate website for the organization from the scratch with a CMS as well. Used Laravel to develop the website and the CMS.', '2015-09-22', '2015-10-29', 1, 7, 10, '2017-06-18 02:05:29'),
(11, 'My To Do List', NULL, 10, 'http://mytodolist.co.nz/', 2, 'WordPress', 'Developed a WordPress website for a NZ Cleaning company', '2016-07-24', '2016-12-30', 1, 5, 10, '2017-06-18 07:04:40'),
(12, 'Zego Sports Boats', NULL, 10, 'http://www.zegosportsboats.com/', 2, 'Laravel', 'Dealer Management system and Dealer controlled website', '2016-12-16', '2017-02-17', 1, 4, 10, '2017-06-18 07:04:41'),
(13, 'Drone Repair Landing Page', NULL, 10, 'http://isuru.host/staging/drone/', 2, 'Laravel', 'A single landing page for drone repair', '2017-06-08', '2017-06-08', 1, 3, 10, '2017-06-18 07:04:41'),
(14, 'Property Manager', NULL, 10, 'http://isuru.host/staging/propertymanager/property', 2, 'Laravel', 'System to manage properties, inspections and tenants', '2017-03-06', NULL, 1, 2, 10, '2017-06-18 07:04:41'),
(15, 'Olympia Fitness Center', NULL, 10, 'http://isuru.host/staging/olympia/', 2, 'Laravel', 'Shopping cart and fitness center website', '2016-02-13', '2016-04-09', 1, 1, 10, '2017-06-18 07:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `project_clients`
--

DROP TABLE IF EXISTS `project_clients`;
CREATE TABLE `project_clients` (
  `id` int(11) NOT NULL,
  `fname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_clients_rel_telephone`
--

DROP TABLE IF EXISTS `project_clients_rel_telephone`;
CREATE TABLE `project_clients_rel_telephone` (
  `id` int(11) NOT NULL,
  `telid` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_media`
--

DROP TABLE IF EXISTS `project_media`;
CREATE TABLE `project_media` (
  `id` int(11) NOT NULL,
  `projectid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_media`
--

INSERT INTO `project_media` (`id`, `projectid`, `type`, `url`, `alt`, `status`, `created_at`) VALUES
(1, 1, 1, NULL, 'Haws snapshot 1', 1, '2017-06-18 03:39:16'),
(2, 1, 1, NULL, 'Haws snapshot 2', 1, '2017-06-18 03:39:16'),
(3, 1, 1, NULL, 'Haws snapshot 3', 1, '2017-06-18 03:39:16'),
(4, 2, 1, NULL, 'Basic Deco snapshot 1', 1, '2017-06-18 06:34:00'),
(5, 2, 1, NULL, 'Basic Deco snapshot 2', 1, '2017-06-18 06:39:35'),
(6, 3, 1, NULL, 'Lee Hung Ming Eye Centre snapshot 1', 1, '2017-06-18 06:39:54'),
(7, 3, 1, NULL, 'Lee Hung Ming Eye Centre snapshot 2', 1, '2017-06-18 06:40:08'),
(8, 4, 1, NULL, 'Amaris B. Clinic snapshot 1', 1, '2017-06-18 06:40:49'),
(9, 5, 1, NULL, 'YEAP snapshot 1', 1, '2017-06-18 06:40:49'),
(10, 6, 1, NULL, 'Carpe Diem snapshot 1', 1, '2017-06-18 06:40:49'),
(11, 7, 1, NULL, 'Aesthetic & Reconstructive', 1, '2017-06-18 06:40:49'),
(12, 8, 1, NULL, 'Car Club snapshot 1', 1, '2017-06-18 06:40:50'),
(13, 8, 1, NULL, 'Car Club snapshot 2', 1, '2017-06-18 06:40:50'),
(14, 8, 1, NULL, 'Car Club snapshot 3', 1, '2017-06-18 06:40:50'),
(15, 9, 1, NULL, 'Kates Catering snapshot 1', 1, '2017-06-18 06:40:50'),
(16, 10, 1, NULL, 'SLEUBC snapshot 1', 1, '2017-06-18 06:40:50'),
(17, 11, 1, NULL, 'My To Do List snapshot 1', 1, '2017-06-18 06:40:51'),
(18, 11, 1, NULL, 'My To Do List snapshot 2', 1, '2017-06-18 06:40:51'),
(19, 12, 1, NULL, 'Zego snapshot 1', 1, '2017-06-18 06:40:51'),
(20, 12, 1, NULL, 'Zego snapshot 2', 1, '2017-06-18 07:09:03'),
(21, 12, 1, NULL, 'Zego snapshot 3', 1, '2017-06-18 07:09:03'),
(22, 12, 1, NULL, 'Zego snapshot 4', 1, '2017-06-18 07:09:03'),
(23, 12, 1, NULL, 'Zego snapshot 5', 1, '2017-06-18 07:09:03'),
(24, 12, 1, NULL, 'Zego snapshot 6', 1, '2017-06-18 07:09:03'),
(25, 13, 1, NULL, 'Core snapshot 1', 1, '2017-06-18 07:09:04'),
(26, 14, 1, NULL, 'Property Manager snapshot 1', 1, '2017-06-18 07:09:04'),
(27, 14, 1, NULL, 'Property Manager snapshot 2', 1, '2017-06-18 07:09:04'),
(28, 14, 1, NULL, 'Property Manager snapshot 3', 1, '2017-06-18 07:09:04'),
(29, 14, 1, NULL, 'Property Manager snapshot 4', 1, '2017-06-18 07:09:04'),
(30, 14, 1, NULL, 'Property Manager snapshot 5', 1, '2017-06-18 07:09:05'),
(31, 15, 1, NULL, 'Olympia snapshot 1', 1, '2017-06-18 07:36:35'),
(32, 15, 1, NULL, 'Olympia snapshot 2', 1, '2017-06-18 07:36:35'),
(33, 15, 1, NULL, 'Olympia snapshot 3', 1, '2017-06-18 07:36:35');

-- --------------------------------------------------------

--
-- Table structure for table `systems`
--

DROP TABLE IF EXISTS `systems`;
CREATE TABLE `systems` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `route` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `allow_default` int(11) DEFAULT NULL,
  `primary` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `systems`
--

INSERT INTO `systems` (`id`, `name`, `route`, `icon`, `listorder`, `visibility`, `allow_default`, `primary`, `status`, `created_by`, `created_at`) VALUES
(1, 'Dashboard', 'admin:dashboard', 'fa-dashboard', 1, 1, 1, 1, 1, 10, '2017-05-29 22:02:25'),
(2, 'Users', '#', 'fa-user', 103, 1, 0, 1, 1, 10, '2017-05-28 23:00:38'),
(3, 'Organizations', '#', ' fa-building-o', 99, 1, 0, 1, 1, 10, '2017-05-28 22:32:46'),
(5, 'System', 'system', 'fa-laptop', 201, 1, 0, 1, 1, 10, '2017-05-28 22:45:05'),
(6, 'Test System', '#', 'fa-hammer', 100, 1, 0, 0, 0, 10, '2017-09-16 04:31:35'),
(7, 'Test 2 system', '#', 'fa-md-user', 100, 1, 0, 0, 0, 10, '2017-09-16 04:38:33'),
(8, 'Path Test System', '#', 'fa-plus', 100, 1, 0, 0, 0, 10, '2017-09-23 03:50:57'),
(9, 'Test Account', '#', 'fa-user', 100, 1, 0, 0, 0, 10, '2017-09-29 09:56:24'),
(10, 'Clients', '#', 'fa-folder-open', 2, 1, 0, 0, 1, 10, '2017-10-04 08:33:09'),
(13, 'Task', '#', 'fa-list', 100, 1, 0, 0, 1, 10, '2017-10-15 08:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `system_class_path`
--

DROP TABLE IF EXISTS `system_class_path`;
CREATE TABLE `system_class_path` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_class_path`
--

INSERT INTO `system_class_path` (`id`, `systemid`, `path`, `status`, `created_at`) VALUES
(1, 10, 'clients', 1, '2017-09-22 22:09:15'),
(2, 8, 'path test system', 1, '2017-09-23 03:50:57'),
(3, 9, 'test account', 1, '2017-09-29 09:56:24'),
(5, 11, 'tasks', 1, '2017-10-11 12:28:35'),
(7, 13, 'task', 1, '2017-10-15 08:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `system_controllers`
--

DROP TABLE IF EXISTS `system_controllers`;
CREATE TABLE `system_controllers` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller_url` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_controllers`
--

INSERT INTO `system_controllers` (`id`, `systemid`, `lang`, `controller_url`, `controller_name`, `description`, `status`, `created_by`, `created_at`) VALUES
(1, 5, 'PHP', 'app/controllers/', 'AccessController', 'Controls and monitor the user access', 1, 10, '2017-05-28 23:14:42'),
(2, 5, 'PHP', 'app/controllers/', 'SystemController', 'Controls the system administrators functionalities', 1, 10, '2017-05-28 23:25:09'),
(3, 1, 'PHP', 'app/controllers/', 'DashboardController', 'Controls all the dashboard functionalities', 1, 10, '2017-05-28 23:34:33'),
(4, 2, 'PHP', 'app/controllers/', 'UserController', 'Controls all the user functionalitis', 1, 10, '2017-05-28 23:35:39'),
(5, 3, 'PHP', 'app/controllers/', 'OrganizationController', 'Controls all the organization functionalities', 1, 10, '2017-05-28 23:36:25'),
(37, 7, 'PHP', 'app/controllers/test2system/', 'validateController', 'test controller', 1, 10, '2017-09-23 02:29:39'),
(40, 9, 'PHP', 'app/controllers/testaccount/', 'TestAccountController', 'test controller', 1, 10, '2017-09-29 09:56:58'),
(41, 9, 'JS', 'assets/js/testaccount/', 'TestAccount', 'test controller', 1, 10, '2017-09-29 09:57:38'),
(43, 10, 'PHP', 'app/controllers/clients/', 'AgreementController', 'tempery controller to remove', 1, 10, '2017-10-04 08:52:02'),
(44, 10, 'JS', 'assets/js/clients/', 'agreement', 'tempery js file', 1, 10, '2017-10-05 00:32:38'),
(13, 5, 'JS', 'assets/backend/js/custom/system/', 'manage', 'Controls system manage page main functionalities', 1, 10, '2017-06-15 03:09:11'),
(42, 10, 'PHP', 'app/controllers/clients/', 'ClientController', 'controlls the client section', 1, 10, '2017-10-04 08:33:39'),
(45, 2, 'JS', 'assets/backend/js/custom/users/', 'user-manage', 'manage users', 1, 10, '2017-10-09 05:28:19'),
(39, 8, 'PHP', 'app/controllers/pathtestsystem/', 'pathTestController', 'testing the path', 1, 10, '2017-09-23 03:51:27'),
(38, 7, 'JS', 'assets/js/test2system/', 'validateController', 'test controller', 1, 10, '2017-09-23 02:29:53'),
(36, 7, 'JS', 'assets/js/test2system/', 'testjs', 'js test', 1, 10, '2017-09-17 12:40:36'),
(33, 7, 'PHP', 'app/controllers/test2system/', 'testController', 'test controller', 1, 10, '2017-09-17 08:46:30'),
(32, 5, 'PHP', 'app/controllers/system/', 'VariableController', 'Control all the system variables', 1, 10, '2017-09-17 04:19:01'),
(35, 7, 'JS', 'assets/js/test2system/', 'testjs', 'testing javascirpt controller', 2, 10, '2017-09-17 12:38:58'),
(48, 13, 'PHP', 'app/controllers/task/', 'TaskController', '', 1, 10, '2017-10-15 08:40:16');

-- --------------------------------------------------------

--
-- Table structure for table `system_deny_access`
--

DROP TABLE IF EXISTS `system_deny_access`;
CREATE TABLE `system_deny_access` (
  `id` int(11) NOT NULL,
  `urlrouteid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_exceptions`
--

DROP TABLE IF EXISTS `system_exceptions`;
CREATE TABLE `system_exceptions` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `controllerid` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functionid` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exception` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_exceptions`
--

INSERT INTO `system_exceptions` (`id`, `systemid`, `controllerid`, `functionid`, `exception`, `userid`, `ip`, `status`, `created_at`) VALUES
(1, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:02:41'),
(2, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:07:06'),
(3, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'5\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:07:46'),
(4, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'5\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:08:11'),
(5, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:08:35'),
(6, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:09:10'),
(7, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:09:47'),
(8, 5, '2', '5', 'exception \'BadMethodCallException\' with message \'Method [where] does not exist.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): Illuminate\\Routing\\Controller->__call(\'where\', Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(69): SystemController->where(\'systemid\', \'1\')\n#2 [internal function]: SystemController->getSystemData()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getSystemData\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routin', 10, '101.98.86.113', 1, '2017-05-30 09:10:04'),
(9, 5, '2', '12', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/idloms/app/controllers/TestController.php): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:497\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 497, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(497): fopen(\'/home1/pavithra...\', \'wb\')\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->c', 10, '101.98.86.113', 1, '2017-06-01 10:20:30'),
(10, 5, '2', '14', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/custom//test.js): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:601\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 601, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(601): fopen(\'/home1/pavithra...\', \'w\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemCont', 10, '101.98.86.113', 1, '2017-06-06 08:53:09'),
(11, 5, '2', '14', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/custom//): failed to open stream: Is a directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:565\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 565, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(565): fopen(\'/home1/pavithra...\', \'wb\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(I', 10, '101.98.86.113', 1, '2017-06-06 09:04:57'),
(12, 5, '2', '14', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/custom//): failed to open stream: Is a directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:565\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 565, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(565): fopen(\'/home1/pavithra...\', \'wb\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(I', 10, '101.98.86.113', 1, '2017-06-06 09:08:38'),
(13, 5, '2', '14', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/custom/system/): failed to open stream: Is a directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:565\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 565, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(565): fopen(\'/home1/pavithra...\', \'wb\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Ob', 10, '101.98.86.113', 1, '2017-06-06 09:23:16'),
(14, 5, '2', '14', 'exception \'ErrorException\' with message \'file_get_contents(/home1/pavithraidl/public_html/assets/js/custom/system/test.js): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:563\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'file_get_conten...\', \'/home1/pavithra...\', 563, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(563): file_get_contents(\'/home1/pavithra...\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatche', 10, '101.98.86.113', 1, '2017-06-06 09:26:51'),
(15, 5, '2', '15', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'name\' in \'field list\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id`, `n...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `id`, `n...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `id`, `n...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `id`, `n...\', Array, Object(Closure))\n#4 /home1/pavithraidl/publi', 10, '111.69.226.100', 1, '2017-06-15 08:36:38'),
(16, 5, '2', '12', 'exception \'ErrorException\' with message \'Undefined variable: fileUrl\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:557\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(557): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 557, Array)\n#1 [internal function]: SystemController->newController()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routi', 10, '101.98.86.117', 1, '2017-06-17 04:41:14'),
(17, 5, '2', '12', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/custom/System/system-test.js): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:557\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 557, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(557): fopen(\'/home1/pavithra...\', \'w\')\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(', 10, '101.98.86.117', 1, '2017-06-17 04:42:54'),
(18, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:559\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 559, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(559): mkdir(\'/assets/js/cust...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-06-17 04:54:34'),
(19, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '165.84.31.245', 1, '2017-08-16 13:29:13'),
(20, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '101.98.86.117', 1, '2017-09-09 03:11:58'),
(21, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '101.98.86.117', 1, '2017-09-16 04:18:43'),
(22, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:567\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 567, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(567): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-09-16 04:39:55'),
(23, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:567\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 567, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(567): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-09-16 04:44:11'),
(24, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:567\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 567, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(567): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-09-16 04:46:58'),
(25, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:567\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 567, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(567): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-09-16 04:53:59'),
(26, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:568\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 568, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(568): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '101.98.86.117', 1, '2017-09-16 05:01:34'),
(27, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '123.255.26.144', 1, '2017-09-17 03:04:51'),
(28, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:568\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 568, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(568): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '123.255.26.144', 1, '2017-09-17 03:07:15'),
(29, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:568\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 568, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(568): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '123.255.26.144', 1, '2017-09-17 03:10:49'),
(30, 5, '2', '12', 'exception \'ErrorException\' with message \'mkdir(): Permission denied\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:568\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): Permis...\', \'/home1/pavithra...\', 568, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(568): mkdir(\'/app/controller...\', 511, true)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#6 /home1/pavithr', 10, '123.255.26.144', 1, '2017-09-17 03:12:50'),
(31, 5, '2', '12', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_htmlapp/controllers/test2system/Test4Controller.php): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:570\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 570, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(570): fopen(\'/home1/pavithra...\', \'w\')\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatche', 10, '123.255.26.144', 1, '2017-09-17 03:32:55'),
(32, 5, '2', '12', 'exception \'ErrorException\' with message \'Undefined variable: controllerId\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:510\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(510): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 510, Array)\n#1 [internal function]: SystemController->newController()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/', 10, '123.255.26.144', 1, '2017-09-17 03:43:17'),
(33, 5, '2', '12', 'exception \'Illuminate\\Database\\Eloquent\\MassAssignmentException\' with message \'status\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php:411\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(1399): Illuminate\\Database\\Eloquent\\Model->fill(Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(581): Illuminate\\Database\\Eloquent\\Model->update(Array)\n#2 [internal function]: SystemController->newController()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Ob', 10, '123.255.26.144', 1, '2017-09-17 03:51:13'),
(34, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '123.255.26.144', 1, '2017-09-17 12:33:51'),
(35, 5, '2', '12', 'exception \'ErrorException\' with message \'Undefined variable: fileName\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:576\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(576): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 576, Array)\n#1 [internal function]: SystemController->newController()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Rout', 10, '123.255.26.144', 1, '2017-09-17 12:38:58'),
(36, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '14.1.70.6', 1, '2017-09-18 09:30:33'),
(37, 6, '4', '16', 'exception \'ErrorException\' with message \'Missing argument 4 for ErrorController::saveException(), called in /home1/pavithraidl/public_html/app/controllers/UserController.php on line 212 and defined\' in /home1/pavithraidl/public_html/app/controllers/ErrorController.php:15\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/ErrorController.php(15): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 15, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/UserController.php(212): ErrorController->saveException(\'UserController\', \'saveActivity\', Object(Illuminate\\Database\\QueryException))\n#2 /home1/pavithraidl/public_html/app/controllers/UserController.php(67): UserController->saveActivity(\'Login to the sy...\', NULL, NULL)\n#3 [internal function]: UserController->postLogin()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/fram', 10, '123.255.26.144', 1, '2017-09-21 11:02:49'),
(38, 5, '2', '14', 'exception \'ErrorException\' with message \'Undefined variable: jsFunctionId\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:655\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(655): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 655, Array)\n#1 [internal function]: SystemController->newFunction()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newFunction\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routin', 10, '123.255.26.144', 1, '2017-09-21 11:23:28'),
(39, 5, '2', '14', 'exception \'ErrorException\' with message \'Missing argument 7 for SystemController::createPhpFunction(), called in /home1/pavithraidl/public_html/app/controllers/SystemController.php on line 659 and defined\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:740\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(740): Illuminate\\Exception\\Handler->handleError(2, \'Missing argumen...\', \'/home1/pavithra...\', 740, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(659): SystemController->createPhpFunction(\'test2Ajax\', \'/home1/pavithra...\', \'GET\', \'33\', 1, \'testajax\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunction\', Array)\n#5 /ho', 10, '123.255.26.144', 1, '2017-09-21 11:50:39'),
(40, 5, '2', '14', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/js/test2system/testjs.php): failed to open stream: Is a directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:791\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 791, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(791): fopen(\'/home1/pavithra...\', \'wb\')\n#2 /home1/pavithraidl/public_html/app/controllers/SystemController.php(659): SystemController->createPhpFunction(\'test2Ajax\', \'/home1/pavithra...\', \'testjs.php\', \'GET\', \'33\', 1, \'testajax\')\n#3 [internal function]: SystemController->newFunction()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFuncti', 10, '123.255.26.144', 1, '2017-09-21 11:51:48'),
(41, 5, '2', '14', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'craeted_at\' in \'field list\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `sy...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'insert into `sy...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'insert into `sy...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(\'insert into `sy...\', Array, Object(Closure))\n#4 /home1/pavithraidl', 10, '123.255.26.144', 1, '2017-09-21 12:37:28'),
(42, 5, '2', '14', 'exception \'ErrorException\' with message \'Argument 2 passed to str_replace_array() must be of the type array, string given, called in /home1/pavithraidl/public_html/app/controllers/SystemController.php on line 668 and defined\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Support/helpers.php:896\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Support/helpers.php(896): Illuminate\\Exception\\Handler->handleError(4096, \'Argument 2 pass...\', \'/home1/pavithra...\', 896, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(668): str_replace_array(\'/\', \'-\', \'test2system/tes...\')\n#2 [internal function]: SystemController->newFunction()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newFunc', 10, '123.255.26.144', 1, '2017-09-21 12:42:26'),
(43, 5, '2', '12', 'exception \'ErrorException\' with message \'Undefined variable: test\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:499\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(499): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 499, Array)\n#1 [internal function]: SystemController->newController()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/', 10, '14.1.70.6', 1, '2017-10-03 08:08:58'),
(44, 5, '2', '12', 'exception \'ErrorException\' with message \'Undefined variable: test\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:499\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/SystemController.php(499): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 499, Array)\n#1 [internal function]: SystemController->newController()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'newController\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'newController\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/', 10, '14.1.70.6', 1, '2017-10-03 08:09:32');
INSERT INTO `system_exceptions` (`id`, `systemid`, `controllerid`, `functionid`, `exception`, `userid`, `ip`, `status`, `created_at`) VALUES
(45, 5, '2', '63', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/assets/backend/js/custom/Manage.js): failed to open stream: Is a directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:870\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 870, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(870): fopen(\'/home1/pavithra...\', \'wb\')\n#2 /home1/pavithraidl/public_html/app/controllers/SystemController.php(778): SystemController->createJsFunction(\'loadNewView\', \'/home1/pavithra...\', \'Manage.js\', NULL, \'13\', 0, NULL, \'get the relevan...\')\n#3 [internal function]: SystemController->newFunction()\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->call', 10, '14.1.70.6', 1, '2017-10-04 02:09:16'),
(46, 5, '2', '70', 'exception \'PDOException\' with message \'SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'jscontrollerid\' cannot be null\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'insert into `sy...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'insert into `sy...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(\'insert into `sy...\', Array, Object(Closure))\n#4 /home1/pavithra', 10, '14.1.70.6', 1, '2017-10-04 05:58:48'),
(47, 5, '2', '70', 'exception \'ErrorException\' with message \'mkdir(): No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:1199\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): No suc...\', \'/home1/pavithra...\', 1199, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(1199): mkdir(\'/home1/pavithra...\', 511)\n#2 [internal function]: SystemController->addView()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'addView\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'addView\')\n#6 /home1/pavithraidl/public_h', 10, '14.1.70.6', 1, '2017-10-04 06:00:44'),
(48, 5, '2', '70', 'exception \'ErrorException\' with message \'mkdir(): No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:1199\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): No suc...\', \'/home1/pavithra...\', 1199, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(1199): mkdir(\'/home1/pavithra...\', 511)\n#2 [internal function]: SystemController->addView()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'addView\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'addView\')\n#6 /home1/pavithraidl/public_h', 10, '14.1.70.6', 1, '2017-10-04 06:19:54'),
(49, 5, '2', '70', 'exception \'ErrorException\' with message \'fopen(/home1/pavithraidl/public_html/app/view/system/testing3-view.blade.php): failed to open stream: No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:1198\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'fopen(/home1/pa...\', \'/home1/pavithra...\', 1198, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(1198): fopen(\'/home1/pavithra...\', \'w\')\n#2 [internal function]: SystemController->addView()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'addView\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(S', 10, '14.1.70.6', 1, '2017-10-04 06:24:27'),
(50, 5, '2', '70', 'exception \'ErrorException\' with message \'mkdir(): No such file or directory\' in /home1/pavithraidl/public_html/app/controllers/SystemController.php:1198\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'mkdir(): No suc...\', \'/home1/pavithra...\', 1198, Array)\n#1 /home1/pavithraidl/public_html/app/controllers/SystemController.php(1198): mkdir(\'/home1/pavithra...\', 511)\n#2 [internal function]: SystemController->addView()\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'addView\', Array)\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(SystemController), Object(Illuminate\\Routing\\Route), \'addView\')\n#6 /home1/pavithraidl/public_h', 10, '14.1.70.6', 1, '2017-10-04 06:30:05'),
(51, 5, '2', '13', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'lang\' in \'where clause\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `id` fro...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `id` fro...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `id` fro...\', Array, Object(Closure))\n#4 /home1/pavithraidl/pub', 10, '14.1.70.6', 1, '2017-10-05 00:29:54'),
(52, 2, '4', '121', 'exception \'InvalidArgumentException\' with message \'View [emails.active] not found.\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(\'emails.active\', Array)\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(\'emails.active\')\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(\'emails.active\', Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(\'emails.active\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(147): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), \'emails.active\', NULL, Array)\n#5 /hom', 10, '14.1.70.6', 1, '2017-10-10 01:38:44'),
(53, 2, '4', '119', 'exception \'PDOException\' with message \'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'pavithra_isuru.activity_log\' doesn\'t exist\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `id` fro...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `id` fro...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `id` fro...\', Array, Object(Closure))\n#4 ', 10, '14.1.70.6', 1, '2017-10-10 01:42:37'),
(54, 2, '4', '119', 'exception \'PDOException\' with message \'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'pavithra_isuru.activity_log\' doesn\'t exist\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `id` fro...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `id` fro...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `id` fro...\', Array, Object(Closure))\n#4 ', 10, '14.1.70.6', 1, '2017-10-10 01:52:36'),
(55, 2, '4', '119', 'exception \'PDOException\' with message \'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'pavithra_isuru.activity_log\' doesn\'t exist\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `id` fro...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `id` fro...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `id` fro...\', Array, Object(Closure))\n#4 ', 10, '14.1.70.6', 1, '2017-10-10 01:54:01'),
(56, 2, '4', '117', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'status\' in \'where clause\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `viewid`...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `viewid`...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `viewid`...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `viewid`...\', Array, Object(Closure))\n#4 /home1/pavithraidl/p', 10, '14.1.70.6', 1, '2017-10-10 03:47:24'),
(57, 2, '4', '117', 'exception \'ErrorException\' with message \'Undefined variable: ope\' in /home1/pavithraidl/public_html/app/controllers/UserController.php:447\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/UserController.php(447): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 447, Array)\n#1 [internal function]: UserController->getAccessList()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getAccessList\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Routing\\Route), \'getAccessList\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Router.ph', 10, '14.1.70.6', 1, '2017-10-10 03:48:21'),
(58, 2, '4', '109', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'operation\' in \'field list\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `operati...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `operati...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `operati...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `operati...\', Array, Object(Closure))\n#4 /home1/pavithraidl/', 10, '14.1.70.6', 1, '2017-10-10 05:15:52'),
(59, 2, '4', '126', 'exception \'ErrorException\' with message \'Undefined variable: status\' in /home1/pavithraidl/public_html/app/controllers/UserController.php:635\nStack trace:\n#0 /home1/pavithraidl/public_html/app/controllers/UserController.php(635): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/home1/pavithra...\', 635, Array)\n#1 [internal function]: UserController->saveAccessChoice()\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'saveAccessChoic...\', Array)\n#4 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Routing\\Route), \'saveAccessChoic...\')\n#5 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/R', 10, '14.1.70.6', 1, '2017-10-10 05:36:02'),
(60, 2, '4', '126', 'exception \'PDOException\' with message \'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'name\' in \'field list\'\' in /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `name` f...\')\n#1 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(623): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `name` f...\', Array)\n#2 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(587): Illuminate\\Database\\Connection->runQueryCallback(\'select `name` f...\', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `name` f...\', Array, Object(Closure))\n#4 /home1/pavithraidl/publi', 10, '14.1.70.6', 1, '2017-10-10 05:38:17'),
(61, 2, '4', '121', 'exception \'InvalidArgumentException\' with message \'View [emails.active] not found.\' in /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(\'emails.active\', Array)\n#1 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(\'emails.active\')\n#2 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(\'emails.active\', Array)\n#3 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(\'emails.active\', Array)\n#4 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/M', 10, '14.1.70.6', 1, '2017-10-09 14:50:49'),
(62, 2, '4', '121', 'exception \'Swift_TransportException\' with message \'Failed to authenticate on SMTP server with username \"noreply@plusmedia-mail.nz\" using 3 possible authenticators\' in /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/Esmtp/AuthHandler.php:181\nStack trace:\n#0 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/EsmtpTransport.php(332): Swift_Transport_Esmtp_AuthHandler->afterEhlo(Object(Swift_SmtpTransport))\n#1 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(118): Swift_Transport_EsmtpTransport->_doHeloCommand()\n#2 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(79): Swift_Transport_AbstractSmtpTransport->start()\n#3 /var/www/vhosts/plusmedia.co.nz/crm.plusmedia.co.nz/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(329): Swift_Mailer->s', 10, '14.1.70.6', 1, '2017-10-09 14:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `system_functions`
--

DROP TABLE IF EXISTS `system_functions`;
CREATE TABLE `system_functions` (
  `id` int(11) NOT NULL,
  `controllerid` int(11) DEFAULT NULL,
  `ajax` int(1) DEFAULT NULL,
  `method` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'get',
  `function_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_functions`
--

INSERT INTO `system_functions` (`id`, `controllerid`, `ajax`, `method`, `function_name`, `description`, `status`, `created_by`, `created_at`) VALUES
(1, 1, 0, 'int', 'visitMonitor', 'Monitor the visit for views', 1, 10, '2017-05-28 23:56:49'),
(2, 1, 0, 'get', 'getManage', 'Get the system manage page', 1, 10, '2017-05-28 23:57:21'),
(3, 3, 0, 'get', 'getView', 'Get the Dashboard', 1, 10, '2017-05-29 00:01:29'),
(4, 2, 1, 'post', 'changeStatus', 'Change the status of the systems', 1, 10, '2017-05-29 21:57:50'),
(5, 2, 1, 'get', 'getSystemData', 'Get the basic data of each individual system', 1, 10, '2017-05-29 22:11:11'),
(6, 2, 1, 'get', 'getSystemExceptions', 'Get the exceptions of specific systems', 1, 10, '2017-05-30 05:20:18'),
(7, 2, 1, 'get', 'getExceptionData', 'Get the data of single exception', 1, 10, '2017-05-31 22:25:14'),
(8, 2, 1, 'post', 'changeExceptionStatus', 'Change the status of each exception', 1, 10, '2017-05-31 23:30:39'),
(9, 2, 1, 'post', 'changeAllExceptionStatus', 'change the all exceptions to fixed', 1, 10, '2017-05-31 23:44:11'),
(10, 2, 1, 'post', 'newSystem', 'Add a new system', 1, 10, '2017-06-01 03:11:14'),
(11, 2, 1, 'get', 'GetControllers', 'Get list of controllers of a specific system', 1, 10, '2017-06-01 04:20:39'),
(12, 2, 1, 'post', 'newController', 'Add a new controller to the system', 1, 10, '2017-06-01 04:53:05'),
(13, 2, 1, 'get', 'getControllerData', 'Get the data of a single controller', 1, 10, '2017-06-05 23:53:38'),
(14, 2, 1, 'post', 'newFunction', 'Add a new function to the system', 1, 10, '2017-06-06 02:24:14'),
(15, 2, 1, 'get', 'getSystemJsScripts', 'get the list of js available for a system', 1, 10, '2017-06-15 03:26:10'),
(16, 4, 0, 'post', 'postLogin', 'Login method', 1, 10, '2017-06-16 22:16:54'),
(17, 4, 0, 'get', 'getUsers', 'Get the users view', 1, 10, '2017-06-16 22:34:24'),
(18, 5, 0, 'get', 'getOrg', 'Get the organization view', 1, 10, '2017-06-16 22:36:18'),
(19, 32, 0, 'get', 'getVariable', 'Return the required variable', 1, 10, '2017-06-16 22:36:18'),
(20, 36, 1, 'get', 'testjs.js', 'test controller', 1, 10, '2017-09-21 11:26:08'),
(21, 36, 1, 'get', 'testjs.js', 'test controller', 1, 10, '2017-09-21 11:29:25'),
(22, 36, 1, 'get', 'test1Ajax', 'test controller', 1, 10, '2017-09-21 11:31:10'),
(23, 36, 1, 'get', 'test2Ajax', 'testajax', 1, 10, '2017-09-21 11:50:39'),
(24, 36, 1, 'get', 'test2Ajax', 'testajax', 1, 10, '2017-09-21 11:51:48'),
(25, 33, 1, 'get', 'test2Ajax', 'testajax', 2, 10, '2017-09-21 11:51:48'),
(26, 36, 1, 'get', 'test2Ajax', 'testajax', 1, 10, '2017-09-21 11:52:38'),
(27, 33, 1, 'get', 'test2Ajax', 'testajax', 1, 10, '2017-09-21 11:52:38'),
(28, 36, 1, 'get', 'test4Ajax', 'testajax', 1, 10, '2017-09-21 11:55:17'),
(29, 33, 1, 'get', 'test4Ajax', 'testajax', 1, 10, '2017-09-21 11:55:17'),
(30, 36, 1, 'get', 'test5Ajax', 'testajax', 1, 10, '2017-09-21 11:56:09'),
(31, 33, 1, 'get', 'test5Ajax', 'testajax', 1, 10, '2017-09-21 11:56:09'),
(32, 33, 0, '', 'testPhp', 'testajax', 1, 10, '2017-09-21 12:01:12'),
(33, 33, 0, '', 'test2Php', 'testing php function', 1, 10, '2017-09-21 12:03:02'),
(34, 36, 0, '', 'testJs', 'testing js function', 1, 10, '2017-09-21 12:07:43'),
(35, 36, 0, '', 'testjsJs', 'testing js effects', 1, 10, '2017-09-21 12:12:32'),
(36, 36, 0, '', 'TestLoadJs', 'testing loading js', 1, 10, '2017-09-21 12:21:13'),
(37, 33, 0, '', 'TestLoadPhp', 'testing loading php', 1, 10, '2017-09-21 12:21:51'),
(38, 33, 0, '', 'testLoadandErace', 'testing', 1, 10, '2017-09-21 12:25:22'),
(39, 36, 1, 'post', 'testRoutes', 'testing routes', 1, 10, '2017-09-21 12:37:28'),
(40, 33, 1, 'post', 'testRoutes', 'testing routes', 1, 10, '2017-09-21 12:37:28'),
(41, 36, 1, 'post', 'test2Routes', 'testing routes 2', 1, 10, '2017-09-21 12:38:39'),
(42, 33, 1, 'post', 'test2Routes', 'testing routes 2', 1, 10, '2017-09-21 12:38:39'),
(43, 36, 1, 'get', 'testRouteUrl', 'testing url', 1, 10, '2017-09-21 12:42:26'),
(44, 33, 1, 'get', 'testRouteUrl', 'testing url', 1, 10, '2017-09-21 12:42:26'),
(45, 36, 1, 'get', 'test2UrlRoutes', '', 1, 10, '2017-09-21 12:43:30'),
(46, 33, 1, 'get', 'test2UrlRoutes', '', 1, 10, '2017-09-21 12:43:30'),
(47, 36, 1, 'get', 'testUese', '', 1, 10, '2017-09-21 12:45:32'),
(48, 33, 1, 'get', 'testUese', '', 1, 10, '2017-09-21 12:45:32'),
(49, 36, 1, 'get', 'testingBackSlash', '', 1, 10, '2017-09-21 12:53:43'),
(50, 33, 1, 'get', 'testingBackSlash', '', 1, 10, '2017-09-21 12:53:43'),
(51, 37, 0, '', 'validateFunction', 'test validate function', 1, 10, '2017-09-23 02:40:27'),
(52, 8, 0, '', 'testPath', 'test path', 1, 10, '2017-09-23 03:53:20'),
(53, 39, 0, '', 'testAjax', '', 1, 10, '2017-09-23 04:01:07'),
(54, 39, 0, '', 'testGaps', 'test', 1, 10, '2017-09-23 04:01:50'),
(55, 39, 0, '', 'test2Gaps', '', 1, 10, '2017-09-23 04:02:45'),
(56, 39, 0, '', 'test3Gpas', '', 1, 10, '2017-09-23 04:03:16'),
(57, 39, 0, '', 'test4Gaps', '', 1, 10, '2017-09-23 04:03:55'),
(58, 39, 0, '', 'testGap5', '', 1, 10, '2017-09-23 04:06:08'),
(59, 41, 1, 'get', 'getData', '', 1, 10, '2017-09-29 09:58:04'),
(60, 40, 1, 'get', 'getData', '', 1, 10, '2017-09-29 09:58:04'),
(61, 36, 1, 'get', 'Navjeet', 'test', 1, 10, '2017-10-03 08:11:49'),
(62, 33, 1, 'get', 'Navjeet', 'test', 1, 10, '2017-10-03 08:11:49'),
(63, 2, 0, 'get', 'createJsFunction', 'private method', 1, 10, '2017-10-03 20:07:49'),
(64, 2, 0, 'get', 'createJPhpFunction', 'private method', 1, 10, '2017-10-03 20:09:49'),
(67, 13, 1, 'get', 'loadNewView', 'get relevant data for the view', 1, 10, '2017-10-04 02:15:45'),
(68, 2, 1, 'get', 'loadNewView', 'get relevant data for the view', 1, 10, '2017-10-04 02:15:45'),
(69, 13, 1, 'post', 'addView', 'add a new view to the system', 1, 10, '2017-10-04 02:49:05'),
(70, 2, 1, 'post', 'addView', 'add a new view to the system', 1, 10, '2017-10-04 02:49:05'),
(71, 1, 0, 'get', 'getSecureRoute', NULL, 1, 10, '2017-10-03 23:55:55'),
(72, 1, 0, 'get', 'denyAccess', 'check the unwanted access', 1, 10, '2017-10-03 23:55:55'),
(73, 2, 0, 'get', 'getTest-View', 'test the view adding function', 2, 10, '2017-10-04 05:58:48'),
(74, 2, 0, 'get', 'getTest-View', 'test', 2, 10, '2017-10-04 06:00:44'),
(75, 2, 0, 'get', 'getTest-View', 'test', 2, 10, '2017-10-04 06:10:08'),
(76, 2, 0, 'get', 'getTesting-View', 'test', 2, 10, '2017-10-04 06:16:28'),
(77, 2, 0, 'get', 'getTesting1-View', 'test', 2, 10, '2017-10-04 06:18:41'),
(78, 2, 0, 'get', 'getTesting2-View', 'test', 2, 10, '2017-10-04 06:19:54'),
(79, 2, 0, 'get', 'getTesting3-View', 'test', 2, 10, '2017-10-04 06:24:27'),
(80, 2, 0, 'get', 'getTesting4-View', 'test', 2, 10, '2017-10-04 06:30:05'),
(81, 2, 0, 'get', 'getTesting6-View', 'test', 2, 10, '2017-10-04 06:31:50'),
(82, 2, 0, 'get', 'getTesting7-View', 'test', 1, 10, '2017-10-04 06:33:07'),
(83, 2, 0, 'get', 'getTesting10View', 'test', 1, 10, '2017-10-04 07:24:24'),
(84, 2, 0, 'get', 'getTesting11View', 'test', 1, 10, '2017-10-04 07:28:39'),
(85, 2, 0, 'get', 'getTesting12View', 'test', 1, 10, '2017-10-04 07:31:36'),
(86, 2, 0, 'get', 'getTest12View', 'test', 1, 10, '2017-10-04 07:32:52'),
(87, 42, 0, 'get', 'getClients', 'clients', 1, 10, '2017-10-04 08:34:39'),
(88, 43, 0, 'get', 'getAgreements', 'agreements', 1, 10, '2017-10-04 08:53:56'),
(89, 43, 0, 'get', 'getServiceAgreement', 'agreement', 1, 10, '2017-10-05 01:38:40'),
(90, 45, 1, 'get', 'getUserList', 'get the full user list', 1, 10, '2017-10-09 05:31:39'),
(91, 4, 1, 'get', 'getUserList', 'get the full user list', 1, 10, '2017-10-09 05:31:39'),
(92, 13, 0, '', 'loadNewOperation', 'load a new operation', 1, 10, '2017-10-09 06:24:49'),
(93, 13, 1, 'post', 'addOperation', 'Add a new operation', 1, 10, '2017-10-09 06:28:39'),
(94, 2, 1, 'post', 'addOperation', 'Add a new operation', 1, 10, '2017-10-09 06:28:39'),
(95, 45, 0, '', 'showAddChoice', 'show input field to add choice', 1, 10, '2017-10-09 06:59:43'),
(96, 13, 0, '', 'showAddChoices', 'show add choices inputs', 1, 10, '2017-10-09 07:03:43'),
(97, 13, 1, 'post', 'addOperationChoice', 'add choices to operations', 1, 10, '2017-10-09 07:15:01'),
(98, 2, 1, 'post', 'addOperationChoice', 'add choices to operations', 1, 10, '2017-10-09 07:15:01'),
(99, 45, 1, 'get', 'getUserBasicDetails', '', 1, 10, '2017-10-10 00:32:59'),
(100, 4, 1, 'get', 'getUserBasicDetails', '', 1, 10, '2017-10-10 00:32:59'),
(101, 45, 1, 'get', 'selectUser', '', 1, 10, '2017-10-10 00:33:16'),
(102, 4, 1, 'get', 'selectUser', '', 1, 10, '2017-10-10 00:33:16'),
(103, 45, 0, '', 'iSwitchOnChange', '', 1, 10, '2017-10-10 00:34:12'),
(104, 45, 1, 'get', 'changeUserStatus', '', 1, 10, '2017-10-10 00:34:29'),
(105, 4, 1, 'get', 'changeUserStatus', '', 1, 10, '2017-10-10 00:34:29'),
(106, 45, 1, 'post', 'changeSystemAccess', '', 1, 10, '2017-10-10 00:34:42'),
(107, 4, 1, 'post', 'changeSystemAccess', '', 1, 10, '2017-10-10 00:34:42'),
(108, 45, 1, 'post', 'changeOperationAccess', '', 1, 10, '2017-10-10 00:34:58'),
(109, 4, 1, 'post', 'changeOperationAccess', '', 1, 10, '2017-10-10 00:34:58'),
(110, 45, 1, 'post', 'deleteUser', '', 1, 10, '2017-10-10 00:35:11'),
(111, 4, 1, 'post', 'deleteUser', '', 1, 10, '2017-10-10 00:35:11'),
(112, 45, 1, 'get', 'resendActivationEmail', '', 1, 10, '2017-10-10 00:35:31'),
(113, 4, 1, 'get', 'resendActivationEmail', '', 1, 10, '2017-10-10 00:35:31'),
(114, 45, 0, '', 'opeAddUser', '', 1, 10, '2017-10-10 00:35:46'),
(115, 45, 0, '', 'triggerActivityListLoad', '', 1, 10, '2017-10-10 00:36:07'),
(116, 45, 1, 'get', 'getAccessList', '', 1, 10, '2017-10-10 00:36:42'),
(117, 4, 1, 'get', 'getAccessList', '', 1, 10, '2017-10-10 00:36:42'),
(118, 45, 1, 'get', 'getActivityLog', '', 1, 10, '2017-10-10 00:36:55'),
(119, 4, 1, 'get', 'getActivityLog', '', 1, 10, '2017-10-10 00:36:55'),
(120, 45, 1, 'post', 'addUser', '', 1, 10, '2017-10-10 00:37:05'),
(121, 4, 1, 'post', 'addUser', '', 1, 10, '2017-10-10 00:37:05'),
(122, 45, 0, '', 'cancelUser', '', 1, 10, '2017-10-10 00:37:18'),
(123, 4, 0, 'get', 'getActivate', '', 1, 10, '2017-06-16 22:16:54'),
(124, 4, 0, 'post', 'postActive', '', 1, 10, '2017-06-16 22:16:54'),
(125, 45, 1, 'post', 'saveAccessChoice', '', 1, 10, '2017-10-10 05:28:05'),
(126, 4, 1, 'post', 'saveAccessChoice', '', 1, 10, '2017-10-10 05:28:05'),
(133, 48, 0, 'get', 'getGetTask', '', 1, 10, '2017-10-15 08:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_intrusions`
--

DROP TABLE IF EXISTS `system_intrusions`;
CREATE TABLE `system_intrusions` (
  `id` int(11) NOT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functionid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_operations`
--

DROP TABLE IF EXISTS `system_operations`;
CREATE TABLE `system_operations` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `functionid` int(1) DEFAULT NULL,
  `viewid` int(11) NOT NULL,
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_operations`
--

INSERT INTO `system_operations` (`id`, `name`, `systemid`, `functionid`, `viewid`, `icon`, `description`, `status`, `created_at`) VALUES
(3, 'View/Edit Access', 2, NULL, 4, 'fa-sliders', 'View users access tab and change user acess levels', 1, '2017-10-10 02:15:28'),
(2, 'View Users', 2, NULL, 4, 'fa-eye', 'level of viewing users', 1, '2017-10-09 06:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `system_operation_access`
--

DROP TABLE IF EXISTS `system_operation_access`;
CREATE TABLE `system_operation_access` (
  `id` int(11) NOT NULL,
  `operationid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `choice` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_operation_access`
--

INSERT INTO `system_operation_access` (`id`, `operationid`, `userid`, `allow`, `choice`, `created_at`) VALUES
(1, 3, 13, 1, 0, '2017-10-10 00:22:12'),
(2, 2, 13, 1, 2, '2017-10-10 00:49:51');

-- --------------------------------------------------------

--
-- Table structure for table `system_operation_choises`
--

DROP TABLE IF EXISTS `system_operation_choises`;
CREATE TABLE `system_operation_choises` (
  `id` int(11) NOT NULL,
  `operationid` int(11) DEFAULT NULL,
  `choise` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_operation_choises`
--

INSERT INTO `system_operation_choises` (`id`, `operationid`, `choise`, `status`, `created_at`) VALUES
(1, 2, 'View All', 1, '2017-10-09 07:34:51'),
(2, 2, 'Department Only', 1, '2017-10-09 07:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_tables`
--

DROP TABLE IF EXISTS `system_tables`;
CREATE TABLE `system_tables` (
  `id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemid` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_table_columns`
--

DROP TABLE IF EXISTS `system_table_columns`;
CREATE TABLE `system_table_columns` (
  `id` int(11) NOT NULL,
  `tableid` int(11) DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_length` int(11) DEFAULT NULL,
  `default_data_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_keys` int(11) DEFAULT NULL,
  `column_key_type` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_url_routes`
--

DROP TABLE IF EXISTS `system_url_routes`;
CREATE TABLE `system_url_routes` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeas` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uses` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phpcontrollerid` int(11) DEFAULT NULL,
  `phpfunctionid` int(11) DEFAULT NULL,
  `jscontrollerid` int(11) NOT NULL,
  `jsfunctionid` int(11) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_url_routes`
--

INSERT INTO `system_url_routes` (`id`, `systemid`, `type`, `url`, `routeas`, `uses`, `phpcontrollerid`, `phpfunctionid`, `jscontrollerid`, `jsfunctionid`, `status`, `created_by`, `created_at`) VALUES
(1, 5, 'get', 'system', 'system', 'SystemController@getManage', 1, 2, 0, 0, 1, 10, '2017-05-28 23:32:23'),
(2, 1, 'get', 'admin', 'admin:dashboard', 'DashboardController@getView', 1, 3, 0, 0, 1, 10, '2017-05-29 00:01:57'),
(4, 5, 'get', 'system/load-system-data', 'system:load-system-data', 'SystemController@getSystemData', 1, 5, 0, 0, 1, 10, '2017-05-29 22:11:25'),
(5, 5, 'get', 'system/get-system-exceptions', 'system:get-system-exception', 'SystemController@getSystemExceptions', 1, 6, 0, 0, 1, 10, '2017-05-30 05:20:29'),
(6, 5, 'get', 'system/get-exception-data', 'system:get-exception-data', 'SystemController@getExceptionData', 1, 7, 0, 0, 1, 10, '2017-05-31 22:25:59'),
(7, 5, 'post', 'system/change-exception-status', 'system:change-exception-status', 'SystemController@changeExceptionStatus', 1, 8, 0, 0, 1, 10, '2017-05-31 23:31:26'),
(8, 5, 'post', 'system/change-all-exception-status', 'system:change-all-exception-status', 'SystemController@changeAllExceptionStatus', 1, 9, 0, 0, 1, 10, '2017-05-31 23:44:22'),
(9, 5, 'post', 'system/new-system', 'system:new-system', 'SystemController@newSystem', 1, 10, 0, 0, 1, 10, '2017-06-01 03:12:01'),
(10, 5, 'get', 'system/get-controllers', 'system:get-controllers', 'SystemController@getControllers', 1, 11, 0, 0, 1, 10, '2017-06-01 04:21:49'),
(11, 5, 'post', 'system/new-controller', 'system:new-controller', 'SystemController@newController', 1, 12, 0, 0, 1, 10, '2017-06-01 04:53:50'),
(12, 5, 'get', 'system/get-controller-data', 'system:get-controller-data', 'SystemController@getControllerData', 1, 13, 0, 0, 1, 10, '2017-06-05 23:54:44'),
(13, 5, 'post', 'system/new-function', 'system:new-function', 'SystemController@newFunction', 1, 14, 0, 0, 1, 10, '2017-06-06 02:24:58'),
(14, 5, 'get', 'system/get-system-scripts', 'system:get-system-script', 'SystemController@getSystemScript', 1, 15, 0, 0, 1, 10, '2017-06-15 03:28:10'),
(15, 2, 'get', 'users', 'users', 'UserController@getUsers', 4, 17, 0, 0, 1, 10, '2017-06-16 22:34:38'),
(16, 3, 'get', 'organizations', 'organizations', 'OrganizationController@getOrg', 5, 18, 0, 0, 1, 10, '2017-06-16 22:36:29'),
(17, 7, 'post', 'test 2 system/test2Routes', 'test 2 system/test2Routes', '', 33, 42, 36, 41, 1, 10, '2017-09-21 12:38:39'),
(18, 7, 'get', 'test2system/test2urlroutes', 'test2system-test2urlroutes', '', 33, 46, 36, 45, 1, 10, '2017-09-21 12:43:30'),
(19, 7, 'get', 'test2system/testuese', 'test2system-testuese', 'Test 2 system@testUese', 33, 48, 36, 47, 1, 10, '2017-09-21 12:45:32'),
(20, 7, 'get', 'test2system/testingbackslash', 'test2system-testingbackslash', 'test2system\\testController@testingBackSlash', 33, 50, 36, 49, 1, 10, '2017-09-21 12:53:43'),
(21, 9, 'get', 'testaccount/getdata', 'testaccount-getdata', 'testaccount\\TestAccountController@getData', 40, 60, 41, 59, 1, 10, '2017-09-29 09:58:04'),
(22, 7, 'get', 'test2system/navjeet', 'test2system-navjeet', 'test2system\\testController@Navjeet', 33, 62, 36, 61, 1, 10, '2017-10-03 08:11:49'),
(23, 5, 'get', 'system/load-new-view', 'system-loadnewview', 'SystemController@loadNewView', 2, 68, 13, 67, 1, 10, '2017-10-04 02:15:45'),
(24, 5, 'post', 'system/addview', 'system-addview', 'SystemController@addView', 2, 70, 13, 69, 1, 10, '2017-10-04 02:49:05'),
(25, 0, 'get', '/test-view', '-test-view', '\\SystemController@getTest-View', 2, 74, 0, 0, 1, 10, '2017-10-04 06:00:44'),
(26, 5, 'get', 'system/test-view', 'system-test-view', 'SystemController@getTest-View', 2, 75, 0, 0, 1, 10, '2017-10-04 06:10:08'),
(27, 5, 'get', 'system/testing-view', 'system-testing-view', 'SystemController@getTesting-View', 2, 76, 0, 0, 1, 10, '2017-10-04 06:16:28'),
(28, 5, 'get', 'system/testing1-view', 'system-testing1-view', 'SystemController@getTesting1-View', 2, 77, 0, 0, 1, 10, '2017-10-04 06:18:41'),
(29, 5, 'get', 'system/testing2-view', 'system-testing2-view', 'SystemController@getTesting2-View', 2, 78, 0, 0, 1, 10, '2017-10-04 06:19:54'),
(30, 5, 'get', 'system/testing3-view', 'system-testing3-view', 'SystemController@getTesting3-View', 2, 79, 0, 0, 1, 10, '2017-10-04 06:24:27'),
(31, 5, 'get', 'system/testing4-view', 'system-testing4-view', 'SystemController@getTesting4-View', 2, 80, 0, 0, 1, 10, '2017-10-04 06:30:05'),
(32, 5, 'get', 'system/testing6-view', 'system-testing6-view', 'SystemController@getTesting6-View', 2, 81, 0, 0, 1, 10, '2017-10-04 06:31:50'),
(33, 5, 'get', 'system/testing7-view', 'system-testing7-view', 'SystemController@getTesting7-View', 2, 82, 0, 0, 1, 10, '2017-10-04 06:33:07'),
(34, 5, 'get', 'system/testing10-view', 'system-testing10-view', 'SystemController@getTesting10View', 2, 83, 0, 0, 1, 10, '2017-10-04 07:24:24'),
(35, 5, 'get', 'system/testing11-view', 'system-testing11-view', 'SystemController@getTesting11View', 2, 84, 0, 0, 1, 10, '2017-10-04 07:28:39'),
(36, 5, 'get', 'system/testing12-view', 'system-testing12-view', 'SystemController@getTesting12View', 2, 85, 0, 0, 1, 10, '2017-10-04 07:31:36'),
(37, 5, 'get', 'system/test13-view', 'system-test13-view', 'SystemController@getTest12View', 2, 86, 0, 0, 1, 10, '2017-10-04 07:32:52'),
(38, 10, 'get', 'clients/clients', 'clients-clients', 'ClientController@getClients', 42, 87, 0, 0, 1, 10, '2017-10-04 08:34:39'),
(39, 10, 'get', 'clients/agreements', 'clients-agreements', 'AgreementController@getAgreements', 43, 88, 0, 0, 1, 10, '2017-10-04 08:53:56'),
(40, 10, 'get', 'clients/agreements/service-agreement', 'clients-service-agreement', 'AgreementController@getServiceAgreement', 43, 89, 0, 0, 1, 10, '2017-10-05 01:38:40'),
(41, 2, 'get', 'users/getuserlist', 'users-getuserlist', 'UserController@getUserList', 4, 91, 45, 90, 1, 10, '2017-10-09 05:31:39'),
(42, 5, 'post', 'system/addoperation', 'system-addoperation', 'SystemController@addOperation', 2, 94, 13, 93, 1, 10, '2017-10-09 06:28:39'),
(43, 5, 'post', 'system/addoperationchoice', 'system-addoperationchoice', 'SystemController@addOperationChoice', 2, 98, 13, 97, 1, 10, '2017-10-09 07:15:01'),
(44, 2, 'get', 'users/getuserbasicdetails', 'users-getuserbasicdetails', 'UserController@getUserBasicDetails', 4, 100, 45, 99, 1, 10, '2017-10-10 00:32:59'),
(45, 2, 'get', 'users/selectuser', 'users-selectuser', 'UserController@selectUser', 4, 102, 45, 101, 1, 10, '2017-10-10 00:33:16'),
(46, 2, 'get', 'users/changeuserstatus', 'users-changeuserstatus', 'UserController@changeUserStatus', 4, 105, 45, 104, 1, 10, '2017-10-10 00:34:30'),
(47, 2, 'post', 'users/changesystemaccess', 'users-changesystemaccess', 'UserController@changeSystemAccess', 4, 107, 45, 106, 1, 10, '2017-10-10 00:34:42'),
(48, 2, 'post', 'users/changeoperationaccess', 'users-changeoperationaccess', 'UserController@changeOperationAccess', 4, 109, 45, 108, 1, 10, '2017-10-10 00:34:58'),
(49, 2, 'post', 'users/deleteuser', 'users-deleteuser', 'UserController@deleteUser', 4, 111, 45, 110, 1, 10, '2017-10-10 00:35:11'),
(50, 2, 'get', 'users/resendactivationemail', 'users-resendactivationemail', 'UserController@resendActivationEmail', 4, 113, 45, 112, 1, 10, '2017-10-10 00:35:31'),
(51, 2, 'get', 'users/getaccesslist', 'users-getaccesslist', 'UserController@getAccessList', 4, 117, 45, 116, 1, 10, '2017-10-10 00:36:42'),
(52, 2, 'get', 'users/getactivitylog', 'users-getactivitylog', 'UserController@getActivityLog', 4, 119, 45, 118, 1, 10, '2017-10-10 00:36:55'),
(53, 2, 'post', 'users/adduser', 'users-adduser', 'UserController@addUser', 4, 121, 45, 120, 1, 10, '2017-10-10 00:37:05'),
(54, 2, 'post', 'users/saveaccesschoice', 'users-saveaccesschoice', 'UserController@saveAccessChoice', 4, 126, 45, 125, 1, 10, '2017-10-10 05:28:05'),
(56, 12, 'get', 'website/website', 'website-website', 'getWebsite@getWebsite', 47, 132, 0, 0, 1, 10, '2017-10-15 08:36:19'),
(57, 13, 'get', 'task/tasks', 'task-tasks', 'TaskController@getGetTask', 48, 133, 0, 0, 1, 10, '2017-10-15 08:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_variables`
--

DROP TABLE IF EXISTS `system_variables`;
CREATE TABLE `system_variables` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_views`
--

DROP TABLE IF EXISTS `system_views`;
CREATE TABLE `system_views` (
  `id` int(11) NOT NULL,
  `view_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_name` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemid` int(11) DEFAULT NULL,
  `url_route` int(11) DEFAULT NULL,
  `functionid` int(1) DEFAULT NULL,
  `main` int(1) NOT NULL DEFAULT '0',
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'icon-stop',
  `allow_default` int(1) NOT NULL DEFAULT '0',
  `show_menu` int(11) NOT NULL DEFAULT '0',
  `list_order` int(11) DEFAULT '50',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_views`
--

INSERT INTO `system_views` (`id`, `view_name`, `menu_name`, `systemid`, `url_route`, `functionid`, `main`, `icon`, `allow_default`, `show_menu`, `list_order`, `status`, `created_by`, `created_at`) VALUES
(1, 'System Administrator', 'Manage', 5, 1, 2, 0, 'fa-laptop', 0, 1, 3, 1, 10, '2017-05-28 23:27:55'),
(2, 'Dashboard', 'Dashboard', 1, 2, 3, 0, 'fa-dashboard', 0, 1, 1, 1, 10, '2017-05-29 00:02:45'),
(5, 'Organizations', 'Organizations', 3, 16, 18, 0, 'fa-building', 0, 1, 10, 1, 10, '2017-06-16 22:37:45'),
(4, 'Users', 'Users', 2, 15, 17, 0, 'fa-user', 0, 1, 10, 1, 10, '2017-06-16 22:37:10'),
(6, 'Test View', 'Test View', 5, NULL, 73, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 05:58:48'),
(7, 'Test View', 'Test View', 0, NULL, 74, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:00:44'),
(8, 'Test View', 'Test View', 5, NULL, 75, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:10:08'),
(9, 'Testing View', 'Testing View', 5, NULL, 76, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:16:28'),
(10, 'Testing1 View', 'Testing1 View', 5, NULL, 77, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:18:41'),
(11, 'Testing2 View', 'Testing2 View', 5, NULL, 78, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:19:54'),
(12, 'Testing3 View', 'Testing3 View', 5, NULL, 79, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:24:27'),
(13, 'Testing4 View', 'Testing4 View', 5, NULL, 80, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:30:05'),
(14, 'Testing6 View', 'Testing6 View', 5, NULL, 81, 0, 'fa-user', 0, 0, 100, 2, 10, '2017-10-04 06:31:50'),
(15, 'Testing7 View', 'Testing7 View', 5, 33, 82, 0, 'fa-user', 0, 0, 100, 0, 10, '2017-10-04 06:33:07'),
(16, 'Testing10 View', 'Testing10 View', 5, NULL, 83, 0, 'fa-user', 0, 0, 100, 0, 10, '2017-10-04 07:24:24'),
(17, 'Testing11 View', 'Testing11 View', 5, 35, 84, 0, 'fa-user', 0, 0, 100, 0, 10, '2017-10-04 07:28:39'),
(18, 'Testing12 View', 'Testing12 View', 5, 36, 85, 0, 'fa-user', 0, 0, 100, 0, 10, '2017-10-04 07:31:36'),
(19, 'Test12 View', 'Test12 View', 5, 37, 86, 0, 'fa-user', 0, 0, 100, 0, 10, '2017-10-04 07:32:52'),
(20, 'Clients', 'Clients', 10, 38, 87, 0, 'fa-users', 0, 1, 100, 1, 10, '2017-10-04 08:34:39'),
(21, 'Agreements', 'Agreements', 10, 39, 88, 0, 'fa-file-text-o', 0, 1, 100, 1, 10, '2017-10-04 08:53:56'),
(22, 'Service Agreement', 'Service Agreemen', 10, 40, 89, 0, 'fa-file-text-o', 0, 0, 100, 1, 10, '2017-10-05 01:38:40'),
(25, 'getTask', 'getTask', 13, 57, 133, 0, 'fa-list', 0, 1, 100, 1, 10, '2017-10-15 08:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_view_access`
--

DROP TABLE IF EXISTS `system_view_access`;
CREATE TABLE `system_view_access` (
  `id` int(11) NOT NULL,
  `viewid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_view_access`
--

INSERT INTO `system_view_access` (`id`, `viewid`, `userid`, `allow`, `created_at`) VALUES
(1, 2, 13, 1, '2017-10-09 23:00:08'),
(2, 20, 13, 0, '2017-10-09 23:04:34');

-- --------------------------------------------------------

--
-- Table structure for table `system_view_visit`
--

DROP TABLE IF EXISTS `system_view_visit`;
CREATE TABLE `system_view_visit` (
  `id` int(11) NOT NULL,
  `viewid` int(11) DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_view_visit`
--

INSERT INTO `system_view_visit` (`id`, `viewid`, `ip`, `userid`, `created_at`) VALUES
(1, 1, '101.98.86.113', 10, '2017-05-29 10:40:15'),
(2, 1, '101.98.86.113', 10, '2017-05-29 10:40:59'),
(3, 1, '101.98.86.113', 10, '2017-05-29 10:51:21'),
(4, 1, '101.98.86.113', 10, '2017-05-29 11:05:20'),
(5, 1, '101.98.86.113', 10, '2017-05-29 11:05:37'),
(6, 1, '101.98.86.113', 10, '2017-05-29 11:06:59'),
(7, 1, '101.98.86.113', 10, '2017-05-29 11:13:26'),
(8, 1, '101.98.86.113', 10, '2017-05-29 11:13:46'),
(9, 1, '101.98.86.113', 10, '2017-05-29 11:14:09'),
(10, 1, '101.98.86.113', 10, '2017-05-29 11:14:21'),
(11, 1, '101.98.86.113', 10, '2017-05-29 11:15:14'),
(12, 1, '101.98.86.113', 10, '2017-05-29 11:17:20'),
(13, 1, '101.98.86.113', 10, '2017-05-29 11:18:02'),
(14, 1, '101.98.86.113', 10, '2017-05-29 11:18:33'),
(15, 1, '101.98.86.113', 10, '2017-05-29 11:23:32'),
(16, 1, '101.98.86.113', 10, '2017-05-29 11:24:40'),
(17, 1, '101.98.86.113', 10, '2017-05-29 11:26:09'),
(18, 1, '101.98.86.113', 10, '2017-05-29 11:26:13'),
(19, 1, '101.98.86.113', 10, '2017-05-29 11:26:14'),
(20, 1, '101.98.86.113', 10, '2017-05-29 11:46:24'),
(21, 1, '101.98.86.113', 10, '2017-05-29 11:48:43'),
(22, 1, '101.98.86.113', 10, '2017-05-29 11:49:14'),
(23, 1, '101.98.86.113', 10, '2017-05-29 11:49:52'),
(24, 1, '101.98.86.113', 10, '2017-05-29 11:51:48'),
(25, 1, '101.98.86.113', 10, '2017-05-29 11:52:18'),
(26, 1, '101.98.86.113', 10, '2017-05-29 11:54:11'),
(27, 1, '101.98.86.113', 10, '2017-05-29 11:54:33'),
(28, 1, '101.98.86.113', 10, '2017-05-29 11:54:58'),
(29, 1, '101.98.86.113', 10, '2017-05-29 11:55:41'),
(30, 1, '101.98.86.113', 10, '2017-05-29 11:55:53'),
(31, 1, '101.98.86.113', 10, '2017-05-29 12:04:54'),
(32, 1, '101.98.86.113', 10, '2017-05-29 12:07:33'),
(33, 1, '101.98.86.113', 10, '2017-05-29 12:07:51'),
(34, 1, '101.98.86.113', 10, '2017-05-29 12:09:16'),
(35, 1, '101.98.86.113', 10, '2017-05-29 12:09:33'),
(36, 1, '101.98.86.113', 10, '2017-05-29 12:09:44'),
(37, 1, '101.98.86.113', 10, '2017-05-29 12:12:58'),
(38, 1, '101.98.86.113', 10, '2017-05-29 12:13:26'),
(39, 1, '101.98.86.113', 10, '2017-05-29 12:14:32'),
(40, 1, '101.98.86.113', 10, '2017-05-29 12:14:52'),
(41, 1, '101.98.86.113', 10, '2017-05-29 12:15:33'),
(42, 1, '101.98.86.113', 10, '2017-05-30 08:08:55'),
(43, 1, '101.98.86.113', 10, '2017-05-30 08:12:18'),
(44, 1, '101.98.86.113', 10, '2017-05-30 08:12:45'),
(45, 1, '101.98.86.113', 10, '2017-05-30 08:13:14'),
(46, 1, '101.98.86.113', 10, '2017-05-30 08:13:56'),
(47, 1, '101.98.86.113', 10, '2017-05-30 08:14:35'),
(48, 1, '101.98.86.113', 10, '2017-05-30 08:14:41'),
(49, 1, '101.98.86.113', 10, '2017-05-30 08:16:38'),
(50, 1, '101.98.86.113', 10, '2017-05-30 08:17:03'),
(51, 1, '101.98.86.113', 10, '2017-05-30 08:19:52'),
(52, 1, '101.98.86.113', 10, '2017-05-30 08:21:36'),
(53, 1, '101.98.86.113', 10, '2017-05-30 08:22:37'),
(54, 1, '101.98.86.113', 10, '2017-05-30 08:22:43'),
(55, 1, '101.98.86.113', 10, '2017-05-30 08:23:11'),
(56, 1, '101.98.86.113', 10, '2017-05-30 08:24:19'),
(57, 1, '101.98.86.113', 10, '2017-05-30 08:24:31'),
(58, 1, '101.98.86.113', 10, '2017-05-30 08:25:03'),
(59, 1, '101.98.86.113', 10, '2017-05-30 08:28:05'),
(60, 1, '101.98.86.113', 10, '2017-05-30 08:29:16'),
(61, 1, '101.98.86.113', 10, '2017-05-30 08:32:06'),
(62, 1, '101.98.86.113', 10, '2017-05-30 08:32:19'),
(63, 1, '101.98.86.113', 10, '2017-05-30 08:32:30'),
(64, 1, '101.98.86.113', 10, '2017-05-30 08:34:39'),
(65, 1, '101.98.86.113', 10, '2017-05-30 08:35:50'),
(66, 1, '101.98.86.113', 10, '2017-05-30 08:42:32'),
(67, 1, '101.98.86.113', 10, '2017-05-30 09:10:45'),
(68, 1, '101.98.86.113', 10, '2017-05-30 14:19:21'),
(69, 1, '101.98.86.113', 10, '2017-05-30 14:23:07'),
(70, 1, '101.98.86.113', 10, '2017-05-30 14:25:56'),
(71, 1, '101.98.86.113', 10, '2017-05-30 14:32:36'),
(72, 1, '101.98.86.113', 10, '2017-05-30 14:36:22'),
(73, 1, '101.98.86.113', 10, '2017-05-30 14:36:42'),
(74, 1, '101.98.86.113', 10, '2017-05-30 14:37:04'),
(75, 1, '101.98.86.113', 10, '2017-05-30 14:37:31'),
(76, 1, '101.98.86.113', 10, '2017-05-30 14:38:33'),
(77, 1, '101.98.86.113', 10, '2017-05-30 14:39:08'),
(78, 1, '101.98.86.113', 10, '2017-05-30 14:39:45'),
(79, 1, '101.98.86.113', 10, '2017-05-30 14:40:44'),
(80, 1, '101.98.86.113', 10, '2017-05-30 14:41:08'),
(81, 1, '101.98.86.113', 10, '2017-05-30 14:41:35'),
(82, 1, '101.98.86.113', 10, '2017-05-30 14:42:23'),
(83, 1, '101.98.86.113', 10, '2017-05-30 14:42:55'),
(84, 1, '101.98.86.113', 10, '2017-05-30 14:43:31'),
(85, 1, '101.98.86.113', 10, '2017-05-30 14:43:47'),
(86, 1, '101.98.86.113', 10, '2017-05-30 14:44:10'),
(87, 1, '101.98.86.113', 10, '2017-05-30 14:44:34'),
(88, 1, '101.98.86.113', 10, '2017-05-30 14:44:55'),
(89, 1, '101.98.86.113', 10, '2017-05-30 14:45:30'),
(90, 1, '101.98.86.113', 10, '2017-05-30 14:45:46'),
(91, 1, '101.98.86.113', 10, '2017-05-30 14:48:53'),
(92, 1, '101.98.86.113', 10, '2017-05-30 14:49:48'),
(93, 1, '101.98.86.113', 10, '2017-05-30 14:50:42'),
(94, 1, '101.98.86.113', 10, '2017-05-30 14:54:26'),
(95, 1, '101.98.86.113', 10, '2017-05-30 15:01:20'),
(96, 1, '101.98.86.113', 10, '2017-05-30 15:03:07'),
(97, 1, '101.98.86.113', 10, '2017-05-30 15:03:49'),
(98, 1, '101.98.86.113', 10, '2017-05-30 15:05:20'),
(99, 1, '101.98.86.113', 10, '2017-05-30 15:06:37'),
(100, 1, '101.98.86.113', 10, '2017-05-30 15:08:30'),
(101, 1, '101.98.86.113', 10, '2017-05-30 15:09:07'),
(102, 1, '101.98.86.113', 10, '2017-05-30 15:11:09'),
(103, 1, '101.98.86.113', 10, '2017-05-30 15:11:52'),
(104, 1, '101.98.86.113', 10, '2017-05-30 15:12:24'),
(105, 1, '101.98.86.113', 10, '2017-05-30 15:13:50'),
(106, 1, '101.98.86.113', 10, '2017-05-30 15:21:42'),
(107, 1, '101.98.86.113', 10, '2017-05-30 15:32:47'),
(108, 1, '101.98.86.113', 10, '2017-05-30 15:33:02'),
(109, 1, '101.98.86.113', 10, '2017-05-30 15:33:38'),
(110, 1, '101.98.86.113', 10, '2017-05-30 15:34:09'),
(111, 1, '101.98.86.113', 10, '2017-05-30 15:37:07'),
(112, 1, '101.98.86.113', 10, '2017-05-30 15:37:31'),
(113, 1, '101.98.86.113', 10, '2017-05-30 15:41:45'),
(114, 1, '101.98.86.113', 10, '2017-05-30 15:54:53'),
(115, 1, '101.98.86.113', 10, '2017-05-30 16:02:15'),
(116, 1, '101.98.86.113', 10, '2017-05-30 16:08:38'),
(117, 1, '101.98.86.113', 10, '2017-05-30 16:12:13'),
(118, 1, '101.98.86.113', 10, '2017-05-30 16:13:00'),
(119, 1, '101.98.86.113', 10, '2017-05-30 16:15:00'),
(120, 1, '101.98.86.113', 10, '2017-05-30 16:17:41'),
(121, 1, '101.98.86.113', 10, '2017-05-30 16:18:23'),
(122, 1, '101.98.86.113', 10, '2017-05-30 16:18:42'),
(123, 1, '101.98.86.113', 10, '2017-05-30 16:21:43'),
(124, 1, '101.98.86.113', 10, '2017-06-01 08:12:33'),
(125, 1, '101.98.86.113', 10, '2017-06-01 08:39:15'),
(126, 1, '101.98.86.113', 10, '2017-06-01 08:42:09'),
(127, 1, '101.98.86.113', 10, '2017-06-01 08:42:41'),
(128, 1, '101.98.86.113', 10, '2017-06-01 08:43:46'),
(129, 1, '101.98.86.113', 10, '2017-06-01 08:44:50'),
(130, 1, '101.98.86.113', 10, '2017-06-01 08:47:46'),
(131, 1, '101.98.86.113', 10, '2017-06-01 08:50:35'),
(132, 1, '101.98.86.113', 10, '2017-06-01 08:52:26'),
(133, 1, '101.98.86.113', 10, '2017-06-01 08:52:32'),
(134, 1, '101.98.86.113', 10, '2017-06-01 09:02:12'),
(135, 1, '101.98.86.113', 10, '2017-06-01 09:02:47'),
(136, 1, '101.98.86.113', 10, '2017-06-01 09:07:32'),
(137, 1, '101.98.86.113', 10, '2017-06-01 09:09:00'),
(138, 1, '101.98.86.113', 10, '2017-06-01 09:10:22'),
(139, 1, '101.98.86.113', 10, '2017-06-01 09:12:01'),
(140, 1, '101.98.86.113', 10, '2017-06-01 09:24:26'),
(141, 1, '101.98.86.113', 10, '2017-06-01 09:32:15'),
(142, 1, '101.98.86.113', 10, '2017-06-01 09:33:04'),
(143, 1, '101.98.86.113', 10, '2017-06-01 09:34:25'),
(144, 1, '101.98.86.113', 10, '2017-06-01 09:35:59'),
(145, 1, '101.98.86.113', 10, '2017-06-01 09:41:20'),
(146, 1, '101.98.86.113', 10, '2017-06-01 09:45:47'),
(147, 1, '101.98.86.113', 10, '2017-06-01 09:52:52'),
(148, 1, '101.98.86.113', 10, '2017-06-01 09:56:25'),
(149, 1, '101.98.86.113', 10, '2017-06-01 09:57:10'),
(150, 1, '101.98.86.113', 10, '2017-06-01 10:03:24'),
(151, 1, '101.98.86.113', 10, '2017-06-01 10:04:26'),
(152, 1, '101.98.86.113', 10, '2017-06-01 10:07:15'),
(153, 1, '101.98.86.113', 10, '2017-06-01 10:08:43'),
(154, 1, '101.98.86.113', 10, '2017-06-01 10:10:31'),
(155, 1, '101.98.86.113', 10, '2017-06-01 10:11:14'),
(156, 1, '101.98.86.113', 10, '2017-06-01 10:18:22'),
(157, 1, '101.98.86.113', 10, '2017-06-01 10:20:04'),
(158, 1, '101.98.86.113', 10, '2017-06-01 10:28:25'),
(159, 1, '101.98.86.113', 10, '2017-06-01 10:29:08'),
(160, 1, '101.98.86.113', 10, '2017-06-01 10:29:36'),
(161, 1, '101.98.86.113', 10, '2017-06-01 10:30:17'),
(162, 1, '101.98.86.113', 10, '2017-06-01 10:32:57'),
(163, 1, '101.98.86.113', 10, '2017-06-01 10:35:56'),
(164, 1, '101.98.86.113', 10, '2017-06-01 10:50:58'),
(165, 1, '101.98.86.113', 10, '2017-06-01 10:56:32'),
(166, 1, '101.98.86.113', 10, '2017-06-01 11:02:30'),
(167, 1, '101.98.86.113', 10, '2017-06-01 11:02:43'),
(168, 1, '101.98.86.113', 10, '2017-06-01 11:04:18'),
(169, 1, '101.98.86.113', 10, '2017-06-01 11:04:35'),
(170, 1, '101.98.86.113', 10, '2017-06-01 11:05:53'),
(171, 1, '101.98.86.113', 10, '2017-06-01 11:12:25'),
(172, 1, '101.98.86.113', 10, '2017-06-01 11:14:33'),
(173, 2, '101.98.86.113', 10, '2017-06-01 11:14:43'),
(174, 1, '101.98.86.113', 10, '2017-06-01 11:14:45'),
(175, 1, '101.98.86.113', 10, '2017-06-01 11:17:44'),
(176, 1, '101.98.86.113', 10, '2017-06-01 11:18:41'),
(177, 1, '101.98.86.113', 10, '2017-06-01 11:19:28'),
(178, 1, '101.98.86.113', 10, '2017-06-01 11:21:19'),
(179, 1, '101.98.86.113', 10, '2017-06-01 11:21:48'),
(180, 1, '101.98.86.113', 10, '2017-06-01 11:22:06'),
(181, 1, '101.98.86.113', 10, '2017-06-01 11:22:27'),
(182, 1, '101.98.86.113', 10, '2017-06-01 11:22:45'),
(183, 1, '101.98.86.113', 10, '2017-06-01 11:23:30'),
(184, 1, '101.98.86.113', 10, '2017-06-01 11:27:52'),
(185, 1, '101.98.86.113', 10, '2017-06-01 11:34:33'),
(186, 1, '101.98.86.113', 10, '2017-06-01 13:19:31'),
(187, 1, '101.98.86.113', 10, '2017-06-01 13:19:53'),
(188, 1, '101.98.86.113', 10, '2017-06-01 13:20:27'),
(189, 1, '101.98.86.113', 10, '2017-06-01 13:29:03'),
(190, 1, '101.98.86.113', 10, '2017-06-01 13:32:30'),
(191, 1, '101.98.86.113', 10, '2017-06-01 13:33:04'),
(192, 1, '101.98.86.113', 10, '2017-06-01 13:34:47'),
(193, 1, '101.98.86.113', 10, '2017-06-01 13:35:39'),
(194, 1, '101.98.86.113', 10, '2017-06-01 13:40:08'),
(195, 1, '101.98.86.113', 10, '2017-06-01 13:42:10'),
(196, 1, '101.98.86.113', 10, '2017-06-01 13:44:03'),
(197, 1, '101.98.86.113', 10, '2017-06-01 13:44:52'),
(198, 1, '101.98.86.113', 10, '2017-06-01 13:45:40'),
(199, 1, '101.98.86.113', 10, '2017-06-01 13:45:44'),
(200, 1, '101.98.86.113', 10, '2017-06-01 13:46:32'),
(201, 1, '101.98.86.113', 10, '2017-06-01 13:59:30'),
(202, 1, '101.98.86.113', 10, '2017-06-01 14:00:15'),
(203, 1, '101.98.86.113', 10, '2017-06-01 14:01:02'),
(204, 1, '101.98.86.113', 10, '2017-06-01 14:02:20'),
(205, 1, '101.98.86.113', 10, '2017-06-01 14:03:55'),
(206, 1, '101.98.86.113', 10, '2017-06-01 14:05:36'),
(207, 1, '101.98.86.113', 10, '2017-06-01 14:06:54'),
(208, 1, '101.98.86.113', 10, '2017-06-01 14:07:17'),
(209, 1, '101.98.86.113', 10, '2017-06-01 14:31:00'),
(210, 1, '101.98.86.113', 10, '2017-06-01 14:52:08'),
(211, 1, '101.98.86.113', 10, '2017-06-01 14:53:02'),
(212, 1, '101.98.86.113', 10, '2017-06-01 14:53:24'),
(213, 1, '101.98.86.113', 10, '2017-06-01 14:55:34'),
(214, 1, '101.98.86.113', 10, '2017-06-01 14:56:00'),
(215, 1, '101.98.86.113', 10, '2017-06-01 14:58:43'),
(216, 1, '101.98.86.113', 10, '2017-06-01 15:00:20'),
(217, 1, '101.98.86.113', 10, '2017-06-01 15:02:01'),
(218, 1, '101.98.86.113', 10, '2017-06-01 15:03:11'),
(219, 1, '101.98.86.113', 10, '2017-06-01 15:05:52'),
(220, 1, '101.98.86.113', 10, '2017-06-01 15:07:24'),
(221, 1, '101.98.86.113', 10, '2017-06-01 15:10:56'),
(222, 1, '101.98.86.113', 10, '2017-06-01 15:11:26'),
(223, 1, '101.98.86.113', 10, '2017-06-01 15:12:13'),
(224, 1, '101.98.86.113', 10, '2017-06-01 15:14:11'),
(225, 1, '101.98.86.113', 10, '2017-06-01 15:34:11'),
(226, 1, '101.98.86.113', 10, '2017-06-01 15:36:04'),
(227, 1, '101.98.86.113', 10, '2017-06-01 15:37:55'),
(228, 1, '101.98.86.113', 10, '2017-06-01 15:48:44'),
(229, 1, '101.98.86.113', 10, '2017-06-01 15:49:01'),
(230, 1, '101.98.86.113', 10, '2017-06-01 15:50:05'),
(231, 1, '101.98.86.113', 10, '2017-06-01 15:50:35'),
(232, 1, '101.98.86.113', 10, '2017-06-01 15:56:27'),
(233, 1, '101.98.86.113', 10, '2017-06-01 15:57:08'),
(234, 1, '101.98.86.113', 10, '2017-06-01 15:58:00'),
(235, 1, '101.98.86.113', 10, '2017-06-01 15:58:40'),
(236, 1, '101.98.86.113', 10, '2017-06-01 16:08:07'),
(237, 1, '101.98.86.113', 10, '2017-06-06 09:55:45'),
(238, 1, '101.98.86.113', 10, '2017-06-06 10:00:45'),
(239, 1, '101.98.86.113', 10, '2017-06-06 10:02:21'),
(240, 1, '101.98.86.113', 10, '2017-06-06 10:15:08'),
(241, 1, '101.98.86.113', 10, '2017-06-06 10:39:11'),
(242, 1, '101.98.86.113', 10, '2017-06-06 10:43:16'),
(243, 1, '101.98.86.113', 10, '2017-06-06 10:54:43'),
(244, 1, '101.98.86.113', 10, '2017-06-06 10:55:55'),
(245, 1, '101.98.86.113', 10, '2017-06-06 11:10:35'),
(246, 1, '101.98.86.113', 10, '2017-06-06 11:24:09'),
(247, 1, '101.98.86.113', 10, '2017-06-06 11:25:56'),
(248, 1, '101.98.86.113', 10, '2017-06-06 11:27:21'),
(249, 1, '101.98.86.113', 10, '2017-06-06 11:30:36'),
(250, 1, '101.98.86.113', 10, '2017-06-06 11:33:17'),
(251, 1, '101.98.86.113', 10, '2017-06-06 11:35:30'),
(252, 1, '101.98.86.113', 10, '2017-06-06 11:36:06'),
(253, 1, '101.98.86.113', 10, '2017-06-06 11:37:46'),
(254, 1, '101.98.86.113', 10, '2017-06-06 11:54:27'),
(255, 1, '101.98.86.113', 10, '2017-06-06 11:56:39'),
(256, 1, '101.98.86.113', 10, '2017-06-06 11:56:59'),
(257, 1, '101.98.86.113', 10, '2017-06-06 11:59:03'),
(258, 1, '101.98.86.113', 10, '2017-06-06 12:06:32'),
(259, 1, '101.98.86.113', 10, '2017-06-06 12:08:31'),
(260, 1, '101.98.86.113', 10, '2017-06-06 12:09:02'),
(261, 1, '101.98.86.113', 10, '2017-06-06 12:10:40'),
(262, 1, '101.98.86.113', 10, '2017-06-06 12:11:02'),
(263, 1, '101.98.86.113', 10, '2017-06-06 12:13:07'),
(264, 1, '101.98.86.113', 10, '2017-06-06 12:14:00'),
(265, 1, '101.98.86.113', 10, '2017-06-06 12:16:39'),
(266, 1, '101.98.86.113', 10, '2017-06-06 12:21:32'),
(267, 1, '101.98.86.113', 10, '2017-06-06 12:22:28'),
(268, 1, '101.98.86.113', 10, '2017-06-06 12:26:41'),
(269, 1, '101.98.86.113', 10, '2017-06-06 12:27:19'),
(270, 1, '101.98.86.113', 10, '2017-06-06 12:28:35'),
(271, 1, '101.98.86.113', 10, '2017-06-06 12:29:40'),
(272, 1, '101.98.86.113', 10, '2017-06-06 12:31:21'),
(273, 1, '101.98.86.113', 10, '2017-06-06 12:32:27'),
(274, 1, '101.98.86.113', 10, '2017-06-06 12:34:16'),
(275, 1, '101.98.86.113', 10, '2017-06-06 12:38:27'),
(276, 1, '101.98.86.113', 10, '2017-06-06 12:40:28'),
(277, 1, '101.98.86.113', 10, '2017-06-06 12:41:21'),
(278, 1, '101.98.86.113', 10, '2017-06-06 12:46:26'),
(279, 1, '101.98.86.113', 10, '2017-06-06 12:46:59'),
(280, 1, '101.98.86.113', 10, '2017-06-06 14:17:01'),
(281, 1, '101.98.86.113', 10, '2017-06-06 14:19:03'),
(282, 1, '101.98.86.113', 10, '2017-06-06 14:22:37'),
(283, 1, '101.98.86.113', 10, '2017-06-06 14:23:14'),
(284, 1, '101.98.86.113', 10, '2017-06-06 14:31:52'),
(285, 1, '101.98.86.113', 10, '2017-06-06 14:34:33'),
(286, 1, '101.98.86.113', 10, '2017-06-06 14:35:06'),
(287, 1, '101.98.86.113', 10, '2017-06-06 14:38:13'),
(288, 1, '101.98.86.113', 10, '2017-06-06 14:38:41'),
(289, 1, '101.98.86.113', 10, '2017-06-06 14:40:13'),
(290, 1, '101.98.86.113', 10, '2017-06-06 14:41:00'),
(291, 1, '::1', 10, '2017-06-06 14:49:33'),
(292, 1, '101.98.86.113', 10, '2017-06-06 14:53:20'),
(293, 1, '101.98.86.113', 10, '2017-06-06 14:56:15'),
(294, 1, '101.98.86.113', 10, '2017-06-06 14:56:54'),
(295, 1, '101.98.86.113', 10, '2017-06-06 14:57:47'),
(296, 1, '111.69.226.100', 10, '2017-06-15 12:10:04'),
(297, 1, '111.69.226.100', 10, '2017-06-15 13:27:12'),
(298, 1, '111.69.226.100', 10, '2017-06-15 13:34:19'),
(299, 1, '111.69.226.100', 10, '2017-06-15 13:36:57'),
(300, 1, '111.69.226.100', 10, '2017-06-15 13:39:38'),
(301, 1, '111.69.226.100', 10, '2017-06-15 13:41:22'),
(302, 1, '111.69.226.100', 10, '2017-06-15 13:43:12'),
(303, 1, '111.69.226.100', 10, '2017-06-15 13:43:52'),
(304, 1, '111.69.226.100', 10, '2017-06-15 14:02:26'),
(305, 1, '111.69.226.100', 10, '2017-06-15 14:04:43'),
(306, 1, '111.69.226.100', 10, '2017-06-15 14:05:46'),
(307, 1, '111.69.226.100', 10, '2017-06-15 14:06:19'),
(308, 1, '111.69.226.100', 10, '2017-06-15 14:06:48'),
(309, 1, '111.69.226.100', 10, '2017-06-15 14:09:05'),
(310, 1, '111.69.226.100', 10, '2017-06-15 14:10:19'),
(311, 1, '111.69.226.100', 10, '2017-06-15 14:12:22'),
(312, 1, '111.69.226.100', 10, '2017-06-15 14:13:00'),
(313, 1, '111.69.226.100', 10, '2017-06-15 14:16:55'),
(314, 1, '111.69.226.100', 10, '2017-06-15 14:20:38'),
(315, 1, '111.69.226.100', 10, '2017-06-15 14:23:19'),
(316, 1, '111.69.226.100', 10, '2017-06-15 14:25:47'),
(317, 1, '111.69.226.100', 10, '2017-06-15 14:26:31'),
(318, 2, '101.98.86.117', 10, '2017-06-17 08:47:21'),
(319, 1, '101.98.86.117', 10, '2017-06-17 08:47:27'),
(320, 1, '101.98.86.117', 10, '2017-06-17 08:49:11'),
(321, 1, '101.98.86.117', 10, '2017-06-17 08:49:22'),
(322, 1, '101.98.86.117', 10, '2017-06-17 08:50:25'),
(323, 1, '101.98.86.117', 10, '2017-06-17 08:51:41'),
(324, 1, '101.98.86.117', 10, '2017-06-17 08:52:00'),
(325, 1, '101.98.86.117', 10, '2017-06-17 08:52:02'),
(326, 1, '101.98.86.117', 10, '2017-06-17 08:53:19'),
(327, 1, '101.98.86.117', 10, '2017-06-17 08:54:01'),
(328, 1, '101.98.86.117', 10, '2017-06-17 08:56:08'),
(329, 1, '101.98.86.117', 10, '2017-06-17 08:57:59'),
(330, 1, '101.98.86.117', 10, '2017-06-17 09:07:50'),
(331, 1, '101.98.86.117', 10, '2017-06-17 09:12:14'),
(332, 1, '101.98.86.117', 10, '2017-06-17 09:14:25'),
(333, 1, '101.98.86.117', 10, '2017-06-17 09:16:59'),
(334, 1, '101.98.86.117', 10, '2017-06-17 09:27:21'),
(335, 1, '101.98.86.117', 10, '2017-06-17 09:35:11'),
(336, 1, '101.98.86.117', 10, '2017-06-17 09:36:54'),
(337, 1, '101.98.86.117', 10, '2017-06-17 09:48:27'),
(338, 1, '101.98.86.117', 10, '2017-06-17 09:49:26'),
(339, 1, '101.98.86.117', 10, '2017-06-17 09:50:46'),
(340, 1, '101.98.86.117', 10, '2017-06-17 10:10:35'),
(341, 1, '101.98.86.117', 10, '2017-06-17 10:11:17'),
(342, 1, '101.98.86.117', 10, '2017-06-17 10:12:24'),
(343, 1, '101.98.86.117', 10, '2017-06-17 10:12:58'),
(344, 1, '101.98.86.117', 10, '2017-06-17 10:21:32'),
(345, 1, '101.98.86.117', 10, '2017-06-17 10:23:39'),
(346, 1, '101.98.86.117', 10, '2017-06-17 10:24:37'),
(347, 2, '165.84.31.245', 10, '2017-08-16 18:59:25'),
(348, 1, '165.84.31.245', 10, '2017-08-16 19:02:37'),
(349, 1, '165.84.31.245', 10, '2017-08-16 19:02:51'),
(350, 1, '165.84.31.245', 10, '2017-08-16 19:03:49'),
(351, 1, '165.84.31.245', 10, '2017-08-16 19:03:56'),
(352, 1, '165.84.31.245', 10, '2017-08-16 19:04:35'),
(353, 1, '165.84.31.245', 10, '2017-08-16 19:05:48'),
(354, 2, '101.98.86.117', 10, '2017-09-09 08:42:05'),
(355, 1, '101.98.86.117', 10, '2017-09-09 08:42:10'),
(356, 1, '101.98.86.117', 10, '2017-09-09 08:42:17'),
(357, 1, '101.98.86.117', 10, '2017-09-09 08:42:42'),
(358, 1, '101.98.86.117', 10, '2017-09-09 08:44:10'),
(359, 1, '101.98.86.117', 10, '2017-09-09 08:44:50'),
(360, 1, '101.98.86.117', 10, '2017-09-09 08:45:25'),
(361, 1, '101.98.86.117', 10, '2017-09-09 08:46:13'),
(362, 1, '101.98.86.117', 10, '2017-09-09 08:46:45'),
(363, 1, '101.98.86.117', 10, '2017-09-09 08:47:11'),
(364, 1, '101.98.86.117', 10, '2017-09-09 08:47:24'),
(365, 1, '101.98.86.117', 10, '2017-09-09 08:47:33'),
(366, 1, '101.98.86.117', 10, '2017-09-09 08:48:19'),
(367, 1, '101.98.86.117', 10, '2017-09-09 08:51:06'),
(368, 1, '101.98.86.117', 10, '2017-09-09 08:52:24'),
(369, 1, '101.98.86.117', 10, '2017-09-09 08:54:50'),
(370, 2, '101.98.86.117', 10, '2017-09-16 09:49:00'),
(371, 1, '101.98.86.117', 10, '2017-09-16 09:49:05'),
(372, 1, '101.98.86.117', 10, '2017-09-16 10:00:34'),
(373, 1, '101.98.86.117', 10, '2017-09-16 10:02:09'),
(374, 1, '101.98.86.117', 10, '2017-09-16 10:08:13'),
(375, 1, '101.98.86.117', 10, '2017-09-16 10:09:59'),
(376, 1, '101.98.86.117', 10, '2017-09-16 10:23:31'),
(377, 1, '101.98.86.117', 10, '2017-09-16 10:25:41'),
(378, 1, '101.98.86.117', 10, '2017-09-16 10:29:21'),
(379, 1, '101.98.86.117', 10, '2017-09-16 10:30:51'),
(380, 2, '123.255.26.144', 10, '2017-09-17 08:35:01'),
(381, 1, '123.255.26.144', 10, '2017-09-17 08:35:05'),
(382, 1, '123.255.26.144', 10, '2017-09-17 08:36:10'),
(383, 1, '123.255.26.144', 10, '2017-09-17 09:02:19'),
(384, 1, '123.255.26.144', 10, '2017-09-17 09:03:58'),
(385, 1, '123.255.26.144', 10, '2017-09-17 09:12:52'),
(386, 1, '123.255.26.144', 10, '2017-09-17 09:16:23'),
(387, 1, '123.255.26.144', 10, '2017-09-17 09:20:48'),
(388, 1, '123.255.26.144', 10, '2017-09-17 09:52:40'),
(389, 1, '123.255.26.144', 10, '2017-09-17 09:53:21'),
(390, 1, '123.255.26.144', 10, '2017-09-17 09:54:38'),
(391, 1, '123.255.26.144', 10, '2017-09-17 10:49:29'),
(392, 1, '123.255.26.144', 10, '2017-09-17 12:46:17'),
(393, 1, '123.255.26.144', 10, '2017-09-17 12:48:32'),
(394, 1, '123.255.26.144', 10, '2017-09-17 12:48:34'),
(395, 1, '123.255.26.144', 10, '2017-09-17 12:54:26'),
(396, 1, '123.255.26.144', 10, '2017-09-17 12:56:17'),
(397, 1, '123.255.26.144', 10, '2017-09-17 12:56:19'),
(398, 1, '123.255.26.144', 10, '2017-09-17 12:56:21'),
(399, 1, '123.255.26.144', 10, '2017-09-17 12:57:41'),
(400, 1, '123.255.26.144', 10, '2017-09-17 12:58:53'),
(401, 1, '123.255.26.144', 10, '2017-09-17 13:05:06'),
(402, 1, '123.255.26.144', 10, '2017-09-17 13:05:08'),
(403, 1, '123.255.26.144', 10, '2017-09-17 14:08:44'),
(404, 1, '123.255.26.144', 10, '2017-09-17 14:09:16'),
(405, 1, '123.255.26.144', 10, '2017-09-17 14:09:51'),
(406, 1, '123.255.26.144', 10, '2017-09-17 14:10:10'),
(407, 1, '123.255.26.144', 10, '2017-09-17 14:15:20'),
(408, 1, '123.255.26.144', 10, '2017-09-17 14:15:56'),
(409, 1, '123.255.26.144', 10, '2017-09-17 18:03:59'),
(410, 1, '123.255.26.144', 10, '2017-09-17 18:04:57'),
(411, 1, '123.255.26.144', 10, '2017-09-17 18:08:26'),
(412, 1, '123.255.26.144', 10, '2017-09-17 18:09:54'),
(413, 1, '123.255.26.144', 10, '2017-09-17 18:34:34'),
(414, 1, '123.255.26.144', 10, '2017-09-17 18:36:09'),
(415, 1, '123.255.26.144', 10, '2017-09-17 18:38:17'),
(416, 1, '123.255.26.144', 10, '2017-09-17 18:44:36'),
(417, 1, '123.255.26.144', 10, '2017-09-17 18:47:01'),
(418, 1, '123.255.26.144', 10, '2017-09-17 18:49:57'),
(419, 1, '14.1.70.6', 10, '2017-09-18 15:00:39'),
(420, 1, '14.1.70.6', 10, '2017-09-18 15:10:39'),
(421, 1, '14.1.70.6', 10, '2017-09-18 15:11:47'),
(422, 1, '14.1.70.6', 10, '2017-09-18 15:12:53'),
(423, 1, '14.1.70.6', 10, '2017-09-18 15:14:05'),
(424, 1, '14.1.70.6', 10, '2017-09-18 15:20:21'),
(425, 1, '14.1.70.6', 10, '2017-09-18 15:21:08'),
(426, 1, '14.1.70.6', 10, '2017-09-18 15:22:41'),
(427, 1, '14.1.70.6', 10, '2017-09-18 15:23:18'),
(428, 1, '14.1.70.6', 10, '2017-09-18 15:24:40'),
(429, 1, '14.1.70.6', 10, '2017-09-18 15:27:03'),
(430, 1, '123.255.26.144', 10, '2017-09-21 16:34:47'),
(431, 2, '123.255.26.144', 10, '2017-09-21 16:34:54'),
(432, 1, '123.255.26.144', 10, '2017-09-21 16:34:57'),
(433, 1, '123.255.26.144', 10, '2017-09-21 16:53:02'),
(434, 1, '123.255.26.144', 10, '2017-09-21 17:20:08'),
(435, 1, '123.255.26.144', 10, '2017-09-21 17:32:35'),
(436, 1, '123.255.26.144', 10, '2017-09-21 17:42:05'),
(437, 1, '123.255.26.144', 10, '2017-09-21 17:50:47'),
(438, 1, '123.255.26.144', 10, '2017-09-21 17:54:46'),
(439, 1, '123.255.26.144', 10, '2017-09-21 18:08:02'),
(440, 1, '123.255.26.144', 10, '2017-09-21 18:11:56'),
(441, 1, '123.255.26.144', 10, '2017-09-21 18:13:06'),
(442, 1, '123.255.26.144', 10, '2017-09-21 18:15:10'),
(443, 1, '123.255.26.144', 10, '2017-09-21 18:23:22'),
(444, 2, '123.255.26.144', 10, '2017-09-23 07:10:52'),
(445, 1, '123.255.26.144', 10, '2017-09-23 07:10:56'),
(446, 1, '123.255.26.144', 10, '2017-09-23 07:23:58'),
(447, 1, '123.255.26.144', 10, '2017-09-23 07:28:47'),
(448, 1, '123.255.26.144', 10, '2017-09-23 07:35:06'),
(449, 1, '123.255.26.144', 10, '2017-09-23 07:37:10'),
(450, 1, '123.255.26.144', 10, '2017-09-23 07:37:56'),
(451, 1, '123.255.26.144', 10, '2017-09-23 07:39:29'),
(452, 1, '123.255.26.144', 10, '2017-09-23 07:40:26'),
(453, 1, '123.255.26.144', 10, '2017-09-23 07:41:15'),
(454, 1, '123.255.26.144', 10, '2017-09-23 07:42:18'),
(455, 1, '123.255.26.144', 10, '2017-09-23 07:42:49'),
(456, 1, '123.255.26.144', 10, '2017-09-23 07:48:16'),
(457, 1, '123.255.26.144', 10, '2017-09-23 07:53:51'),
(458, 1, '123.255.26.144', 10, '2017-09-23 07:55:18'),
(459, 1, '123.255.26.144', 10, '2017-09-23 07:56:51'),
(460, 1, '123.255.26.144', 10, '2017-09-23 07:57:36'),
(461, 1, '123.255.26.144', 10, '2017-09-23 07:58:31'),
(462, 1, '123.255.26.144', 10, '2017-09-23 08:09:53'),
(463, 1, '123.255.26.144', 10, '2017-09-23 08:45:07'),
(464, 1, '123.255.26.144', 10, '2017-09-23 08:45:14'),
(465, 1, '123.255.26.144', 10, '2017-09-23 08:57:44'),
(466, 1, '123.255.26.144', 10, '2017-09-23 08:58:16'),
(467, 1, '123.255.26.144', 10, '2017-09-23 09:11:07'),
(468, 1, '123.255.26.144', 10, '2017-09-23 09:20:35'),
(469, 1, '123.255.26.144', 10, '2017-09-23 09:29:25'),
(470, 1, '123.255.26.144', 10, '2017-09-23 09:30:40'),
(471, 1, '123.255.26.144', 10, '2017-09-23 09:35:50'),
(472, 1, '123.255.26.144', 10, '2017-09-23 09:44:05'),
(473, 1, '123.255.26.144', 10, '2017-09-23 09:47:45'),
(474, 2, '123.255.26.144', 10, '2017-09-24 08:24:58'),
(475, 1, '123.255.26.144', 10, '2017-09-24 08:25:01'),
(476, 2, '14.1.70.6', 10, '2017-09-28 07:28:09'),
(477, 1, '14.1.70.6', 10, '2017-09-28 08:04:07'),
(478, 2, '14.1.70.6', 10, '2017-09-29 13:09:41'),
(479, 1, '14.1.70.6', 10, '2017-09-29 13:09:45'),
(480, 1, '14.1.70.6', 10, '2017-09-29 13:11:30'),
(481, 2, '123.255.26.144', 10, '2017-09-29 15:25:15'),
(482, 1, '123.255.26.144', 10, '2017-09-29 15:25:18'),
(483, 1, '123.255.26.144', 10, '2017-09-29 15:30:52'),
(484, 2, '123.255.26.144', 10, '2017-10-01 07:42:41'),
(485, 2, '123.255.26.144', 10, '2017-10-01 08:18:47'),
(486, 1, '123.255.26.144', 10, '2017-10-01 08:18:50'),
(487, 1, '123.255.26.144', 10, '2017-10-01 17:04:45'),
(488, 1, '123.255.26.144', 10, '2017-10-01 17:04:57'),
(489, 1, '123.255.26.144', 10, '2017-10-01 17:05:08'),
(490, 1, '123.255.26.144', 10, '2017-10-01 17:05:19'),
(491, 1, '123.255.26.144', 10, '2017-10-01 17:07:05'),
(492, 1, '123.255.26.144', 10, '2017-10-01 17:12:28'),
(493, 1, '123.255.26.144', 10, '2017-10-01 17:12:38'),
(494, 2, '14.1.70.6', 10, '2017-10-03 06:02:14'),
(495, 1, '14.1.70.6', 10, '2017-10-03 06:02:17'),
(496, 1, '14.1.70.6', 10, '2017-10-03 06:02:47'),
(497, 1, '14.1.70.6', 10, '2017-10-03 06:02:54'),
(498, 1, '14.1.70.6', 10, '2017-10-03 06:48:44'),
(499, 1, '14.1.70.6', 10, '2017-10-03 06:59:52'),
(500, 2, '14.1.70.6', 10, '2017-10-03 13:29:23'),
(501, 1, '14.1.70.6', 10, '2017-10-03 13:29:26'),
(502, 1, '14.1.70.6', 10, '2017-10-03 13:29:28'),
(503, 1, '14.1.70.6', 10, '2017-10-03 13:30:13'),
(504, 1, '14.1.70.6', 10, '2017-10-03 13:30:19'),
(505, 1, '14.1.70.6', 10, '2017-10-03 13:36:19'),
(506, 1, '14.1.70.6', 10, '2017-10-03 13:36:36'),
(507, 1, '14.1.70.6', 10, '2017-10-03 13:37:22'),
(508, 1, '14.1.70.6', 10, '2017-10-03 13:38:02'),
(509, 1, '14.1.70.6', 10, '2017-10-03 13:40:30'),
(510, 2, '14.1.70.6', 10, '2017-10-04 05:54:54'),
(511, 1, '14.1.70.6', 10, '2017-10-04 05:54:59'),
(512, 1, '14.1.70.6', 10, '2017-10-04 05:57:22'),
(513, 1, '14.1.70.6', 10, '2017-10-04 06:00:16'),
(514, 1, '14.1.70.6', 10, '2017-10-04 06:06:21'),
(515, 1, '14.1.70.6', 10, '2017-10-04 06:16:54'),
(516, 1, '14.1.70.6', 10, '2017-10-04 06:19:05'),
(517, 1, '14.1.70.6', 10, '2017-10-04 06:20:59'),
(518, 1, '14.1.70.6', 10, '2017-10-04 06:23:26'),
(519, 1, '14.1.70.6', 10, '2017-10-04 06:25:46'),
(520, 1, '14.1.70.6', 10, '2017-10-04 06:41:43'),
(521, 1, '14.1.70.6', 10, '2017-10-04 06:59:55'),
(522, 1, '14.1.70.6', 10, '2017-10-04 07:00:16'),
(523, 1, '14.1.70.6', 10, '2017-10-04 07:06:10'),
(524, 1, '14.1.70.6', 10, '2017-10-04 07:06:48'),
(525, 1, '14.1.70.6', 10, '2017-10-04 07:08:45'),
(526, 1, '14.1.70.6', 10, '2017-10-04 07:09:11'),
(527, 1, '14.1.70.6', 10, '2017-10-04 07:25:18'),
(528, 1, '14.1.70.6', 10, '2017-10-04 07:26:09'),
(529, 1, '14.1.70.6', 10, '2017-10-04 07:37:48'),
(530, 1, '14.1.70.6', 10, '2017-10-04 07:42:27'),
(531, 1, '14.1.70.6', 10, '2017-10-04 07:45:02'),
(532, 1, '14.1.70.6', 10, '2017-10-04 07:55:24'),
(533, 1, '14.1.70.6', 10, '2017-10-04 07:56:14'),
(534, 1, '14.1.70.6', 10, '2017-10-04 07:58:35'),
(535, 1, '14.1.70.6', 10, '2017-10-04 07:59:54'),
(536, 1, '14.1.70.6', 10, '2017-10-04 08:01:24'),
(537, 1, '14.1.70.6', 10, '2017-10-04 08:02:07'),
(538, 1, '14.1.70.6', 10, '2017-10-04 08:14:35'),
(539, 1, '14.1.70.6', 10, '2017-10-04 08:17:45'),
(540, 1, '14.1.70.6', 10, '2017-10-04 11:24:40'),
(541, 1, '14.1.70.6', 10, '2017-10-04 11:27:29'),
(542, 1, '14.1.70.6', 10, '2017-10-04 11:30:07'),
(543, 1, '14.1.70.6', 10, '2017-10-04 11:39:39'),
(544, 1, '14.1.70.6', 10, '2017-10-04 11:45:53'),
(545, 1, '14.1.70.6', 10, '2017-10-04 11:47:37'),
(546, 1, '14.1.70.6', 10, '2017-10-04 11:49:14'),
(547, 1, '14.1.70.6', 10, '2017-10-04 11:53:57'),
(548, 1, '14.1.70.6', 10, '2017-10-04 11:59:29'),
(549, 1, '14.1.70.6', 10, '2017-10-04 12:01:18'),
(550, 1, '14.1.70.6', 10, '2017-10-04 12:02:39'),
(551, 1, '14.1.70.6', 10, '2017-10-04 12:06:23'),
(552, 1, '14.1.70.6', 10, '2017-10-04 12:07:15'),
(553, 1, '14.1.70.6', 10, '2017-10-04 12:12:29'),
(554, 1, '14.1.70.6', 10, '2017-10-04 12:37:05'),
(555, 1, '14.1.70.6', 10, '2017-10-04 12:38:03'),
(556, 1, '14.1.70.6', 10, '2017-10-04 12:38:25'),
(557, 1, '14.1.70.6', 10, '2017-10-04 12:38:49'),
(558, 1, '14.1.70.6', 10, '2017-10-04 12:39:07'),
(559, 1, '14.1.70.6', 10, '2017-10-04 12:39:26'),
(560, 1, '14.1.70.6', 10, '2017-10-04 12:40:07'),
(561, 1, '14.1.70.6', 10, '2017-10-04 12:40:33'),
(562, 1, '14.1.70.6', 10, '2017-10-04 12:50:21'),
(563, 1, '14.1.70.6', 10, '2017-10-04 12:50:36'),
(564, 1, '14.1.70.6', 10, '2017-10-04 12:51:43'),
(565, 1, '14.1.70.6', 10, '2017-10-04 12:51:47'),
(566, 1, '14.1.70.6', 10, '2017-10-04 12:51:52'),
(567, 1, '14.1.70.6', 10, '2017-10-04 12:52:36'),
(568, 1, '14.1.70.6', 10, '2017-10-04 12:52:59'),
(569, 1, '14.1.70.6', 10, '2017-10-04 12:53:26'),
(570, 1, '14.1.70.6', 10, '2017-10-04 12:53:44'),
(571, 1, '14.1.70.6', 10, '2017-10-04 12:54:37'),
(572, 1, '14.1.70.6', 10, '2017-10-04 12:54:42'),
(573, 1, '14.1.70.6', 10, '2017-10-04 12:58:06'),
(574, 1, '14.1.70.6', 10, '2017-10-04 12:58:43'),
(575, 17, '14.1.70.6', 10, '2017-10-04 12:58:47'),
(576, 1, '14.1.70.6', 10, '2017-10-04 13:00:34'),
(577, 1, '14.1.70.6', 10, '2017-10-04 13:00:47'),
(578, 1, '14.1.70.6', 10, '2017-10-04 13:01:41'),
(579, 18, '14.1.70.6', 10, '2017-10-04 13:01:46'),
(580, 1, '14.1.70.6', 10, '2017-10-04 13:02:23'),
(581, 1, '14.1.70.6', 10, '2017-10-04 13:03:00'),
(582, 19, '14.1.70.6', 10, '2017-10-04 13:03:04'),
(583, 1, '14.1.70.6', 10, '2017-10-04 13:34:09'),
(584, 1, '14.1.70.6', 10, '2017-10-04 13:53:56'),
(585, 1, '14.1.70.6', 10, '2017-10-04 13:54:32'),
(586, 1, '14.1.70.6', 10, '2017-10-04 14:03:13'),
(587, 1, '14.1.70.6', 10, '2017-10-04 14:04:50'),
(588, 1, '14.1.70.6', 10, '2017-10-04 14:04:54'),
(589, 1, '14.1.70.6', 10, '2017-10-04 14:06:05'),
(590, 1, '14.1.70.6', 10, '2017-10-04 14:06:36'),
(591, 20, '14.1.70.6', 10, '2017-10-04 14:15:55'),
(592, 1, '14.1.70.6', 10, '2017-10-04 14:17:06'),
(593, 20, '14.1.70.6', 10, '2017-10-04 14:17:22'),
(594, 20, '14.1.70.6', 10, '2017-10-04 14:18:21'),
(595, 1, '14.1.70.6', 10, '2017-10-04 14:18:35'),
(596, 20, '14.1.70.6', 10, '2017-10-04 14:18:37'),
(597, 1, '14.1.70.6', 10, '2017-10-04 14:18:39'),
(598, 1, '14.1.70.6', 10, '2017-10-04 14:20:38'),
(599, 20, '14.1.70.6', 10, '2017-10-04 14:21:15'),
(600, 1, '14.1.70.6', 10, '2017-10-04 14:21:23'),
(601, 1, '14.1.70.6', 10, '2017-10-04 14:24:06'),
(602, 20, '14.1.70.6', 10, '2017-10-04 14:24:11'),
(603, 21, '14.1.70.6', 10, '2017-10-04 14:24:14'),
(604, 2, '14.1.70.6', 10, '2017-10-05 05:58:41'),
(605, 21, '14.1.70.6', 10, '2017-10-05 05:58:47'),
(606, 1, '14.1.70.6', 10, '2017-10-05 05:59:20'),
(607, 1, '14.1.70.6', 10, '2017-10-05 06:01:40'),
(608, 21, '14.1.70.6', 10, '2017-10-05 06:04:26'),
(609, 2, '14.1.70.6', 10, '2017-10-05 06:12:19'),
(610, 2, '14.1.70.6', 10, '2017-10-05 06:14:45'),
(611, 2, '14.1.70.6', 10, '2017-10-05 06:17:02'),
(612, 21, '14.1.70.6', 10, '2017-10-05 06:17:06'),
(613, 21, '14.1.70.6', 10, '2017-10-05 06:27:43'),
(614, 1, '14.1.70.6', 10, '2017-10-05 06:27:50'),
(615, 21, '14.1.70.6', 10, '2017-10-05 06:28:11'),
(616, 21, '14.1.70.6', 10, '2017-10-05 06:28:43'),
(617, 1, '14.1.70.6', 10, '2017-10-05 06:39:58'),
(618, 1, '14.1.70.6', 10, '2017-10-05 06:42:46'),
(619, 1, '14.1.70.6', 10, '2017-10-05 06:44:22'),
(620, 1, '14.1.70.6', 10, '2017-10-05 06:44:57'),
(621, 21, '14.1.70.6', 10, '2017-10-05 06:52:35'),
(622, 1, '14.1.70.6', 10, '2017-10-05 06:52:44'),
(623, 21, '14.1.70.6', 10, '2017-10-05 06:54:07'),
(624, 1, '14.1.70.6', 10, '2017-10-05 06:54:10'),
(625, 1, '14.1.70.6', 10, '2017-10-05 06:54:36'),
(626, 1, '14.1.70.6', 10, '2017-10-05 06:54:40'),
(627, 1, '14.1.70.6', 10, '2017-10-05 06:56:36'),
(628, 1, '14.1.70.6', 10, '2017-10-05 06:57:33'),
(629, 1, '14.1.70.6', 10, '2017-10-05 06:59:37'),
(630, 1, '14.1.70.6', 10, '2017-10-05 07:00:19'),
(631, 21, '14.1.70.6', 10, '2017-10-05 07:00:25'),
(632, 1, '14.1.70.6', 10, '2017-10-05 07:02:17'),
(633, 1, '14.1.70.6', 10, '2017-10-05 07:03:06'),
(634, 1, '14.1.70.6', 10, '2017-10-05 07:03:20'),
(635, 21, '14.1.70.6', 10, '2017-10-05 07:03:27'),
(636, 1, '14.1.70.6', 10, '2017-10-05 07:06:30'),
(637, 1, '14.1.70.6', 10, '2017-10-05 07:08:47'),
(638, 1, '14.1.70.6', 10, '2017-10-05 07:10:10'),
(639, 1, '14.1.70.6', 10, '2017-10-05 07:11:25'),
(640, 1, '14.1.70.6', 10, '2017-10-05 07:12:42'),
(641, 22, '14.1.70.6', 10, '2017-10-05 07:14:32'),
(642, 21, '14.1.70.6', 10, '2017-10-05 07:15:34'),
(643, 22, '14.1.70.6', 10, '2017-10-05 07:15:39'),
(644, 22, '14.1.70.6', 10, '2017-10-05 07:17:22'),
(645, 22, '14.1.70.6', 10, '2017-10-05 07:23:14'),
(646, 22, '14.1.70.6', 10, '2017-10-05 07:23:24'),
(647, 22, '14.1.70.6', 10, '2017-10-05 07:58:58'),
(648, 22, '14.1.70.6', 10, '2017-10-05 08:02:57'),
(649, 22, '14.1.70.6', 10, '2017-10-05 08:03:03'),
(650, 22, '14.1.70.6', 10, '2017-10-05 08:03:42'),
(651, 22, '14.1.70.6', 10, '2017-10-05 08:03:55'),
(652, 22, '14.1.70.6', 10, '2017-10-05 08:05:48'),
(653, 22, '14.1.70.6', 10, '2017-10-05 08:07:18'),
(654, 22, '14.1.70.6', 10, '2017-10-05 08:10:31'),
(655, 22, '14.1.70.6', 10, '2017-10-05 08:13:44'),
(656, 22, '14.1.70.6', 10, '2017-10-05 08:14:20'),
(657, 22, '14.1.70.6', 10, '2017-10-05 08:16:05'),
(658, 22, '14.1.70.6', 10, '2017-10-05 08:17:04'),
(659, 22, '14.1.70.6', 10, '2017-10-05 08:19:02'),
(660, 22, '14.1.70.6', 10, '2017-10-05 08:20:46'),
(661, 22, '14.1.70.6', 10, '2017-10-05 08:31:27'),
(662, 22, '14.1.70.6', 10, '2017-10-05 08:32:06'),
(663, 22, '14.1.70.6', 10, '2017-10-05 08:32:39'),
(664, 22, '14.1.70.6', 10, '2017-10-05 08:34:06'),
(665, 22, '14.1.70.6', 10, '2017-10-05 08:34:56'),
(666, 22, '14.1.70.6', 10, '2017-10-05 08:41:28'),
(667, 22, '14.1.70.6', 10, '2017-10-05 08:42:32'),
(668, 22, '14.1.70.6', 10, '2017-10-05 08:42:40'),
(669, 22, '14.1.70.6', 10, '2017-10-05 08:44:47'),
(670, 22, '14.1.70.6', 10, '2017-10-05 08:45:23'),
(671, 22, '14.1.70.6', 10, '2017-10-05 08:45:58'),
(672, 22, '14.1.70.6', 10, '2017-10-05 08:46:19'),
(673, 22, '14.1.70.6', 10, '2017-10-05 08:47:01'),
(674, 22, '14.1.70.6', 10, '2017-10-05 08:47:24'),
(675, 22, '14.1.70.6', 10, '2017-10-05 08:48:42'),
(676, 22, '14.1.70.6', 10, '2017-10-05 08:49:37'),
(677, 22, '14.1.70.6', 10, '2017-10-05 08:53:48'),
(678, 22, '14.1.70.6', 10, '2017-10-05 08:54:49'),
(679, 22, '14.1.70.6', 10, '2017-10-05 08:55:34'),
(680, 22, '14.1.70.6', 10, '2017-10-05 08:56:43'),
(681, 22, '14.1.70.6', 10, '2017-10-05 09:00:16'),
(682, 22, '14.1.70.6', 10, '2017-10-05 09:00:56'),
(683, 22, '14.1.70.6', 10, '2017-10-05 09:01:10'),
(684, 22, '14.1.70.6', 10, '2017-10-05 09:02:26'),
(685, 22, '14.1.70.6', 10, '2017-10-05 09:02:48'),
(686, 22, '14.1.70.6', 10, '2017-10-05 09:03:59'),
(687, 22, '14.1.70.6', 10, '2017-10-05 09:06:33'),
(688, 22, '14.1.70.6', 10, '2017-10-05 09:06:52'),
(689, 22, '14.1.70.6', 10, '2017-10-05 09:08:30'),
(690, 22, '14.1.70.6', 10, '2017-10-05 09:10:12'),
(691, 22, '14.1.70.6', 10, '2017-10-05 09:10:28'),
(692, 22, '14.1.70.6', 10, '2017-10-05 09:11:32'),
(693, 22, '14.1.70.6', 10, '2017-10-05 09:12:40'),
(694, 22, '14.1.70.6', 10, '2017-10-05 09:13:16'),
(695, 22, '14.1.70.6', 10, '2017-10-05 09:13:55'),
(696, 22, '14.1.70.6', 10, '2017-10-05 09:14:44'),
(697, 22, '14.1.70.6', 10, '2017-10-05 09:16:14'),
(698, 22, '14.1.70.6', 10, '2017-10-05 09:18:01'),
(699, 22, '14.1.70.6', 10, '2017-10-05 09:19:06'),
(700, 22, '14.1.70.6', 10, '2017-10-05 09:25:54'),
(701, 22, '14.1.70.6', 10, '2017-10-05 09:27:18'),
(702, 22, '14.1.70.6', 10, '2017-10-05 09:27:38'),
(703, 22, '14.1.70.6', 10, '2017-10-05 09:30:32'),
(704, 22, '14.1.70.6', 10, '2017-10-05 09:31:21'),
(705, 22, '14.1.70.6', 10, '2017-10-05 09:31:35'),
(706, 22, '14.1.70.6', 10, '2017-10-05 09:31:52'),
(707, 22, '14.1.70.6', 10, '2017-10-05 10:30:20'),
(708, 22, '14.1.70.6', 10, '2017-10-05 10:41:40'),
(709, 22, '14.1.70.6', 10, '2017-10-05 10:56:42'),
(710, 22, '14.1.70.6', 10, '2017-10-05 10:56:59'),
(711, 22, '14.1.70.6', 10, '2017-10-05 10:57:34'),
(712, 22, '14.1.70.6', 10, '2017-10-05 10:57:50'),
(713, 22, '14.1.70.6', 10, '2017-10-05 10:58:38'),
(714, 22, '14.1.70.6', 10, '2017-10-05 10:58:57'),
(715, 22, '14.1.70.6', 10, '2017-10-05 10:59:40'),
(716, 22, '14.1.70.6', 10, '2017-10-05 11:01:11'),
(717, 22, '14.1.70.6', 10, '2017-10-05 11:05:31'),
(718, 22, '14.1.70.6', 10, '2017-10-05 11:10:32'),
(719, 22, '14.1.70.6', 10, '2017-10-05 11:12:58'),
(720, 22, '14.1.70.6', 10, '2017-10-05 11:14:53'),
(721, 22, '14.1.70.6', 10, '2017-10-05 11:15:45'),
(722, 22, '14.1.70.6', 10, '2017-10-05 11:19:39'),
(723, 22, '14.1.70.6', 10, '2017-10-05 11:21:41'),
(724, 22, '14.1.70.6', 10, '2017-10-05 11:21:59'),
(725, 22, '14.1.70.6', 10, '2017-10-05 11:24:56'),
(726, 22, '14.1.70.6', 10, '2017-10-05 11:28:48'),
(727, 22, '14.1.70.6', 10, '2017-10-05 11:30:43'),
(728, 22, '14.1.70.6', 10, '2017-10-05 11:36:40'),
(729, 22, '14.1.70.6', 10, '2017-10-05 11:38:39'),
(730, 22, '14.1.70.6', 10, '2017-10-05 11:51:29'),
(731, 22, '14.1.70.6', 10, '2017-10-05 11:52:41'),
(732, 22, '14.1.70.6', 10, '2017-10-05 11:53:46'),
(733, 22, '14.1.70.6', 10, '2017-10-05 11:54:09'),
(734, 22, '14.1.70.6', 10, '2017-10-05 11:57:12'),
(735, 22, '14.1.70.6', 10, '2017-10-05 11:59:19'),
(736, 22, '14.1.70.6', 10, '2017-10-05 12:04:54'),
(737, 22, '14.1.70.6', 10, '2017-10-05 12:07:03'),
(738, 22, '14.1.70.6', 10, '2017-10-05 12:07:50'),
(739, 22, '14.1.70.6', 10, '2017-10-05 12:09:05'),
(740, 22, '14.1.70.6', 10, '2017-10-05 12:11:02'),
(741, 22, '14.1.70.6', 10, '2017-10-05 12:12:00'),
(742, 22, '14.1.70.6', 10, '2017-10-05 12:12:35'),
(743, 22, '14.1.70.6', 10, '2017-10-05 12:13:13'),
(744, 22, '14.1.70.6', 10, '2017-10-05 12:14:07'),
(745, 22, '14.1.70.6', 10, '2017-10-05 12:14:49'),
(746, 22, '14.1.70.6', 10, '2017-10-05 12:18:10'),
(747, 22, '14.1.70.6', 10, '2017-10-05 12:28:47'),
(748, 22, '14.1.70.6', 10, '2017-10-05 12:29:11'),
(749, 22, '14.1.70.6', 10, '2017-10-05 12:34:36'),
(750, 22, '14.1.70.6', 10, '2017-10-05 12:39:32'),
(751, 22, '14.1.70.6', 10, '2017-10-05 12:42:54'),
(752, 22, '14.1.70.6', 10, '2017-10-05 12:43:36'),
(753, 22, '14.1.70.6', 10, '2017-10-05 12:45:08'),
(754, 22, '14.1.70.6', 10, '2017-10-05 12:59:12'),
(755, 22, '14.1.70.6', 10, '2017-10-05 13:15:33'),
(756, 22, '14.1.70.6', 10, '2017-10-05 13:17:30'),
(757, 22, '14.1.70.6', 10, '2017-10-05 13:20:38'),
(758, 22, '14.1.70.6', 10, '2017-10-05 13:21:53'),
(759, 22, '14.1.70.6', 10, '2017-10-05 13:23:26'),
(760, 22, '14.1.70.6', 10, '2017-10-05 13:26:27'),
(761, 22, '14.1.70.6', 10, '2017-10-05 13:27:40'),
(762, 22, '14.1.70.6', 10, '2017-10-05 13:28:31'),
(763, 22, '14.1.70.6', 10, '2017-10-05 13:29:12'),
(764, 22, '14.1.70.6', 10, '2017-10-05 13:36:08'),
(765, 22, '14.1.70.6', 10, '2017-10-05 13:38:45'),
(766, 22, '14.1.70.6', 10, '2017-10-05 13:39:27'),
(767, 22, '14.1.70.6', 10, '2017-10-05 13:40:10'),
(768, 22, '14.1.70.6', 10, '2017-10-05 13:47:34'),
(769, 22, '14.1.70.6', 10, '2017-10-05 13:48:34'),
(770, 22, '14.1.70.6', 10, '2017-10-05 13:50:01'),
(771, 22, '14.1.70.6', 10, '2017-10-05 13:52:14'),
(772, 22, '14.1.70.6', 10, '2017-10-05 13:53:04'),
(773, 22, '14.1.70.6', 10, '2017-10-05 13:55:29'),
(774, 22, '14.1.70.6', 10, '2017-10-05 13:58:09'),
(775, 22, '14.1.70.6', 10, '2017-10-05 13:59:38'),
(776, 22, '14.1.70.6', 10, '2017-10-05 14:00:49'),
(777, 22, '14.1.70.6', 10, '2017-10-05 14:03:53'),
(778, 22, '14.1.70.6', 10, '2017-10-05 14:04:24'),
(779, 22, '14.1.70.6', 10, '2017-10-05 14:04:53'),
(780, 22, '14.1.70.6', 10, '2017-10-05 14:15:59'),
(781, 22, '14.1.70.6', 10, '2017-10-05 14:16:29'),
(782, 1, '14.1.70.6', 10, '2017-10-05 14:17:50'),
(783, 22, '14.1.70.6', 10, '2017-10-05 14:17:53'),
(784, 22, '14.1.70.6', 10, '2017-10-05 14:18:35'),
(785, 22, '14.1.70.6', 10, '2017-10-05 14:22:00'),
(786, 22, '14.1.70.6', 10, '2017-10-05 14:23:00'),
(787, 22, '14.1.70.6', 10, '2017-10-05 14:26:04'),
(788, 22, '14.1.70.6', 10, '2017-10-05 14:26:39'),
(789, 22, '14.1.70.6', 10, '2017-10-05 14:27:22'),
(790, 22, '14.1.70.6', 10, '2017-10-05 14:28:11'),
(791, 2, '14.1.70.6', 10, '2017-10-06 06:27:03'),
(792, 1, '14.1.70.6', 10, '2017-10-06 06:27:26'),
(793, 21, '14.1.70.6', 10, '2017-10-06 06:27:31'),
(794, 22, '14.1.70.6', 10, '2017-10-06 06:27:40'),
(795, 22, '14.1.70.6', 10, '2017-10-06 06:29:03'),
(796, 22, '14.1.70.6', 10, '2017-10-06 06:32:35'),
(797, 22, '14.1.70.6', 10, '2017-10-06 06:33:31'),
(798, 22, '14.1.70.6', 10, '2017-10-06 06:34:33'),
(799, 22, '14.1.70.6', 10, '2017-10-06 06:34:47'),
(800, 22, '14.1.70.6', 10, '2017-10-06 06:35:14'),
(801, 22, '14.1.70.6', 10, '2017-10-06 06:35:35'),
(802, 22, '14.1.70.6', 10, '2017-10-06 06:36:33'),
(803, 22, '14.1.70.6', 10, '2017-10-06 06:37:20'),
(804, 22, '14.1.70.6', 10, '2017-10-06 06:38:39'),
(805, 22, '14.1.70.6', 10, '2017-10-06 06:41:39'),
(806, 22, '14.1.70.6', 10, '2017-10-06 06:42:41'),
(807, 22, '14.1.70.6', 10, '2017-10-06 06:43:07'),
(808, 22, '14.1.70.6', 10, '2017-10-06 07:04:47'),
(809, 22, '14.1.70.6', 10, '2017-10-06 07:06:06'),
(810, 22, '14.1.70.6', 10, '2017-10-06 07:07:11'),
(811, 22, '14.1.70.6', 10, '2017-10-06 07:08:38'),
(812, 22, '14.1.70.6', 10, '2017-10-06 07:14:12'),
(813, 22, '14.1.70.6', 10, '2017-10-06 07:16:29'),
(814, 22, '14.1.70.6', 10, '2017-10-06 07:16:55'),
(815, 22, '14.1.70.6', 10, '2017-10-06 07:17:38'),
(816, 22, '14.1.70.6', 10, '2017-10-06 07:18:04'),
(817, 22, '14.1.70.6', 10, '2017-10-06 07:21:24'),
(818, 22, '14.1.70.6', 10, '2017-10-06 07:21:59'),
(819, 22, '14.1.70.6', 10, '2017-10-06 07:27:43'),
(820, 22, '14.1.70.6', 10, '2017-10-06 07:29:52'),
(821, 22, '14.1.70.6', 10, '2017-10-06 07:52:05'),
(822, 22, '14.1.70.6', 10, '2017-10-06 07:53:24'),
(823, 22, '14.1.70.6', 10, '2017-10-06 07:55:46'),
(824, 22, '14.1.70.6', 10, '2017-10-06 07:58:02'),
(825, 22, '14.1.70.6', 10, '2017-10-06 07:58:26'),
(826, 22, '14.1.70.6', 10, '2017-10-06 07:59:32'),
(827, 22, '14.1.70.6', 10, '2017-10-06 08:00:48'),
(828, 22, '14.1.70.6', 10, '2017-10-06 08:01:30'),
(829, 22, '14.1.70.6', 10, '2017-10-06 08:02:13'),
(830, 22, '14.1.70.6', 10, '2017-10-06 08:03:03'),
(831, 22, '14.1.70.6', 10, '2017-10-06 08:04:39'),
(832, 22, '14.1.70.6', 10, '2017-10-06 08:05:05'),
(833, 22, '14.1.70.6', 10, '2017-10-06 08:22:13'),
(834, 22, '14.1.70.6', 10, '2017-10-06 08:22:38'),
(835, 22, '14.1.70.6', 10, '2017-10-06 08:23:31'),
(836, 22, '14.1.70.6', 10, '2017-10-06 08:24:05'),
(837, 22, '14.1.70.6', 10, '2017-10-06 08:26:31'),
(838, 22, '14.1.70.6', 10, '2017-10-06 08:28:45'),
(839, 22, '14.1.70.6', 10, '2017-10-06 08:29:07'),
(840, 22, '14.1.70.6', 10, '2017-10-06 08:38:19'),
(841, 22, '14.1.70.6', 10, '2017-10-06 08:39:41'),
(842, 22, '14.1.70.6', 10, '2017-10-06 08:41:24'),
(843, 22, '14.1.70.6', 10, '2017-10-06 08:42:04'),
(844, 22, '14.1.70.6', 10, '2017-10-06 08:44:34'),
(845, 22, '14.1.70.6', 10, '2017-10-06 08:44:37'),
(846, 22, '14.1.70.6', 10, '2017-10-06 08:45:01'),
(847, 22, '14.1.70.6', 10, '2017-10-06 08:45:07'),
(848, 22, '14.1.70.6', 10, '2017-10-06 08:45:39'),
(849, 22, '14.1.70.6', 10, '2017-10-06 08:46:27'),
(850, 22, '14.1.70.6', 10, '2017-10-06 08:49:42'),
(851, 22, '14.1.70.6', 10, '2017-10-06 08:50:14'),
(852, 22, '14.1.70.6', 10, '2017-10-06 08:52:03'),
(853, 22, '14.1.70.6', 10, '2017-10-06 08:52:41'),
(854, 22, '14.1.70.6', 10, '2017-10-06 08:53:41'),
(855, 22, '14.1.70.6', 10, '2017-10-06 08:54:17'),
(856, 22, '14.1.70.6', 10, '2017-10-06 08:58:31'),
(857, 22, '14.1.70.6', 10, '2017-10-06 08:59:49'),
(858, 22, '14.1.70.6', 10, '2017-10-06 09:00:27'),
(859, 22, '14.1.70.6', 10, '2017-10-06 09:07:11'),
(860, 22, '14.1.70.6', 10, '2017-10-06 09:08:41'),
(861, 22, '14.1.70.6', 10, '2017-10-06 09:09:23'),
(862, 22, '14.1.70.6', 10, '2017-10-06 11:00:00'),
(863, 22, '14.1.70.6', 10, '2017-10-06 11:00:16'),
(864, 2, '14.1.70.6', 10, '2017-10-06 11:08:56'),
(865, 1, '14.1.70.6', 10, '2017-10-06 11:09:01'),
(866, 21, '14.1.70.6', 10, '2017-10-06 11:09:05'),
(867, 22, '14.1.70.6', 10, '2017-10-06 11:09:09'),
(868, 22, '14.1.70.6', 10, '2017-10-06 11:09:14'),
(869, 22, '14.1.70.6', 10, '2017-10-06 11:09:59'),
(870, 22, '14.1.70.6', 10, '2017-10-06 11:12:01'),
(871, 22, '14.1.70.6', 10, '2017-10-06 11:16:56'),
(872, 22, '14.1.70.6', 10, '2017-10-06 11:36:18'),
(873, 22, '14.1.70.6', 10, '2017-10-06 11:38:39'),
(874, 22, '14.1.70.6', 10, '2017-10-06 11:39:21'),
(875, 22, '14.1.70.6', 10, '2017-10-06 11:44:25'),
(876, 22, '14.1.70.6', 10, '2017-10-06 11:47:00'),
(877, 22, '14.1.70.6', 10, '2017-10-06 11:49:12'),
(878, 22, '14.1.70.6', 10, '2017-10-06 11:49:51'),
(879, 22, '14.1.70.6', 10, '2017-10-06 11:56:09'),
(880, 22, '14.1.70.6', 10, '2017-10-06 12:02:22'),
(881, 22, '14.1.70.6', 10, '2017-10-06 12:03:03'),
(882, 22, '14.1.70.6', 10, '2017-10-06 12:03:33'),
(883, 22, '14.1.70.6', 10, '2017-10-06 12:04:06'),
(884, 22, '14.1.70.6', 10, '2017-10-06 12:04:40'),
(885, 22, '14.1.70.6', 10, '2017-10-06 12:11:36'),
(886, 22, '14.1.70.6', 10, '2017-10-06 12:12:25'),
(887, 22, '14.1.70.6', 10, '2017-10-06 12:17:16'),
(888, 22, '14.1.70.6', 10, '2017-10-06 12:22:22'),
(889, 22, '14.1.70.6', 10, '2017-10-06 12:23:21'),
(890, 22, '14.1.70.6', 10, '2017-10-06 12:23:54'),
(891, 22, '14.1.70.6', 10, '2017-10-06 12:25:22'),
(892, 22, '14.1.70.6', 10, '2017-10-06 12:26:39'),
(893, 22, '14.1.70.6', 10, '2017-10-06 12:29:57'),
(894, 22, '14.1.70.6', 10, '2017-10-06 12:31:07'),
(895, 22, '14.1.70.6', 10, '2017-10-06 12:32:02'),
(896, 22, '14.1.70.6', 10, '2017-10-06 12:32:46'),
(897, 22, '14.1.70.6', 10, '2017-10-06 12:34:03'),
(898, 22, '14.1.70.6', 10, '2017-10-06 12:34:41'),
(899, 22, '14.1.70.6', 10, '2017-10-06 12:36:41'),
(900, 22, '14.1.70.6', 10, '2017-10-06 12:37:16'),
(901, 22, '14.1.70.6', 10, '2017-10-06 12:41:21'),
(902, 22, '14.1.70.6', 10, '2017-10-06 12:42:31'),
(903, 22, '14.1.70.6', 10, '2017-10-06 12:43:56'),
(904, 22, '14.1.70.6', 10, '2017-10-06 12:45:18'),
(905, 22, '14.1.70.6', 10, '2017-10-06 12:50:11'),
(906, 22, '14.1.70.6', 10, '2017-10-06 12:51:05'),
(907, 22, '14.1.70.6', 10, '2017-10-06 12:51:06'),
(908, 2, '14.1.70.6', 10, '2017-10-06 12:51:09'),
(909, 20, '14.1.70.6', 10, '2017-10-06 12:51:10'),
(910, 20, '14.1.70.6', 10, '2017-10-06 12:51:11'),
(911, 21, '14.1.70.6', 10, '2017-10-06 12:51:18'),
(912, 21, '14.1.70.6', 10, '2017-10-06 12:51:18'),
(913, 1, '14.1.70.6', 10, '2017-10-06 12:51:27'),
(914, 1, '14.1.70.6', 10, '2017-10-06 12:51:28'),
(915, 22, '14.1.70.6', 10, '2017-10-06 12:53:41'),
(916, 22, '14.1.70.6', 10, '2017-10-06 12:59:45'),
(917, 22, '14.1.70.6', 10, '2017-10-06 13:00:11'),
(918, 22, '14.1.70.6', 10, '2017-10-06 13:00:35'),
(919, 22, '14.1.70.6', 10, '2017-10-06 13:00:58'),
(920, 22, '14.1.70.6', 10, '2017-10-06 13:01:28'),
(921, 22, '14.1.70.6', 10, '2017-10-06 13:02:28'),
(922, 22, '14.1.70.6', 10, '2017-10-06 13:03:06'),
(923, 22, '14.1.70.6', 10, '2017-10-06 13:03:34'),
(924, 22, '14.1.70.6', 10, '2017-10-06 13:04:35'),
(925, 22, '14.1.70.6', 10, '2017-10-06 13:06:22'),
(926, 22, '14.1.70.6', 10, '2017-10-06 13:07:24'),
(927, 22, '14.1.70.6', 10, '2017-10-06 13:08:10'),
(928, 22, '14.1.70.6', 10, '2017-10-06 13:09:08'),
(929, 22, '14.1.70.6', 10, '2017-10-06 13:13:07'),
(930, 22, '14.1.70.6', 10, '2017-10-06 13:13:36'),
(931, 22, '14.1.70.6', 10, '2017-10-06 13:16:08'),
(932, 22, '14.1.70.6', 10, '2017-10-06 13:17:08'),
(933, 22, '14.1.70.6', 10, '2017-10-06 13:17:58'),
(934, 22, '14.1.70.6', 10, '2017-10-06 13:20:17'),
(935, 22, '14.1.70.6', 10, '2017-10-06 13:24:16'),
(936, 22, '14.1.70.6', 10, '2017-10-06 13:26:44'),
(937, 22, '14.1.70.6', 10, '2017-10-06 13:27:33'),
(938, 22, '14.1.70.6', 10, '2017-10-06 13:28:54'),
(939, 22, '14.1.70.6', 10, '2017-10-06 13:29:40'),
(940, 22, '14.1.70.6', 10, '2017-10-06 13:30:38'),
(941, 22, '14.1.70.6', 10, '2017-10-06 13:32:02'),
(942, 22, '14.1.70.6', 10, '2017-10-06 13:32:57'),
(943, 22, '14.1.70.6', 10, '2017-10-06 13:34:11'),
(944, 22, '14.1.70.6', 10, '2017-10-06 13:34:33'),
(945, 22, '14.1.70.6', 10, '2017-10-06 13:34:55'),
(946, 22, '14.1.70.6', 10, '2017-10-06 13:51:31'),
(947, 22, '14.1.70.6', 10, '2017-10-06 13:52:17'),
(948, 22, '14.1.70.6', 10, '2017-10-06 13:52:44'),
(949, 22, '14.1.70.6', 10, '2017-10-06 13:54:05'),
(950, 22, '14.1.70.6', 10, '2017-10-06 13:55:23'),
(951, 22, '14.1.70.6', 10, '2017-10-06 13:56:24'),
(952, 22, '14.1.70.6', 10, '2017-10-06 13:58:05'),
(953, 22, '14.1.70.6', 10, '2017-10-06 13:58:38'),
(954, 22, '14.1.70.6', 10, '2017-10-06 14:02:21'),
(955, 22, '14.1.70.6', 10, '2017-10-06 14:08:25'),
(956, 2, '14.1.70.6', 10, '2017-10-09 05:37:22'),
(957, 1, '14.1.70.6', 10, '2017-10-09 05:37:42'),
(958, 2, '14.1.70.6', 10, '2017-10-09 05:37:46'),
(959, 21, '14.1.70.6', 10, '2017-10-09 05:37:51'),
(960, 22, '14.1.70.6', 10, '2017-10-09 05:37:56'),
(961, 22, '14.1.70.6', 10, '2017-10-09 05:46:22'),
(962, 22, '14.1.70.6', 10, '2017-10-09 05:47:05'),
(963, 22, '14.1.70.6', 10, '2017-10-09 05:48:15'),
(964, 22, '14.1.70.6', 10, '2017-10-09 05:49:08'),
(965, 22, '14.1.70.6', 10, '2017-10-09 05:52:23'),
(966, 22, '14.1.70.6', 10, '2017-10-09 05:53:28'),
(967, 22, '14.1.70.6', 10, '2017-10-09 05:54:05'),
(968, 22, '14.1.70.6', 10, '2017-10-09 05:54:54'),
(969, 22, '14.1.70.6', 10, '2017-10-09 05:55:33'),
(970, 22, '14.1.70.6', 10, '2017-10-09 05:56:06'),
(971, 22, '14.1.70.6', 10, '2017-10-09 05:58:35'),
(972, 22, '14.1.70.6', 10, '2017-10-09 05:58:59'),
(973, 22, '14.1.70.6', 10, '2017-10-09 05:59:30'),
(974, 22, '14.1.70.6', 10, '2017-10-09 06:01:13'),
(975, 22, '14.1.70.6', 10, '2017-10-09 06:03:13'),
(976, 22, '14.1.70.6', 10, '2017-10-09 06:04:33'),
(977, 1, '14.1.70.6', 10, '2017-10-09 06:05:08'),
(978, 22, '14.1.70.6', 10, '2017-10-09 06:06:09'),
(979, 22, '14.1.70.6', 10, '2017-10-09 06:09:44'),
(980, 22, '14.1.70.6', 10, '2017-10-09 06:10:22'),
(981, 22, '14.1.70.6', 10, '2017-10-09 06:10:49'),
(982, 22, '14.1.70.6', 10, '2017-10-09 06:15:07'),
(983, 22, '14.1.70.6', 10, '2017-10-09 06:17:53'),
(984, 22, '14.1.70.6', 10, '2017-10-09 06:19:25'),
(985, 22, '14.1.70.6', 10, '2017-10-09 06:22:03'),
(986, 22, '14.1.70.6', 10, '2017-10-09 06:34:04'),
(987, 22, '14.1.70.6', 10, '2017-10-09 06:35:30'),
(988, 22, '14.1.70.6', 10, '2017-10-09 06:38:24'),
(989, 22, '14.1.70.6', 10, '2017-10-09 06:38:48'),
(990, 22, '14.1.70.6', 10, '2017-10-09 06:39:55'),
(991, 22, '14.1.70.6', 10, '2017-10-09 06:44:08'),
(992, 22, '14.1.70.6', 10, '2017-10-09 06:46:09');
INSERT INTO `system_view_visit` (`id`, `viewid`, `ip`, `userid`, `created_at`) VALUES
(993, 22, '14.1.70.6', 10, '2017-10-09 06:47:08'),
(994, 22, '14.1.70.6', 10, '2017-10-09 06:47:37'),
(995, 22, '14.1.70.6', 10, '2017-10-09 06:51:29'),
(996, 22, '14.1.70.6', 10, '2017-10-09 06:52:11'),
(997, 22, '14.1.70.6', 10, '2017-10-09 06:52:35'),
(998, 22, '14.1.70.6', 10, '2017-10-09 06:53:16'),
(999, 22, '14.1.70.6', 10, '2017-10-09 06:54:42'),
(1000, 22, '14.1.70.6', 10, '2017-10-09 07:02:57'),
(1001, 22, '14.1.70.6', 10, '2017-10-09 07:03:36'),
(1002, 22, '14.1.70.6', 10, '2017-10-09 07:04:11'),
(1003, 22, '14.1.70.6', 10, '2017-10-09 07:08:53'),
(1004, 22, '14.1.70.6', 10, '2017-10-09 07:11:43'),
(1005, 22, '14.1.70.6', 10, '2017-10-09 07:12:22'),
(1006, 22, '14.1.70.6', 10, '2017-10-09 07:13:57'),
(1007, 22, '14.1.70.6', 10, '2017-10-09 07:15:36'),
(1008, 22, '14.1.70.6', 10, '2017-10-09 07:16:50'),
(1009, 22, '14.1.70.6', 10, '2017-10-09 08:20:03'),
(1010, 22, '14.1.70.6', 10, '2017-10-09 09:15:49'),
(1011, 2, '14.1.70.6', 10, '2017-10-09 09:39:44'),
(1012, 1, '14.1.70.6', 10, '2017-10-09 09:39:50'),
(1013, 21, '14.1.70.6', 10, '2017-10-09 09:39:56'),
(1014, 22, '14.1.70.6', 10, '2017-10-09 09:40:02'),
(1015, 1, '14.1.70.6', 10, '2017-10-09 09:40:36'),
(1016, 22, '14.1.70.6', 10, '2017-10-09 10:39:49'),
(1017, 22, '14.1.70.6', 10, '2017-10-09 10:40:25'),
(1018, 22, '14.1.70.6', 10, '2017-10-09 10:40:40'),
(1019, 1, '14.1.70.6', 10, '2017-10-09 10:57:51'),
(1020, 1, '14.1.70.6', 10, '2017-10-09 11:09:07'),
(1021, 1, '14.1.70.6', 10, '2017-10-09 11:15:19'),
(1022, 1, '14.1.70.6', 10, '2017-10-09 11:16:33'),
(1023, 1, '14.1.70.6', 10, '2017-10-09 11:21:30'),
(1024, 1, '14.1.70.6', 10, '2017-10-09 11:23:05'),
(1025, 1, '14.1.70.6', 10, '2017-10-09 11:27:32'),
(1026, 1, '14.1.70.6', 10, '2017-10-09 11:27:53'),
(1027, 1, '14.1.70.6', 10, '2017-10-09 11:30:46'),
(1028, 1, '14.1.70.6', 10, '2017-10-09 11:32:57'),
(1029, 1, '14.1.70.6', 10, '2017-10-09 11:41:23'),
(1030, 1, '14.1.70.6', 10, '2017-10-09 11:43:03'),
(1031, 1, '14.1.70.6', 10, '2017-10-09 11:48:42'),
(1032, 1, '14.1.70.6', 10, '2017-10-09 11:49:22'),
(1033, 1, '14.1.70.6', 10, '2017-10-09 11:52:04'),
(1034, 1, '14.1.70.6', 10, '2017-10-09 12:00:38'),
(1035, 1, '14.1.70.6', 10, '2017-10-09 12:13:14'),
(1036, 1, '14.1.70.6', 10, '2017-10-09 12:21:37'),
(1037, 1, '14.1.70.6', 10, '2017-10-09 12:23:12'),
(1038, 1, '14.1.70.6', 10, '2017-10-09 12:25:29'),
(1039, 1, '14.1.70.6', 10, '2017-10-09 12:27:41'),
(1040, 1, '14.1.70.6', 10, '2017-10-09 12:28:48'),
(1041, 1, '14.1.70.6', 10, '2017-10-09 12:37:15'),
(1042, 1, '14.1.70.6', 10, '2017-10-09 12:39:59'),
(1043, 1, '14.1.70.6', 10, '2017-10-09 12:40:27'),
(1044, 1, '14.1.70.6', 10, '2017-10-09 12:40:55'),
(1045, 1, '14.1.70.6', 10, '2017-10-09 12:55:02'),
(1046, 1, '14.1.70.6', 10, '2017-10-09 13:00:07'),
(1047, 1, '14.1.70.6', 10, '2017-10-09 13:01:18'),
(1048, 1, '14.1.70.6', 10, '2017-10-09 13:04:35'),
(1049, 1, '14.1.70.6', 10, '2017-10-09 13:06:08'),
(1050, 1, '14.1.70.6', 10, '2017-10-09 13:11:14'),
(1051, 1, '14.1.70.6', 10, '2017-10-09 13:12:28'),
(1052, 1, '14.1.70.6', 10, '2017-10-09 13:14:31'),
(1053, 1, '14.1.70.6', 10, '2017-10-09 13:25:35'),
(1054, 1, '14.1.70.6', 10, '2017-10-09 13:26:08'),
(1055, 1, '14.1.70.6', 10, '2017-10-09 13:52:49'),
(1056, 4, '14.1.70.6', 10, '2017-10-09 14:18:44'),
(1057, 4, '14.1.70.6', 10, '2017-10-09 14:20:19'),
(1058, 4, '14.1.70.6', 10, '2017-10-09 14:21:49'),
(1059, 4, '14.1.70.6', 10, '2017-10-09 14:30:19'),
(1060, 4, '14.1.70.6', 10, '2017-10-09 14:31:16'),
(1061, 2, '14.1.70.6', 10, '2017-10-10 05:56:06'),
(1062, 2, '14.1.70.6', 10, '2017-10-10 05:59:23'),
(1063, 4, '14.1.70.6', 10, '2017-10-10 05:59:32'),
(1064, 4, '14.1.70.6', 10, '2017-10-10 05:59:56'),
(1065, 1, '14.1.70.6', 10, '2017-10-10 06:02:33'),
(1066, 4, '14.1.70.6', 10, '2017-10-10 06:36:29'),
(1067, 4, '14.1.70.6', 10, '2017-10-10 06:38:11'),
(1068, 4, '14.1.70.6', 10, '2017-10-10 06:40:12'),
(1069, 4, '14.1.70.6', 10, '2017-10-10 06:42:13'),
(1070, 4, '14.1.70.6', 10, '2017-10-10 06:42:28'),
(1071, 4, '14.1.70.6', 10, '2017-10-10 06:43:57'),
(1072, 4, '14.1.70.6', 10, '2017-10-10 06:44:56'),
(1073, 4, '14.1.70.6', 10, '2017-10-10 06:45:50'),
(1074, 4, '14.1.70.6', 10, '2017-10-10 06:46:14'),
(1075, 4, '14.1.70.6', 10, '2017-10-10 06:49:19'),
(1076, 4, '14.1.70.6', 10, '2017-10-10 06:50:03'),
(1077, 4, '14.1.70.6', 10, '2017-10-10 06:51:26'),
(1078, 4, '14.1.70.6', 10, '2017-10-10 06:53:47'),
(1079, 4, '14.1.70.6', 10, '2017-10-10 06:55:03'),
(1080, 4, '14.1.70.6', 10, '2017-10-10 06:55:26'),
(1081, 4, '14.1.70.6', 10, '2017-10-10 06:55:54'),
(1082, 4, '14.1.70.6', 10, '2017-10-10 06:56:50'),
(1083, 4, '14.1.70.6', 10, '2017-10-10 06:57:50'),
(1084, 4, '14.1.70.6', 10, '2017-10-10 06:59:15'),
(1085, 4, '14.1.70.6', 10, '2017-10-10 07:00:10'),
(1086, 4, '14.1.70.6', 10, '2017-10-10 07:01:41'),
(1087, 4, '14.1.70.6', 10, '2017-10-10 07:04:45'),
(1088, 1, '14.1.70.6', 10, '2017-10-10 07:05:32'),
(1089, 4, '14.1.70.6', 10, '2017-10-10 07:06:09'),
(1090, 4, '14.1.70.6', 10, '2017-10-10 07:08:15'),
(1091, 1, '14.1.70.6', 10, '2017-10-10 07:08:51'),
(1092, 4, '14.1.70.6', 10, '2017-10-10 07:10:03'),
(1093, 1, '14.1.70.6', 10, '2017-10-10 07:10:32'),
(1094, 4, '14.1.70.6', 10, '2017-10-10 07:12:30'),
(1095, 1, '14.1.70.6', 10, '2017-10-10 07:12:46'),
(1096, 4, '14.1.70.6', 10, '2017-10-10 07:22:25'),
(1097, 1, '14.1.70.6', 10, '2017-10-10 07:22:43'),
(1098, 4, '14.1.70.6', 10, '2017-10-10 07:23:53'),
(1099, 4, '14.1.70.6', 10, '2017-10-10 07:24:36'),
(1100, 4, '14.1.70.6', 10, '2017-10-10 07:26:20'),
(1101, 4, '14.1.70.6', 10, '2017-10-10 07:29:10'),
(1102, 4, '14.1.70.6', 10, '2017-10-10 07:30:17'),
(1103, 4, '14.1.70.6', 10, '2017-10-10 07:31:49'),
(1104, 4, '14.1.70.6', 10, '2017-10-10 07:33:50'),
(1105, 4, '14.1.70.6', 10, '2017-10-10 07:36:00'),
(1106, 1, '14.1.70.6', 10, '2017-10-10 07:44:35'),
(1107, 4, '14.1.70.6', 10, '2017-10-10 07:57:39'),
(1108, 4, '14.1.70.6', 10, '2017-10-10 07:59:04'),
(1109, 4, '14.1.70.6', 10, '2017-10-10 08:03:32'),
(1110, 4, '14.1.70.6', 10, '2017-10-10 08:25:36'),
(1111, 4, '14.1.70.6', 10, '2017-10-10 08:26:50'),
(1112, 4, '14.1.70.6', 10, '2017-10-10 08:27:48'),
(1113, 4, '14.1.70.6', 10, '2017-10-10 08:28:24'),
(1114, 4, '14.1.70.6', 10, '2017-10-10 08:30:02'),
(1115, 4, '14.1.70.6', 10, '2017-10-10 09:17:18'),
(1116, 1, '14.1.70.6', 10, '2017-10-10 09:17:36'),
(1117, 4, '14.1.70.6', 10, '2017-10-10 09:18:16'),
(1118, 1, '14.1.70.6', 10, '2017-10-10 09:18:30'),
(1119, 4, '14.1.70.6', 10, '2017-10-10 09:19:15'),
(1120, 4, '14.1.70.6', 10, '2017-10-10 09:19:48'),
(1121, 4, '14.1.70.6', 10, '2017-10-10 09:21:48'),
(1122, 4, '14.1.70.6', 10, '2017-10-10 09:24:11'),
(1123, 4, '14.1.70.6', 10, '2017-10-10 09:28:03'),
(1124, 4, '14.1.70.6', 10, '2017-10-10 09:29:31'),
(1125, 4, '14.1.70.6', 10, '2017-10-10 09:30:32'),
(1126, 4, '14.1.70.6', 10, '2017-10-10 09:32:23'),
(1127, 4, '14.1.70.6', 10, '2017-10-10 09:33:38'),
(1128, 4, '14.1.70.6', 10, '2017-10-10 09:34:11'),
(1129, 4, '14.1.70.6', 10, '2017-10-10 10:39:37'),
(1130, 4, '14.1.70.6', 10, '2017-10-10 10:44:18'),
(1131, 4, '14.1.70.6', 10, '2017-10-10 10:45:33'),
(1132, 1, '14.1.70.6', 10, '2017-10-10 10:46:01'),
(1133, 4, '14.1.70.6', 10, '2017-10-10 10:49:03'),
(1134, 4, '14.1.70.6', 10, '2017-10-10 10:51:48'),
(1135, 4, '14.1.70.6', 10, '2017-10-10 10:52:04'),
(1136, 4, '14.1.70.6', 10, '2017-10-10 10:52:14'),
(1137, 1, '14.1.70.6', 10, '2017-10-10 10:57:41'),
(1138, 4, '14.1.70.6', 10, '2017-10-10 11:05:43'),
(1139, 1, '14.1.70.6', 10, '2017-10-10 11:06:12'),
(1140, 4, '14.1.70.6', 10, '2017-10-10 11:07:45'),
(1141, 1, '14.1.70.6', 10, '2017-10-10 11:08:43'),
(1142, 4, '14.1.70.6', 10, '2017-10-10 11:10:39'),
(1143, 4, '14.1.70.6', 10, '2017-10-10 11:15:01'),
(1144, 4, '14.1.70.6', 10, '2017-10-10 11:16:09'),
(1145, 4, '14.1.70.6', 10, '2017-10-10 11:17:23'),
(1146, 4, '14.1.70.6', 10, '2017-10-10 11:18:02'),
(1147, 4, '14.1.70.6', 10, '2017-10-10 11:19:14'),
(1148, 4, '14.1.70.6', 10, '2017-10-10 11:20:01'),
(1149, 4, '14.1.70.6', 10, '2017-10-10 11:22:22'),
(1150, 4, '14.1.70.6', 10, '2017-10-10 11:24:22'),
(1151, 2, '14.1.70.6', 10, '2017-10-09 18:14:52'),
(1152, 1, '14.1.70.6', 10, '2017-10-09 18:15:07'),
(1153, 1, '14.1.70.6', 10, '2017-10-09 18:49:46'),
(1154, 4, '14.1.70.6', 10, '2017-10-09 18:52:42'),
(1155, 4, '14.1.70.6', 10, '2017-10-09 18:54:53'),
(1156, 1, '14.1.70.6', 10, '2017-10-09 18:55:11'),
(1157, 4, '14.1.70.6', 10, '2017-10-09 18:55:24'),
(1158, 4, '14.1.70.6', 10, '2017-10-09 19:14:07'),
(1159, 2, '14.1.70.6', 10, '2017-10-09 19:24:37'),
(1160, 4, '14.1.70.6', 10, '2017-10-09 19:54:18'),
(1161, 1, '14.1.70.6', 10, '2017-10-09 19:54:48'),
(1162, 21, '14.1.70.6', 10, '2017-10-09 19:54:51'),
(1163, 1, '14.1.70.6', 10, '2017-10-09 20:05:35'),
(1164, 4, '14.1.70.6', 10, '2017-10-09 20:05:38'),
(1165, 1, '14.1.70.6', 10, '2017-10-09 20:22:41'),
(1166, 4, '14.1.70.6', 10, '2017-10-09 20:23:09'),
(1167, 4, '14.1.70.6', 10, '2017-10-09 20:23:13'),
(1168, 1, '14.1.70.6', 10, '2017-10-09 20:23:43'),
(1169, 4, '14.1.70.6', 10, '2017-10-09 20:24:34'),
(1170, 4, '14.1.70.6', 10, '2017-10-09 20:25:18'),
(1171, 2, '14.1.70.6', 10, '2017-10-11 17:50:15'),
(1172, 1, '14.1.70.6', 10, '2017-10-11 17:50:19'),
(1173, 4, '14.1.70.6', 10, '2017-10-11 17:50:22'),
(1174, 1, '14.1.70.6', 10, '2017-10-11 17:55:44'),
(1175, 1, '14.1.70.6', 10, '2017-10-11 17:58:42'),
(1176, 1, '14.1.70.6', 10, '2017-10-11 17:59:47'),
(1177, 1, '14.1.70.6', 10, '2017-10-11 17:59:56'),
(1178, 4, '14.1.70.6', 10, '2017-10-11 18:01:42'),
(1179, 4, '14.1.70.6', 10, '2017-10-11 18:02:11'),
(1180, 1, '14.1.70.6', 10, '2017-10-11 18:08:13'),
(1181, 23, '14.1.70.6', 10, '2017-10-11 18:10:22'),
(1182, 23, '14.1.70.6', 10, '2017-10-11 18:11:04'),
(1183, 1, '14.1.70.6', 10, '2017-10-11 18:11:26'),
(1184, 1, '14.1.70.6', 10, '2017-10-11 18:12:08'),
(1185, 4, '14.1.70.6', 10, '2017-10-11 18:17:24'),
(1186, 2, '14.1.70.6', 10, '2017-10-15 13:51:22'),
(1187, 1, '14.1.70.6', 10, '2017-10-15 13:51:28'),
(1188, 1, '14.1.70.6', 10, '2017-10-15 13:51:58'),
(1189, 1, '14.1.70.6', 10, '2017-10-15 13:52:55'),
(1190, 1, '14.1.70.6', 10, '2017-10-15 13:53:07'),
(1191, 1, '14.1.70.6', 10, '2017-10-15 13:56:27'),
(1192, 1, '14.1.70.6', 10, '2017-10-15 13:56:37'),
(1193, 1, '14.1.70.6', 10, '2017-10-15 13:57:40'),
(1194, 1, '14.1.70.6', 10, '2017-10-15 13:59:02'),
(1195, 1, '14.1.70.6', 10, '2017-10-15 14:00:28'),
(1196, 1, '14.1.70.6', 10, '2017-10-15 14:01:11'),
(1197, 1, '14.1.70.6', 10, '2017-10-15 14:04:04'),
(1198, 1, '14.1.70.6', 10, '2017-10-15 14:05:08'),
(1199, 1, '14.1.70.6', 10, '2017-10-15 14:06:32'),
(1200, 1, '14.1.70.6', 10, '2017-10-15 14:09:02'),
(1201, 1, '14.1.70.6', 10, '2017-10-15 14:09:45'),
(1202, 1, '14.1.70.6', 10, '2017-10-15 14:10:01'),
(1203, 1, '14.1.70.6', 10, '2017-10-15 14:10:45'),
(1204, 1, '14.1.70.6', 10, '2017-10-15 14:10:54'),
(1205, 1, '14.1.70.6', 10, '2017-10-15 16:57:38'),
(1206, 2, '14.1.70.6', 10, '2017-10-23 14:18:36'),
(1207, 21, '14.1.70.6', 10, '2017-10-23 14:18:44'),
(1208, 2, '14.1.70.6', 10, '2017-10-23 18:19:00'),
(1209, 1, '14.1.70.6', 10, '2017-10-23 18:19:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_temp` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remember_token` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `roll` int(11) DEFAULT NULL,
  `dept` int(11) NOT NULL DEFAULT '1',
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`, `password_temp`, `code`, `active`, `remember_token`, `jobtitle`, `roll`, `dept`, `orgid`, `status`, `updated_at`, `created_at`) VALUES
(10, 'Pavithra', 'Isuru', 'pavithraisuru@gmail.com', '$2y$10$iIBVz3lmWdA2cX5pB75jPuk/ebFjdl05zBtuN6/JDOT6jMusQwcb6', NULL, '', 1, '5ZKr4bqKkSu56VJM7vsTWN7EJyghh0FKFu2lmVWhITO8WgX6h6mi8KQmn5lg', 'Developer', 1, 0, 1, 1, '2017-10-09 00:10:31', '2017-10-09 20:31:35'),
(11, 'Jone', '', '', NULL, NULL, 'HhJytKRcTygoYrA2h73yXyDUAEPFULlK4pKpzeHykpTY0sai84QXVO8xsznx', 0, NULL, 'Tester', 4, 1, 1, 0, '2017-10-09 21:00:28', '2017-10-09 21:00:28'),
(12, 'Jone', 'Doe', '', NULL, NULL, 'c22NrOlEAP3UhahnNn1avBKVLQQ4Dar7UxkbPbIHEXrgkEuvlokEsO1vMIFY', 0, NULL, 'Tester', 4, 1, 1, 0, '2017-10-09 21:04:02', '2017-10-09 21:04:02'),
(13, 'Jhon', 'Doe', 'JhonDo@yahoo.com', NULL, NULL, '0EU7MpolywksqetKOYBr6HbgDLzyJ5KnEbSdgEzfVYl9ffBJWBdgZogxD7Qe', 0, NULL, 'Tester', 4, 1, 1, 3, '2017-10-09 21:05:00', '2017-10-10 02:05:00'),
(14, 'Test', 'Email', '', NULL, NULL, 'F6yngUA7pR1efuhKlaeFdRkWoBunyQrtGMaKWVRg33uf069u1kTUVF8BNPpv', 0, NULL, 'email tester', 4, 1, 1, 0, '2017-10-10 03:53:18', '2017-10-10 03:53:18'),
(15, 'Test', 'Email', '', NULL, NULL, 'goTkE0DfdFqdAmuktNXdJ5Mj5ZQRRhEBSLgHW2YaM7lAjZhN1WkPH3ElPuFm', 0, NULL, 'test email', 4, 1, 1, 0, '2017-10-10 03:54:42', '2017-10-10 03:54:42'),
(16, 'Test', 'Email', '', NULL, NULL, '3s8Ht8wF2fsWJ1BIRSMX0zLKFw9oSb2z0Go4iEVIAH6sfJzdCJ71ZHQVSfkK', 0, NULL, 'test email', 4, 1, 1, 0, '2017-10-10 03:55:11', '2017-10-10 03:55:11'),
(17, 'Yahoo', 'Isuru', 'pavithraidl@yahoo.com', NULL, NULL, 'rmZ3vQxsTuWEBNfRoBLdzjFB5Oo2lYuNJmGJm7zScf4lgv4fafLedi1E0SUA', 0, NULL, 'Tester', 4, 1, 1, 3, '2017-10-10 03:55:44', '2017-10-09 14:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity_log`
--

DROP TABLE IF EXISTS `user_activity_log`;
CREATE TABLE `user_activity_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `activity` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_activity_log`
--

INSERT INTO `user_activity_log` (`id`, `userid`, `activity`, `orgid`, `created_at`) VALUES
(1, 10, 'Deleted - Jone  User account', 1, '2017-10-10 02:00:28'),
(2, 10, 'Deleted - Jone Doe\'s User account', 1, '2017-10-10 02:04:02'),
(3, 10, 'Login to the system', 1, '2017-10-10 02:14:34'),
(4, 10, 'Enabled user - Jhon Doe(JhonDo@yahoo.com) access to Dashboard', 1, '2017-10-10 03:59:43'),
(5, 10, 'Disabled user - Jhon Doe(JhonDo@yahoo.com) access to Dashboard', 1, '2017-10-10 03:59:59'),
(6, 10, 'Enabled user - Jhon Doe(JhonDo@yahoo.com) access to Dashboard', 1, '2017-10-10 04:00:08'),
(7, 10, 'Enabled user - Jhon Doe(JhonDo@yahoo.com) access to Clients', 1, '2017-10-10 04:00:20'),
(8, 10, 'Disabled user - Jhon Doe(JhonDo@yahoo.com) access to Clients', 1, '2017-10-10 04:04:34'),
(9, 10, 'Enabled user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View/Edit Access', 1, '2017-10-10 05:19:18'),
(10, 10, 'Disabled user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View/Edit Access', 1, '2017-10-10 05:22:02'),
(11, 10, 'Enabled user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View/Edit Access', 1, '2017-10-10 05:22:11'),
(12, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:41:04'),
(13, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:43:43'),
(14, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:45:19'),
(15, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:45:47'),
(16, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:46:23'),
(17, 10, 'Allow to  user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:47:20'),
(18, 10, 'Allow to View All user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:47:51'),
(19, 10, 'Allow to Department Only user - Jhon Doe(JhonDo@yahoo.com) access in the Users Page to View Users', 1, '2017-10-10 05:49:51'),
(20, 10, 'Login to the system', 1, '2017-10-09 12:44:52'),
(21, 10, 'Deleted - Test Email\'s User account', 1, '2017-10-09 14:53:18'),
(22, 10, 'Deleted - Test Email\'s User account', 1, '2017-10-09 14:54:42'),
(23, 10, 'Deleted - Test Email\'s User account', 1, '2017-10-09 14:55:11'),
(24, 10, 'Login to the system', 1, '2017-10-11 12:20:15'),
(25, 10, 'Login to the system', 1, '2017-10-15 08:21:22'),
(26, 10, 'Login to the system', 1, '2017-10-23 08:48:36'),
(27, 10, 'Login to the system', 1, '2017-10-23 12:49:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `general_address`
--
ALTER TABLE `general_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_email`
--
ALTER TABLE `general_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_telephone`
--
ALTER TABLE `general_telephone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_departments`
--
ALTER TABLE `org_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientid` (`clientid`),
  ADD KEY `authorid` (`authorid`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `project_clients`
--
ALTER TABLE `project_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `project_clients_rel_telephone`
--
ALTER TABLE `project_clients_rel_telephone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_media`
--
ALTER TABLE `project_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_class_path`
--
ALTER TABLE `system_class_path`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_controllers`
--
ALTER TABLE `system_controllers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_deny_access`
--
ALTER TABLE `system_deny_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controllerid` (`controllerid`),
  ADD KEY `functionid` (`functionid`),
  ADD KEY `userid` (`userid`),
  ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_functions`
--
ALTER TABLE `system_functions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controllerid` (`controllerid`);

--
-- Indexes for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `functionid` (`functionid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `system_operations`
--
ALTER TABLE `system_operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `systemid` (`systemid`),
  ADD KEY `functionid` (`functionid`);

--
-- Indexes for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operationid` (`operationid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `system_operation_choises`
--
ALTER TABLE `system_operation_choises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_tables`
--
ALTER TABLE `system_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `systemid` (`systemid`);

--
-- Indexes for table `system_table_columns`
--
ALTER TABLE `system_table_columns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tableid` (`tableid`);

--
-- Indexes for table `system_url_routes`
--
ALTER TABLE `system_url_routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `systemid` (`systemid`),
  ADD KEY `controllerid` (`phpcontrollerid`),
  ADD KEY `functionid` (`phpfunctionid`);

--
-- Indexes for table `system_variables`
--
ALTER TABLE `system_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_views`
--
ALTER TABLE `system_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `systemid` (`systemid`),
  ADD KEY `functionid` (`functionid`);

--
-- Indexes for table `system_view_access`
--
ALTER TABLE `system_view_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `viewid` (`viewid`);

--
-- Indexes for table `system_view_visit`
--
ALTER TABLE `system_view_visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `viewid` (`viewid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_activity_log`
--
ALTER TABLE `user_activity_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `general_address`
--
ALTER TABLE `general_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_email`
--
ALTER TABLE `general_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_telephone`
--
ALTER TABLE `general_telephone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `org_departments`
--
ALTER TABLE `org_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `project_clients`
--
ALTER TABLE `project_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_clients_rel_telephone`
--
ALTER TABLE `project_clients_rel_telephone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_media`
--
ALTER TABLE `project_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `systems`
--
ALTER TABLE `systems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `system_class_path`
--
ALTER TABLE `system_class_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `system_controllers`
--
ALTER TABLE `system_controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `system_deny_access`
--
ALTER TABLE `system_deny_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `system_functions`
--
ALTER TABLE `system_functions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_operations`
--
ALTER TABLE `system_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_operation_choises`
--
ALTER TABLE `system_operation_choises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_tables`
--
ALTER TABLE `system_tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_table_columns`
--
ALTER TABLE `system_table_columns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_url_routes`
--
ALTER TABLE `system_url_routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `system_variables`
--
ALTER TABLE `system_variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_views`
--
ALTER TABLE `system_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `system_view_access`
--
ALTER TABLE `system_view_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_view_visit`
--
ALTER TABLE `system_view_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1210;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user_activity_log`
--
ALTER TABLE `user_activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
